#include "sprites.h"
#include <wl/fileio.h>
#include <wl/util.h>
#include <boost/tokenizer.hpp>
#include <sstream>

using namespace WL;

void SpriteMan::load_sprites(ResourceIOBlock b, IImage *image) {
    IFileIO *file = b.file();
    file->seek(0, IFileIO::SK_START);

    std::string line;
    while (util::fileio_readline(file, line)) {
        typedef boost::char_separator<char> SepT;
        typedef boost::tokenizer<SepT> tokenizer;
        if (line.length() == 0 || line[0] == '#')
            continue;

        SepT sep(":,");
        tokenizer tok(line, sep);
        tokenizer::iterator it = tok.begin();
        
        float bounds[4];
        std::string name = *it;
        it++;
        for (int i=0; i < 4; ++i) {
            float n;
            std::stringstream s(*it);
            s >> n;
            bounds[i] = n;
            it++;
        }

        set_sprite(name, ISprite::create_sprite(image,
            Vec2(bounds[0], bounds[1]),
            Vec2(bounds[2], bounds[3])));
    }
}


