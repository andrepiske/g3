#include "precompiled.h"
#include <wl/sprite.h>
#include <wl/glstate.h>
#include <wl/log.h>

wlStartNS

class CSprite : public ISprite
{
public:
    IImage *m_image;

public:
    CSprite() {
        m_color = Color(0xFF, 0xFF, 0xFF);
    }

    virtual ~CSprite() {
        m_image->rc_pop();
    }

    virtual void draw(IScreen *scr) {
        GLEnable _tx(GL_TEXTURE_2D);
        GLBindTexture2D _tx_bind(m_image->get_glbind());

        m_color.gl_set();
    
        const Vec2 imgsize(m_image->size_vec());
        Vec2 ts(m_sproffset.x / imgsize.x, m_sproffset.y / imgsize.y);
        Vec2 tt(
            (m_sproffset.x + m_sprsize.x) / imgsize.x,
            (m_sproffset.y + m_sprsize.y) / imgsize.y);

        // draw ccw sprite
        glBegin(GL_QUADS);
        glTexCoord2f(ts.x, tt.y);
        glVertex2f(0.f, 1.f);
        glTexCoord2f(tt.x, tt.y);
        glVertex2f(1.f, 1.f);
        glTexCoord2f(tt.x, ts.y);
        glVertex2f(1.f, 0.f);
        glTexCoord2f(ts.x, ts.y);
        glVertex2f(0.f, 0.f);
        glEnd();
    }

    virtual IImage *get_image() {
        return m_image;
    }

    virtual const Vec2& spr_size() const {
        return m_sprsize;
    }

};

ISprite *ISprite::create_sprite(IImage *image,
    const Vec2 &offset, const Vec2 &size) {
    CSprite *spr = new CSprite();
    spr->m_sproffset = offset;
    spr->m_sprsize = size;
    spr->m_image = image;
    if (image)
        image->rc_push();
    return spr;
}


///////////////////////////////////////////////////////////////////////////////
// ISpriteNode
///////////////////////////////////////////////////////////////////////////////

class CSpriteNode : public ISpriteNode
{
public:
    ISprite *m_sprite;

public:
    CSpriteNode() {
        m_sprite = wlNull;
    }

    virtual ~CSpriteNode() {
        if (m_sprite)
            m_sprite->rc_pop();
    }

    virtual void set_sprite(ISprite *spr) {
        if (m_sprite)
            m_sprite->rc_pop();
        if (spr)
            spr->rc_push();
        m_sprite = spr;
    }

    virtual ISprite *get_sprite() {
        return m_sprite;
    }

    virtual void draw_self(IScreen *scr) {
        if (m_sprite)
            m_sprite->draw(scr);
    }

    virtual BBox2 calculate_bbox() const {
        return BBox2(Vec2(0.f, 0.f), Vec2(1.f, 1.f));
    }

};

ISpriteNode *ISpriteNode::create() {
    return new CSpriteNode();
}



wlEndNS




