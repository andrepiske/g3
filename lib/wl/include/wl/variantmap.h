#ifndef _WL_VariantMap_H_
#define _WL_VariantMap_H_

#include <wl/config.h>
#include <wl/variant.h>
#include <boost/unordered_map.hpp>

#include <algorithm>

wlStartNS

class VariantMapHandler : public VariantHandler 
{
public:
    struct Var_Eq : std::binary_function<Variant, Variant, bool> {
        bool operator() (const Variant &x, const Variant &y) const {
            return 0 == x.compare(y);
        }
    };

    struct Var_Hash : std::unary_function<Variant, std::size_t> {
        std::size_t operator() (Variant const &v) const {
            return (std::size_t)v.hash();
        }
    };

    typedef ::boost::unordered_map<Variant, Variant,
        struct Var_Hash, struct Var_Eq> VarMapT;

    virtual u32 hash(const Variant *self) {
        return 0;
    }

    virtual int compare(const Variant *self, const Variant *other) {
        return 1;
    }

    virtual void release(Variant *self) {
        delete (VarMapT*)self->to_pointer();
    }

    virtual void copy_from(Variant *self, const Variant *other) {
        set_handled(self, other->to_pointer());
    }

    virtual void set_handled(Variant *self, void *pointer) {
        VarMapT *t = new VarMapT(*(VarMapT*)pointer);
        self->__setptr(t);
    }

    virtual std::string classname(const Variant *self) {
        return "WL.VariantMap";
    }

    virtual void set(Variant *self, const Variant *idx, const Variant *value) { 
        VarMapT *t = (VarMapT*)self->to_pointer();
        (*t)[*idx] = *value;
    }

    virtual void get(const Variant *self, const Variant *idx, Variant *out_value) {
        VarMapT *t = (VarMapT*)self->to_pointer();
        VarMapT::iterator it = t->find(*idx);
        if (it != t->end())
            *out_value = it->second;
    }

};

/// XXX MEMORY LEAK
WLINLINE static Variant new_variant_map() {
    Variant v;
    v.__sethandler(new VariantMapHandler());
    v.set_handled(new VariantMapHandler::VarMapT());
    return v;
}

wlEndNS

#endif


