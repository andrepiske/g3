#ifndef _WL_Font_H_
#define _WL_Font_H_

#include <wl/config.h>
#include <wl/resource.h>
#include <string>
#include <wl/math.h>

wlStartNS
class IFont : public Resource
{
public:
    IFont() : Resource() {}
    virtual ~IFont() {}

    struct TextInfo {
        // from font metrics
        float width;
        float bearY;
        float height;

        // bounding box (from bitmap)
        int bb_width;
        int bb_height;
    };

    static IFont *create_resource(ResourceIOBlock blk, float size);

    virtual void get_text_info(const std::string &text,
        TextInfo &tinfo) = 0;
    virtual void draw_text(const std::string &text,
        const Vec2 &origin) = 0;

    virtual void load_data() = 0;
    virtual void unload_data() = 0;

};

wlEndNS
#endif

