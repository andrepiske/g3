#include "precompiled.h"
#include <wl/scenegraph.h>
#include <wl/log.h>
#include <wl/glstate.h>
#include <wl/color.h>

wlStartNS
namespace sg {

void Node::add_child(Node *node) {
    assert(node);
    if (node->m_parent == this) {
        WL_LOG_ERROR("Node already inserted (%s)",
            node->m_name.c_str());
    }

    PVector::iterator it;
    node->m_parent = this;

    node->rc_push();
    m_children.push_back(node);
}

void Node::remove_child(Node *node) {
    if (node->m_parent != this)
        WL_LOG_ERROR("Removing node which parent is not me! (node=%s, me=%s)",
            node->m_name.c_str(), m_name.c_str());

    PVector::iterator it = find_child(node);
    
    if (it == m_children.end())
        WL_LOG_ERROR("Removing node that is not contained", 0);

    m_children.erase(it);
    node->m_parent = NULL;
    node->rc_pop();
}

Node *Node::find_descendant(const std::string &name) {
    Node::PVector::iterator it = m_children.begin();
    for (; it != m_children.end(); ++it) {
        Node *node = *it;
        if (node->m_name == name)
            return node;
        Node *child_node = node->find_descendant(name);
        if (child_node)
            return child_node;
    }
    return wlNull;
}

Node::PVector::iterator Node::find_child(const std::string &name) {
    Node::PVector::iterator it;
    for (it = m_children.begin(); it != m_children.end(); ++it) {
        if ((*it)->m_name == name)
            break;
    }
    return it;
}

Node::PVector::iterator Node::find_child(Node *node) {
    Node::PVector::iterator it;
    for (it = m_children.begin(); it != m_children.end(); ++it) {
        if (*it == node)
            break;
    }
    return it;
}

void Node::_pre_draw(IScreen *sc) {
    if (m_transform) {
        glPushMatrix();
        float m_matrix[16];
        m_transform->get_matrix().to_vec16(m_matrix);
        glMultMatrixf((GLfloat*)m_matrix);
    }

    // TODO draw borders and such
    // maybe draw cache, if available?
}

void Node::_post_draw(IScreen *sc) {
    if (m_transform)
        glPopMatrix();
    // save cache?
    // return;

    // some borders
    /*
    Color(0x48, 0xBF, 0x3D).gl_set();

    const BBox &bb = get_bbox();

    glBegin(GL_LINE_LOOP);
    glVertex2f(bb.pos.x, bb.pos.y+bb.size.y); 
    glVertex2f(bb.pos.x+bb.size.x, bb.pos.y+bb.size.y);
    glVertex2f(bb.pos.x+bb.size.x, bb.pos.y);
    glVertex2f(bb.pos.x, bb.pos.y);
    glEnd();
    */
}

void Node::draw_children(IScreen *sc) {
    if (!m_children.empty()) {
        PVector::iterator it;
        for (it = m_children.begin(); it != m_children.end(); ++it)
            (*it)->draw(sc);
    }
}

} // ::sg

wlEndNS

