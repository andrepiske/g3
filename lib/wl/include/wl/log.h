#ifndef _WL_Log_H_
#define _WL_Log_H_

#include <wl/config.h>
#include <stdio.h>
#include <stdarg.h>

// #define DEBUG_LEVEL_LOG 1

wlStartNS

class Log
{
protected:
    enum {
        MAX_FILES = 16,
        RESERVE_SIZE = 16*1024,
        BUFFER_SIZE = 8*1024,
    };

    Log();
    static Log *s_log;

    char *m_buffer, *m_reserve;
    FILE *m_files[MAX_FILES];

public:
    static Log *get_singleton();

    void push_file(FILE *fp);

    static void log_error_va(const char *str, va_list &l);
    static void log_warn_va(const char *str, va_list &l);
    static void log_info_va(const char *str, va_list &l);

    static void log_error(
        #ifdef DEBUG_LEVEL_LOG
            const char *_filename, int _lineno,
        #endif
            const char *str, ...) {
        va_list args;
        va_start(args, str);
        log_error_va(str, args);
        va_end(args);
    }

    static void log_warn(
        #ifdef DEBUG_LEVEL_LOG
            const char *_filename, int _lineno,
        #endif
            const char *str, ...) {
        va_list args;
        va_start(args, str);
        log_warn_va(str, args);
        va_end(args);
    }

    static void log_info(const char *str, ...) {
        va_list args;
        va_start(args, str);
        log_info_va(str, args);
        va_end(args);
    }

    void _log_buffer();

};

wlEndNS

#ifdef DEBUG_LEVEL_LOG

#endif

WLINLINE void WL_LOG_ERROR(const char *s, ...) {
    va_list args;
    va_start(args, s);
    WL::Log::log_error_va(s, args);
    va_end(args);
}

WLINLINE void WL_LOG_WARN(const char *s, ...) {
    va_list args;
    va_start(args, s);
    WL::Log::log_warn_va(s, args);
    va_end(args);
}

WLINLINE void WL_LOG_INFO(const char *s, ...) {
    va_list args;
    va_start(args, s);
    WL::Log::log_info_va(s, args);
    va_end(args);
}

#endif

