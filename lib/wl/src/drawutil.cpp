#include "precompiled.h"
#include <wl/drawutil.h>
#include <GL/gl.h>

wlStartNS
namespace du {

void draw_rectangle(const Vec2 &pos, const Vec2 &size) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEdgeFlag(GL_FALSE);

    glBegin(GL_LINES);

    glVertex2f(pos.x, pos.y);
    glVertex2f(pos.x + size.x, pos.y);

    glVertex2f(pos.x + size.x, pos.y);
    glVertex2f(pos.x + size.x, pos.y + size.y);

    glVertex2f(pos.x + size.x, pos.y + size.y);
    glVertex2f(pos.x, pos.y + size.y);

    glVertex2f(pos.x, pos.y + size.y);
    glVertex2f(pos.x, pos.y);
    glEnd();
}

void fill_rectangle(const Vec2 &pos, const Vec2 &size) {
    glBegin(GL_QUADS);
    glVertex2f(pos.x, pos.y);
    glVertex2f(pos.x + size.x, pos.y);
    glVertex2f(pos.x + size.x, pos.y + size.y);
    glVertex2f(pos.x, pos.y + size.y);
    glEnd();
}

void draw_curved_rect(const Vec2 &size, float radius,
        wlBool filled, int round_corners, int num_steps)
{
    const float step_size = radius / (float)(num_steps-1);
    const float radius2 = radius * radius;

    if (radius == 0.f)
        round_corners = 0;

    if (filled) {
        glBegin(GL_QUADS);
        glVertex2f(0.f, radius);
        glVertex2f(size.x, radius);
        glVertex2f(size.x, size.y-radius);
        glVertex2f(0.f, size.y-radius);

        glVertex2f(radius, 0.f);
        glVertex2f(size.x-radius, 0.f);
        glVertex2f(size.x-radius, radius);
        glVertex2f(radius, radius);

        glVertex2f(radius, size.y-radius);
        glVertex2f(size.x-radius, size.y-radius);
        glVertex2f(size.x-radius, size.y);
        glVertex2f(radius, size.y);
        glEnd();
    } else {
        glBegin(GL_LINES);
        glVertex2f(radius, 0.f);
        glVertex2f(size.x-radius, 0.f);
        glVertex2f(size.x, radius);
        glVertex2f(size.x, size.y-radius);
        glVertex2f(0.f, radius);
        glVertex2f(0.f, size.y-radius);
        glVertex2f(radius, size.y);
        glVertex2f(size.x-radius, size.y);
        glEnd();
    }

    // top right
    glBegin(filled ? GL_TRIANGLE_FAN : GL_LINE_STRIP);
    if (filled)
        glVertex2f(size.x-radius, radius);
    for (int i = 0; i < num_steps; i++) {
        float x = step_size * (float)i;
        glVertex2f(size.x-radius+x, radius-sqrt(radius2 - x*x));
    }
    glEnd();
    // top left
    glBegin(filled ? GL_TRIANGLE_FAN : GL_LINE_STRIP);
    if (filled)
        glVertex2f(radius, radius);
    for (int i = 0; i < num_steps; i++) {
        float x = step_size * (float)i;
        glVertex2f(x, radius-sqrt(radius2-(radius-x)*(radius-x)));
    }
    glEnd();
    // bottom left
    glBegin(filled ? GL_TRIANGLE_FAN : GL_LINE_STRIP);
    if (filled)
        glVertex2f(radius, size.y-radius);
    for (int i = 0; i < num_steps; i++) {
        float x = step_size * (float)i;
        glVertex2f(x, size.y+sqrt(radius2-(x-radius)*(x-radius))-radius);
    }
    glEnd();
    // bottom right
    glBegin(filled ? GL_TRIANGLE_FAN : GL_LINE_STRIP);
    if (filled)
        glVertex2f(size.x-radius, size.y-radius);
    for (int i = 0; i < num_steps; i++) {
        float x = step_size * (float)i;
        glVertex2f(size.x-radius+x, size.y+sqrt(radius2 - x*x)-radius);
    }
    glEnd();
}

wlBool curved_rect_hit_test(const Vec2 &size, float radius,
    int round_corners, const Vec2 &point)
{
    if (point.x >= size.x || point.x < 0.f ||
        point.y >= size.y || point.y < 0.f)
        return wlFalse;

    if (round_corners == 0 || radius == 0.f)
        return wlTrue;

    // TODO: round box hit test

    return wlTrue;
}


} // ::du
wlEndNS
