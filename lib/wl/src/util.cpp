#include "precompiled.h"
#include <wl/util.h>
#include <wl/script.h>
#include <sstream>
#include <memory.h>

// Linux
#include <time.h>

wlStartNS

// TODO: make Windows & Mac compatible
void util::sleep_ms(::WL::u64 t) {
    if (t < 1000ULL) {
        const struct timespec v = { (time_t)0, (long)t * 1000000L };
        nanosleep(&v, wlNull);
    } else {
        const struct timespec v = { (time_t)(t / 1000ULL),
            (long)(t % 1000ULL) * 1000000L };
        nanosleep(&v, wlNull);
    }
}

std::string util::script_stackdump(IScript *s) {
    std::stringstream result;
    int size = s->stack_size();
    result << "Stack dump (" << size << "):" << std::endl;
    result << "\tstack top:" << std::endl;
    for (int i=size; i > 0; --i) {
        result << "\t" << i << " (" << s->luatypename(i) << "): ";
        int t = s->luatype(i);
        std::string content = "<unknown>";
        if (t == LUA_TSTRING || t == LUA_TNUMBER)
            content = s->to_string(i);
        else if (t == LUA_TBOOLEAN)
            content = s->to_bool(i) ? "true" : "false";
        result << content << std::endl;
    }
    result << "End of stack dump" << std::endl;
    return result.str();
}

wlBool util::fileio_readline(IFileIO *f, std::string &c) {
    if (f->eof())
        return wlFalse;
    
    c = "";

    const int BS = 128;
    int buf_size = 0;
    char buf[BS+1];
    wlBool done = wlFalse;
    for (;!done;) {
        for (;;) {
            if (1 != f->read((void*)&buf[buf_size], 1)) {
                done = wlTrue;
                break;
            }
            if (buf[buf_size] == '\n') {
                done = wlTrue;
                break;
            }
            else if (buf[buf_size] == '\r')
                buf_size--;
            if (++buf_size == BS-1)
                break;
        }
        buf[buf_size] = 0;
        buf_size = 0;
        c += buf;
    }
    return wlTrue;
}

u32 util::djb2_string_hash(const char *s, u32 len) {
    u32 hash = 5381;
    if (len == 0xFFFFFFFF)
        len = (u32)strlen(s);
    for (; len > 0; len--, s++)
        hash = ((hash << 5) + hash) + (u32)(*s);

    return hash;
}

wlEndNS


