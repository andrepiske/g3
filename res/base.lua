-- vim:syntax=lua

-------------------------------------------------------------------------------
-- Sprite
-------------------------------------------------------------------------------

Sprite = {}
Sprite_mt = { __index = Sprite }

function Sprite:new(name, x, y, width, height, tx)
    return setmetatable({
        name = name, x = x, y = y, width = width, height = height,
        texture = tx
    },Sprite_mt)
end


-------------------------------------------------------------------------------
-- Texture
-------------------------------------------------------------------------------

Texture = {}
Texture_mt = { __index = Texture }

function Texture:new(name, resource_name)
    return setmetatable({
        name = name,
        resource_name = resource_name
    }, Texture_mt) 
end

function Texture:fobar()
    local x = papa()
    x:say("hello")
    return "bar"
end


function FoBarZ(a)
    a.x = -4.0
    return a
end

-------------------------------------------------------------------------------
-- Painter
-------------------------------------------------------------------------------


