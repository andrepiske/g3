#ifndef _G2_Sprites_H_
#define _G2_Sprites_H_

#include "config.h"
#include <boost/unordered_map.hpp>
#include <wl/image.h>
#include <wl/sprite.h>
#include <wl/math.h>

class SpriteMan
{
public:
    SpriteMan() {
    }

    struct SpriteDef {
        int x, y, width, height;
    };
    
    typedef boost::unordered_map<std::string, WL::ISprite*> SpriteMap;
    SpriteMap m_sprites;

    WLINLINE WL::ISprite *get_sprite(const std::string &name) {
        SpriteMap::iterator it = m_sprites.find(name);
        if (it == m_sprites.end())
            WL_LOG_ERROR("G2 SpriteMap: sprite '%s' not found", name.c_str());
        return it->second;
    }

    WLINLINE void set_sprite(const std::string &name, WL::ISprite *spr) {
        m_sprites[name] = spr;
    }

    WLINLINE void add_sprite(const std::string &name, SpriteDef sd,
            WL::IImage *image) {
        WL::ISprite *spr = WL::ISprite::create_sprite(image,
                WL::Vec2((float)sd.x, (float)sd.y),
                WL::Vec2((float)sd.width, (float)sd.height));
        set_sprite(name, spr);
    }

    void load_sprites(WL::ResourceIOBlock b, WL::IImage *image);

};


#endif


