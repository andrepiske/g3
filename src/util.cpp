#include "util.h"

namespace G3 {
namespace util {

IVar *var_from_vec3(const WL::Vec3 &v) {
    IVar *var = IVar::new_table();
    var->set_pair("x", IVar::new_double(v.x));
    var->set_pair("y", IVar::new_double(v.y));
    var->set_pair("z", IVar::new_double(v.z));
    return var;
}

wlBool vec3_from_var(IVar *var, WL::Vec3 &out_v) {
    if (!var->get_type() == IVar::VT_TABLE)
        return wlFalse;

    IVar *x, *y, *z;
    if (  !(x = var->get_pair("x")) 
       || !(y = var->get_pair("y"))
       || !(z = var->get_pair("z")) )
        return wlFalse;

    out_v.x = x->get_double();
    out_v.y = y->get_double();
    out_v.z = z->get_double();
    return wlTrue;
}


/*
void connect_gsignal(void *object, const char *signal_name,
        boost::function<void()> callback)
{
}
*/

} // NS util
} // NS G3

