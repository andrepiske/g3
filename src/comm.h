#ifndef __G3_COMM_H__
#define __G3_COMM_H__

#include <wl/config.h>
#include <wl/refcounted.h>
#include <wl/event.h>
#include <boost/function.hpp>

namespace G3 {

class ICommServer : public WL::RefCounted
{
public:
    struct ReceivedData : public WL::RefCounted {
        WL::u64 client_id;
        std::string data;
    };

    virtual bool listen(WL::u32 port) = 0;
    virtual void send(const std::string &data) = 0;

    virtual void set_receive_callback(boost::function<void(void*)> cb) = 0;

public:
    static ICommServer *create();
};


} // NS G3

#endif

