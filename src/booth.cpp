#include "booth.h"

namespace G3 {

class CJuggler : public IJuggler
{
public:
    virtual GtkWidget *get_widget() {
        return 0;
    }

    virtual std::string get_name() {
        return "foo";
    }

};

class CBooth : public IBooth
{
public:
    static gboolean
    _event_handler(GtkWidget *widget, GdkEvent *ev, gpointer ud) {
        CBooth *b = static_cast<CBooth*>(ud);
        return b->_handle_widget_event(widget, ev);
    }

public:
    GtkWidget *m_scroll;
    GtkWidget *m_fixed;
    GtkAdjustment *m_hadj, *m_vadj;

public:
    CBooth() {
        m_hadj = gtk_adjustment_new(0.0, -10.0, 10.0, 1.0, 1.0, 1.0);
        m_vadj = gtk_adjustment_new(0.0, -10.0, 10.0, 1.0, 1.0, 1.0);
        m_scroll = gtk_scrolled_window_new(m_hadj, m_vadj);

        g_object_ref_sink(G_OBJECT(m_scroll));

        m_fixed = gtk_fixed_new();

        gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(m_scroll),
            m_fixed);

        gtk_widget_set_hexpand(m_fixed, TRUE);
        gtk_widget_set_vexpand(m_fixed, TRUE);
        gtk_widget_set_hexpand(m_scroll, TRUE);
        gtk_widget_set_vexpand(m_scroll, TRUE);
    }

    gboolean _handle_widget_event(GtkWidget *wid, GdkEvent *ev) {
        return 1;
    }

    virtual ~CBooth() {
        g_object_unref(G_OBJECT(m_scroll));
    }

    virtual GtkWidget *get_widget() {
        return m_scroll;
    }


};

IBooth *IBooth::create() {
    return new CBooth();
}


}

