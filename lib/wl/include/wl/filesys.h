#ifndef _WL_Filesys_H_
#define _WL_Filesys_H_

#include <wl/config.h>
#include <string>
#include <list>

wlStartNS

namespace fsys {

wlBool dir_list(const std::string &path, std::list<std::string> &out);

wlBool is_directory(const std::string &path);
wlBool is_file_readable(const std::string &path);

wlBool make_directories(const std::string &path);

std::string dir_join(const char *a, ...);

std::string dir_current();

}

wlEndNS

#endif

