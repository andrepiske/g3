#include "precompiled.h"
#include <wl/script.h>
#include <boost/scoped_array.hpp>
#include <wl/fileio.h>
#include <wl/log.h>
#include <memory.h>
#include <sstream>

#include <wl/util.h>

wlStartNS

class CScript : public IScript
{
public:
    lua_State *m_lua;

public:
    CScript() : IScript() {
        m_lua = wlNull;
    }

    virtual ~CScript() {
        if (m_lua) {
            lua_close(m_lua);
            m_lua = wlNull;
        }
    }

    wlBool __create_new() {
        m_lua = luaL_newstate();
        if (!m_lua) {
            WL_LOG_WARN("script loading: failed to create lua stat", 0);
            return wlFalse;
        }
        luaL_openlibs(m_lua);
        return wlTrue;
    }

    wlBool load_resource(ResourceIOBlock blk) {
        IFileIO *file = blk.file();
        if (!file) {
            WL_LOG_WARN("script loading: file==null", 0);
            return wlFalse;
        }

        file->seek(0, IFileIO::SK_START);
        long file_size = file->size();

        wlByte *buffer = new wlByte[file_size + 1];
        boost::scoped_array<wlByte> buffer_ptr(buffer);

        if (file_size != file->read(buffer, file_size)) {
            WL_LOG_WARN("script loading: incomplete resource read", 0);
            return wlFalse;
        }

        buffer[file_size] = 0;
        
        m_lua = luaL_newstate();
        if (!m_lua) {
            WL_LOG_WARN("script loading: lua new state failed", 0);
            return wlFalse;
        }
            
        if (luaL_loadstring(m_lua, (const char*)buffer)) {
            lua_close(m_lua);
            m_lua = wlNull;
            WL_LOG_WARN("script loading: loadstring failed", 0);
            return wlFalse;
        }

        return wlTrue;
    }

    virtual lua_State *get_luastate() {
        return m_lua;
    }

    virtual wlBool execute() {
        return (!lua_pcall(m_lua, 0, LUA_MULTRET, 0)) ? wlTrue : wlFalse;
    }

    virtual wlBool exists(const std::string &var) {
        return wlFalse;
        // this->exec_expr(var, 1);
    }

    virtual wlBool assign(const std::string &var, const std::string &expr) {
        std::stringstream e;
        e << var << " = " << expr;
        std::string _e = e.str();
        return exec_expr(_e, 0);
    }

    virtual wlBool exec_expr(const std::string &expr, int nresult) {
        luaL_loadstring(m_lua, expr.c_str());
        const int ret = lua_pcall(m_lua, 0, nresult, 0); // ? wlFalse : wlTrue;
        const char *errs = "unknown";
        switch (ret) {
            case LUA_ERRRUN: errs = "Runtime error"; break;
            case LUA_ERRMEM: errs = "Memory allocation error"; break;
            case LUA_ERRERR: errs = "Error while running error handler"; break;
        }
        if (ret) {
            const char *errh = lua_tostring(m_lua, -1);
            WL_LOG_WARN("wlScript::exec_expr error (%d, %s): %s",
                    ret, errs, errh);
        }
        return ret ? 0 : 1;
    }

    virtual std::string string_expr(const std::string &expr,
            const std::string &default_) {
        if (!this->exec_expr(expr, 1))
            return default_;

        if (lua_isstring(m_lua, -1)) {
            std::string ret = this->to_string(-1);
            lua_pop(m_lua, 1);
            return ret;
        }

        lua_pop(m_lua, 1);
        return default_;
    }

    virtual int int_expr(const std::string &expr, int default_) {
        if (!this->exec_expr(expr, 1))
            return default_;
        if (lua_isnumber(m_lua, -1))
            default_ = (int)this->to_int(-1);
        lua_pop(m_lua, 1);
        return default_;
    }

    virtual int double_expr(const std::string &expr, double default_) {
        if (!this->exec_expr(expr, 1))
            return default_;
        if (lua_isnumber(m_lua, -1))
            default_ = (int)this->to_double(-1);
        lua_pop(m_lua, 1);
        return default_;
    }

    virtual void gc_call() {
        int memory_using = lua_gc(m_lua, LUA_GCCOUNT, 0);
        WL_LOG_INFO("Running GC on script (resource %s)", m_resname.c_str());
        WL_LOG_INFO("Script memory: %d Kbytes", memory_using);
        lua_gc(m_lua, LUA_GCCOLLECT, 0);
        int new_memory = lua_gc(m_lua, LUA_GCCOUNT, 0);
        WL_LOG_INFO("Script memory: %d Kbytes (freed %d Kbytes)", new_memory,
                new_memory - memory_using);
    }

    // not applicable on scripts
    virtual void load_data() { }
    virtual void unload_data() { }

    // TODO: install error handler, fix this...
    void report_error() {
        lua_Debug dbg;
        memset(&dbg, 0, sizeof(dbg));

        if (lua_getstack(m_lua, 0, &dbg) != 1)
            WL_LOG_ERROR("script: lua_getstack returned non-one", 0);
        if (!lua_getinfo(m_lua, "Sln", &dbg))
            WL_LOG_ERROR("script: lua_getinfo returned zero!", 0);

        WL_LOG_WARN("script failed", 0);
        WL_LOG_WARN("Script error:\nSource=%s\nshort_src=%s\n"
            "function line=%d\ncurrent line=%d\nfunction name=%s\n"
            "namewhat=%s\n",
            dbg.source, dbg.short_src, dbg.linedefined, dbg.currentline,
            dbg.name, dbg.namewhat);
    }

public:
    virtual wlBool equals(int index1, int index2) {
        // return lua_compare(m_lua, index1, index2, LUA_OPEQ) ? wlTrue : wlFalse;
        // below is code prior to lua 5.2
        return (!lua_equal(m_lua, index1, index2)) ? wlFalse : wlTrue;
    }

    virtual std::string to_string(int index) {
        const char *str = lua_tolstring(m_lua, index, wlNull);
        if (!str)
            WL_LOG_ERROR("script to_string returned NULL", 0);
        return str;
    }

    virtual wlBool to_bool(int index) {
        if (!lua_isboolean(m_lua, index))
            WL_LOG_ERROR("script to_bool not a boolean", 0);
        return (!lua_toboolean(m_lua, index)) ? wlFalse : wlTrue;
    }

    virtual long to_int(int index) {
        return (long)lua_tointeger(m_lua, index);
    }

    virtual double to_double(int index) {
        return (double)lua_tonumber(m_lua, index);
    }

    virtual void *to_pointer(int index) {
        return (void*)lua_topointer(m_lua, index);
    }

    virtual void *to_userdata(int index) {
        return (void*)lua_touserdata(m_lua, index);
    }

    virtual int luatype(int index) {
        return (int)lua_type(m_lua, index);
    }

    virtual std::string luatypename(int index) {
        int type = (int)lua_type(m_lua, index);
        return lua_typename(m_lua, type);
    }

    virtual void pop(int n) {
        lua_pop(m_lua, n);
    }

    virtual void push_bool(wlBool v) {
        lua_pushboolean(m_lua, v ? 1 : 0);
    }

    virtual void push_string(const std::string &str) {
        int len = str.size();
        lua_pushlstring(m_lua, str.c_str(), len);
    }

    virtual void push_int(long n) { 
        lua_pushinteger(m_lua, (lua_Integer)n);
    }

    virtual void push_nil() {
        lua_pushnil(m_lua);
    }

    virtual void push_double(double v) {
        lua_pushnumber(m_lua, (lua_Number)v);
    }

    static int _closure_cb(lua_State *l) {
        int idx = lua_upvalueindex(1);
        CScript tmp_s;
        tmp_s.m_lua = l;

        CallbackCProc p = (CallbackCProc) tmp_s.to_userdata(idx);
        int n_ret = p(&tmp_s);

        tmp_s.m_lua = wlNull;
        return n_ret;
    }

    static int _bfunction_cb(lua_State *l) {
        int idx = lua_upvalueindex(1);
        CScript tmp_script;
        tmp_script.m_lua = l;

        CallbackProc *p = (CallbackProc*) tmp_script.to_userdata(idx);
        int n_ret = (*p)(&tmp_script);

        tmp_script.m_lua = wlNull;
        return n_ret;
    }

    virtual void push_bfunction(CallbackProc func, int n) {
        // TODO: put a metatable with __gc to free up things
        CallbackProc *proc = new CallbackProc(func);
        lua_pushlightuserdata(m_lua, (void*)proc);
        lua_pushcclosure(m_lua, CScript::_bfunction_cb, n + 1);
    }

    virtual void push_cclosure(CallbackCProc func, int n) {
        lua_pushlightuserdata(m_lua, (void*)func);
        lua_insert(m_lua, -(n+1));
        lua_pushcclosure(m_lua, CScript::_closure_cb, n + 1);
    }

    virtual void push_lightuserdata(void *p) {
        lua_pushlightuserdata(m_lua, p);
    }

    virtual void push_copy(int index) {
        lua_pushvalue(m_lua, index);
    }

    virtual void insert(int index) {
        lua_insert(m_lua, index);
    }

    virtual void replace(int index) {
        lua_replace(m_lua, index);
    }

    virtual void remove(int index) {
        lua_remove(m_lua, index);
    }

    virtual wlBool getfield_safe(int index, const std::string &name) {
        lua_pushnil(m_lua);
        while (lua_next(m_lua, index)) {
            if (lua_isstring(m_lua, -2)) {
                const char *_s = lua_tostring(m_lua, -2);
                if (name == _s) {
                    lua_remove(m_lua, -2);
                    return wlTrue;
                }
            }
            lua_pop(m_lua, 1);
        }
        return wlFalse;
    }

    virtual void getfield(int index, const std::string &name) {
        lua_getfield(m_lua, index, name.c_str());
    }

    virtual void gettable(int index) {
        lua_gettable(m_lua, index);
    }

    /*
    virtual void getenv(const std::string &name) {
        lua_getfield(m_lua, LUA_ENVIRONINDEX, name.c_str());
    } */

    virtual void getglobal(const std::string &name) {
        // lua_pushinteger(m_lua, LUA_RIDX_GLOBALS);
        // lua_gettable(m_lua, LUA_REGISTRYINDEX);
        // lua_getfield(m_lua, -1, name.c_str());
        // below is prior to lua 5.2
        lua_getfield(m_lua, LUA_GLOBALSINDEX, name.c_str());
    }

    virtual wlBool getmetatable(int index) {
        if (0 == lua_getmetatable(m_lua, index)) {
            WL_LOG_WARN("getmetatable: no metatable!", 0);
            return wlFalse;
        }
        return wlTrue;
    }

    virtual void setfield(int index, const std::string &name) {
        lua_setfield(m_lua, index, name.c_str());
    }

    virtual void settable(int index) {
        lua_settable(m_lua, index);
    }

    /*
    virtual void setenv(int index) {
        lua_setfenv(m_lua, index);
    } */

    virtual void setglobal(const std::string &name) {
        // lua_pushinteger(m_lua, LUA_RIDX_GLOBALS);
        // lua_gettable(m_lua, LUA_REGISTRYINDEX);
        // lua_insert(m_lua, -2);
        // lua_setfield(m_lua, -2, name.c_str());
        // below is prior to lua 5.2
        lua_setfield(m_lua, LUA_GLOBALSINDEX, name.c_str());
    }

    virtual wlBool setmetatable(int index) {
        if (0 == lua_setmetatable(m_lua, index)) {
            WL_LOG_WARN("setmetatable: no metatable!", 0);
            return wlFalse;
        }
        return wlTrue;
    }

    virtual int stack_size() {
        return lua_gettop(m_lua);
    }

    virtual wlBool call(int nargs, int nresults = 1) {
        int ret = 
            lua_pcall(m_lua, nargs, nresults, 0);
        if (ret == 0)
            return wlTrue;

        const char *msg = lua_tostring(m_lua, -1);

        WL_LOG_WARN("script call failed (%d): %s", ret, msg);
        return wlFalse;
    }

    virtual void new_table() {
        lua_newtable(m_lua);
    }

    virtual void *new_userdata(int size) {
        return (void*)lua_newuserdata(m_lua, (size_t)size);
    }

};

IScript *IScript::create_resource(ResourceIOBlock blk) {
    CScript *cs = new CScript();
    if (!cs->load_resource(blk)) {
        delete cs;
        return wlNull;
    }
    return cs;
}

IScript *IScript::create_new() {
    CScript *cs = new CScript();
    if (!cs->__create_new()) {
        delete cs;
        return wlNull;
    }
    return cs;
}

wlEndNS

