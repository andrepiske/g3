#include "precompiled.h"
#include <wl/text.h>
#include <wl/glstate.h>
#include <wl/math.h>

wlStartNS

class CText : public IText
{
public:
    // Textinfo cache for current text
    IFont::TextInfo m_textinfo;
    std::string m_text;

public:
    CText() {
    }

    virtual ~CText() {
        m_font->rc_pop();
    }

    virtual void set_text(const std::string &text) {
        m_text = text;
        m_font->get_text_info(m_text, m_textinfo);
        invalidate_bounding_box();
    }

    virtual std::string get_text() {
        return m_text;
    }

    virtual BBox2 calculate_bbox() const {
        return BBox2(Vec2(0.f, 0.f),
            Vec2( (float)m_textinfo.width, (float)m_textinfo.height) );
    }

    virtual void draw_self(IScreen *s) {
        m_textcolor.gl_set();
        m_font->draw_text(m_text, Vec2(0.f, (float)m_textinfo.bearY));
    }

};


IText *IText::create_text(const std::string &text, const Color &color,
    IFont *font) {
    CText *t = new CText();
    t->m_textcolor = color;
    t->m_font = font;
    t->set_text(text);
    font->rc_push();
    return t;
}

wlEndNS


