#include "g3d.h"
#include <wl/scriptable.h>
#include <GL/gl.h> 
#include <GL/glu.h> 
#include "g3.h"
#include <wl/script.h>
#include <wl/math.h>
#include "cvmesh.h"
#include <sys/time.h>

extern void _G3_IGd__luapush(lua_State *L, WL::RefCounted *obj);

using namespace WL::sg;

namespace G3 {

class CGd : public IGd,
    public WL::scriptable::RegPush<WL::RefCounted>
{
public:
    WL::RCBucket<Node> m_rootnode;
    WL::Vec2 m_camera_args;
    WL::Vec3 m_camera_pos;

public:
    CGd() : WL::scriptable::RegPush<WL::RefCounted>(this, _G3_IGd__luapush)
    {
        m_rootnode = new ContainerNode();
        m_camera_args = WL::Vec2(0.f, 0.f);
        m_camera_pos = WL::Vec3(0.f, 1.f, 2.f);
    }

    void _draw_grid() {
        glColor3ub(0xBF, 0xBF, 0xBF);
        glLineWidth(1.0f);
        for (int x = -10; x < 10; ++x) {
            for (int y = -10; y < 10; ++y) {
                const float sx = (float)(x * 1);
                const float sy = (float)(y * 1);
                const float ex = sx + 1.f;
                const float ey = sy + 1.f;
                glBegin(GL_LINE_LOOP);
                glVertex3f(sx, 0.f, sy);
                glVertex3f(ex, 0.f, sy);
                glVertex3f(ex, 0.f, ey);
                glVertex3f(sx, 0.f, ey);
                glEnd();
            }
        }
    }

    void _draw_axes() {
        glLineWidth(3.0f);
        glBegin(GL_LINES);
        glColor3ub(0xFF, 0, 0); // x is red
        glVertex3f(0.f, 0.f, 0.f);
        glVertex3f(2.f, 0.f, 0.f);
        glColor3ub(0, 0, 0xFF); // y is blue
        glVertex3f(0.f, 0.f, 0.f);
        glVertex3f(0.f, 2.f, 0.f);
        glColor3ub(0, 0xFF, 0); // z is green
        glVertex3f(0.f, 0.f, 0.f);
        glVertex3f(0.f, 0.f, 2.f);
        glEnd();
    }

    virtual void draw(IDrawSurface *surface) {
        using namespace WL;

        glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

        glClearColor(0x62/255.f, 0x73/255.f, 0x99/255.f, 1.f);
        glClearDepth(1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        const Vec2 wsz = surface->get_size();

        gluPerspective(45.f, wsz.x / wsz.y, 0.1, 500.f);
        // glFrustum(-1.0, 1.0, -1.0, 1.0, 0.1, 100.f);
        // glOrtho(-1.f, 1.f, -1.f, 1.f, -1.f, 1.f);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        const Vec3 &cp = m_camera_pos;
        Vec3 vl, up;
        _calculate_camera_vectors(vl, up);
        vl += cp;

        gluLookAt(
                cp.x, cp.y, cp.z,
                vl.x, vl.y, vl.z,
                up.x, up.y, up.z);

        glDisable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);
        _draw_grid();
        _draw_axes();

        glEnable(GL_LIGHTING);
        glDisable(GL_NORMALIZE);
        glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 0);

        {
            glEnable(GL_LIGHT0);
            // const float lWhite[] = { 0.99f, 0.99f, 0.99f, 1.f };
            const float lAmb[] = { 0.2f, 0.2f, 0.2f, 1.f };
            const float lDiff[] = { 0.5f, 0.5f, 0.5f, 1.f };
            glLightfv(GL_LIGHT0, GL_AMBIENT, (GLfloat*)lAmb);
            glLightfv(GL_LIGHT0, GL_DIFFUSE, (GLfloat*)lDiff);

            const float lPos[] = { -2.f, -2.f, 4.f, 0.f };
            glLightfv(GL_LIGHT0, GL_POSITION, (GLfloat*)lPos);
        }
        {
            glEnable(GL_LIGHT1);
            const float lAmb[] = { 0.1f, 0.1f, 0.1f, 1.f };
            const float lDiff[] = { 0.4f, 0.4f, 0.4f, 1.f };
            glLightfv(GL_LIGHT1, GL_AMBIENT, (GLfloat*)lAmb);
            glLightfv(GL_LIGHT1, GL_DIFFUSE, (GLfloat*)lDiff);

            const float lPos[] = { -11.f, 3.f, -6.f, 0.f };
            glLightfv(GL_LIGHT1, GL_POSITION, (GLfloat*)lPos);
        }

        glColor3ub(0, 0xFF, 0);

        const float lMat[] = { 0.92f, 0.92f, 0.92f, 1.f  };
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, (GLfloat*)lMat);
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (GLfloat*)lMat);

        m_rootnode->draw(NULL);
    }

    void _calculate_camera_vectors(WL::Vec3 &direction, WL::Vec3 &up) {
        const float alpha = m_camera_args.x;
        const float beta = -m_camera_args.y;
        const float betap = wlPI/2.f - beta;
        const float cb = cos(beta);
        const float sa = sin(alpha);
        const float cbp = cos(betap);
        const float ca = cos(alpha);
        direction = WL::Vec3( cb*ca, sin(beta), cb*sa );
        up = WL::Vec3( -cbp*ca, sin(betap), -cbp*sa );
    }

// input
public:
    virtual void input_motion_start() { }

    virtual void input_motion(double dx, double dy) {
        m_camera_args += WL::Vec2(dx/100.f, dy/100.f);
    }

    virtual void input_motion_stop() { }

// interface implementation
public:
    virtual WL::Vec3 get_camera_pos() {
        return m_camera_pos;
    }

    virtual void set_camera_pos(const WL::Vec3 &cp) {
        m_camera_pos = cp;
    }

    virtual void set_draw_wireframe(int wireframe) {
        // m_draw_wireframe = wireframe;
    }

    virtual void rem_mesh(IMesh* mesh) {
        m_rootnode->remove_child(mesh);
    }

    virtual IMesh *get_mesh(int index) {
        return static_cast<IMesh*>(m_rootnode->m_children[index]);
    }

    virtual int num_meshes() {
        return (size_t)m_rootnode->m_children.size();
    }

    virtual IMesh *add_mesh() {
        // IMesh *mesh = IMesh::create();
        //  m_rootnode->add_child(mesh);
        // return mesh;
        return NULL;
    }

    virtual void calculate_camera_vectors(WL::Vec3 &direction, WL::Vec3 &up) {
        _calculate_camera_vectors(direction, up);
    }

    virtual void get_camera_args(double &alpha, double &beta) {
        alpha = m_camera_args.x;
        beta = m_camera_args.y;
    }

    virtual void set_camera_args(double alpha, double beta) {
        m_camera_args = WL::Vec2(alpha, beta);
    }

    /*virtual ::G3::Math::IVertexGroup *new_vertexgroup() {
        return ::G3::Math::IVertexGroup::create();
    }*/

public:
    virtual WL::sg::Node *get_rootnode() {
        return m_rootnode();
    }


};

IGd *IGd::create() {
    return new CGd();
}


} // NS G3

