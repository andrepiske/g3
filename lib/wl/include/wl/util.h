#ifndef _WL_Util_H_
#define _WL_Util_H_

#include <wl/config.h>
#include <wl/script.h>

wlStartNS

template<class Vec2>
WLINLINE void IScript::push_t(const Vec2 &v) {
    this->new_table();
    this->push_double(v.x);
    this->setfield(-2, "x");
    this->push_double(v.y);
    this->setfield(-2, "y");
}

template<class Vec2>
WLINLINE wlBool IScript::to_t(Vec2 &v) {
    this->getfield(-1, "x");
    v.x = this->to_double(-1);
    this->pop(1);
    this->getfield(-1, "y");
    v.y = this->to_double(-1);
    this->pop(1);
    return wlTrue;
}

namespace util {

std::string script_stackdump(IScript *s);

// sleep thread for 't' milliseconds
// TODO: make it pause-safe (like computer hibernation, or other things)
void sleep_ms(::WL::u64 t);

wlBool fileio_readline(IFileIO *f, std::string &c);
u32 djb2_string_hash(const char *s, u32 len = 0xFFFFFFFF);

template<class T>
struct bounds
{
    T left;
    T right;
    T bottom;
    T top;

    typedef bounds<T> ThisT;
    typedef const bounds<T> ThisT_const;
    
    bounds() :
        left(0), right(0), bottom(0), top(0) {
    }

    bounds(T l, T r, T b, T t) :
        left(l), right(r), bottom(b), top(t) {
    }

    bounds(const ThisT_const &c) :
        left(c.left), right(c.right), bottom(c.bottom), top(c.top) {
    }

    ThisT &operator=(ThisT_const &c) {
        this->left = c.left;
        this->right = c.right;
        this->bottom = c.bottom;
        this->top = c.top;
        return *this;
    }

};

} // ::util
wlEndNS
#endif

