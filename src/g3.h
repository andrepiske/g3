#ifndef _G3_Main_H_
#define _G3_Main_H_

#include <wl/refcounted.h>
#include <string>

namespace G3 {

class IMain : virtual public WL::RefCounted
{
public:
    enum MsgType {
        MSG_INFO,
        MSG_WARNING,
        MSG_ERROR
    };

public:
    static IMain *instance();

    virtual void initialize(int argc, char **argv) = 0;
    virtual void run() = 0;

    virtual void ui_display_msg(const std::string &msg,
        const std::string &title, MsgType t) = 0;

    virtual int ui_ask_yesno(const std::string &msg,
        const std::string &title, MsgType t, wlBool hasCancel=0) = 0;


    virtual void run_script_code(const std::string &t) = 0;

    virtual wlBool ui_choose_folder(const std::string &title,
        const std::string &initial_dir, std::string &outdir) = 0;

};

} // NS G3

#endif


