#ifndef __G3_COMMPROTO_H__
#define __G3_COMMPROTO_H__

#include <wl/config.h>
#include <wl/refcounted.h>
#include <string>

namespace G3 {

class ICommProto : public WL::RefCounted
{
public:

    virtual long read_long() = 0;
    virtual double read_double() = 0;
    virtual std::string read_string() = 0;

    virtual void write_long(long val) = 0;
    virtual void write_double(double val) = 0;
    virtual void write_string(const std::string &val) = 0;

    virtual void reset() = 0;

    virtual void encode(char *buffer, size_t *length) = 0;
    virtual void decode(const char *buffer, size_t len) = 0;

    static ICommProto *create();

};

} // NS G3

#endif

