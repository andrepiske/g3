#include "curve.h"
#include <vector>

using namespace WL;

namespace G3 {

class CPolynomialCurve : public IPolynomialCurve
{
private:
    typedef std::vector<Vec3> Vec3Vec;
    Vec3Vec m_coeffs;

public:
    CPolynomialCurve() {
    }

    virtual void set_degree(u32 degree) {
        m_coeffs.resize((size_t)degree + 1, Vec3(0, 0, 0));
    }

    virtual u32 get_degree() {
        return (u32)(m_coeffs.size() - 1);
    }

    virtual void set_coeff(u32 coeff, const Vec3 &value) {
        m_coeffs[coeff] = value;
    }

    virtual Vec3 get_coeff(u32 coeff) {
        return m_coeffs[coeff];
    }

    virtual Vec3 get_value_at(const double t) {
        double tp = 1.0;
        const size_t deg = m_coeffs.size();
        Vec3 value(0, 0, 0);
        for (size_t i = 0; i < deg; ++i) {
            const Vec3 &cf = m_coeffs[i];
            for (size_t j = 0; j < 3; ++j)
                value[j] += cf[j] * tp;
            tp *= t;
        }
        return value;
    }

    virtual Vec3 get_derivative_at(const double t) {
        double tp = 1.0;
        double cm = 1;
        const size_t deg = m_coeffs.size();
        Vec3 value(0, 0, 0);
        for (size_t i = 1; i < deg; ++i) {
            const Vec3 &cf = m_coeffs[i];
            for (size_t j = 0; j < 3; ++j)
                value[j] += cf[j] * tp * cm;
            tp *= t;
            cm++;
        }
        return value;
    }

    virtual double calc_dt_for_length(double t_base, double length) {
        // TODO implement this! very important!
        return 0.0;
    }

};

IPolynomialCurve* IPolynomialCurve::create() {
    return new CPolynomialCurve();
}

} // NS G3


