#ifndef _WL_BRUSH_H_
#define _WL_BRUSH_H_

#include <wl/config.h>
#include <wl/image.h>
#include <wl/resource.h>
#include <vector>

wlStartNS

// ax + by + cz + d = 0
struct Plane {
    float a, b, c, d;
};

struct Triangle {
    int vertex_indices[3];
    Vec3 normal;
    Plane plane_equation();
    Vec2 tex_coords[3];
    IImage *texture; // Soft pointer.. won't consume nor clone.
};

class IBrush : public Resource
{
public:
    static IBrush *create_resource(ResourceIOBlock blk, Resource *loader);

    typedef std::vector<Vec3> VerticesT;
    typedef std::vector<Triangle> TrianglesT;

    virtual VerticesT *get_vertices() = 0;
    virtual TrianglesT *get_triangles() = 0;
};

wlEndNS

#endif

