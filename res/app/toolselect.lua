require('class')
require('math')
require('forms')

Forms.FormView.builders.tip_type = function(field)
    local c = gtk.ComboBoxText.new()
    c:append('conical', 'Conical')
    c:append('flat', 'Flat')
    c:append('spherical', 'Spherical')
    return c
end

local tools_view = Forms.FormView.new(Forms.FormData.new({
    -- page 5 ISO 14649-11
    cutspeed={
        type='speed_measure',
        optional=false,
        label='Cut speed',
    },
    -- page 56 ISO 14649-10 
    tool_offset_length={
        type='length_measure',
        optional=false,
        label='Tool offset',
    },
    -- tip type
    tip_type={
        type='tip_type',
        optional=false,
        label='Tip type'
    },
}))

ToolSelectWindow = Class(function(S)
    S.base = gtk.Box.new(gtk.ORIENTATION_VERTICAL)

    local editors = tools_view:get_editors()
    local data = {}
    data.cutspeed = 4
    data.tool_offset_length = 2
    data.tip_type = 'spherical'
    -- OptionEditor.set_options_data(options, ToolOptions, data)

    for cn,c in pairs(editors) do
        S.base:pack_start(c.widget, false, false, 5)
    end

    -- local bts_con, bts = OptionEditorHelper.make_ok_cancel_buttons()
    -- S.base:pack_start(bts_con, false, false, 5)

    function S:get_widget()
        return self.base
    end
end)

