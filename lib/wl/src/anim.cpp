#include <wl/anim.h>
#include <wl/time.h>

wlStartNS

class CAnimTime : public IAnimTime
{
public:
    u64 m_initial_time;
    float m_time;

    CAnimTime() {
        m_initial_time = TimeSpan::get_ticks();
    }
    
    virtual float get_time() {
        return m_time;
    }

    virtual void update() {
        m_time = (float)((double)(
                    TimeSpan::get_ticks() - m_initial_time) / 1000.0);
    }

};

IAnimTime *IAnimTime::create() {
    return new CAnimTime();
}

/*
Anim:
Return value refers to get_value with NULL index.

SINEF
-----
Return value: float, from -P*X to P*X

*/


wlEndNS

