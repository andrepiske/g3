#include "precompiled.h"
#include <wl/log.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <exception>

wlStartNS

Log *Log::s_log = wlNull;
///////////////////////////////////////////////////////////////////////////////

Log::Log() {
    m_buffer = new char[BUFFER_SIZE];
    m_reserve = new char[RESERVE_SIZE];

    for (int i = 0; i < MAX_FILES; ++i)
        m_files[i] = wlNull;
}

Log *Log::get_singleton() {
    if (!s_log)
        s_log = new Log();
    return s_log;
}

void Log::push_file(FILE *fp) {
    for (int i = 0; i < MAX_FILES; ++i) {
        if (!m_files[i]) {
            m_files[i] = fp;
            break;
        }
    }
}

void Log::log_error_va(const char *str, va_list &args) {
    if (s_log)
        delete [] s_log->m_reserve;

    char *buffer;
    Log *log = get_singleton();
    buffer = log->m_buffer;

    strcpy(buffer, "ERROR: ");
    vsprintf(&buffer[7], str, args);

    strcat(buffer, "\n");

    log->_log_buffer();

    throw std::exception();
    // exit(1);
}

void Log::log_warn_va(const char *str, va_list &args) {
    Log *log = get_singleton();

    char *buffer = log->m_buffer;

    strcpy(buffer, "WARN: ");
    vsprintf(&buffer[6], str, args);

    strcat(buffer, "\n");

    log->_log_buffer();
}

void Log::log_info_va(const char *str, va_list &args) {
    Log *log = get_singleton();

    char *buffer = log->m_buffer;

    strcpy(buffer, "INFO: ");
    vsprintf(&buffer[6], str, args);

    strcat(buffer, "\n");

    log->_log_buffer();
}

void Log::_log_buffer() {
    int len = 0;
    for (; len < BUFFER_SIZE && m_buffer[len]; ++len) {
    }

    for (int i = 0; i < MAX_FILES; ++i) {
        FILE *fp = m_files[i];
        if (!fp)
            break;

        fwrite(m_buffer, 1, len, fp);
        fflush(fp);
    }
}

wlEndNS

