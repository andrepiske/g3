#include <wl/time.h>
#include <time.h>
#include <sys/time.h>
#include <wl/log.h>

wlStartNS

// TODO catch pause signals?
::WL::u64 TimeSpan::get_ticks() {
    struct timeval tv;
    gettimeofday(&tv, wlNull);
    return ((::WL::u64)tv.tv_sec * 1000ULL) +
        ((::WL::u64)tv.tv_usec / 1000ULL);
}

/*
::WL::u64 TimeSpan::get_ticks() {
    clock_t v = clock();
    WL_LOG_INFO("Clock now: %lu", v);
    return (1000ULL * (::WL::u64)clock()) / (::WL::u64)CLOCKS_PER_SEC;
}
*/

wlEndNS

