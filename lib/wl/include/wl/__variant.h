#ifndef _WL_Variant_H_
#define _WL_Variant_H_

#include <wl/config.h>

#include <string>
#include <sstream>
// #include <boost/make_shared.hpp>
#include <boost/function.hpp>

#include <wl/math.h>
#include <wl/refcounted.h>
#include <wl/log.h>
// #include <wl/script.h>

wlStartNS

class Variant;

class VariantHandler
{
public:
    virtual int compare(const Variant *self, const Variant *other) = 0;
    virtual u32 hash(const Variant *self) = 0;
    virtual void release(Variant *self) = 0;
    virtual void copy_from(Variant *self, const Variant *other) = 0;
    virtual void set_handled(Variant *self, void *pointer) = 0;
    virtual std::string classname(const Variant *self) = 0;
    virtual void set(Variant *self, const Variant *idx,
            const Variant *value) { }
    virtual void get(const Variant *self, const Variant *idx,
            Variant *out_value) { }
};

class Variant : virtual public RefCounted
{
public:
    typedef boost::function<void*(const Variant&)> FunctionT;

protected:
    /* The variant handler. May be NULL */
    VariantHandler *m_handler;

    /* One of DataTypeE */
    s32 m_type;

    /**
     * Data size is used for T_BYTEARRAY
     */
    u32 m_datasize;
        
    union {
        char *s;
        void *ptr;
        int i;
        float f;
        wlBool b;
        RefCounted *obj;
        wlByte *bytearray;
        FunctionT *func;
    } m_value;

public:
    Variant() : m_type(T_NIL) {
        m_value.i = 0;
        m_datasize = 0;
        m_handler = wlNull;
    }

    Variant(const Variant &c) {
        m_handler = wlNull;
        m_type = T_NIL;
        copy_from(c);
    }

    Variant &operator=(const Variant &d) {
        release();
        copy_from(d);
        return *this;
    }

public:
    Variant(const std::string &n) {
        m_type = T_NIL;
        m_handler = wlNull;
        set_string(n);
    }

    Variant(int n) {
        m_type = T_NIL;
        m_handler = wlNull;
        set_int(n);
    }

    Variant(float n) {
        m_type = T_NIL;
        m_handler = wlNull;
        set_float(n);
    }

    Variant(void *ptr) {
        m_type = T_NIL;
        m_handler = wlNull;
        set_pointer(ptr);
    }

    Variant(RefCounted *rf) {
        m_type = T_NIL;
        m_handler = wlNull;
        set_object(rf);
    }

public:
    enum DataTypeE {
        T_NIL = 0,
        T_STRING,
        T_POINTER,
        T_INTEGER,
        T_FLOAT,
        T_BOOL,
        T_OBJECT,
        T_FUNCTION,
        T_BYTEARRAY,
        T_HANDLED
    };

    /* WARNING: red alert, do not use */
    WLINLINE void __setptr(void *ptr) {
        m_value.ptr = ptr;
    }
    WLINLINE void __sethandler(VariantHandler *handler) {
        m_handler = handler;
    }

    virtual ~Variant() {
        release();
    }

    WLINLINE s32 datatype() const {
        return m_type;
    }

    virtual void release();

    virtual int compare(const Variant &v) const;
    virtual u32 hash() const;
    virtual std::string classname() const;

    // virtual void _v_scriptpush(IScript *script);
    // virtual wlBool _v_scriptget(IScript *script, int index);

    wlBool is_nil() const { return m_type == T_NIL; }
    wlBool is_string() const { return m_type == T_STRING; }
    wlBool is_pointer() const { return m_type == T_POINTER; }
    wlBool is_int() const { return m_type == T_INTEGER; }
    wlBool is_float() const { return m_type == T_FLOAT; }
    wlBool is_bool() const { return m_type == T_BOOL; }
    wlBool is_object() const { return m_type == T_OBJECT; }
    wlBool is_function() const { return m_type == T_FUNCTION; }
    wlBool is_bytearray() const { return m_type == T_BYTEARRAY; }
    wlBool is_handled() const { return m_type == T_HANDLED; }

    WLINLINE u32 get_datasize() const {
        return m_datasize;
    }

    /* CONVERTORS */
    virtual std::string to_string() const;
    virtual void *to_pointer() const;
    virtual int to_int() const;
    virtual float to_float() const;
    virtual wlBool to_bool() const;
    virtual RefCounted *to_object() const;
    virtual FunctionT to_function() const;
    virtual wlByte *to_bytearray() const;

    operator std::string() const {
        return to_string();
    }
    operator void *() const {
        return to_pointer();
    }
    operator int() const {
        return to_int();
    }
    operator float() const {
        return to_float();
    }
    operator wlBool() const {
        return to_bool();
    }
    operator RefCounted *() const {
        return to_object();
    }
    operator FunctionT() const {
        return to_function();
    }
    operator wlByte *() const {
        return to_bytearray();
    }

    /* SETTERS */
    virtual void set_nil();
    virtual void set_string(const std::string &c);
    virtual void set_pointer(void *ptr);
    // virtual wlBool _v_setvariant(Variant *c);
    virtual void set_int(int n);
    virtual void set_float(float n);
    virtual void set_bool(wlBool n);
    virtual void set_object(RefCounted *obj);
    virtual void set_function(const FunctionT & p);
    virtual void set_bytearray(wlByte const* array, u32 size);
    virtual void set_handled(void *ptr);

    virtual void copy_from(const Variant &v);

    virtual Variant get(Variant const& idx) const;
    virtual void set(Variant const& idx, const Variant &c);

    // this pops a thing and create a variant
    // static Variant *__vs_create_fromscript(IScript *, int index);
};

template<class T>
class TypedVariantHandler : public VariantHandler
{
public:
    typedef T Type;

    virtual int compare(const Variant *self, const Variant *other) {
        return 1;
    }

    virtual u32 hash(const Variant *self) {
        return 0;
    }

    virtual void release(Variant *self) {
        delete (Type*)self->to_pointer();
    }

    virtual void copy_from(Variant *self, const Variant *other) {
        set_handled(self, other->to_pointer());
    }

    virtual void set_handled(Variant *self, void *pointer) {
        Type *t = new Type(*(Type*)pointer);
        self->__setptr(t);
    }

    virtual std::string classname(const Variant *self) {
        return "TypedVariant<?>";
    }

};

template<class T>
Variant TypedVariant(const T &c) {
    Variant v;
    v.__sethandler(new TypedVariantHandler<T>());
    v.set_handled(new T(c));
    return v;
}

#if 0
template<class T>
class TypedVariant : public Variant
{
public:
    typedef T ObjT;
    typedef TypedVariant<ObjT> ThisT;
    typedef boost::shared_ptr<ThisT> ThisTS;

    class Handler : public VariantHandler {
        virtual int compare(const Variant *self, const Variant *other) {
            return 0;
        }
        virtual void release(Variant *self) {
        }
        virtual void copy_from(Variant *self, const Variant *other) {
        }
        virtual void set_pointer(Variant *self, void *pointer) {
        }
    };

protected:
    ObjT m_v_obj;

    TypedVariant() {
        m_type = T_POINTER;
        m_value.ptr = &m_v_obj;
    }

    template<typename U>
    friend boost::shared_ptr<U> boost::make_shared();

public:
    static ThisTS make(const T &c) {
        ThisTS v = boost::make_shared<ThisT>();
        v->m_v_obj = c;
        return v;
    }

    static ThisTS make() {
        return boost::make_shared<ThisT>();
    }

    virtual void release() {
        if (m_type == T_POINTER)
            m_v_obj = ObjT();

        Variant::release();
    }

    void assign(const T &c) {
        release();
        m_v_obj = c;
        m_type = T_POINTER;
        m_value.ptr = &m_v_obj;
    }

    operator ObjT() const {
        return m_v_obj;
    }

    virtual std::string classname() const {
        return "WL.TypedVariant<?>";
    }

    ObjT value() const {
        return m_v_obj;
    }

};
#endif

/*
class RefVariant : public Variant
{
protected:
    RefVariant(int data_type, void *ptr) {
        _v_datatype = data_type;
        _v_value.ptr = ptr;
    } 

public:
    virtual ~RefVariant();

    WLINLINE static RefVariant *create_string(std::string *n) {
        return new RefVariant(T_STRING, (void*)n);
    }
    WLINLINE static RefVariant *create_int(::WL::s32 *n) {
        return new RefVariant(T_INTEGER, (void*)n);
    }
    WLINLINE static RefVariant *create_float(float *n) {
        return new RefVariant(T_FLOAT, (void*)n);
    }
    template<class T>
    WLINLINE static RefVariant *create_object(T *obj) {
        WL::VariantBinding<T> binding();
        return wlNull;
    }
    WLINLINE static RefVariant *create_ptr(void *ptr) {
        return new RefVariant(T_POINTER, ptr);
    }

    // virtual void _v_scriptpush(IScript *script);

    virtual std::string _v_to_string() const;
    virtual void *_v_to_ptr() const;
    virtual int _v_to_int() const;
    virtual float _v_to_float() const;
    virtual wlBool _v_to_bool() const;
    virtual RefCounted *_v_to_object() const;
    virtual ClosureProc _v_to_closure() const;

    virtual void _v_setnil();
    virtual void _v_setstring(const std::string &c);
    virtual void _v_setptr(void *ptr);
    virtual void _v_setint(int n);
    virtual void _v_setfloat(float n);
    virtual void _v_setbool(wlBool n);
    virtual void _v_setobject(RefCounted *obj);
    virtual void _v_setclosure(ClosureProc p);

};

/ *
template< >
WLINLINE void IScript::push_t<RefVariant>(RefVariant &c) {
    c._v_scriptpush(this);
}
*/

wlEndNS
#endif


