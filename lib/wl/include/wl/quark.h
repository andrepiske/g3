#ifndef _WL_Quark_H_
#define _WL_Quark_H_

#include <wl/config.h>

wlStartNS

typedef u64 Quark;

Quark quark_from_string(const char *s);

wlEndNS

#endif


