#include "precompiled.h"
#include <GL/gl.h>
#include <FreeImage.h>
#include <string>
#include <memory.h>

#include <wl/glstate.h>
#include <wl/image.h>
#include "image_type.h"

wlStartNS

///////////////////////////////////////////////////////////////////////////////
// CImage
///////////////////////////////////////////////////////////////////////////////

void CImage::draw_quad(const Vec2 &o, const Vec2 &d) {
    GLEnable tex2d(GL_TEXTURE_2D);
    GLBindTexture2D tex_bind(get_glbind());
    
    glBegin(GL_QUADS);

    glTexCoord2f(0.f, 1.f);
    glVertex2f(o.x, o.y + d.y);

    glTexCoord2f(1.f, 1.f);
    glVertex2f(o.x + d.x, o.y + d.y);

    glTexCoord2f(1.f, 0.f);
    glVertex2f(o.x + d.x, o.y);

    glTexCoord2f(0.f, 0.f);
    glVertex2f(o.x, o.y);
    glEnd();
}

GLuint CImage::get_glbind() {
    return m_glid;
}

void CImage::set_gl_hints() {
    GLBindTexture2D tex_bind(m_glid);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);
}

///////////////////////////////////////////////////////////////////////////////
// CResourceImage
///////////////////////////////////////////////////////////////////////////////

unsigned cres_img_readproc(void *buffer, unsigned size,
    unsigned count, void* handle) {
    ResourceIOBlock *blk = (ResourceIOBlock*) handle;
    return (unsigned) (blk->file()->read(buffer, size*count) / (long)size);
}

int cres_img_seekproc(void *handle, long offset, int origin) {
    ResourceIOBlock *blk = (ResourceIOBlock*)handle;
    int orig = (origin==SEEK_SET?IFileIO::SK_START:(
        origin==SEEK_CUR?IFileIO::SK_CUR : IFileIO::SK_END));
    return blk->file()->seek(offset, orig) ? 0 : -1;
}

long cres_img_tellproc(void* handle) {
    ResourceIOBlock *blk = (ResourceIOBlock*)handle;
    return (long) blk->file()->tell();
}

wlBool CResourceImage::load_image(wlBool load_gl) {
    FreeImageIO io;
    FIBITMAP *bmp;

    if (!m_resblock.valid())
        return wlFalse;
    m_resblock.file()->seek(0, IFileIO::SK_START);
    
    io.read_proc = (FI_ReadProc)cres_img_readproc;
    io.write_proc = (FI_WriteProc)wlNull;
    io.seek_proc = (FI_SeekProc)cres_img_seekproc;
    io.tell_proc = (FI_TellProc)cres_img_tellproc;

    bmp = FreeImage_LoadFromHandle(FIF_PNG, &io, (fi_handle)&m_resblock,
        PNG_DEFAULT);

    if (!bmp)
        return wlFalse;
    if (load_gl)
        load_gl_image(bmp);

    FreeImage_Unload(bmp);
    return wlTrue;
}

GLuint CResourceImage::get_glbind() {
    if (m_glid == GL_INVALID_VALUE)
        load_image(wlTrue);

    return CImage::get_glbind();
}

void CResourceImage::load_gl_image(FIBITMAP *bitmap) {
    glGenTextures(1, &m_glid);
    
    m_width = FreeImage_GetWidth(bitmap);
    m_height = FreeImage_GetHeight(bitmap);

    const int n_pixels = m_width * m_height; 
    FIBITMAP *tmp_bmp = FreeImage_ConvertTo32Bits(bitmap);

    wlByte *buffer = new wlByte[4 * n_pixels];
    wlByte *_buf = buffer;
    
    for (int _y = m_height-1; _y>=0; --_y) {
        // const int y = m_height - _y;
        wlByte *scan = (wlByte*)FreeImage_GetScanLine(tmp_bmp, _y);
        for (int x=0; x < m_width; ++x) {
            _buf[0] = scan[2];
            _buf[1] = scan[1];
            _buf[2] = scan[0];
            _buf[3] = scan[3];
            _buf += 4;
            scan += 4;
        }
    }
    FreeImage_Unload(tmp_bmp);

    GLBindTexture2D tex_bind(m_glid);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0,
        GL_RGBA, GL_UNSIGNED_BYTE, buffer);

    set_gl_hints();

    delete [] buffer; 
}

void CResourceImage::load_data() {
}

void CResourceImage::unload_data() {
}

///////////////////////////////////////////////////////////////////////////////
// CBufferedImage
///////////////////////////////////////////////////////////////////////////////

CBufferedImage::CBufferedImage() {
    m_buffer = wlNull;
    m_rowsize = 0;
    m_pixelformat = -1;
}


int CBufferedImage::_pixelsize(int pf) {
    switch (pf) {
        case PF_GRAY:
            return 1;
        case PF_RGB:
            return 3;
        default:
            return 4;
    }
}

int CBufferedImage::_gl_pixelsize(int pf) {
    switch (pf) {
        case PF_GRAY:
            return 1;
        case PF_RGB:    
            return 3;
        default:
            return 4;
    }
}

int CBufferedImage::_gl_pixelformat(int pf) {
    switch (pf) {
        case PF_GRAY:
            return GL_LUMINANCE;
            // return GL_RGBA; // TODO: alpha
        case PF_RGB:
            return GL_RGB;
        default:
            return GL_RGBA;
    }
}

wlBool CBufferedImage::get_row(wlByte **bb, int row) {
    *bb = &m_buffer[row * m_rowsize];
    return 1;
}

void CBufferedImage::update_from_buffer() {
    if (m_glid == GL_INVALID_VALUE)
        return;

    glBindTexture(GL_TEXTURE_2D, m_glid);
    glTexImage2D(GL_TEXTURE_2D, 0, _gl_pixelsize(m_pixelformat),
            m_width, m_height, 0, _gl_pixelformat(m_pixelformat),
            GL_UNSIGNED_BYTE, m_buffer);

    set_gl_hints();
}

void CBufferedImage::internal_create_blank(int w, int h, int pf) {
    m_width = w;
    m_height = h;
    m_pixelformat = pf;

    const int pixel_size = _pixelsize(pf);
    m_rowsize = m_width * pixel_size;
    m_rowsize += (4 - (m_rowsize % 4)) % 4; 

    m_buffer = new wlByte[m_rowsize * m_height];
}

void CBufferedImage::load_buffer(const void *_buf, int w, int h, int pf, int rs) {
    m_width = w;
    m_height = h;
    m_pixelformat = pf;

    int pixel_size = _pixelsize(pf);
    int dst_row_size = m_width * pixel_size;

    // align 4 bytes
    dst_row_size += (4 - (dst_row_size % 4)) % 4; 
    m_rowsize = dst_row_size;

    m_buffer = new wlByte[dst_row_size * m_height];
    
    const wlByte *src = (wlByte*)_buf;
    for (int y=0; y < m_height; ++y) {
        if (pf == PF_GRAY) {
            wlByte *row_buffer = &m_buffer[y * dst_row_size];
            const wlByte *src_buffer = &src[y * rs];
            for (int x=0; x < m_width; ++x) {
                row_buffer[x] = src_buffer[x];
            }
        } else 
            memcpy(&m_buffer[y * dst_row_size], (void*)&src[y * rs],
                    dst_row_size);
    }
}

    
GLuint CBufferedImage::get_glbind() {
    if (m_glid == GL_INVALID_VALUE) {
        glGenTextures(1, &m_glid);
        /* WL_LOG_INFO("TexImage2D size=(%d,%d)",
                m_width, m_height); */
        update_from_buffer();
    }
    return CImage::get_glbind();
}

CBufferedImage::~CBufferedImage() {
    if (m_buffer)
        delete [] m_buffer;
}


///////////////////////////////////////////////////////////////////////////////
// IImage
///////////////////////////////////////////////////////////////////////////////

IImage *IImage::create_blank(int width, int height, int pf) {
    CBufferedImage *img = new CBufferedImage();
    img->internal_create_blank(width, height, pf);
    return img;
}

IImage *IImage::create_from_buffer(const void *buffer,
    int width, int height, int pixel_format, int row_size) {
    CBufferedImage *image = new CBufferedImage();
    image->load_buffer(buffer, width, height, pixel_format, row_size);
    return image;
}

IImage *IImage::create_resource(ResourceIOBlock blk) {
    CResourceImage *image = new CResourceImage();
    image->m_resblock = blk;
    if (!image->load_image(wlFalse)) {
        image->rc_pop();
        image = NULL;
        WL_LOG_WARN("Could not load an image", 0);
    }

    return image; 
}

wlEndNS

