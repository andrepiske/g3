#ifndef _WL_Color_H_
#define _WL_Color_H_

#include <wl/config.h>
wlStartNS

class Color
{
public:
    // yeah, it's RGBA
    wlByte m_color[4];

    Color() {
        m_color[3] = 0xFF;
    }

    Color(wlByte r, wlByte g, wlByte b) {
        m_color[0] = r;
        m_color[1] = g;
        m_color[2] = b;
        m_color[3] = 0xFF;
    }

    Color(wlByte r, wlByte g, wlByte b, wlByte a) {
        m_color[0] = r;
        m_color[1] = g;
        m_color[2] = b;
        m_color[3] = a;
    }

    Color &operator=(const Color &b) {
        m_color[0] = b.m_color[0];
        m_color[1] = b.m_color[1];
        m_color[2] = b.m_color[2];
        m_color[3] = b.m_color[3];
        return *this;
    }

    void set(const Color &c) {
        this->operator=(c);
    }

    bool operator==(const Color &c) const {
        return c.m_color == m_color;
    }

    /* call glColor setting RGBA */
    void gl_set() const;

};

wlEndNS
#endif

