#include <wl/quark.h>
#include <map>
#include <string>
#include <boost/thread.hpp>

wlStartNS
// TODO: multithread

typedef std::map<std::string, Quark> QuarkMap;

static QuarkMap g_quarks;
static Quark g_quark_c = 1000;
static boost::shared_mutex g_quark_mutex;

Quark quark_from_string(const char *s) {
	if (!s)
		return 0;
	QuarkMap::iterator it;

	g_quark_mutex.lock_shared();
	it = g_quarks.find(s);
	g_quark_mutex.unlock();

	if (it == g_quarks.end()) {
		g_quark_mutex.lock_upgrade();

		const Quark q = g_quark_c;
		g_quark_c++;
		g_quarks[s] = q;

		g_quark_mutex.unlock();
		return q;
	}
	return it->second;
}

wlEndNS

