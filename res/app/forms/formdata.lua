require('class')

FormData = Class(function(S, fields_def)

    S.fields = fields_def
    S.data = {}

    function S:get_value(field_name)
        local value = self.data[field_name]
        if value == nil then
            return self:get_default_value(field_name)
        end
        return value
    end

    function S:get_default_value(field_name)
        return ''
    end

    function S:set_value(field_name, value)
        -- TODO check if field exists and validate data
        self.data[field_name] = value
    end
    
end)

return FormData

