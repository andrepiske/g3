#ifndef _WL_Scene_H_
#define _WL_Scene_H_

#include <wl/config.h>
#include <wl/util.h>
#include <wl/math.h>

wlStartNS
class IScreen;

class IScene
{
public:
    enum scene_state {
        state_unloaded = 0,
        state_paused = 1,
        state_onscreen = 2
    };

    virtual ~IScene() {} 

    virtual util::bounds<float> get_bounds() const = 0;
    virtual void transition(int state, IScreen*) = 0;
    virtual void draw(IScreen*) = 0;

    virtual void update(IScreen*) = 0;

    // virtual void handle_sdl_event(SDL_Event &ev) = 0;
    virtual void mouse_motion(const Vec2 &w) {}

    static IScene *load_script_scene();


};
   
wlEndNS
#endif

