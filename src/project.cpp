#include "session.h"
#include <string>
#include <boost/filesystemh.hpp>
#include "var.h"

namespace G3 {

class CProject : public IProject
{
public:
    std::string m_project_path;
    WL::RCBucket<IVar> m_options;

public:
    CProject() {
        m_options = IVar::new_table();
    }

    virtual ~CProject() {
    }

    virtual void store_project() {
    }

};



} // NS G3

