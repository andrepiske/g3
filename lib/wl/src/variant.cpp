#include <wl/variant.h>
#include <wl/script.h>
#include <memory.h>
#include <boost/bind.hpp>
#include <wl/util.h>

wlStartNS

void Variant::release() {
    switch (m_type) {
        case T_STRING:
            delete [] m_value.s;
            break;
        case T_OBJECT:
            m_value.obj->rc_pop();
            break;
        case T_BYTEARRAY:
            delete [] m_value.bytearray;
            break;
        case T_HANDLED:
            m_handler->release(this);
            break;
        case T_FUNCTION:
            delete m_value.func;
            break;
    }
    m_type = T_NIL;
}

int Variant::compare(const Variant &v) const {
    if (v.m_type != m_type)
        return 1;

    switch (m_type) {
        case T_HANDLED:
            return m_handler->compare(this, &v);
        case T_NIL:
            return 0;
        case T_STRING:
            return strcmp(v.m_value.s, m_value.s);
        case T_POINTER: {
            WL_LOG_ERROR("Cannot compare T_pointers", 0);
            break;
        }
        case T_INTEGER:
            return m_value.i - v.m_value.i;
        case T_FLOAT:
            return (int)(m_value.f - v.m_value.f);
        case T_BOOL:
            return m_value.b != v.m_value.b;
        case T_FUNCTION:
            WL_LOG_WARN("Should not compare functions, they'll always differ", 0);
            return 1;
            // return (*m_value.func) == (*v->m_value.func);
        case T_OBJECT: {
            WL_LOG_ERROR("Cannot compare T_objects", 0);
            return 1;
        }
        case T_BYTEARRAY: {
            if (v.m_datasize != m_datasize)
                return m_datasize - v.m_datasize;
            return memcmp(v.m_value.bytearray, m_value.bytearray, m_datasize);
        }
    }
    WL_LOG_ERROR("Unknown variant type (compare)", 0);
    return 1; // won't reach here
}

u32 Variant::hash() const {
    switch (m_type) {
        case T_OBJECT:
        case T_FUNCTION:
        case T_POINTER:
        case T_NIL:
            return 0;
        case T_HANDLED:
            return m_handler->hash(this);
        case T_BYTEARRAY:
        case T_STRING:
            return (u32)util::djb2_string_hash(m_value.s, strlen(m_value.s));
        case T_INTEGER:
            return (u32)m_value.i;
        case T_FLOAT:
            return (u32)m_value.f;
        case T_BOOL:
            return (u32)(m_value.b ? wlTrue : wlFalse);
    }
    WL_LOG_ERROR("Unknown variant type (hash)", 0);
    return 1; // won't reach here
}

std::string Variant::classname() const {
    switch (m_type) {
        case T_NIL:
            return "nil";
        case T_STRING:
            return "string";
        case T_POINTER:
            return "<pointer>";
        case T_INTEGER:
            return "integer";
        case T_FLOAT:
            return "float";
        case T_BOOL:
            return "boolean";
        case T_OBJECT:
            return "<object>";
        case T_FUNCTION:
            return "<function>";
        case T_BYTEARRAY:
            return "<bytearray>";
        case T_HANDLED:
            return m_handler->classname(this);
    }
    return "<unknown>";
}

#if 0
void Variant::_v_scriptpush(IScript *s) {
    switch (_v_datatype) {
        case T_NIL:
            s->push_nil();
            break;
        case T_STRING: 
            s->push_string(_v_to_string());
            break;
        case T_POINTER: {
            u32 *_v = (u32*)s->new_userdata(sizeof(u32));
            *_v = (u32)_v_to_ptr();
            break;
        }
        case T_INTEGER:
            s->push_int(_v_to_int());
            break;
        case T_FLOAT:
            s->push_double((double)_v_to_float());
            break;
        case T_BOOL:
            s->push_bool(_v_to_bool());
            break;
        case T_OBJECT:
            WL_LOG_ERROR("How do we push an object?", 0);
            break;
        case T_CLOSURE:
            s->push_cclosure(_v_to_closure(), 0);
            break;
        default:
            WL_LOG_ERROR("How did we even reach here?", 0);
    }
    /*
    const int _wl_index = LUA_REGISTRYINDEX;
    void *data = s->new_userdata(sizeof(u32));
    if (!s->getfield_safe(_wl_index, "WL_VARMT")) {
        s->new_table();
        // s->push_cclosure(&_some_cb, 0);
        s->push_bfunction(boost::bind(&Variant::__v_cb_index, this, _1), 0);
        s->setfield(-2, "__index");
        // s->push_cclosure(&_some_set_cb, 0);
        s->push_bfunction(boost::bind(&Variant::__v_cb_newindex, this, _1), 0);
        s->setfield(-2, "__newindex");
        s->push_bool(wlTrue);
        s->setfield(-2, "___wl_variant");
        s->push_copy(-1);
        s->setfield(_wl_index, "WL_VARMT");
    }
    *((u32*)data) = (u32)this;
    s->setmetatable(-2);
    */
}

int Variant::__v_cb_newindex(IScript *s) {
    return 0;
    /*
    Variant *_value = Variant::__vs_create_fromscript(s, -1);
    Variant *_key = Variant::__vs_create_fromscript(s, -1);
    s->pop(1); // pop the userdata variant
    if (s->stack_size() != 0)
        WL_LOG_ERROR("Stack fcked up: %d", s->stack_size());
    this->_v_set(*_key, _value);
    if (_value)
        _value->rc_pop();
    _key->rc_pop();
    */
    return 0;
}

int Variant::__v_cb_index(IScript *s) {
    return 0;
    /*
    Variant *v = Variant::__vs_create_fromscript(s, -1);
    s->pop(1);
    Variant result = this->_v_get(*v);

    v->rc_pop();
    if (result._v_isnil())
        s->push_nil();
    else {
        result._v_scriptpush(s);
    }
    return 1;
    */
}

wlBool Variant::_v_scriptget(IScript *s, int index) {
    return wlFalse;
    /*
    int lua_type = s->luatype(index);
    _v_release();
    switch (lua_type) {
        case LUA_TNIL: {
            s->remove(index);
            _v_setnil();
            break;
        }
        case LUA_TNUMBER: {
            // TODO: some way to differ INTEGER and DOUBLE
            double _f = s->to_double(index);
            s->remove(index);
            _v_setfloat((float)_f);
            break;
        }
        case LUA_TBOOLEAN: {
            wlBool _f = s->to_bool(index);
            s->remove(index);
            _v_setbool(_f);
            break;
        }
        case LUA_TSTRING: {
            std::string _s = s->to_string(index);
            s->remove(index);
            _v_setstring(_s);
            break;
        }
        / *
        case LUA_TUSERDATA: {
            if (s->getmetatable(index)) {
                s->getfield(-1, "___wl_variant");
                if (s->to_bool(-1)) {
                    s->pop(1);
                    Variant *_v = (Variant*) s->to_userdata(index);
                    _v->rc_push();
                    s->remove(index);
                    return _v;
                }
            }
            // s->remove(index);
            WL_LOG_ERROR("Invlaid user data?", 0);
        }
        * /
        default:
            WL_LOG_ERROR("Not implemented variant: %d", lua_type);
            return wlFalse;
    }
    return wlTrue;
    */
}
#endif

#if 0
Variant *Variant::__vs_create_fromscript(IScript *s, int index) {
    return wlNull;
    /*
    Variant *_v = new Variant();
    if (!_v->_v_scriptget(s, index)) {
        _v->rc_pop();
        return wlNull;
    }
    return _v;
    */
}
#endif

std::string Variant::to_string() const {
    switch (m_type) {
        case T_STRING:
            return m_value.s;
        case T_FLOAT: {
            std::stringstream _s;
            _s << m_value.f;
            return _s.str();
        }
        case T_INTEGER: {
            std::stringstream _s;
            _s << m_value.i;
            return _s.str();
        }
    }
    return "";
}

void* Variant::to_pointer() const {
    if (m_type != T_POINTER && m_type != T_HANDLED)
        return wlNull;

    return m_value.ptr;
}

int Variant::to_int() const {
    switch (m_type) {
        case T_STRING: {
            std::stringstream _s(m_value.s);
            int k;
            _s >> k;
            return k;
        }
        case T_FLOAT:
           return (int)m_value.f;
        case T_INTEGER:
            return m_value.i;
    }
    return 0;
}

float Variant::to_float() const {
    switch (m_type) {
        case T_STRING: {
            std::stringstream _s(m_value.s);
            float k;
            _s >> k;
            return k;
        }
        case T_FLOAT:
           return m_value.f;
        case T_INTEGER:
            return (float)m_value.i;
    }
    return NAN;
}

wlBool Variant::to_bool() const {
    return (m_type == T_BOOL) & m_value.b;
}

RefCounted *Variant::to_object() const {
    if (m_type != T_OBJECT /*|| !_v_value.obj*/)
        return wlNull;
    m_value.obj->rc_push();
    return m_value.obj;
}

Variant::FunctionT Variant::to_function() const {
    if (m_type != T_FUNCTION)
        return wlNull;

    return *m_value.func;
}

wlByte *Variant::to_bytearray() const {
    if (m_type != T_BYTEARRAY)
        return wlNull;

    return m_value.bytearray;
}

Variant Variant::get(Variant const& idx) const {
    Variant v;
    if (m_type == T_HANDLED)
        m_handler->get((const Variant*)this, &idx, &v);
    return v;
}

void Variant::set(Variant const& idx, const Variant &c) {
    if (m_type == T_HANDLED)
        m_handler->set(this, &idx, &c);
}

void Variant::copy_from(const Variant &v) {
    // watch out: do not try do make better performance here
    // just not calling _v_to* methods. it will break things.
    // WL_LOG_INFO("now copy %d", v._v_datatype);
    switch (v.m_type) {
        case T_HANDLED:
            release();
            m_handler = v.m_handler;
            m_type = T_HANDLED;
            m_handler->copy_from(this, &v);
            break;
        case T_NIL:
            set_nil();
            break;
        case T_STRING:
            set_string(v.to_string());
            break;
        case T_POINTER:
            set_pointer(v.to_pointer());
            break;
        case T_INTEGER:
            set_int(v.to_int());
            break;
        case T_FLOAT:    
            set_float(v.to_float());
            break;
        case T_BOOL:
            set_bool(v.to_bool());
            break;
        case T_OBJECT: {
            RefCounted *obj = v.to_object();
            set_object(obj);
            obj->rc_pop();
            break;
        }
        case T_FUNCTION:
            set_function(v.to_function());
            break;
        case T_BYTEARRAY:
            set_bytearray(v.to_bytearray(), v.get_datasize());
            break;
        default:
            WL_LOG_ERROR("copy from what?", 0);
    }
}

void Variant::set_nil() {
    release();
    m_type = T_NIL;
}

void Variant::set_string(const std::string &c) {
    release();
    size_t size = c.size() + 1;
    m_type = T_STRING;
    m_value.s = new char[size];
    memcpy(m_value.s, c.c_str(), size);
}

void Variant::set_pointer(void *ptr) {
    release();
    m_type = T_POINTER;
    m_value.ptr = ptr;
}

void Variant::set_int(int n) {
    release();
    m_type = T_INTEGER;
    m_value.i = n;
}

void Variant::set_float(float n) {
    release();
    m_type = T_FLOAT;
    m_value.f = n;
}

void Variant::set_bool(wlBool n) {
    release();
    m_type = T_BOOL;
    m_value.b = n ? wlTrue : wlFalse;
}

void Variant::set_object(RefCounted *obj) {
    release();
    m_type = T_OBJECT;
    m_value.obj = obj;
    obj->rc_push();
}

void Variant::set_function(const Variant::FunctionT &func) {
    release();
    m_type = T_FUNCTION;
    m_value.func = new Variant::FunctionT(func);
}

void Variant::set_bytearray(wlByte const* buffer, u32 size) {
    release();
    m_type = T_BYTEARRAY;
    m_value.bytearray = new wlByte[size];
    memcpy(m_value.bytearray, buffer, size);
    m_datasize = size;
}

void Variant::set_handled(void *ptr) {
    release();
    m_type = T_HANDLED;
    m_handler->set_handled(this, ptr);
}

///////////////////////////////////////////////////////////////////////////////
// RefVariant
///////////////////////////////////////////////////////////////////////////////

#if 0

RefVariant::~RefVariant() {
    // Variant::~Variant should not destroy anything here
    if (_v_datatype == T_OBJECT)
        (*(RefCounted**)_v_value.ptr)->rc_pop();
    _v_datatype = T_NIL;
}

std::string RefVariant::_v_to_string() const {
    switch (_v_datatype) {
        case T_STRING:
            return _v_value.s;
        case T_FLOAT: {
            std::stringstream _s;
            _s << *(float*)_v_value.ptr;
            return _s.str();
        }
        case T_INTEGER: {
            std::stringstream _s;
            _s << *(::WL::s32*)_v_value.ptr;
            return _s.str();
        }
    }
    return "";
}

void *RefVariant::_v_to_ptr() const {
    if (_v_datatype != T_POINTER)
        return wlNull;
    return *(void**)_v_value.ptr;
}

int RefVariant::_v_to_int() const {
    switch (_v_datatype) {
        case T_STRING: {
            std::stringstream _s(*(std::string*)_v_value.ptr);
            int k;
            _s >> k;
            return k;
        }
        case T_INTEGER:
            return *(int*)_v_value.ptr;
        case T_FLOAT:
            return (int)*(float*)_v_value.ptr;
    }
    return 0;
}

float RefVariant::_v_to_float() const {
    switch (_v_datatype) {
        case T_STRING: {
            std::stringstream _s(*(std::string*)_v_value.ptr);
            float k;
            _s >> k;
            return k;
        }
        case T_INTEGER:
            return (float)*(int*)_v_value.ptr;
        case T_FLOAT:
            return *(float*)_v_value.ptr;
    }
    return 0;
}

wlBool RefVariant::_v_to_bool() const {
    return (_v_datatype == T_BOOL) & *(wlBool*)_v_value.ptr;
}

RefCounted *RefVariant::_v_to_object() const {
    if (_v_datatype != T_OBJECT)
        return wlNull;
    RefCounted *obj = *(RefCounted**)_v_value.ptr;
    obj->rc_push();
    return obj;
}

Variant::ClosureProc RefVariant::_v_to_closure() const {
    if (_v_datatype != T_CLOSURE)
        return wlNull;
    return *(Variant::ClosureProc*)_v_value.ptr;
}

void RefVariant::_v_setnil() {
    WL_LOG_ERROR("And now, what?", 0);
}

void RefVariant::_v_setstring(const std::string &c) {
    // FIXME: workaround! yey!
    if (_v_datatype == T_INTEGER) {
        std::stringstream _n(c);
        int k;
        _n >> k;
        _v_setint(k);
        return;
    }
    if (_v_datatype == T_FLOAT) {
        std::stringstream _n(c);
        float k;
        _n >> k;
        _v_setint(k);
        return;
    }
    if (_v_datatype != T_STRING) {
        WL_LOG_WARN("RefVar not set because is not T_STRING", 0);
        return;
    }
    _v_release();
    *(std::string*)_v_value.ptr = c;
}

void RefVariant::_v_setptr(void *ptr) {
    if (_v_datatype != T_POINTER) {
        WL_LOG_WARN("RefVar not set because is not T_POINTER", 0);
        return;
    }
    _v_release();
    *(void**)_v_value.ptr = ptr;
}

void RefVariant::_v_setint(int n) {
    // FIXME: workaround! yey!
    if (_v_datatype == T_FLOAT) {
        _v_setfloat((float)n);
        return;
    }
    if (_v_datatype == T_STRING) {
        std::stringstream _s;
        _s << n;
        _v_setstring(_s.str());
        return;
    }
    if (_v_datatype != T_INTEGER) {
        WL_LOG_WARN("RefVar not set because is not T_INTEGER", 0);
        return;
    }
    _v_release();
    *(::WL::s32*)_v_value.ptr = (::WL::s32)n;
}

void RefVariant::_v_setfloat(float n) {
    // FIXME: workaround! yey!
    if (_v_datatype == T_INTEGER) {
        _v_setint((int)n);
        return;
    }
    if (_v_datatype == T_STRING) {
        std::stringstream _s;
        _s << n;
        _v_setstring(_s.str());
        return;
    }
    if (_v_datatype != T_FLOAT) {
        WL_LOG_WARN("RefVar not set because is not T_FLOAT", 0);
        return;
    }
    _v_release();
    *(float*)_v_value.ptr = n; 
}

void RefVariant::_v_setbool(wlBool n) {
    if (_v_datatype != T_BOOL) {
        WL_LOG_WARN("RefVar not set because is not T_BOOL", 0);
        return;
    }
    _v_release();
    *(wlBool*)_v_value.ptr = n;
}

void RefVariant::_v_setobject(RefCounted *obj) {
    if (_v_datatype != T_OBJECT) {
        WL_LOG_WARN("RefVar not set because is not T_OBJECT", 0);
        return;
    }
    _v_release();
    obj->rc_push();
    *(RefCounted**)_v_value.ptr = obj;
}

void RefVariant::_v_setclosure(Variant::ClosureProc p) {
    if (_v_datatype != T_CLOSURE) {
        WL_LOG_WARN("RefVar not set because is not T_CLOSURE", 0);
        return;
    }
    _v_release();
    *(Variant::ClosureProc*)_v_value.ptr = p;
}

#endif

/*
void RefVariant::_v_scriptpush(IScript *s) {
    u32 *_ptr = (u32*)s->new_userdata(sizeof(u32));
    *_ptr = (u32)this;
    // this is the metatable
    s->new_table();
    s->push_cclosure(&__RefVariant_setV, 0);
    s->setfield(-1, "set");
    s->push_cclosure(&__RefVariant_getV, 0);
    s->setfield(-1, "get");
    // s->push_cclosure(&__RefVariant_call, 0);
    // s->setfield(-1, "__call");
    s->push_copy(-2);
    s->setmetatable(-2);
}
*/

wlEndNS

