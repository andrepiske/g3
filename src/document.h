#ifndef _G3_Document_H_
#define _G3_Document_H_

#include <wl/refcounted.h>
#include <wl/fileio.h>

namespace G3 {

class IDocument : public WL::RefCounted
{
public:

    virtual wlBool store_to(WL::IFileIO *file) = 0;
    virtual wlBool load_from(WL::IFileIO *file) = 0;


    virtual const std::string get_code() const = 0;
    virtual void set_code(const std::string &code) = 0;

    static IDocument *create();
};

} // NS G3

#endif

