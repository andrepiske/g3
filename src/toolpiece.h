#ifndef __G3_TOOLPIECE_H__
#define __G3_TOOLPIECE_H__

#include "piece.h"
#include <string>
#include <wl/scenegraph.h>
#include <wl/math.h>

namespace G3 {

class ITool;
class ICurve;

class IToolPiece : public WL::sg::Node
{
public:
    virtual void move_start(ICurve *curve) = 0;
    virtual void move_step(double step_size) = 0;
    virtual void move_finish() = 0;

    virtual ITool *get_tool() = 0;
    virtual void set_tool(ITool *tool) = 0;

    virtual WL::Vec3 get_position() = 0;
    virtual void set_position(const WL::Vec3 &p) = 0;

    static IToolPiece *create();
};

} // NS G3

#endif

