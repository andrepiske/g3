#ifndef __G3_CUTTER_H__
#define __G3_CUTTER_H__

#include <wl/refcounted.h>
#include <wl/scenegraph.h>
#include "tool.h"

namespace G3 {

class IWorkPiece;
class IToolPiece;

class ICutter : public WL::sg::Node
{
public:
    virtual void apply_cut() = 0;
    virtual void translate_tool(double x, double y, double z) = 0;
    virtual void workpiece_stats1(int *vc, int *ec, int *fc) = 0;

    virtual void set_cutting_quality(double curvedt, double numsteps) = 0;
    virtual void set_display_flags(
            int show_sweep,
            int show_workpiece,
            int show_toolpiece,
            int show_cut) = 0;
    virtual void get_display_flags(
            int *show_sweep,
            int *show_workpiece,
            int *show_toolpiece,
            int *show_cut) = 0;

    virtual void create_raw_block(double width, double height, double depth) = 0;
    virtual void set_tool_state(bool is_on) = 0;
    virtual void move_tool_straight(double x, double y, double z) = 0;

    virtual IWorkPiece *get_workpiece() = 0;
    virtual IToolPiece *get_toolpiece() = 0;

    virtual void set_cutspeed(double cv) = 0;
    
    virtual void dbg_machining_step() = 0;
    virtual void dbg_set_paused(bool paused) = 0;

    virtual ITool *get_tool() = 0;

    static ICutter *create();


};


} // NS G3

#endif

