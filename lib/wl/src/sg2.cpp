#include "precompiled.h"
#include <wl/sg2.h>
#include <wl/log.h>
#include <wl/glstate.h>
#include <wl/color.h>

wlStartNS
namespace sg {


const BBox2 &Node2D::get_bbox() const {
    if (!m_bbox_cache_valid) {
        m_bbox_cache_valid = wlTrue;
        BBox2 this_bbox = calculate_bbox();

        PVector::const_iterator it = m_children.begin();
        for (; it != m_children.end(); ++it) {
            const Node2D *node = static_cast<const Node2D*>(*it);
            BBox2 child_bbox = node->get_bbox();
            // child_bbox.pos += m_bbox_cache.pos;
            // TODO this
            this_bbox.expand_to_fill(child_bbox);
        }

        m_bbox_cache = this_bbox;
    }
    return m_bbox_cache;
}

void Node2D::event_received(Quark signal, RefCounted *param) {
	/*
    Variant v = params.get(Variant("Node2DEvent"));
    if (!v.is_bool() || !v.to_bool())
        return;

    / *
    v = params.get(Variant("Coords2D"));
    if (!v.is_nil()) {
        Vec2 vec = *static_cast<Vec2*>(v.to_pointer());
        if (m_transform) {
            vec = mul44_1(m_transform->get_matrix(),
                Vec3(vec.x, vec.y, 0.f));
        }
        process_event(signal, params, &vec);
    } else {
        process_event(signal, params, wlNull);
    }
    */

    
}

void Node2D::_post_draw(IScreen *screen) {
    /*
    const BBox2 &b = get_bbox();
    Color(0x39, 0xf9, 0x43).gl_set();
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_LINE_LOOP);
    glVertex2f(b.low.x, b.low.y);
    glVertex2f(b.high.x, b.low.y);
    glVertex2f(b.high.x, b.high.y);
    glVertex2f(b.low.x, b.high.y);
    glEnd();
    */

    Node::_post_draw(screen);
}

}
wlEndNS

