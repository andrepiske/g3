#ifndef _WL_Kreator_H_
#define _WL_Kreator_H_

#include <wl/config.h>
#include <wl/quark.h>
#include <wl/refcounted.h>

wlStartNS

class IKreatorRegistry : public RefCounted
{
public:
    virtual RefCounted *create();
};

class Kreator : public RefCounted
{
public:
    static Kreator *default_instance();

    virtual RefCounted *create(Quark type) = 0;

    virtual void register_local(Quark type, IKreatorRegistry *r) = 0;

};

///////////////////////////////////////////////////////////////////////////////

RefCounted *k_create(Quark type);

///////////////////////////////////////////////////////////////////////////////

typedef RefCounted*(*KCreatorCFunction)();

template<class T>
class KLocalRegistry : public IKreatorRegistry
{
public:
    KLocalRegistry() {
        Kreator::default_instance()->register_local(this);
    }

    virtual RefCounted *create() {
        return new T();
    }
};

wlEndNS

#endif

