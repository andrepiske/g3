#include "tool.h"
#include <wl/scriptable.h>

extern void _G3_ITool__luapush(lua_State *L, WL::RefCounted *obj);

namespace G3 {

class CTool : public ITool,
    public WL::scriptable::RegPush<WL::RefCounted>
{
public:
    int m_tip;

    // used for TIP_FLAT_WITH_RADIUS and TIP_CONICAL
    double m_tip_radius;

    double m_tip_angle;

    // tool radius
    double m_tool_radius;

    double m_height_limit;

public:
    CTool() :
        WL::scriptable::RegPush<WL::RefCounted>(this, _G3_ITool__luapush)
    {
        m_tip = TIP_CONICAL;
        m_tip_radius = 1.0;
        m_tip_angle = 0.0;
        m_tool_radius = 1.0;
        m_height_limit = 10.0;
    }

    virtual double get_tip_radius() {
        return m_tip_radius;
    }

    virtual double get_tip_angle() {
        return m_tip_angle;
    }

    virtual double get_tool_radius() {
        return m_tool_radius;
    }

    virtual int get_tip() {
        return m_tip;
    }

    virtual double get_height_limit() {
        return m_height_limit;
    }

    virtual void set_tip(int tip) {
        m_tip = tip;
    }

    virtual void set_tip_radius(double value) {
        m_tip_radius = value;
    }

    virtual void set_tip_angle(double value) {
        m_tip_angle = value;
    }

    virtual void set_tool_radius(double value) {
        m_tool_radius = value;
    }

    virtual void set_height_limit(double value) {
        m_height_limit = value;
    }

};

ITool *ITool::create() {
    return new CTool();
}

} // NS G3


