require('class')

MachiningWindow = Class(function(S)

    function S:get_widget()
        local lay = gtk.Box.new(gtk.ORIENTATION_VERTICAL)
        local crb = gtk.Button.new_with_label("Create raw block")
        local sm = gtk.Button.new_with_label("Start machining")
        local step = gtk.Button.new_with_label("Machining step")
        local pause = gtk.ToggleButton.new_with_label("Pause")

        crb:connect('clicked', function() self:create_raw_block() end)
        sm:connect('clicked', function() self:start_machining() end)
        step:connect('clicked', function() self:machining_step() end)
        pause:connect('toggled', function() self:machining_pause(pause) end)

        lay:pack_start(crb, false, false, 4)
        lay:pack_start(sm, false, false, 4)
        lay:pack_start(step, false, false, 4)
        lay:pack_start(pause, false, false, 4)

        return lay
    end

    function S:machining_pause(bt)
        local ac = bt:get('active')
        local c = g3_appctx:get_cutter()
        c:dbg_set_paused(ac)
    end

    function S:machining_step()
        local c = g3_appctx:get_cutter()
        c:dbg_machining_step()
    end

    function S:start_machining()
        local c = g3_appctx:get_cutter()
        c:set_cutting_quality(0.7, 0.4)

        c:get_tool():set_tool_radius(1)

        local do_plan1 = false
        local do_plan2 = false
        local do_plan3 = true
        
        if do_plan3 then
            c:set_cutspeed(0.1)
            c:set_tool_state(true)
            c:move_tool_straight(2, 0, 0)
        end

        if do_plan1 then
            c:set_cutspeed(0.09)
            c:set_tool_state(false)
            c:move_tool_straight(0, 2.8, 0)
            c:move_tool_straight(-3, 0, 0)

            c:set_cutspeed(0.06)
            c:set_tool_state(true)
            c:move_tool_straight(2, 0, 0)

            c:set_cutspeed(0.09)
            c:set_tool_state(false)
            c:move_tool_straight(-5, 0, 0)
            c:move_tool_straight(0, 2.2, 0)
        end

        if do_plan2 then
            c:set_cutspeed(0.09)
            c:set_tool_state(false)
            c:move_tool_straight(0, 2, 0)
            c:move_tool_straight(3, 0, 0)
            c:set_cutspeed(0.02283)
            c:set_tool_state(true)
            c:move_tool_straight(-6.2, 1, 0)
            c:set_tool_state(false)
            c:move_tool_straight(0, 0, 6)
            c:move_tool_straight(2, 0, 0)
            c:set_tool_state(true)
            c:move_tool_straight(0, -1.5, -10)
            c:set_tool_state(false)
            c:move_tool_straight(2.0)
            c:set_tool_state(true)
            c:set_cutspeed(0.04283)
            c:move_tool_straight(1, 0.3, 3)
            c:set_tool_state(false)
            c:move_tool_straight(-3, -0.3, 5)
        end
    end

    function S:create_raw_block()
        local cutter = g3_appctx:get_cutter()
        cutter:create_raw_block(2, 2, 3)
    end

end)

