#ifndef _WL_Dict_H_
#define _WL_Dict_H_

#include <wl/config.h>
#include <wl/refcounted.h>
#include <wl/quark.h>
#include <boost/unordered_map.hpp>
#include <stdlib.h>
#include <string.h>

wlStartNS

class Dict : public RefCounted
{
private:
	enum DictType {
		DT_STRING = 1,
		DT_U64,
		DT_RC,
		DT_PTR
	};

	struct DictItem {
		DictType type;
		union {
			RefCounted *rc;
			void *ptr;
			u64 v_u64;
		} data;

		DictItem() {
		}

		DictItem(const DictItem &c) : type(c.type), data(c.data) {
			if (c.type == DT_RC)
				data.rc->rc_push();
			else if (c.type == DT_STRING)
				data.ptr = (void*)::strdup((const char*)data.ptr);
		}

		~DictItem() {
			if (type == DT_RC)
				data.rc->rc_pop();
			else if (type == DT_STRING)
				::free(data.ptr);
		}
	};

	typedef boost::unordered_map<Quark, DictItem> DictMap;

	DictMap m_values;

public:
	Dict();
	virtual ~Dict();

	wlBool has_key(Quark index);

	std::string get_string(Quark index);
	Quark get_string_quark(Quark index);
	u64 get_u64(Quark index);
	RefCounted *get_rc(Quark index);
	void* get_ptr(Quark index);

	void set_string(Quark index, const std::string &n);
	void set_set_u64(Quark index, u64 value);
	void set_rc(Quark index, RefCounted *rc);
	void set_rc_nopush(Quark index, RefCounted *rc);
	void set_ptr(Quark index, void *ptr);


};

wlEndNS

#endif

