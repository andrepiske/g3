#include "workpiece.h"
#include "cvmesh.h"

namespace G3 {

class CWorkPiece : public IWorkPiece
{
    WL::RCBucket<ICVMesh> m_mesh;

public:
    CWorkPiece() {
    }

    virtual ICVMesh *get_cvmesh() {
        return m_mesh;
    }

    virtual void set_cvmesh(ICVMesh *mesh) {
        m_mesh = mesh;
    }

    virtual void draw_self(WL::IScreen *u) {
        if (m_mesh.get_ptr())
            m_mesh->draw(u);
    }

};

IWorkPiece *IWorkPiece::create() {
    return new CWorkPiece();
}


} // NS G3

