#ifndef _G3_DrawSurface_H_
#define _G3_DrawSurface_H_

#include <wl/refcounted.h>
#include <gtk/gtk.h>
#include <wl/math.h>

namespace G3 {

class IDrawSurface;

class IDrawGuy : public WL::RefCounted
{
public:
    virtual void draw(IDrawSurface *surface) = 0;
};

class IDrawSurface : public WL::RefCounted
{
public:
    virtual void set_drawing_guy(IDrawGuy *guy) = 0;
    virtual IDrawGuy *get_drawing_guy() = 0;

    virtual void set_widget(GtkWidget *widget) = 0;
    virtual GtkWidget *get_widget() = 0;

    virtual WL::Vec2 get_size() = 0;

public:
    static IDrawSurface *create();

};


};

#endif

