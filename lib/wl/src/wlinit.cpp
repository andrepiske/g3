#include <wl/config.h>
#include <wl/kreator.h>

wlStartNS

void wl_init_lib() {
    // new KLocalRegistry< IAnimTime >();
    // new KLocalRegistry< IAnim >();
    // new KLocalRegistry< IFileIO >();
}

/*
wl_files = [ 'anim.cpp',
    'ar.cpp',
    'color.cpp',
    'drawutil.cpp',
    'event.cpp',
    'fileio.cpp',
    'font.cpp',
    'glstate.cpp',
    'image.cpp',
    # 'input.cpp',
    'log.cpp',
    'math.cpp',
    # 'polybuilder.cpp',
    'poly.cpp',
    'resource.cpp',
    'scene.cpp',
    'sg2.cpp',
    'scenegraph.cpp',
    # 'screen.cpp',
    # 'scriptbind.cpp',
    'script.cpp',
    'sprite.cpp',
    'text.cpp',
    'time.cpp',
    'dict.cpp',
    'util.cpp',
    'kreator.cpp',
    'wlinit.cpp',
    'quark.cpp' ]
*/

wlEndNS

