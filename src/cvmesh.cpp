#include "cvmesh.h"
#include <carve/csg.hpp>
#include <carve/poly.hpp>
#include <carve/mesh.hpp>
#include <vector>

#include <stdio.h>
#include <stdlib.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>


typedef carve::mesh::MeshSet<3> meshset_t;
typedef carve::poly::Polyhedron poly_t;
typedef meshset_t::mesh_t mesh_t;

struct vt_t {
    double x, y, z;
};

static void
tess_vertex(vt_t *v) {
    glVertex3d(v->x, v->y, v->z);
}

static void
tess_end() {
    glEnd();
}

static void
tess_begin(GLenum which) {
    glBegin(which);
}

namespace G3 {

class CCVMesh : public ICVMesh
{
public:
    meshset_t *m_polyms;

public:
    CCVMesh() {
        m_polyms = NULL;
    }

    virtual ~CCVMesh() {
        if (m_polyms)
            delete m_polyms;
    }

    virtual wlBool load_from_off(const std::string &filename) {
        typedef poly_t::vertex_t vertex_t;
        typedef poly_t::face_t face_t;

        FILE *fp;
        fp = fopen(filename.c_str(), "r");
        char off_sig[128];

        if (1 != fscanf(fp, "%s", off_sig) ||
            strcmp("OFF", off_sig))
        {
            WL_LOG_WARN("Read OFF: Invalid off signature", 0);
            return 0;
        }

        int num_vertices, num_faces, num_edges;
        fscanf(fp, "%d %d %d", &num_vertices, &num_faces, &num_edges);

        std::vector<vertex_t> vertices;
        vertices.reserve(num_vertices);
        for (int i = 0; i < num_vertices; ++i) {
            float x, y, z; 
            fscanf(fp, "%f %f %f", &x, &y, &z);
            vertices.push_back(carve::geom::VECTOR((double)x, (double)y,
                (double)z));
        }

        std::vector<face_t> faces;
        faces.reserve(num_faces);
        for (int i = 0; i < num_faces; ++i) {
            int nv;
            fscanf(fp, "%d", &nv);
            std::vector<const vertex_t *> fv;
            fv.reserve(nv);
            for (int j = 0; j < nv; ++j) {
                int vid;
                fscanf(fp, "%d", &vid);
                fv.push_back(&vertices[vid]);
            }
            faces.push_back(face_t(fv));
        }

        fclose(fp);

        poly_t *poly = new poly_t(faces);
        m_polyms = carve::meshFromPolyhedron(poly, -1);
        delete poly;
        return 1;
    }

    virtual void copy_from(ICVMesh *mesh) {
        if (m_polyms)
            delete m_polyms;
        CCVMesh *msh = static_cast<CCVMesh*>(mesh);
        m_polyms = msh->m_polyms->clone();
    }

    virtual void apply_transform(const WL::Mat44 &mat) {
        for (size_t i = 0; i < m_polyms->vertex_storage.size(); ++i) {
            mesh_t::vertex_t::vector_t &v = m_polyms->vertex_storage[i].v;
            const WL::Vec3 w = WL::mul44_1(mat, WL::Vec3(v[0], v[1], v[2]));
            v[0] = w.x;
            v[1] = w.y;
            v[2] = w.z;
        }
        m_polyms->meshes[0]->recalc();
    }

    virtual void build_cube() {
        std::vector<poly_t::face_t::vertex_t> v;
        v.push_back(carve::geom::VECTOR( 1.0,  1.0,  1.0));
        v.push_back(carve::geom::VECTOR(-1.0,  1.0,  1.0));
        v.push_back(carve::geom::VECTOR(-1.0, -1.0,  1.0));
        v.push_back(carve::geom::VECTOR( 1.0, -1.0,  1.0));
        v.push_back(carve::geom::VECTOR( 1.0,  1.0, -1.0));
        v.push_back(carve::geom::VECTOR(-1.0,  1.0, -1.0));
        v.push_back(carve::geom::VECTOR(-1.0, -1.0, -1.0));
        v.push_back(carve::geom::VECTOR( 1.0, -1.0, -1.0));

        std::vector<poly_t::face_t> faces;
        faces.push_back(poly_t::face_t(&v[0], &v[1], &v[2], &v[3]));
        faces.push_back(poly_t::face_t(&v[7], &v[6], &v[5], &v[4]));
        faces.push_back(poly_t::face_t(&v[0], &v[4], &v[5], &v[1]));
        faces.push_back(poly_t::face_t(&v[1], &v[5], &v[6], &v[2]));
        faces.push_back(poly_t::face_t(&v[2], &v[6], &v[7], &v[3]));
        faces.push_back(poly_t::face_t(&v[3], &v[7], &v[4], &v[0]));

        if (m_polyms)
            delete m_polyms;

        poly_t *poly = new poly_t(faces);
        m_polyms = carve::meshFromPolyhedron(poly, -1);
        delete poly;
    }

    virtual void stats1(int *vert_count, int *edge_count,
            int *face_count)
    {
        const mesh_t *const msh = m_polyms->meshes[0];
        if (face_count)
            *face_count = (int)msh->faces.size();
        if (edge_count)
            *edge_count = (int)(msh->closed_edges.size()
                + msh->open_edges.size());
        if (vert_count)
            *vert_count = (int)m_polyms->vertex_storage.size();
    }

    virtual void merge_with(ICVMesh *other) {
        meshset_t *other_ms = ((CCVMesh*)other)->m_polyms;
        static carve::csg::CSG csg;
        meshset_t *un = csg.compute(m_polyms, other_ms,
            carve::csg::CSG::UNION, 0,
            carve::csg::CSG::CLASSIFY_NORMAL);
        delete m_polyms;
        m_polyms = un;
    }

    virtual void apply_cut(ICVMesh *tool) {
        static carve::csg::CSG csg;
        meshset_t *tool_ms = ((CCVMesh*)tool)->m_polyms;

        meshset_t *cut = csg.compute(m_polyms, tool_ms,
            carve::csg::CSG::A_MINUS_B, 0,
            carve::csg::CSG::CLASSIFY_NORMAL);
        delete m_polyms;
        m_polyms = cut;
        /*
        meshset_t *tool_poly = ((CCVMesh*)tool)->m_polyms;
        carve::mesh::MeshSet<3> *res, *ms, *ms_tool;
        ms = carve::meshFromPolyhedron(m_poly, -1);
        ms_tool = carve::meshFromPolyhedron(tool_poly, -1);
        delete m_poly;
        carve::csg::CSG csg;
        res = csg.compute(ms_tool, ms, carve::csg::CSG::B_MINUS_A);
        m_poly = carve::polyhedronFromMesh(res, -1);
        delete res;
        delete ms;
        delete ms_tool;
        / *
        poly_t *tool_poly = ((CCVMesh*)tool)->m_poly;

        poly_t *old_poly = m_poly;
        carve::csg::CSG csg;
        m_poly = csg.compute(tool_poly, m_poly,
            carve::csg::CSG::B_MINUS_A);
        delete old_poly;
        */
    }

    virtual void translate(double x, double y, double z) {
        carve::math::matrix_transformation tr(carve::math::Matrix::TRANS(
            carve::geom::VECTOR(x, y, z)));
        m_polyms->transform(tr);
    }

    virtual void draw_self(WL::IScreen *_u) {
        typedef void(*GLUTessCallback)();

        if (!m_polyms)
            return;

        {
            const GLfloat dif[] = { 0x98/255.f, 0xE0/255.f, 0x7E/255.f, 1.f };
            const GLfloat cdif[] = { 0xDB/255.f, 0x65/255.f, 0x6D/255.f, 1.f };
            glMaterialfv(GL_BACK, GL_DIFFUSE, cdif);
            glMaterialfv(GL_FRONT, GL_DIFFUSE, dif);
        }

        GLUtesselator *tess = gluNewTess();
        gluTessCallback(tess, GLU_TESS_BEGIN, (GLUTessCallback)tess_begin);
        gluTessCallback(tess, GLU_TESS_VERTEX_DATA, (GLUTessCallback)tess_vertex);
        gluTessCallback(tess,  GLU_TESS_END, (GLUTessCallback)tess_end);

        meshset_t *const poly = m_polyms;
        for (meshset_t::face_iter i = poly->faceBegin(); i != poly->faceEnd(); ++i) {
            meshset_t::face_t *f = *i;
            std::vector<vt_t> vc(f->nVertices());

            for (meshset_t::face_t::edge_iter_t e = f->begin(); e != f->end(); ++e) {
                vc[e.idx()].x = e->vert->v.x;
                vc[e.idx()].y = e->vert->v.y;
                vc[e.idx()].z = e->vert->v.z;
            }

            glNormal3dv(f->plane.N.v);

            gluTessBeginPolygon(tess, 0);
            gluTessBeginContour(tess);

            for (size_t j = 0; j != vc.size(); ++j)
                gluTessVertex(tess, (GLdouble *)&vc[j], (GLvoid *)&vc[j]);

            gluTessEndContour(tess);
            gluTessEndPolygon(tess);
        }
        gluDeleteTess(tess);
    }

    virtual void set_meshset(meshset_t *ms) {
        m_polyms = ms; // FIXME memory leak
    }

};

ICVMesh *ICVMesh::create() {
    return new CCVMesh();
}


} // NS G3


