#ifndef __G3_OPENCSGDRAW_H___
#define __G3_OPENCSGDRAW_H__

#include <opencsg.h>
#include <wl/scenegraph.h>

namespace G3 {

class OCSGPrimitive : public OpenCSG::Primitive
{
public:
    OCSGPrimitive(WL::sg::Node *node, OpenCSG::Operation op);

    virtual ~OCSGPrimitive();

    WL::sg::Node *get_node();
    void set_node(WL::sg::Node *node);

    virtual void render();

private:
    WL::RCBucket<WL::sg::Node> m_node;

};


} // NS G3

#endif

