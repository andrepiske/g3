#include <wl/input.h>
#include <wl/screen.h>
#include <wl/scene.h>
#include <SDL.h>

wlStartNS

class CInput : public IInput
{
public:
    IScreen *m_screen;

public:
    CInput(IScreen *screen) {
        m_screen = screen;
    }

    virtual void handle_sdl_event(SDL_Event *ev) {
        IScene *scene = m_screen->get_scene(m_screen->current_scene());
        if (scene) {
            switch (ev->type) {
                case SDL_KEYDOWN:
                case SDL_KEYUP:
                {
                    SDL_KeyboardEvent *e = (SDL_KeyboardEvent*)ev;
                    Variant params = new_variant_map();
                    params.set(Variant("SDLEvent"),
                        TypedVariant<SDL_KeyboardEvent>(*e));

                    emit_event(Variant("Keyboard"), params);
                    break;
                }
                case SDL_MOUSEMOTION: {
                    SDL_MouseMotionEvent *e = (SDL_MouseMotionEvent*)ev;
                    const Vec2 coords = m_screen->scene_to_screen_coords(
                        Point2D(e->x, e->y), scene);
                    const Vec2 vector = m_screen->scene_to_screen_coords(
                        Point2D(e->xrel, e->yrel), scene);

                    Variant params = new_variant_map();
                    params.set(Variant("Coord"),
                            TypedVariant<Vec2>(coords));
                    params.set(Variant("Vector"),
                            TypedVariant<Vec2>(vector));
                    params.set(Variant("SDLEvent"),
                        TypedVariant<SDL_MouseMotionEvent>(*e));

                    emit_event(Variant("Mouse.Motion"), params);
                    break;
                }
                case SDL_MOUSEBUTTONDOWN:
                case SDL_MOUSEBUTTONUP:
                {
                    SDL_MouseButtonEvent *e = (SDL_MouseButtonEvent*)ev; 
                    Variant params = new_variant_map();
                    params.set(Variant("SDLEvent"), Variant((void*)e));

                    emit_event(Variant("Mouse.Button"), params);
                    std::cout << "fire mouse button" << std::endl;
                    break;
                }
            }
        }
    }

    

};

IInput *IInput::create(IScreen *screen) {
    return new CInput(screen);
}

wlEndNS

