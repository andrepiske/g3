#include <wl/uri.h>
#include <pcrecpp.h>

wlStartNS

namespace URI {

wlBool valid(const std::string &s) {
    return extract(s);
}

wlBool extract(const std::string &uri,
        std::string *schema,
        std::string *hier_path,
        std::string *query,
        std::string *frag)
{
    static pcrecpp::RE re("^([-_a-zA-Z]+):([^?#]+)(\\?([^#?]+))?(#([^#?]+))?$");
    pcrecpp::Arg none;
    return re.FullMatch(uri, schema, hier_path, none, query, none, frag);
}

}

wlEndNS

