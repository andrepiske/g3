/* This is a generated file, do not edit it */
#include <boost/unordered_map.hpp>
#include <lua.hpp>
#include <wl/scriptable.h>
#include "script_app.if.h"


static
int _G3_IAppContext__closure(lua_State *L) {
    const int proc_id = lua_tonumber(L, lua_upvalueindex(1));
    G3::IAppContext *obj = *(G3::IAppContext**)luaL_checkudata(L, 1, "WL-G3::IAppContext");
    switch (proc_id) {
        case 1: { // get_gd_object
            int _valid[] = {0};
            ::WL::RefCounted* _v_ret;
            _v_ret = obj->get_gd_object();            
            ::WL::scriptable::push(L, (::WL::RefCounted*)_v_ret);
            return 1;
        }
        case 2: { // set_parameter_int
            int _valid[] = {0,0,0};
            _valid[0] = lua_isstring(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            std::string v_param_name;
            int v_value;
            v_param_name = lua_tostring(L, 2);
            v_value = lua_tonumber(L, 3);
            obj->set_parameter_int(v_param_name, v_value);            
            return 0;
        }
        case 3: { // set_parameter_double
            int _valid[] = {0,0,0};
            _valid[0] = lua_isstring(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            std::string v_param_name;
            double v_value;
            v_param_name = lua_tostring(L, 2);
            v_value = lua_tonumber(L, 3);
            obj->set_parameter_double(v_param_name, v_value);            
            return 0;
        }
        case 4: { // get_parameter_string
            int _valid[] = {0,0};
            _valid[0] = lua_isstring(L, 2);
            std::string _v_ret;
            std::string v_param_name;
            v_param_name = lua_tostring(L, 2);
            _v_ret = obj->get_parameter_string(v_param_name);            
            lua_pushstring(L, _v_ret.c_str());
            return 1;
        }
        case 5: { // has_parameter
            int _valid[] = {0,0};
            _valid[0] = lua_isstring(L, 2);
            int _v_ret;
            std::string v_param_name;
            v_param_name = lua_tostring(L, 2);
            _v_ret = obj->has_parameter(v_param_name);            
            lua_pushboolean(L, _v_ret);
            return 1;
        }
        case 6: { // get_parameter_double
            int _valid[] = {0,0};
            _valid[0] = lua_isstring(L, 2);
            double _v_ret;
            std::string v_param_name;
            v_param_name = lua_tostring(L, 2);
            _v_ret = obj->get_parameter_double(v_param_name);            
            lua_pushnumber(L, _v_ret);
            return 1;
        }
        case 7: { // set_parameter_string
            int _valid[] = {0,0,0};
            _valid[0] = lua_isstring(L, 2);
            _valid[1] = lua_isstring(L, 3);
            std::string v_param_name;
            std::string v_value;
            v_param_name = lua_tostring(L, 2);
            v_value = lua_tostring(L, 3);
            obj->set_parameter_string(v_param_name, v_value);            
            return 0;
        }
        case 8: { // get_cutter
            int _valid[] = {0};
            ::WL::RefCounted* _v_ret;
            _v_ret = obj->get_cutter();            
            ::WL::scriptable::push(L, (::WL::RefCounted*)_v_ret);
            return 1;
        }
        case 9: { // get_parameter_int
            int _valid[] = {0,0};
            _valid[0] = lua_isstring(L, 2);
            int _v_ret;
            std::string v_param_name;
            v_param_name = lua_tostring(L, 2);
            _v_ret = obj->get_parameter_int(v_param_name);            
            lua_pushnumber(L, _v_ret);
            return 1;
        }
    }
    return 0;
}

static
int _G3_IAppContext__gc(lua_State *L) {
    G3::IAppContext *ptr = *(G3::IAppContext**)lua_touserdata(L, 1);
    ptr->rc_pop();
    return 0;
}

void _G3_IAppContext__regMT(lua_State *L) {
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, _G3_IAppContext__gc);
    lua_setfield(L, -2, "__gc");
    lua_pushnumber(L, 1);
    lua_pushcclosure(L, _G3_IAppContext__closure, 1);
    lua_setfield(L, -2, "get_gd_object");
    lua_pushnumber(L, 2);
    lua_pushcclosure(L, _G3_IAppContext__closure, 1);
    lua_setfield(L, -2, "set_parameter_int");
    lua_pushnumber(L, 3);
    lua_pushcclosure(L, _G3_IAppContext__closure, 1);
    lua_setfield(L, -2, "set_parameter_double");
    lua_pushnumber(L, 4);
    lua_pushcclosure(L, _G3_IAppContext__closure, 1);
    lua_setfield(L, -2, "get_parameter_string");
    lua_pushnumber(L, 5);
    lua_pushcclosure(L, _G3_IAppContext__closure, 1);
    lua_setfield(L, -2, "has_parameter");
    lua_pushnumber(L, 6);
    lua_pushcclosure(L, _G3_IAppContext__closure, 1);
    lua_setfield(L, -2, "get_parameter_double");
    lua_pushnumber(L, 7);
    lua_pushcclosure(L, _G3_IAppContext__closure, 1);
    lua_setfield(L, -2, "set_parameter_string");
    lua_pushnumber(L, 8);
    lua_pushcclosure(L, _G3_IAppContext__closure, 1);
    lua_setfield(L, -2, "get_cutter");
    lua_pushnumber(L, 9);
    lua_pushcclosure(L, _G3_IAppContext__closure, 1);
    lua_setfield(L, -2, "get_parameter_int");
}

void _G3_IAppContext__luapush(lua_State *L, WL::RefCounted *_obj) {
    G3::IAppContext *obj = dynamic_cast<G3::IAppContext*>(_obj);
    *(void**)lua_newuserdata(L, sizeof(void*)) = (void*)obj;
    if (luaL_newmetatable(L, "WL-G3::IAppContext"))
        _G3_IAppContext__regMT(L);
    lua_setmetatable(L, -2);
    obj->rc_push();
}


