#ifndef _WL_Event_H_
#define _WL_Event_H_

#include <wl/config.h>
#include <wl/refcounted.h>
#include <wl/quark.h>
#include <wl/dict.h>
#include <boost/function.hpp>
#include <vector>
#include <list>

// for Bag
#include <boost/unordered_map.hpp>

wlStartNS

/* XXX Bag says: Dear Santa, I would appreciate to have my own file */
template<class K>
class Bag
{
public:
    typedef K key_type;
    #if 0
    typedef H hasher;
    typedef P key_equal;
    typedef A allocator_type;
    #endif

    // typedef typename boost::types::iterator_base iterator_base;
    
    typedef typename boost::unordered_map<key_type, u32> map_type;
    #if 0
    typedef typename boost::unordered_map<key_type, u32, hasher,
        key_equal, allocator_type> map_type;
    #endif
    typedef typename map_type::iterator map_iterator_type;
    typedef typename map_type::const_iterator map_iterator_const_type;

    typedef map_iterator_type iterator;
    typedef map_iterator_const_type const_iterator;

protected:
    map_type m_bag;

public:
    Bag() {
    }

    iterator find(key_type const& k) {
        return m_bag.find(k);
    }

    iterator begin() {
        return m_bag.begin();
    }

    iterator end() {
        return m_bag.end();
    }

    void insert(key_type const& k) {
        map_iterator_type t = m_bag.find(k);
        if (t == m_bag.end()) {
            m_bag[k] = (u32)1;
        } else {
            t->second++;
        }
    }

    u32 occurances(key_type const& k) const {
        map_iterator_const_type t = m_bag.find(k);
        if (t == m_bag.end())
            return 0;

        return t->second;
    }

    void remove(key_type const& k) {
        remove(k, 1);
    }

    void remove(key_type const& k, u32 num) {
        map_iterator_type t = m_bag.find(k);
        if (t != m_bag.end()) {
            t->second -= num;
            if (!t->second)
                m_bag.erase(t);
        }
    }

};

class BindEvent;

class ISink
{
public:
    typedef Bag<BindEvent*> BindBagT;

protected:
    BindBagT __event_binds;
    friend class BindEvent;
    
    virtual void __on_unbind(BindEvent *b);
    virtual void __on_bind(BindEvent *b);

public:
    virtual void event_received(Quark signal, RefCounted *param) = 0;

    virtual ~ISink();

};

class IEmitter
{
public:
    typedef Bag<BindEvent*> BindBagT;

protected:
    BindBagT __event_binds;
    friend class BindEvent;

    virtual void __on_unbind(BindEvent *b);
    virtual void __on_bind(BindEvent *b);

public:
    virtual ~IEmitter(); 

    void emit_event(Quark signal, RefCounted *param); 

};

class BindEvent
{
public:
    struct EvBind {
        Quark sign;
        IEmitter *emitter;
        ISink *sink;
    };
    typedef std::list<EvBind> EvBindList;
    EvBindList m_binds;

    WLINLINE void cleanup() {
        EvBindList::iterator it = m_binds.begin();
        for (; it != m_binds.end(); ++it) {
            if (it->emitter)
                it->emitter->__on_unbind(this);
            it->sink->__on_unbind(this);
        }
        m_binds.clear();
    }

    WLINLINE void unbind(Quark sig, IEmitter *emitter, ISink *sink) {
        EvBindList::iterator it = m_binds.begin();
        for (; it != m_binds.end(); ++it) {
            if ((!sig || sig==it->sign) && 
                (!emitter || !it->emitter || it->emitter==emitter) &&
                (!sink || sink == it->sink))
            {
                if (it->emitter)
                    it->emitter->__on_unbind(this);
                it->sink->__on_unbind(this);
                m_binds.erase(it);
            }
        }
    }

    WLINLINE void bind(Quark sig, IEmitter *emitter, ISink *sink) {
        EvBind b;
        b.sign = sig;
        b.emitter = emitter;
        b.sink = sink;
        m_binds.push_back(b);
        emitter->__on_bind(this);
        sink->__on_bind(this);
    }

    ~BindEvent() {
        cleanup();
    }

    WLINLINE void __unbind_emitter(IEmitter *emitter) {
        EvBindList::iterator it = m_binds.begin();
        for (; it != m_binds.end(); ++it) {
            if (it->emitter == emitter) {
                it->sink->__on_unbind(this);
                m_binds.erase(it);
            }
        }
    }

    WLINLINE void __unbind_sink(ISink *sink) {
        EvBindList::iterator it = m_binds.begin();
        for (; it != m_binds.end(); ++it) {
            if (it->sink == sink) {
                if (it->emitter)
                    it->emitter->__on_unbind(this);
                m_binds.erase(it);
            }
        }
    }

    WLINLINE void __event(IEmitter *emitter, Quark signal,
            RefCounted *param) {
        EvBindList::iterator it = m_binds.begin();
        for (; it != m_binds.end(); ++it) {
            if (
                (!it->sign || it->sign == signal)
                &&
                (!it->emitter || emitter == it->emitter))
            {
                it->sink->event_received(signal, param);
            }
        }
    }
    
};

class FunctionSink : virtual public ISink, virtual public RefCounted
{
protected:
    boost::function<void(RefCounted*)> m_function;

public:
    FunctionSink(const boost::function<void(RefCounted*)> &F) : m_function(F) {
    }

    virtual void event_received(Quark signal, RefCounted *param) {
        m_function(param);
    }

    virtual void __on_bind(BindEvent *u) {
        rc_push();
    }

    virtual void __on_unbind(BindEvent *u) {
        rc_pop();
    }

};

wlEndNS

#endif


