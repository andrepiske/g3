#ifndef _WL_PolyBuilder_H_
#define _WL_PolyBuilder_H_

#include <wl/config.h>
#include <wl/poly.h>
#include <wl/math.h>

wlStartNS

class IPolyBuilder
{
public:
    static IPolyBuilder *create();

    virtual void color(const Color &c) = 0;
    virtual void vertex(const Vec2 &v) = 0;
    
    virtual void set_poly(IPoly *poly) = 0;
    virtual IPoly *get_poly() = 0;

};


wlEndNS


#endif


