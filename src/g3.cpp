#include <GL/glew.h>
#include "g3.h"
#include <gtk/gtk.h>
#include <string>
#include <vector>
#include <Scintilla.h>
#include <ScintillaWidget.h>
#include <SciLexer.h>
#include "drawsurface.h"
#include <tegtkgl.h>
#include "document.h" 
#include "g3d.h"
#include <wl/script.h>
#include <wl/scriptable.h>
#include <wl/fileio.h>
#include "var.h"
#include <wl/filesys.h>
#include <unistd.h>
#include <boost/filesystem.hpp>
#include <wl/math.h>
#include "util.h"
#include <sstream>
#include "script_app.if.h"

#include "comm.h"
#include <boost/bind.hpp>

extern void _G3_IAppContext__luapush(lua_State *L, WL::RefCounted *obj);

namespace G3 {

namespace bfs = boost::filesystem;

static IMain *g_IMain_instance = 0;

class CMain : virtual public IMain, virtual public IAppContext,
    virtual public WL::scriptable::RegPush<WL::RefCounted>
{
// UI widgets and related state
public:
    GtkWidget *m_ui_paned;
    GtkWidget *m_ui_toolbar;
    GtkWidget *m_ui_menubar;
    GtkWidget *m_ui_infobar;
    GtkWidget *m_ui_main_window;
    GtkWidget *m_ui_coder; // scintilla view

    // the layout where m_ui_coder lays.
    // also this is passed to main() of script. so the script may
    // do whatever it wants.
    GtkWidget *m_ui_coder_layout;

    int m_ui_waiting_load_session : 1;
    int m_ui_shown : 1;

public:
    WL::RCBucket<IDocument> m_doc;
    IDrawSurface *m_view3; // 3D view

    // the path of the G3 executable file
    bfs::path m_g3_root_path;
    std::string m_session_dirname;

    // graphics draw
    IGd *m_gd;

    WL::IScript *m_user_script;
    WL::IScript *m_app_script;

    ICommServer *m_comm_srv;
    ICutter *m_cutter;
    
public:
    CMain() :
        WL::scriptable::RegPush<WL::RefCounted>(this, _G3_IAppContext__luapush)
    {
        m_view3 = IDrawSurface::create();
        m_doc = IDocument::create();

        m_ui_waiting_load_session = 0;
        m_ui_shown = 0;

        m_view3->set_drawing_guy(m_gd = IGd::create());
        m_user_script = WL::IScript::create_new();

        m_cutter = ICutter::create();
        m_gd->get_rootnode()->add_child(m_cutter); // ->get_workpiece());
        //m_gd->get_rootnode()->add_child(m_cutter->get_toolpiece());

        m_comm_srv = ICommServer::create();
        if (!m_comm_srv->listen(3455)) {
            WL_LOG_ERROR("Could not open communication server", 0);
            exit(1);
        }
        m_comm_srv->set_receive_callback(boost::bind<>(&CMain::on_comm_receive, this, _1));

        m_app_parameters = IVar::new_table();
        m_app_parameters->set_pair("g3.loaded", IVar::new_long(1));
        m_app_script = WL::IScript::create_new();
    }

    virtual IGd *get_gd_object() {
        return m_gd;
    }

    /**
     * Stores the session into the given file
     */
    void store_session(WL::IFileIO *fp) {
        WL::RCBucket<IVar> gv = IVar::new_table();

        // store text code
        gv->set_pair("code_text1", IVar::new_string(
            coder_get_text()
        ));

        gv->set_pair("app_properties", m_app_parameters);

        {
            const WL::Vec3 cam_pos = m_gd->get_camera_pos();
            double alpha, beta;
            m_gd->get_camera_args(alpha, beta);

            WL::RCBucket<IVar> cam = util::var_from_vec3(cam_pos);
            cam->set_pair("alpha", IVar::new_double(alpha));
            cam->set_pair("beta", IVar::new_double(beta));

            gv->set_pair("camera", cam);
        }

        gv->serialize(fp);
    }

    /**
     * Stores the session to the file at
     * <m_session_dirname>/session
    */
    void store_session_to_file() {
        const std::string sessfile = WL::fsys::dir_join(
                m_session_dirname.c_str(), "session", 0);
        WL::RCBucket<WL::IFileIO> f = WL::IFileIO::open_file(
                sessfile, WL::IFileIO::M_WRITE);
        store_session(f());
        WL_LOG_INFO("Stores session to file %s", sessfile.c_str());
    }

    /**
     * Loads the session from the given file
     */
    wlBool load_session(WL::IFileIO *fp) {
        if (!fp)
            return 0;

        WL::RCBucket<IVar> gv = IVar::new_table();

        if (!gv->unserialize(fp))
            return 0;

        IVar *v;
        if ((v = gv->get_pair("code_text1"))) {
            coder_set_text(v->get_string());
        }

        if ((v = gv->get_pair("app_properties"))) {
            m_app_parameters = v;
        }

        if ((v = gv->get_pair("camera"))) {
            WL::Vec3 cam_pos;
            if (!util::vec3_from_var(v, cam_pos))
                return 0;
            IVar *s;
            const double alpha = (s = v->get_pair("alpha")) ?
                s->get_double() : 0.0;
            const double beta = (s = v->get_pair("beta")) ?
                s->get_double() : 0.0;

            m_gd->set_camera_pos(cam_pos);
            m_gd->set_camera_args(alpha, beta);
        }

        return 1;
    }

    /**
     * Parse argc and argv
     */
    void parse_init_arguments(int argc, char **argv) {
        m_g3_root_path = bfs::canonical(argv[0]);
        WL_LOG_INFO("This is %s", m_g3_root_path.c_str());

        for (int i = 1; i < argc; ++i) {
            const std::string arg = argv[i];
            if (arg == "-s") {
                m_session_dirname = argv[++i];
                m_ui_waiting_load_session = 1;
            }
        }
    }

    /*
     * Initialize the program
     */
    virtual void initialize(int argc, char **argv) {

        // initialize user interface
        ui_initialize();

        // parse argc and argv.
        parse_init_arguments(argc, argv);

        // show all GUI 
        //while (gtk_events_pending())
            //gtk_main_iteration();

        // loads or creates a session.
        // when this function returns the variable
        // m_session_dirname is a valid directory.
        initialize_session();

        // initialize script
        initialize_script();

        // run app script
        run_app_script();
    }

    void run_app_script() {
        // TODO make checks
        lua_State *L = m_app_script->get_luastate();
        WL::IFileIO *fp = WL::IFileIO::open_file("res/app/app.lua",
            WL::IFileIO::M_READ | WL::IFileIO::M_TEXT);
        const long fs = fp->size();
        char *buffer = new char[fs];
        fp->read(buffer, fs);
        luaL_loadbuffer(L, buffer, (size_t)fs, "app_chunk_main");
        delete [] buffer;
        lua_call(L, 0, 0);

        m_app_script->getglobal("main");
        WL::scriptable::push(L, this);
        _script_push_lgob_object(m_app_script, m_ui_menubar);
        _script_push_lgob_object(m_app_script, m_ui_toolbar);
        _script_push_lgob_object(m_app_script, m_ui_coder_layout);
        m_app_script->call(4, 0);
    }

    virtual ICutter *get_cutter() {
        printf("cut it half!\n");
        return m_cutter;
    }

    void _script_add_lib_path(WL::IScript *sc, const bfs::path &p,
        wlBool add_to_path = 1, wlBool add_to_cpath = 1)
    {
        sc->getglobal("package");
        const char *c_p[] = { "path", "path", "cpath" };
        const char *c_f[] = { "?.lua;", "?/init.lua;", "?.so;" };
        const wlBool c_i[] = { add_to_path, add_to_cpath };
        for (size_t i = 0; i < 2; ++i) {
            if (!c_i[i])
                continue;
            sc->getfield(-1, c_p[i]);
            std::stringstream path;
            path << (p / c_f[i]).string();
            path << sc->to_string(-1);
            sc->pop(1);
            sc->push_string(path.str());
            sc->setfield(-2, c_p[i]);
        }
        sc->pop(1);
    }

    /**
     * Initialize local libraries (package.path & package.cpath)
     * in a lua script. This is required to use libs such as LGOB
     * and possibly network communications.
     */
    void _script_initialize_local_libs(WL::IScript *sc) {
        _script_add_lib_path(sc, bfs::path(m_session_dirname));
        _script_add_lib_path(sc, m_g3_root_path.parent_path() / "res/lua");
        if (!sc->exec_expr("require('lgob.gtk')", 0)) {
            WL_LOG_WARN("Failed to require lgob.gtk from script code");
            // ui_display_msg("Failed to require lgob.gtk", "Script failed",
                // MSG_ERROR);
            return;
        }
    }

    /**
     * Initialize script engine.
     */
    void initialize_script() {
        _script_initialize_local_libs(m_user_script);
        _script_initialize_local_libs(m_app_script);
        _script_add_lib_path(m_app_script,
            m_g3_root_path.parent_path() / "res/app");
    }

    void initialize_session() {
        if (m_ui_waiting_load_session) {
            m_ui_waiting_load_session = 0;

            if (m_session_dirname[0] != '/') {
                bfs::path p = WL::fsys::dir_current();
                p /= m_session_dirname;
                m_session_dirname = p.native();
            }

            WL::RCBucket<WL::IFileIO> f =
                WL::IFileIO::open_file(WL::fsys::dir_join(
                            m_session_dirname.c_str(), "session", 0), 1);
            if (load_session(f())) {
                WL_LOG_INFO("G3 Session loaded");
            } else {
                m_session_dirname = "";
                WL_LOG_WARN("Failed to load G3 session");
                ui_display_msg("Failed to load session. Aborting.",
                        "Session load", IMain::MSG_ERROR);
                exit(1);
            }
            return;
        }


        GtkWidget *dlg = 
            gtk_message_dialog_new_with_markup(0,
                    GTK_DIALOG_MODAL,
                    GTK_MESSAGE_QUESTION,
                    GTK_BUTTONS_NONE,
                    "<span size=\"larger\" weight=\"heavy\">Welcome to G3</span>.\nYou must have a session loaded to run this software.\n\n"
                    "You can either:\n1. Load an existing session\n2. Create a new session\n\nWhat would it be?");

        gtk_dialog_add_buttons(GTK_DIALOG(dlg),
            "Create new session", 100,
            "Load existing session", 101,
            "Cancel and quit", GTK_RESPONSE_CANCEL, NULL);
            
        const int response = gtk_dialog_run(GTK_DIALOG(dlg));
        if (response == GTK_RESPONSE_CANCEL || response == GTK_RESPONSE_NONE)
            exit(2);

        gtk_widget_destroy(dlg);

        if (response == 100) {
            std::string sess_dir;
            if (!ui_choose_folder("Create new session", "", sess_dir))
                exit(2);

            const std::string sess_file = WL::fsys::dir_join(
                sess_dir.c_str(), "session", 0);
            WL::RCBucket<WL::IFileIO> f =
                WL::IFileIO::open_file(sess_file, WL::IFileIO::M_WRITE);
            if (!f()) {
                ui_display_msg("Could not create session file. Aborting.",
                    "Session creation", IMain::MSG_ERROR);
                exit(1);
            }

            store_session(f());
            m_session_dirname = sess_dir;
            WL_LOG_INFO("Created new session at %s", sess_dir.c_str());
            return;
        }

        if (response == 101) {
            std::string sess_dir;
            if (!ui_choose_folder("Load session", "", sess_dir))
                exit(2);
            
            const std::string sess_file = WL::fsys::dir_join(
                sess_dir.c_str(), "session", 0);
            WL::RCBucket<WL::IFileIO> f =
                WL::IFileIO::open_file(sess_file, WL::IFileIO::M_READ);
            if (!load_session(f())) {
                ui_display_msg("Could not load session. Aborting.",
                    "Load session", IMain::MSG_ERROR);
                exit(1);
            }

            m_session_dirname = sess_dir;
            WL_LOG_INFO("Loaded session from %s", sess_dir.c_str());
            return;
        }

        // wtf?
        WL_LOG_WARN("Response?", 0);
        exit(4);
    }

    /**
     * Run all. This won't return.
     */
    virtual void run() {
        gtk_main();
    }

    /**
     * Push a LGOB object into a lua script stack.
     */
    void _script_push_lgob_object(WL::IScript *sc, void *object) {
        lua_State *L = sc->get_luastate();
        lua_pushliteral(L, "lgobObjectNew");
        lua_rawget(L, LUA_REGISTRYINDEX);
        lua_pushlightuserdata(L, object);
        lua_pushboolean(L, 0);
        sc->call(2, 1);
    }

    /**
     * Run user script.
     */
    virtual void run_script_code(const std::string &t) {
        if (!m_user_script->exec_expr(t, 0)) {
            ui_display_msg("Failed to execute script",
                "Script failed", MSG_ERROR);
            return;
        }

        lua_State *L = m_user_script->get_luastate();
        m_user_script->getglobal("main");
        if (!lua_isnil(L, -1)) {
            WL::scriptable::push(L, m_gd);
            _script_push_lgob_object(m_user_script, m_ui_coder_layout);
            m_user_script->call(2, 0);
        } else {
            ui_display_msg("No main function",
                "Scriptless", MSG_ERROR);
        }
    }

// relative to UI, but not callbacks. (those are below)
public:
    void ui_create_menubar() {
        GtkWidget *bar; // not THAT bar (from the foo one ;-)...
        GtkWidget *menu_file;

        m_ui_menubar = bar = gtk_menu_bar_new();

        menu_file = gtk_menu_item_new_with_label("File");
        gtk_container_add(GTK_CONTAINER(bar), menu_file);
        /*
        const int file_actions[] = {
            // UIACTION_NEW, UIACTION_SAVE, UIACTION_OPEN
        };
        GtkWidget *menu_file_menu = gtk_menu_new();
        gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_file), menu_file_menu);
        for (int i = 0; i < (int)(sizeof(file_actions)/sizeof(int)); ++i) {
            const int ac = file_actions[i];
            gtk_menu_shell_append(GTK_MENU_SHELL(menu_file_menu),
                gtk_action_create_menu_item(m_ui_actions[ac]));
        }
        */
    }

    void ui_create_toolbar() {
        GtkWidget *bar;
        m_ui_toolbar = bar = gtk_toolbar_new();

        const int tool_actions[] = {
            UIACTION_RUN,
            UIACTION_WIREFRAME,
            UIACTION_SHOWNORMALS,
            UIACTION_SHOWGRID
        };
        for (int i = 0; i < (int)(sizeof(tool_actions)/sizeof(int)); ++i) {
            const int ac = tool_actions[i];
            gtk_container_add(GTK_CONTAINER(bar), 
                gtk_action_create_tool_item(m_ui_actions[ac]));
        }
    }

    void ui_create_infobar() {
        GtkWidget *ib = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
        GtkWidget *lb = gtk_label_new("some info here");
        gtk_box_pack_start(GTK_BOX(ib), lb, 0, 0, 0);
        m_ui_infobar = ib;
    }

    // UI Actions
    enum Actions {
        UIACTION_RUN = 0,
        UIACTION_WIREFRAME,
        UIACTION_SHOWNORMALS,
        UIACTION_SHOWGRID,
        /*
        UIACTION_NEW,
        UIACTION_SAVE,
        UIACTION_OPEN,
        */
        UIACTION_LAST
    };
    GtkAction *m_ui_actions[UIACTION_LAST];

    void ui_create_actions() {
        enum Type {
            ttNone = 0,
            ttToggle
        };
        struct A {
            const char *name, *display, *hint, *ic;
            int type;
            GCallback cb;
        } as[] = {
            {"run_script", "_Run", "Run Script", "res/run_icon.png", ttNone, G_CALLBACK(&CMain::ui_on_action_run)},
            {"hwireframe", "_Wireframe", "Wireframe", "res/wireframe_icon.png", ttToggle, G_CALLBACK(&CMain::ui_on_action_wireframe)},
            {"shownormals", "Show _Normals", "Show Normals", "res/shownormals_icon.png", ttToggle, G_CALLBACK(&CMain::ui_on_action_shownormals)},
            {"showgrid", "Show _Grid", "Show Grid", "res/showgrid_icon.png", ttToggle, G_CALLBACK(&CMain::ui_on_action_showgrid)}
        };

        for (int i = 0; i < (int)(sizeof(as)/sizeof(as[0])); ++i) {
            const A &a = as[i];
            GtkAction *ac;
            if (a.type == ttNone)
                ac = gtk_action_new(a.name, a.display, a.hint, NULL);
            else
                ac = (GtkAction*)gtk_toggle_action_new(a.name, a.display, a.hint, NULL);
            m_ui_actions[i] = ac;
            g_signal_connect_swapped(G_OBJECT(ac), "activate", a.cb, this);
            gtk_action_set_gicon(ac, g_file_icon_new(g_file_new_for_path(a.ic)));
        }
    }

    void coder_set_text(const std::string &t) {
        ScintillaObject *sci;
        sci = SCINTILLA(m_ui_coder);
        scintilla_send_message(sci, SCI_SETTEXT, 0, (sptr_t)t.c_str());
    }

    std::string coder_get_text() {
        ScintillaObject *sci;
        size_t textlen;
        sci = SCINTILLA(m_ui_coder);
        textlen = (size_t)scintilla_send_message(sci, SCI_GETTEXTLENGTH, 0, 0);

        char *data = new char[textlen + 1];
        scintilla_send_message(sci, SCI_GETTEXT, (uptr_t)(textlen + 1),
            (sptr_t)data);
        data[textlen] = 0;
        std::string out = data;
        delete []data;
        return out;
    }

    void ui_initialize() {
        m_ui_main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

        ui_create_actions();

        GtkWidget *vbox;
        vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
        gtk_container_add(GTK_CONTAINER(m_ui_main_window), vbox);

        ui_create_menubar();
        ui_create_toolbar();
        ui_create_infobar();

        gtk_box_pack_start(GTK_BOX(vbox), m_ui_menubar, 0, 1, 0);
        gtk_box_pack_start(GTK_BOX(vbox), m_ui_toolbar, 0, 1, 0);
        gtk_box_pack_end(GTK_BOX(vbox), m_ui_infobar, 0, 1, 0);

        g_signal_connect_swapped(G_OBJECT(m_ui_main_window), "delete-event",
                G_CALLBACK(&CMain::ui_on_close_request), this);
        g_signal_connect_swapped(G_OBJECT(m_ui_main_window), "destroy",
                G_CALLBACK(&CMain::ui_on_destroy), this);

        GtkWidget *paned;
        m_ui_paned = paned = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
        gtk_box_pack_start(GTK_BOX(vbox), paned, 1, 1, 0);

        m_ui_coder_layout = gtk_notebook_new();

        Scintilla_LinkLexers();
        m_ui_coder = scintilla_new();
        {
            ScintillaObject *s;
            s = SCINTILLA(m_ui_coder);
            scintilla_set_id(s, 0);
            // scintilla_send_message(s, SCI_SETLEXER, SCLEX_LUA, 0);
        }

        gtk_notebook_append_page(GTK_NOTEBOOK(m_ui_coder_layout), m_ui_coder,
            gtk_label_new("Coder"));
        gtk_paned_pack1(GTK_PANED(paned),
            m_ui_coder_layout, 0, TRUE);

        GtkWidget *gl_wid = te_gtkgl_new();
        m_view3->set_widget(gl_wid);

        gtk_widget_add_events(gl_wid, GDK_BUTTON_PRESS_MASK
            | GDK_BUTTON_RELEASE_MASK
            | GDK_POINTER_MOTION_MASK
            | GDK_KEY_PRESS_MASK
            | GDK_KEY_RELEASE_MASK
            );
        gtk_widget_set_can_focus(gl_wid, 1);

        g_signal_connect_swapped(gl_wid, "button-press-event",
            G_CALLBACK(&CMain::_ui_on_view3_buttonpress), this);
        g_signal_connect_swapped(gl_wid, "button-release-event",
            G_CALLBACK(&CMain::_ui_on_view3_buttonrelease), this);
        g_signal_connect_swapped(gl_wid, "motion-notify-event",
            G_CALLBACK(&CMain::_ui_on_view3_motion), this);
        g_signal_connect_swapped(gl_wid, "key-press-event",
            G_CALLBACK(&CMain::_ui_on_view3_keypress), this);
        g_signal_connect_swapped(gl_wid, "key-release-event",
            G_CALLBACK(&CMain::_ui_on_view3_keyrelease), this);

        gtk_paned_pack2(GTK_PANED(paned),
            gl_wid, 0, TRUE);

        // gtk_widget_set_size_request(gl_wid, 300, 300);
        gtk_paned_set_position(GTK_PANED(paned), 270);

        //g_signal_connect_swapped(G_OBJECT(m_ui_main_window), "show",
                //G_CALLBACK(&CMain::ui_on_show), this);

        gtk_window_set_default_size(GTK_WINDOW(m_ui_main_window),
            900, 500);
        gtk_window_maximize(GTK_WINDOW(m_ui_main_window));
        gtk_widget_show_all(m_ui_main_window);

        te_gtkgl_make_current(TE_GTKGL(gl_wid));
        if (GLEW_OK != glewInit()) {
            WL_LOG_ERROR("Fatal: GLEW could not be started");
        }
    }

    virtual wlBool ui_choose_folder(const std::string &title,
        const std::string &initial_dir, std::string &outdir)
    {
        GtkWidget *fc;
        fc = gtk_file_chooser_dialog_new(title.c_str(),
                GTK_WINDOW(m_ui_main_window),
                GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT, NULL);

        gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(fc),
                initial_dir.c_str());

        const int resp = gtk_dialog_run(GTK_DIALOG(fc));
        if (resp == GTK_RESPONSE_ACCEPT) {
            char *const filename =
                gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(fc));
            outdir = filename;
            g_free(filename);
        }

        gtk_widget_destroy(fc);
        return (resp == GTK_RESPONSE_ACCEPT);
    }

// UI callbacks.
public:
    void ui_on_destroy() {
        gtk_main_quit();
    }

    // main window close request (aka. user clicked "X" button)
    void ui_on_close_request() {
        if (!m_session_dirname.empty())
            store_session_to_file();

        WL_LOG_INFO("G3 exiting");
        gtk_widget_destroy(m_ui_main_window);
    }

    void ui_on_action_shownormals(GtkToggleAction *action) {
        gboolean toggled = gtk_toggle_action_get_active(action);
    }

    void ui_on_action_showgrid(GtkToggleAction *action) {
        gboolean toggled = gtk_toggle_action_get_active(action);
    }

    void ui_on_action_wireframe(GtkToggleAction *action) {
        // gboolean toggled = gtk_toggle_action_get_active(action);
        // _S_G_DrawWireframe = toggled;
        // m_gd->set_draw_wireframe((int)toggled);
    }

    void ui_on_action_run(GtkAction *action) {
        const std::string p = coder_get_text();
        run_script_code(p);
        // initialize_session();
    }

    int ui_on_save_dialog_confirm_overwrite(GtkFileChooser *fc) {
        char *fn = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(fc));
        int r = (fn == m_session_dirname) ?
            GTK_FILE_CHOOSER_CONFIRMATION_ACCEPT_FILENAME :
            GTK_FILE_CHOOSER_CONFIRMATION_CONFIRM;
        g_free(fn);
        return r;
    }

    struct View3Motion {
        int active : 1;
        double delta_x, delta_y;

        GdkEventButton ev_button;

        View3Motion() : active(0) {
        }

        void start(GtkWidget *wid, GdkEvent *ev) {
            memcpy(&ev_button, &ev->button, sizeof(GdkEventButton));
            active = 1;
            delta_x = delta_y = 0.0;
        }

        void motion(GtkWidget *wid, GdkEvent *ev) {
            if (!active)
                return;
            delta_x = -ev_button.x + ev->motion.x;
            delta_y = -ev_button.y + ev->motion.y;

            ev_button.x = ev->motion.x;
            ev_button.y = ev->motion.y;
        }

        void stop(GtkWidget *wid, GdkEvent *ev) {
            active = 0;
            delta_x = delta_y = 0.0;
        }

    } m_view3motion;

    gboolean _ui_on_view3_motion(GdkEvent *ev, GtkWidget *widget) {
        m_view3motion.motion(widget, ev);
        if (m_view3motion.active)
            m_gd->input_motion(m_view3motion.delta_x, m_view3motion.delta_y);
        return true;
    }

    gboolean _ui_on_view3_buttonpress(GdkEvent *ev, GtkWidget *widget) {
        m_view3motion.start(widget, ev);
        if (m_view3motion.active)
            m_gd->input_motion_start();
        gtk_widget_grab_focus(widget);
        return true;
    }

    gboolean _ui_on_view3_buttonrelease(GdkEvent *ev, GtkWidget *widget) {
        int active = m_view3motion.active;
        m_view3motion.stop(widget, ev);
        if (active && !m_view3motion.active)
            m_gd->input_motion_stop();
        return true;
    }

    gboolean _ui_on_view3_keypress(GdkEvent *ev, GtkWidget *widget) {
        using namespace WL;
        const Vec3 cp = m_gd->get_camera_pos();
        const float nav_i = 0.1f;
        Vec3 dir, up;
        m_gd->calculate_camera_vectors(dir, up);
        const Vec3 sdir = dir.cross(up);

        if (ev->key.keyval == (guint)'w') {
            m_gd->set_camera_pos(cp + dir*nav_i);
        } else if (ev->key.keyval == (guint)'s') {
            m_gd->set_camera_pos(cp - dir*nav_i);
        } else if (ev->key.keyval == (guint)'d') {
            m_gd->set_camera_pos(cp + sdir*nav_i);
        } else if (ev->key.keyval == (guint)'a') {
            m_gd->set_camera_pos(cp - sdir*nav_i);
        }
        return true;
    }

    gboolean _ui_on_view3_keyrelease(GdkEvent *ev, GtkWidget *widget) {
        return true;
    }

// UI utils
public:
    GtkWidget *_ui_message(const std::string &msg,
        const std::string &title, MsgType t, int buttons)
    {
        GtkWidget *w;
        w = gtk_message_dialog_new(GTK_WINDOW(m_ui_main_window),
            GTK_DIALOG_DESTROY_WITH_PARENT, 
            t==MSG_INFO?GTK_MESSAGE_INFO:
            (t==MSG_WARNING?GTK_MESSAGE_WARNING:GTK_MESSAGE_ERROR),
            (GtkButtonsType)buttons, NULL);
        g_object_set(G_OBJECT(w), "text", title.c_str(), NULL);
        g_object_set(G_OBJECT(w), "secondary-text", msg.c_str(), NULL);
        return w;
    }


    virtual void ui_display_msg(const std::string &msg,
        const std::string &title, MsgType t)
    {
        GtkWidget *w = _ui_message(msg, title, t, GTK_BUTTONS_OK);
        gtk_dialog_run(GTK_DIALOG(w));
        gtk_widget_destroy(w);
    }

    virtual int ui_ask_yesno(const std::string &msg,
        const std::string &title, MsgType t, wlBool hasCancel)
    {
        GtkWidget *w = _ui_message(msg, title, t,
            GTK_BUTTONS_YES_NO);
        if (hasCancel)
            gtk_dialog_add_buttons(GTK_DIALOG(w),
                "gtk-cancel", GTK_RESPONSE_CANCEL, NULL);

        int r = (int)gtk_dialog_run(GTK_DIALOG(w));
        gtk_widget_destroy(w);
        return r;
    }

///////////////////////////////////////////////////////////////////////////////
///////////// IAppContext implementation
public:
    WL::RCBucket<IVar> m_app_parameters;

public:
    virtual void set_parameter_string(const std::string &param_name,
            const std::string &value)
    {
        m_app_parameters->set_pair(param_name, IVar::new_string(value));
    }

    virtual void set_parameter_int(const std::string &param_name,
        int value)
    {
        m_app_parameters->set_pair(param_name, IVar::new_long((long)value));
    }

    virtual void set_parameter_double(const std::string &param_name,
        double value)
    {
        m_app_parameters->set_pair(param_name, IVar::new_double(value));
    }

    virtual std::string get_parameter_string(const std::string &param_name) {
        IVar *v = m_app_parameters->get_pair(param_name);
        if (v && v->get_type() == IVar::VT_STRING)
            return v->get_string();
        return "";
    }

    virtual int get_parameter_int(const std::string &param_name) {
        IVar *v = m_app_parameters->get_pair(param_name);
        if (v && v->get_type() == IVar::VT_LONG)
            return (int)v->get_long();
        return 0;
    }

    virtual double get_parameter_double(const std::string &param_name) {
        IVar *v = m_app_parameters->get_pair(param_name);
        if (v && v->get_type() == IVar::VT_DOUBLE)
            return v->get_double();
        return 0.0;
    }

    virtual int has_parameter(const std::string &param_name) {
        return (m_app_parameters->get_pair(param_name) != 0);
    }


// remote communication handler
public:
    void on_comm_receive(void *_msg) {
        char *msg = (char*)_msg;
        printf("msg: '%s'\n", msg);
        delete [] msg;
    }

};

IMain *IMain::instance() {
    if (!g_IMain_instance)
        g_IMain_instance = new CMain();
    return g_IMain_instance;
}


};

