#ifndef _WL_Drawutil_H_
#define _WL_Drawutil_H_

#include <wl/config.h>
#include <wl/scenegraph.h>

wlStartNS
namespace du {

enum Corners {
    LEFT = 1,
    RIGHT = 2,
    BOTTOM = 4,
    TOP = 8,
    ALL = 15
};

void draw_rectangle(const Vec2 &pos, const Vec2 &size);
void fill_rectangle(const Vec2 &pos, const Vec2 &size);

void draw_curved_rect(const Vec2 &size, float radius,
        wlBool filled, int round_corners=ALL, int num_steps=10);
wlBool curved_rect_hit_test(const Vec2 &size, float radius,
    int round_corners, const Vec2 &point);


} // ::du
wlEndNS

#endif


