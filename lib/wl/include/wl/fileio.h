#ifndef _WL_FileIO_H_
#define _WL_FileIO_H_

#include <wl/config.h>
#include <wl/refcounted.h>
#include <string>
#include <boost/noncopyable.hpp>

wlStartNS
class IFileIO : private boost::noncopyable, public RefCounted
{
public:
    enum seek_from {
        SK_START = 0,
        SK_CUR,
        SK_END
    };

    enum open_mode {
        M_READ = 1,
        M_WRITE = 1<<1,
        M_TEXT = 1<<2
    };

    virtual ~IFileIO() {}

    virtual long tell() = 0;

    virtual long size() = 0;

    virtual wlBool eof() = 0;

    /**
    * Seek 'bytes' bytes from 'from'.
    * Returns false if failed.
    */
    virtual wlBool seek(long bytes, int from) = 0;

    virtual long read(void *data, long length) = 0;
    virtual long write(const void *data, long length) = 0;

    virtual void force_close() = 0;

    static IFileIO *open_file(const std::string &path, int mode);
};

class IRangedFileIO : public IFileIO
{
public:
    virtual ~IRangedFileIO() {}

    virtual long tell() = 0;
    virtual long size() = 0;
    virtual wlBool eof() = 0;
    virtual wlBool seek(long bytes, int from) = 0;
    virtual long read(void *data, long length) = 0;
    virtual long write(const void *data, long length) = 0;
    virtual void force_close() = 0;

    static IRangedFileIO *create(IFileIO *fileio, long init, long length);
};

class IBufferedFileIO : public IFileIO
{
public:
    virtual ~IBufferedFileIO() {}

    virtual long tell() = 0;
    virtual long size() = 0;
    virtual wlBool eof() = 0;
    virtual wlBool seek(long bytes, int from) = 0;
    virtual long read(void *data, long length) = 0;
    virtual long write(const void *data, long length) = 0;
    virtual void force_close() = 0;

    static IBufferedFileIO *create_from_string(const std::string &text);

    static IBufferedFileIO *create(void *buffer, int size,
            wlBool readonly = wlTrue, wlBool free_on_delete = wlFalse);
};

wlEndNS
#endif


