#ifndef _WL_SceneGraph_H_
#define _WL_SceneGraph_H_

#include <wl/config.h>
#include <wl/math.h>
#include <wl/util.h>
#include <wl/refcounted.h>

#include <vector>
#include <string>
#include <boost/function.hpp>

wlStartNS

class IScreen;

// The SG (SceneGraph) namespace
namespace sg {

class Transform : public RefCounted
{
protected:
    mutable Mat44 m_matrix;

public:
    virtual const Mat44 &get_matrix() const {
        return m_matrix;
    }

    virtual void set_matrix(const Mat44 &matrix) {
        m_matrix = matrix;
    }

};

class Node : public RefCounted
{
public:
    typedef std::vector<Node*> PVector;
    typedef ::WL::util::bounds<float> BoundsT;

/* Those are read-only public. Follow this rule and don't expect problem */
public:
    /* name */
    std::string m_name;

    /* Use as read-only */
    Node *m_parent;

    /* Use this a read-only. Use add_child() or remove_child() methods for
    manipulation.
    */
    PVector m_children;

    wlBool m_attr_valid;

    /* Whether the bounding box of this node is still valid. */
    wlBool m_bbox_valid;

    /* Transform applied on this BEFORE processing (i.e. rendering) */
    Transform *m_transform;

public:
    /* This is expected to be true for debugging purposes only,
    but no restrictions apply.
    */
    wlBool m_drawborders;

public:
    Node() {
        m_parent = wlNull;
        m_transform = wlNull;
    }

    virtual ~Node() {
    }

    WLINLINE void set_transform(Transform *tr) {
        if (tr)
            tr->rc_push();
        if (m_transform)
            m_transform->rc_pop();
        m_transform = tr;
        invalidate_bounding_box();
    }

    virtual void invalidate_bounding_box() {
        if (!m_bbox_valid)
            return;

        m_bbox_valid = wlFalse;
        if (m_parent)
            m_parent->invalidate_bounding_box();
    }

    virtual wlBool has_intersection(const Vec3 &point) const {
        return wlFalse;
    }

    virtual void add_child(Node *node);
    virtual void remove_child(Node *node);

    /**
    * It might be useful sometimes to get the first child.
    */
    WLINLINE Node *get_first_child() const {
        return m_children.empty() ? wlNull : m_children[0];
    }

    PVector::iterator find_child(const std::string &name);
    PVector::iterator find_child(Node *node);

    Node *find_descendant(const std::string &name);

    void draw_children(IScreen*);

    virtual void draw_self(IScreen *screen) = 0;

    virtual void draw(IScreen *screen) {
        _pre_draw(screen);
        draw_self(screen);
        draw_children(screen);
        _post_draw(screen);
    }

    virtual void _pre_draw(IScreen *screen);
    virtual void _post_draw(IScreen *screen);

};

class ContainerNode : public Node
{
public:
    virtual void draw_self(IScreen* s) {
        // nothing here
    }

    virtual void draw(IScreen *screen) {
        _pre_draw(screen);
        draw_children(screen);
        _post_draw(screen);
    }

};

class CallbackNode : public Node
{
public:
    typedef boost::function<void(Node*, WL::IScreen*)> DrawFunction;

    CallbackNode() {
        m_draw_func = wlNull;
    }

    DrawFunction *m_draw_func;

    virtual void draw_self(IScreen *screen) {
        // Node::draw(screen);
        if (m_draw_func)
            (*m_draw_func)(this, screen);
    }
};

}
wlEndNS

#endif


