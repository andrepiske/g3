#ifndef _G3_Booth_H_
#define _G3_Booth_H_

#include <wl/refcounted.h>
#include <gtk/gtk.h>
#include <string>

namespace G3 {

class IBooth : public WL::RefCounted
{
public:
    
    virtual GtkWidget *get_widget() = 0;

public:
    static IBooth *create();

};

class IJuggler : public WL::RefCounted
{
public:
    virtual GtkWidget *get_widget() = 0;

    virtual std::string get_name() = 0;

};

}

#endif

