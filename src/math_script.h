#if 0

#ifndef _G3_MATH_SCRIPT_INTERFACE_H_
#define _G3_MATH_SCRIPT_INTERFACE_H_

#include <wl/refcounted.h>
#include <wl/scriptable.h>

namespace G3 {
namespace Math {

class IVertexGroup : public WL::RefCounted
{
public:
    virtual void set_size(int newsize) = 0;
    virtual int get_size() = 0;

    virtual void set_vertex(int index, double x, double y, double z) = 0;
    virtual void get_vertex(int index, double *x, double *y, double *z) = 0;

    static IVertexGroup *create();

};

} // NS Math
} // NS G3

#endif

#endif

