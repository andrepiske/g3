#include <wl/list.h>

wlStartNS

// TODO: This class is a stub.

class CList : public IList
{
public:
    LT m_els;

public:
    virtual void insert(int position, RefCounted *el) {
        if (position == 0)
            m_els.push_front(RCBucket<RefCounted>(el));
        else if (position == -1)
            m_els.push_back(RCBucket<RefCounted>(el));
        //else
            //m_els.insert(m_els.iterator()+position, RCBucket<RefCounted>(el));
    }

    virtual int size() {
        return (int)m_els.size();
    }

    virtual RefCounted *get(int index) {
        LT::iterator it = m_els.begin();
        while (index--)
            ++it;
        if (it == m_els.end())
            return 0;
        return (*it)();
    }

    virtual LT &get_lt() {
        return m_els;
    }

};

IList *IList::create() {
    return new CList();
}

wlEndNS

