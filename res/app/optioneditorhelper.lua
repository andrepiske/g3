
OptionEditorHelper = {}

function OptionEditorHelper.make_ok_cancel_buttons()
    local B = {}
    B.bt_cancel = gtk.Button.new_with_label('Cancel')
    B.bt_ok = gtk.Button.new_with_label('OK')
    B.bts_con = gtk.Box.new(gtk.ORIENTATION_HORIZONTAL)
    B.bts_con:pack_start(B.bt_cancel, true, true, 1)
    B.bts_con:pack_start(B.bt_ok, true, true, 1)
    return B.bts_con, B
end

module 'OptionEditorHelper'

