#include <wl/filesys.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <sstream>

wlStartNS
namespace fsys {

wlBool dir_list(const std::string &path, std::list<std::string> &out) {
    DIR *d;
    dirent *e;
    
    d = ::opendir(path.c_str());
    if (!d)
        return 0;

    e = ::readdir(d);
    if (!e) {
        ::closedir(d);
        return 0;
    }

    while (e) {
        out.push_back(e->d_name);
        e = readdir(d);
    }

    ::closedir(d);
    return 1;
}

wlBool is_directory(const std::string &path) {
    struct stat b;
    if (::stat(path.c_str(), &b))
        return 0;
    return S_ISDIR(b.st_mode);
}

wlBool is_file_readable(const std::string &path) {
    const int r = ::access(path.c_str(), R_OK);
    return !r;
}

wlBool make_directories(const std::string &path) {
    return ! ::mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}

std::string dir_join(const char *a, ...) {
    const size_t MP = 8192;
    size_t l = (size_t)strlen(a);

    char buf[MP + 1];
    ::memset(buf, 0, MP + 1);

    size_t p = (!l) ? 0 : (a[l-1] == '/' ? l-1 : l);
    ::memcpy(buf, a, p);

    va_list vl;
    va_start(vl, a);
    for (;;) {
        const char *b = va_arg(vl, const char*);
        if (!b)
            break;

        l = (size_t)strlen(b);
        if (p + l + 1 >= MP) {
            va_end(vl);
            return "";
        }

        buf[p++] = '/';
        ::memcpy(&buf[p], b, l);
        p += l;
    }
    va_end(vl);

    return std::string(buf);
}

std::string dir_current() {
    char buf[4096];
    getcwd(buf, 4096);
    return buf;
}

}
wlEndNS

