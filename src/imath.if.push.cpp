/* This is a generated file, do not edit it */
#include <boost/unordered_map.hpp>
#include <lua.hpp>
#include <wl/scriptable.h>
#include "imath.if.h"


static
int _G3_Math_IVertexGroup__closure(lua_State *L) {
    const int proc_id = lua_tonumber(L, lua_upvalueindex(1));
    G3::Math::IVertexGroup *obj = *(G3::Math::IVertexGroup**)luaL_checkudata(L, 1, "WL-G3::Math::IVertexGroup");
    switch (proc_id) {
        case 1: { // get_vertex
            int _valid[] = {0,0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            _valid[3] = lua_isnumber(L, 5);
            int v_index;
            double v_x;
            double v_y;
            double v_z;
            v_index = lua_tonumber(L, 2);
            if (_valid[1]) {
                v_x = lua_tonumber(L, 3);
            }
            if (_valid[2]) {
                v_y = lua_tonumber(L, 4);
            }
            if (_valid[3]) {
                v_z = lua_tonumber(L, 5);
            }
            obj->get_vertex(v_index, _valid[1]?((&v_x)):0, _valid[2]?((&v_y)):0, _valid[3]?((&v_z)):0);            
            lua_pushnumber(L, v_x);
            lua_pushnumber(L, v_y);
            lua_pushnumber(L, v_z);
            return 3;
        }
        case 2: { // set_size
            int _valid[] = {0,0};
            _valid[0] = lua_isnumber(L, 2);
            int v_newsize;
            v_newsize = lua_tonumber(L, 2);
            obj->set_size(v_newsize);            
            return 0;
        }
        case 3: { // set_vertex
            int _valid[] = {0,0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            _valid[3] = lua_isnumber(L, 5);
            int v_index;
            int v_x;
            int v_y;
            int v_z;
            v_index = lua_tonumber(L, 2);
            v_x = lua_tonumber(L, 3);
            v_y = lua_tonumber(L, 4);
            v_z = lua_tonumber(L, 5);
            obj->set_vertex(v_index, v_x, v_y, v_z);            
            return 0;
        }
        case 4: { // get_size
            int _valid[] = {0};
            int _v_ret;
            _v_ret = obj->get_size();            
            lua_pushnumber(L, _v_ret);
            return 1;
        }
    }
    return 0;
}

static
int _G3_Math_IVertexGroup__gc(lua_State *L) {
    G3::Math::IVertexGroup *ptr = *(G3::Math::IVertexGroup**)lua_touserdata(L, 1);
    ptr->rc_pop();
    return 0;
}

void _G3_Math_IVertexGroup__regMT(lua_State *L) {
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, _G3_Math_IVertexGroup__gc);
    lua_setfield(L, -2, "__gc");
    lua_pushnumber(L, 1);
    lua_pushcclosure(L, _G3_Math_IVertexGroup__closure, 1);
    lua_setfield(L, -2, "get_vertex");
    lua_pushnumber(L, 2);
    lua_pushcclosure(L, _G3_Math_IVertexGroup__closure, 1);
    lua_setfield(L, -2, "set_size");
    lua_pushnumber(L, 3);
    lua_pushcclosure(L, _G3_Math_IVertexGroup__closure, 1);
    lua_setfield(L, -2, "set_vertex");
    lua_pushnumber(L, 4);
    lua_pushcclosure(L, _G3_Math_IVertexGroup__closure, 1);
    lua_setfield(L, -2, "get_size");
}

void _G3_Math_IVertexGroup__luapush(lua_State *L, WL::RefCounted *_obj) {
    G3::Math::IVertexGroup *obj = dynamic_cast<G3::Math::IVertexGroup*>(_obj);
    *(void**)lua_newuserdata(L, sizeof(void*)) = (void*)obj;
    if (luaL_newmetatable(L, "WL-G3::Math::IVertexGroup"))
        _G3_Math_IVertexGroup__regMT(L);
    lua_setmetatable(L, -2);
    obj->rc_push();
}


