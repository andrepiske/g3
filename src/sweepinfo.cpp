#include "sweepgenerator.h"
#include <GL/gl.h>
#include <GL/glu.h>

namespace G3 {

using namespace WL;

class CSweepInfoDrawing : public sg::Node
{
public:
    CSweepInfoDrawing(SweepInfo *si) {
        m_si = si;
    }

    virtual void draw_self(IScreen *_u) {
        const size_t ax_count = m_si->axes.size();
        glDisable(GL_LIGHTING);
        glLineWidth(2.5f);
        for (size_t i = 0; i < ax_count; ++i) {
            const SweepInfo::axis_t ax = m_si->axes[i];
            const Vec3 x = ax.x + ax.pos;
            const Vec3 y = ax.y + ax.pos;
            const Vec3 z = ax.z + ax.pos;
            glBegin(GL_LINES);
            glColor3ub(0xFF, 0, 0);
            glVertex3fv((const GLfloat*)&ax.pos);
            glVertex3fv((const GLfloat*)&x);
            glColor3ub(0, 0, 0xFF);
            glVertex3fv((const GLfloat*)&ax.pos);
            glVertex3fv((const GLfloat*)&y);
            glColor3ub(0, 0xFF, 0);
            glVertex3fv((const GLfloat*)&ax.pos);
            glVertex3fv((const GLfloat*)&z);
            glEnd();
        }
    }

    SweepInfo *m_si;
};

sg::Node *_sweepinfo_new_drawing(SweepInfo *si) {
    return new CSweepInfoDrawing(si);
}

} // NS G3

