#include <wl/event.h>

wlStartNS


ISink::~ISink() {
    BindBagT::iterator it = __event_binds.begin();
    for (; it != __event_binds.end(); ++it) {
        for (u32 n = 0; n < it->second; ++n)
            it->first->__unbind_sink(this);
    }
}

IEmitter::~IEmitter() {
    BindBagT::iterator it = __event_binds.begin();
    for (; it != __event_binds.end(); ++it)
        for (u32 n = 0; n < it->second; ++n)
            it->first->__unbind_emitter(this);
}

void IEmitter::emit_event(Quark signal, RefCounted *param) {
	if (param)
		param->rc_push();
    BindBagT::iterator it = __event_binds.begin();
    for (; it != __event_binds.end(); ++it)
        it->first->__event(this, signal, param);
	if (param)
		param->rc_pop();
}

void ISink::__on_bind(BindEvent *b) {
    __event_binds.insert(b);
}

void ISink::__on_unbind(BindEvent *b) {
    __event_binds.remove(b);
}

void IEmitter::__on_bind(BindEvent *b) {
    __event_binds.insert(b);
}

void IEmitter::__on_unbind(BindEvent *b) {
    __event_binds.remove(b);
}

wlEndNS

