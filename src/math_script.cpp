#if 0

#include "math_script.h"
#include <vector>

extern void _G3_Math_IVertexGroup__luapush(lua_State*, G3::Math::IVertexGroup*);

namespace G3 {
namespace Math {

using namespace WL;

class CVertexGroup : public IVertexGroup,
                     public scriptable::RegPush<IVertexGroup>
{
public:
    typedef std::vector<Vec3> VertexVec;

    VertexVec m_vertices;

public:
    CVertexGroup() : scriptable::RegPush<IVertexGroup>
            (this, _G3_Math_IVertexGroup__luapush)
    {
    }

    virtual void set_size(int newsize) {
        m_vertices.resize( (size_t)newsize, Vec3(0, 0, 0) );
    }

    virtual int get_size() {
        return (int)m_vertices.size();
    }

    virtual void set_vertex(int index, double x, double y, double z) {
        m_vertices[index] = Vec3((float)x, (float)y, (float)z);
    }

    virtual void get_vertex(int index, double *x, double *y, double *z) {
        const Vec3 &v = m_vertices[index];
        if (x) *x = v.x;
        if (y) *y = v.y;
        if (z) *z = v.z;
    }

};

IVertexGroup *IVertexGroup::create() {
    return new CVertexGroup();
}

} // NS Math
} // NS G3

#endif

