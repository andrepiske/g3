#include "precompiled.h"
#include <wl/config.h>
#include <wl/screen.h>
#include <wl/log.h>
#include <GL/gl.h>
#include <wl/poly.h>
#include <map>
#include <wl/util.h>
#include <stack>
#include <wl/glstate.h>

wlStartNS

class CScreen : public IScreen
{
public:
    SDL_Surface *m_screen;

    // current scene
    std::string m_current_scene_name;
    IScene *m_current_scene;

    typedef std::map<std::string, IScene*> SceneMap;
    typedef std::stack<std::string> SceneStack;

    // all registered scenes
    SceneMap m_scenes;

    // paused scenes stack
    SceneStack m_pausedstack;

    wlBool _create_screen(const Point2D &dim, int bpp) {
        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_GetAttribute(SDL_GL_STENCIL_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
        // SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

        // SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

        // SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        m_screen = SDL_SetVideoMode((int)dim.x, (int)dim.y, bpp, SDL_OPENGL | SDL_RESIZABLE);
        if (!m_screen) {
            WL_LOG_ERROR("Failed to set video mode: %s",
                    SDL_GetError());
            return wlFalse;
        }
        
        this->resize(dim);
        return wlTrue;
    }
    
    virtual void resize(const Point2D &dim) {
        m_screen = SDL_SetVideoMode((int)dim.x, (int)dim.y, 32, SDL_OPENGL | SDL_RESIZABLE);
        glViewport(0, 0, (GLsizei)dim.x, (GLsizei)dim.y);
    }

    virtual Point2D get_size() const {
        return Point2D(m_screen->w, m_screen->h);
    }

    virtual void update_scene() {
        if (m_current_scene)
            m_current_scene->update(this);
    }

    virtual void draw() {
        if (m_current_scene) {
            glClearColor(0, 0, 0, 0);
            glClear(GL_COLOR_BUFFER_BIT);

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();

            const util::bounds<float> bo =
                m_current_scene->get_bounds();
            glOrtho(bo.left, bo.right, bo.bottom, bo.top, 
                    -1., 1.);

            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            glEnable(GL_BLEND);
            glDisable(GL_DEPTH_TEST);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glBlendFunc(GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            // glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    /*
    static int k = 0, dir = 0;
    if (!dir)
        k++;
    else
        k--;
    if (k == 0 || k == 255)
        dir = !dir;
    glDisable(GL_TEXTURE_2D);
    WL::Color(0xf3, 0x11, 0xcc, 0xFF).gl_set();
    glBegin(GL_TRIANGLE_STRIP);
    glVertex2f(10.f, 10.f);
    glVertex2f(300.f, 100.f);
    glVertex2f(20.f, 40.f);
    glEnd();
    WL::Color(0x3C, k, 0x0F, k).gl_set();
    glBegin(GL_TRIANGLE_STRIP);
    glVertex2f(100.f, 10.f);
    glVertex2f(50.f, 200.f);
    glVertex2f(10.f, 10.f);
    glEnd();
             */
             m_current_scene->draw(this);
        } else {
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            glClearColor(0, 0, 0xFF, 0);
            glClear(GL_COLOR_BUFFER_BIT);
        }
    }

    virtual void go_screen_2d() {
        GLMatrixModePush m(GL_PROJECTION);
        const Point2D dim = get_size();
        glLoadIdentity();
        glOrtho(0.f, dim.x, dim.y, 0.f, 1.f, -1.f);
    }

    virtual void adjust_2d() {
        // as in:
        // http://opengl.org/resources/faq/technical/transformations.htm#tran0030
        glTranslatef(0.375f, 0.375f, 0.f);
    }

    virtual void flip_screen() {
        SDL_GL_SwapBuffers();
        // SDL_Flip(m_screen) ;
    }

    virtual void release() {
    }

    virtual void add_scene(const std::string &id, IScene *scene) {
        SceneMap::iterator it;
        it = m_scenes.find(id);
        if (it != m_scenes.end())
            WL_LOG_ERROR("Scene %d already registered", id.c_str());

        m_scenes[id] = scene;
    }

    virtual IScene *get_scene(const std::string &id) {
        SceneMap::iterator it = m_scenes.find(id);
        if (it == m_scenes.end())
            return wlNull;
        return it->second;
    }

    virtual std::string current_scene() const {
        return m_current_scene_name;
    }

    virtual void current_scene(const std::string &id, wlBool pause) {
        SceneMap::iterator it;
        it = m_scenes.find(id); 
        if (it == m_scenes.end())
            WL_LOG_ERROR("Scene %s not found", id.c_str());

        IScene *scene = it->second;
        if (m_current_scene == scene) {
            WL_LOG_INFO("Switching to current scene! (%s)",
                m_current_scene_name.c_str());
            return;
        }

        if (m_current_scene) {
            m_current_scene->transition(pause ? IScene::state_paused :
                IScene::state_unloaded, this);

            if (pause)
                m_pausedstack.push(m_current_scene_name);
        }

        m_current_scene_name = id;
        m_current_scene = scene;
        scene->transition(IScene::state_onscreen, this);
    }

    virtual void gl_scissor(const Vec2 &pos, const Vec2 &size) {
        glScissor((int)pos.x, (int)m_screen->h - (int)pos.y,
            (int)size.x, (int)size.y);
        /*
        printf("%d %d\n",
            (int)pos.x,
            (int)m_screen->h - (int)pos.y);
    */
    }

    virtual int unpause_scene() {
        int size = m_pausedstack.size();
        if (!size)
            return -1;

        std::string scene_name = m_pausedstack.top();
        m_pausedstack.pop();

        current_scene(scene_name, wlFalse);

        return size - 1;
    }

};

IScreen *IScreen::create_screen(const Point2D &dim, int bpp) {
    CScreen *sc = new CScreen();
    if (! sc->_create_screen(dim, bpp)) {
        delete sc;
        return NULL;
    }
    return sc;
}

wlEndNS

