#ifndef __G3_WORKPIECE_H__
#define __G3_WORKPIECE_H__

#include <wl/config.h>
#include "piece.h"
#include <wl/math.h>
#include <wl/scenegraph.h>

namespace G3 {

class ICVMesh;

class IWorkPiece : public WL::sg::Node
{
public:
    // virtual void reset_working_piece() = 0;

    // virtual void make_cube(const WL::Vec3 &dimensions) = 0;

    virtual void set_cvmesh(ICVMesh *mesh) = 0;
    virtual ICVMesh* get_cvmesh() = 0;

    static IWorkPiece *create();
};

} // NS G3

#endif


