#ifndef _WL_Script_H_
#define _WL_Script_H_

#include <boost/function.hpp>
#include <wl/config.h>
#include <wl/resource.h>

extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}

wlStartNS

class IScript : public Resource
{
public:
    typedef ::boost::function<int (IScript*)> CallbackProc;
    typedef int(*CallbackCProc)(IScript*);

    virtual ~IScript() {}

    virtual void load_data() = 0;
    virtual void unload_data() = 0;
    
    static IScript *create_resource(ResourceIOBlock blk);
    static IScript *create_new();

    virtual wlBool assign(const std::string &var, const std::string &expr) = 0;
    virtual wlBool exists(const std::string &var) = 0;

    virtual wlBool exec_expr(const std::string &expr, int nresults) = 0;
    virtual std::string string_expr(const std::string &expr,
            const std::string &default_) = 0;
    virtual int int_expr(const std::string &expr, int default_) = 0;
    virtual int double_expr(const std::string &expr, double default_) = 0;

    virtual void gc_call() = 0;

    virtual lua_State *get_luastate() = 0;
    virtual wlBool execute() = 0;

    virtual wlBool equals(int index1, int index2) = 0;

    /* convertion 
       these won't pop anything from stack */

    virtual std::string to_string(int index) = 0;
    virtual wlBool to_bool(int index) = 0;
    virtual long to_int(int index) = 0;
    virtual double to_double(int index) = 0;
    virtual void *to_pointer(int index) = 0;
    virtual void *to_userdata(int index) = 0;

    virtual int luatype(int index) = 0;
    virtual std::string luatypename(int index) = 0;

    /* stack */
    virtual void pop(int n) = 0;
    virtual void push_bool(wlBool v) = 0;
    virtual void push_string(const std::string &str) = 0;
    virtual void push_int(long n) = 0;
    virtual void push_nil() = 0;
    virtual void push_double(double v) = 0;
    // push boost::function
    virtual void push_bfunction(CallbackProc func, int num_params) = 0;
    // push C function ptr
    virtual void push_cclosure(CallbackCProc func, int num_params) = 0;
    virtual void push_lightuserdata(void *p) = 0;
    virtual void push_copy(int index) = 0;

    virtual void insert(int index) = 0;
    virtual void replace(int index) = 0;
    virtual void remove(int index) = 0;

    virtual wlBool getfield_safe(int index, const std::string &name) = 0;
    virtual void getfield(int index, const std::string &name) = 0;
    virtual void gettable(int index) = 0;
    // virtual void getenv(const std::string &name) = 0;
    virtual void getglobal(const std::string &name) = 0;
    virtual wlBool getmetatable(int index) = 0;

    virtual void setfield(int index, const std::string &name) = 0;
    virtual void settable(int index) = 0;
    // virtual void setenv(int index) = 0;
    virtual void setglobal(const std::string &name) = 0;
    virtual wlBool setmetatable(int index) = 0;

    virtual int stack_size() = 0;
    virtual wlBool call(int nargs, int nresults = 1) = 0;

    virtual void new_table() = 0;
    virtual void *new_userdata(int size) = 0;

    template<class T>
    void push_t(const T &c);

    template<class T>
    void push_t(T &c);

    template<class T>
    wlBool to_t(T &c);

};

wlEndNS


#endif

