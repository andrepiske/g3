#include <wl/polybuilder.h>

wlStartNS

class CPolyBuilder : public IPolyBuilder
{
public:
    IPoly *m_poly;
    Color m_currcolor;

public:

    int _colorindex(const Color &c) {
        int index = 0;
        IPoly::ColorVec *v = m_poly->get_colorsptr();
        IPoly::ColorVec::iterator it = v->begin();
        for (; it != v->end(); ++it) {
            if (*it == c)
                return index; 
            ++index;
        }
        
        v->push_back(c);
        return v->length() - 1;
    }

    void color(const Color &c) {
        m_currcolor = c;
    }

    void vertex(const Vec2 &v) {
    }

    void set_poly(IPoly *poly) {
        if (m_poly)
            m_poly->rc_pop();
        m_poly = poly;
        poly->rc_push();
    }

    IPoly *get_poly() {
        m_poly->rc_push();
        return m_poly;
    }

};

IPolyBuilder *IPolyBuilder::create() {
    return new CPolyBuilder();
}

wlEndNS

