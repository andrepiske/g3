#include <wl/math.h>
#include <sstream>
#include <iomanip>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#include <Eigen/Dense>
#pragma GCC diagnostic pop

wlStartNS

std::string mat_to_string(const Mat33 &m) {
    std::stringstream s;
    s << "[[" << std::setprecision(6) << std::fixed << 
        m.u.x << ',' << m.v.x << ',' << m.w.x << "], [" <<
        m.u.y << ',' << m.v.y << ',' << m.w.y << "], [" <<
        m.u.z << ',' << m.v.z << ',' << m.w.z << "]]";
    return s.str();
}

std::string mat_to_string(const Mat44 &m) {
    std::stringstream s;
    s << "[[" << std::setprecision(6) << std::fixed << 
        m.u.x << ',' << m.v.x << ',' << m.w.x << ',' << m.t.x << "], [" <<
        m.u.y << ',' << m.v.y << ',' << m.w.y << ',' << m.t.y << "], [" <<
        m.u.z << ',' << m.v.z << ',' << m.w.z << ',' << m.t.z << "], [" <<
        m.u.w << ',' << m.v.w << ',' << m.w.w << ',' << m.t.w << "]]";
    return s.str();
}

Mat33 Mat33::inverse() const {
    using namespace Eigen;
    Matrix3f m;
    m << u.x, u.y, u.z,
         v.x, v.y, v.z,
         w.x, w.y, w.z;
    m = m.inverse().eval();
    return Mat33(
        Vec3( m(0,0), m(0,1), m(0,2) ),
        Vec3( m(1,0), m(1,1), m(1,2) ),
        Vec3( m(2,0), m(2,1), m(2,2) ));
}

Vec3 Mat33::solve(const Vec3 &s) const {
    using namespace Eigen;
    Matrix3f m;
    m << u.x, v.x, w.x,
        u.y, v.y, w.y,
        u.z, v.z, w.z;
    Vector3f ag;
    ag << s.x, s.y, s.z;
    // const Vector3f r = m.fullPivHouseholderQr().solve(ag);
    const Vector3f r = m.colPivHouseholderQr().solve(ag);
    return Vec3(r[0], r[1], r[2]);
}

Mat33 calc_basechanger(const Vec3 &u1, const Vec3 &u2, const Vec3 &u3,
        const Vec3 &b1, const Vec3 &b2, const Vec3 &b3)
{
    const Mat33 cb_m(
        Vec3(u1.x, u1.y, u1.z),
        Vec3(u2.x, u2.y, u2.z),
        Vec3(u3.x, u3.y, u3.z));
    const Vec3 sa = cb_m.solve(b1);
    const Vec3 sb = cb_m.solve(b2);
    const Vec3 sc = cb_m.solve(b3);
    return Mat33(sa, sb, sc);
}

wlEndNS

