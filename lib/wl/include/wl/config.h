#ifndef _WL_Config_Header_H_
#define _WL_Config_Header_H_

typedef unsigned char wlByte;

#define wlPI (3.14159265)
typedef int wlBool;
#define wlFalse (0)
#define wlTrue (~wlFalse)

#define wlNull (0)

#define WLINLINE inline
#define WLSTDCALL __attribute__((stdcall))

#define wlStartNS namespace WL {
#define wlEndNS }

wlStartNS
typedef unsigned long long int u64;
typedef unsigned int u32;
typedef long long int s64;
typedef int s32;
typedef wlByte u8;
typedef char s8;
wlEndNS 

#endif

