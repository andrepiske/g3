/* This is a generated file, do not edit it */
#include <boost/unordered_map.hpp>
#include <lua.hpp>
#include <wl/scriptable.h>
#include "mesh.h"
#include "g3d.if.h"


static
int _G3_IGd__closure(lua_State *L) {
    const int proc_id = lua_tonumber(L, lua_upvalueindex(1));
    G3::IGd *obj = *(G3::IGd**)luaL_checkudata(L, 1, "WL-G3::IGd");
    switch (proc_id) {
        case 1: { // num_meshes
            int _valid[] = {0};
            int _v_ret;
            _v_ret = obj->num_meshes();            
            lua_pushnumber(L, _v_ret);
            return 1;
        }
        case 2: { // get_mesh
            int _valid[] = {0,0};
            _valid[0] = lua_isnumber(L, 2);
            ::WL::RefCounted* _v_ret;
            int v_index;
            v_index = lua_tonumber(L, 2);
            _v_ret = obj->get_mesh(v_index);            
            ::WL::scriptable::push(L, (::WL::RefCounted*)_v_ret);
            return 1;
        }
        case 3: { // rem_mesh
            int _valid[] = {0,0};
            _valid[0] = lua_istable(L, 2);
            ::WL::RefCounted* v_mesh;
            v_mesh = (::WL::RefCounted*)lua_touserdata(L, 2);
            obj->rem_mesh((G3::IMesh*)v_mesh);            
            return 0;
        }
        case 4: { // add_mesh
            int _valid[] = {0};
            ::WL::RefCounted* _v_ret;
            _v_ret = obj->add_mesh();            
            ::WL::scriptable::push(L, (::WL::RefCounted*)_v_ret);
            return 1;
        }
    }
    return 0;
}

static
int _G3_IGd__gc(lua_State *L) {
    G3::IGd *ptr = *(G3::IGd**)lua_touserdata(L, 1);
    ptr->rc_pop();
    return 0;
}

void _G3_IGd__regMT(lua_State *L) {
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, _G3_IGd__gc);
    lua_setfield(L, -2, "__gc");
    lua_pushnumber(L, 1);
    lua_pushcclosure(L, _G3_IGd__closure, 1);
    lua_setfield(L, -2, "num_meshes");
    lua_pushnumber(L, 2);
    lua_pushcclosure(L, _G3_IGd__closure, 1);
    lua_setfield(L, -2, "get_mesh");
    lua_pushnumber(L, 3);
    lua_pushcclosure(L, _G3_IGd__closure, 1);
    lua_setfield(L, -2, "rem_mesh");
    lua_pushnumber(L, 4);
    lua_pushcclosure(L, _G3_IGd__closure, 1);
    lua_setfield(L, -2, "add_mesh");
}

void _G3_IGd__luapush(lua_State *L, WL::RefCounted *_obj) {
    G3::IGd *obj = dynamic_cast<G3::IGd*>(_obj);
    *(void**)lua_newuserdata(L, sizeof(void*)) = (void*)obj;
    if (luaL_newmetatable(L, "WL-G3::IGd"))
        _G3_IGd__regMT(L);
    lua_setmetatable(L, -2);
    obj->rc_push();
}


