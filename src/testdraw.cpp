#include "testdraw.h"
#include <GL/gl.h>

namespace G3 {

class CDrawTest : public IDrawGuy
{
public:
    virtual void draw(IDrawSurface *surface) {

        glColor3ub(0xFF, 0, 0);
        glBegin(GL_TRIANGLES);

        glVertex2d(-0.5, -0.5);
        glVertex2d(0.5, -0.5);
        glVertex2d(0.0, 0.5);

        glEnd();

    }

};

IDrawGuy *test_draw_create_guy() {
    return new CDrawTest();
}

};

