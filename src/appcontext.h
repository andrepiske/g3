#ifndef __G3_APPCONTEXT_H__
#define __G3_APPCONTEXT_H__

#include <wl/refcounted.h>
#include <string>
#include "g3d.h"
#include "cutter.h"

namespace G3 {

class IAppContext : virtual public WL::RefCounted
{
public:
    virtual void set_parameter_string(const std::string &param_name,
            const std::string &value) = 0;

    virtual void set_parameter_int(const std::string &param_name,
        int value) = 0;

    virtual void set_parameter_double(const std::string &param_name,
        double value) = 0;

    virtual std::string get_parameter_string(const std::string &param_name) = 0;
    virtual int get_parameter_int(const std::string &param_name) = 0;
    virtual double get_parameter_double(const std::string &param_name) = 0;

    virtual int has_parameter(const std::string &param_name) = 0;

    virtual IGd* get_gd_object() = 0;
    virtual ICutter *get_cutter() = 0;

};

} // NS G3

#endif


