#ifndef _WL_URI_H_
#define _WL_URI_H_

#include <wl/config.h>
#include <wl/refcounted.h>
#include <string>

wlStartNS

namespace URI {

wlBool valid(const std::string &);
wlBool extract(const std::string &uri,
        std::string *schema = wlNull,
        std::string *hier_path = wlNull,
        std::string *query = wlNull,
        std::string *frag = wlNull);

};

wlEndNS

#endif

