#ifndef _WL_Sprite_H_
#define _WL_Sprite_H_

#include <wl/config.h>
#include <wl/image.h>
#include <wl/sg2.h>
#include <wl/color.h>
#include <wl/math.h>

wlStartNS
class ISprite : public RefCounted
{
public:
    Vec2 m_sproffset;
    Vec2 m_sprsize;
    Color m_color;

public:
    static ISprite *create_sprite(IImage *image,
        const Vec2 &offset, const Vec2 &size);

    virtual void draw(IScreen*) = 0;

    // virtual void set_image(IImage *img) = 0;

    // does not rc_push
    virtual IImage *get_image() = 0;

    virtual const Vec2 &spr_size() const = 0;
};

class ISpriteNode : public sg::Node2D
{
public:
    static ISpriteNode *create();

    virtual void set_sprite(ISprite *spr) = 0;
    virtual ISprite *get_sprite() = 0;

    virtual void draw_self(IScreen *scr) = 0;

};

wlEndNS
#endif

