#include "opencsgdraw.h"

namespace G3 {

OCSGPrimitive::OCSGPrimitive(WL::sg::Node *node, OpenCSG::Operation op)
    : OpenCSG::Primitive(op, 2)
{
    m_node = node;
}

OCSGPrimitive::~OCSGPrimitive() {
}

WL::sg::Node *OCSGPrimitive::get_node() {
    return m_node;
}

void OCSGPrimitive::set_node(WL::sg::Node *node) {
    m_node = node;
}

void OCSGPrimitive::render() {
    if (m_node())
        m_node->draw(NULL);
}

} // NS G3

