require('class')
require('properties')
require('lmat')
require('auxmat')

--                -!- WARNING -!-
--
--      Do not confuse mesh types, there are this class
--      "ShMesh" and the interface "WL.IMesh"


-------------------------------------------------------------------------------

ShMesh = Class(function(S, _g3, _mesh)
    S.g3 = _g3
    S.mesh = _mesh -- type WL.IMesh
    S.unused_vertices = {}
    S._next_vertex_id = 0
    S._next_face_id = 0
    
    function S:new_vertex(coords, color)
        local id = self._next_vertex_id
        self._next_vertex_id = id + 1
        self.mesh:set_num_vertices(id + 1)
        local vtx = ShVertex.new(self, id)
        if coords ~= nil then
            vtx.x = coords[1]
            vtx.y = coords[2]
            vtx.z = coords[3]
        end
        if color ~= nil then
            vtx.color = color
        end
        return vtx
    end

    function S:get_vertex(index)
        return ShVertex.new(self, index)
    end

    function S:get_face(index)
        return ShFace.new(self, index)
    end

    -- this method has one form:
    -- s:new_face(va, vb, vc) where those are ShVertex objects
    function S:new_face(va, vb, vc)
        local fcid = self._next_face_id
        self._next_face_id = fcid + 1
        self.mesh:set_num_faces(fcid + 1)
        local fc = ShFace.new(self, fcid)
        if va ~= nil then
            fc.va = va
            fc.vb = vb
            fc.vc = vc
        end
        return fc
    end

end)

-- a shared vertex of a mesh
ShVertex = Class(function(S, _mesh, _vtid)

    S.vtid = _vtid
    S.mesh = _mesh -- type ShMesh (not WL.IMesh) 

    function S:_pset_color(c)
        local r = c[1]
        local g = c[2]
        local b = c[3]
        local a = c[4] or 0xFF
        self.mesh.mesh:set_vertex_color(self.vtid, r, g, b, a)
    end

    function S:_pget_color()
        return { self.mesh.mesh:get_vertex_color(self.vtid, 0, 0, 0, 0) }
    end

    for _,n in ipairs{'x', 'y', 'z'} do
        S['_pset_' .. n] = function(self, nv)
            local x, y, z = self.mesh.mesh:get_vertex(self.vtid, 0, 0, 0)
            local t = { ['x']=x, ['y']=y, ['z']=z }
            t[n] = nv
            self.mesh.mesh:set_vertex(self.vtid, t.x, t.y, t.z)
        end
        S['_pget_' .. n] = function(self)
            local x, y, z = self.mesh.mesh:get_vertex(self.vtid, 0, 0, 0)
            local t = { ['x']=x, ['y']=y, ['z']=z }
            return t[n]
        end
    end

end, AutoInit(HasProperties))

ShFace = Class(function(S, _mesh, _fcid)

    S.fcid = _fcid
    S.mesh = _mesh -- type ShMesh (not WL.IMesh)

    local Tb = { ['va']=1, ['vb']=2, ['vc']=3 }

    for n,n_val in pairs(Tb) do
        S['_pset_' .. n] = function(self, nv)
            local t = { self.mesh.mesh:get_face(self.fcid, 0, 0, 0) }
            t[n_val] = nv.vtid
            self.mesh.mesh:set_face(self.fcid, unpack(t))
        end
        S['_pget_' .. n] = function(self)
            local t = { self.mesh.mesh:get_face(self.fcid, 0, 0, 0) }
            return t[n_val]
        end
    end

    function S:set_normal(v)
        self.mesh.mesh:set_face_normal(self.fcid, v[1], v[2], v[3])
    end

    function S:auto_normals()
        self.mesh.mesh:autoset_face_normal(self.fcid)
    end

end, AutoInit(HasProperties))

------------------------------------------------------------------------

-- this transforms to base { <1, 0, 0>, <0, 1, 0> }
local function default_face_transform_function(v)
    return lmat.vec(v[1], v[2], 0)
end

Mesh = Class(function(S)

    S.faces = { }

    function S:apply_mesh(mesh)
        for fcid,face in ipairs(self.faces) do
            local tf = face.transform or default_face_transform_function
            face:calculate_normal()
            face:compute_triangles()
            for tri_id,tri in ipairs(face.triangles) do
                local vts = {}
                for i=1,3,1 do
                    local vs = tri[i]
                    vts[i] = mesh:new_vertex(tf{ vs[1], vs[2] }, face.color)
                end
                local fc = mesh:new_face(unpack(vts))
                fc:set_normal(face.normal)
            end
        end
    end

end)

Edge = Class(function(S)

    S.vert_front = nil
    S.vert_back = nil

    S.face_right = nil
    S.face_left = nil

    S.edge_right_next = nil
    S.edge_left_next = nil

    S.edge_right_prev = nil
    S.edge_left_prev = nil



end);

Face = Class(function(S)

    S.edges = {} -- of type Edge
    S.normal = lmat.vec(0, 0, 0)
    S.color = { 0x6F, 0x4F, 0x6F, 0xFF } 
    S.triangles = nil
    S.transform = nil

    function S:clone()
        local nf = Face.new()
        nf.normal = self.normal:clone()
        nf.color = {}
        table.insert(nf.color, unpack(self.color))
        for i,v in ipairs(self.vertices) do
            nf.vertices[i] = v:clone()
        end
        nf.transform = self.transform
        return nf
    end

    -- line is in its parametric form
    -- if collision occurs in line (v_i, v_i+1), returns tuple (true, i, t)
    -- if selfcollide==true then collision also occurs with t==1 or t==0
    function S:does_line_collide(line, selfcollide)
        local n = #self.vertices
        if selfcollide == nil then
            selfcollide = true
        end
        for i,a in ipairs(self.vertices) do
            local b = self.vertices[i%n+1]
            local ip = auxmat.line_intersect_2d(
                auxmat.make_pmetric_line_2d(a, b), line)
            if (ip ~= nil) and (
               (ip[1] > 0 and ip[2] > 0 and ip[1] < 2 and ip[2] < 1) or
                (selfcollide and
                  ip[1] >= 0 and ip[2] >= 0 and ip[1] <= 2 and ip[2] <= 1))
            then
                return true, i, ip
            end
        end
        return false
    end

    function S:apply_transform(mat)
        self.triangles = nil
        local vtcs = { }
        for _,vt in ipairs(self.vertices) do
            table.insert(vtcs, mat:mulvec(vt))
        end
        self.vertices = vtcs
    end

    -- ofa = other face
    function S:compute_difference(ofa)
        -- assume ofa is totally inside self
        self.triangles = nil
        local nver = #self.vertices
        local ofan = #ofa.vertices

        local initial_vidx = -1
        for j=1,nver,1 do
            local l1 = auxmat.make_pmetric_line_2d(
                self.vertices[j], ofa.vertices[1])
            local collide, where, tc = self:does_line_collide(l1, false)
            if not collide then
                initial_vidx = j
                break
            end
        end

        if initial_vidx < 0 then
            print('No initial vidx found')
            return false
        end

        for i=1,ofan,1 do
            local ov = ofa.vertices[i]
            table.insert(self.vertices, initial_vidx+1, ov:clone())
        end
        table.insert(self.vertices, initial_vidx+1,
                ofa.vertices[1]:clone())
        table.insert(self.vertices, initial_vidx+2+ofan,
                self.vertices[initial_vidx]:clone())
    end

    -- calculates the normal and set it. self.vertices
    -- must have at least 3 vertices
    function S:calculate_normal()
        local tf = self.transform or default_face_transform_function
        local sv = self.vertices
        local v1 = tf(sv[2] - sv[1])
        local v2 = tf(sv[3] - sv[2])
        self.normal = v1:cross(v2):normlz()
    end

    -- fill the S.triangles table
    function S:compute_triangles()
        local tris = { }
        local vcs = { }

        local function ivc(a)
            return lmat.vec(-a[2], a[1])
        end

        local function wrpn(n, q)
            return ( (n-1) % q ) + 1
        end

        local function makevc(a, b)
            return lmat.vec(b[1]-a[1], b[2]-a[2])
        end

        for i,v in ipairs(self.vertices) do
            vcs[i] = v
        end

        local qt = #vcs
        while qt >= 3 do
            local found = false
            for idx=1,qt+3,1 do
                local i1 = wrpn(idx, qt)
                local i2 = wrpn(i1 + 1, qt)
                local i3 = wrpn(i2 + 1, qt)
                local v1 = vcs[i1]
                local v2 = vcs[i2]
                local v3 = vcs[i3]
                local dp = ivc(makevc(v1, v2)):dot(makevc(v1, v3))
                if dp > 0 then 
                    local va = makevc(v2, v3)
                    local vb = makevc(v3, v1)
                    local vc = makevc(v1, v2)
                    local okay = true
                    for i,v in ipairs(self.vertices) do
                        if not (rawequal(v, v1) or
                                rawequal(v, v2) or rawequal(v, v3))
                        then
                            local v_in_tri = 
                                  ((ivc(makevc(v1, v2)):dot(makevc(v1, v))) > 0)
                              and ((ivc(makevc(v2, v3)):dot(makevc(v2, v))) > 0)
                              and ((ivc(makevc(v3, v1)):dot(makevc(v3, v))) > 0)
                            if v_in_tri then
                                okay = false
                                break
                            end
                        end
                    end
                    if okay then
                        table.insert(tris, {
                            lmat.vec(v1[1], v1[2]),
                            lmat.vec(v2[1], v2[2]),
                            lmat.vec(v3[1], v3[2]) })
                        table.remove(vcs, i2)
                        found = true
                        break
                    end
                end
            end
            qt = #vcs
            if not found then
                print('not found, abort')
                break
            end
        end
        self.triangles = tris
        return self.triangles
    end
    

end)



