#include <SDL.h>
#include "screen.h"
#include "err.h"
#include <GL/gl.h>
#include "poly.h"
#include <glm/gtc/matrix_transform.hpp>


namespace WL {


class CScreen : public IScreen
{
public:
    SDL_Surface *m_screen;

    bool _create_screen(const dim_type &dim, int bpp) {
        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        m_screen = SDL_SetVideoMode(dim.first, dim.second, bpp, SDL_OPENGL);
        if (!m_screen) {
            WL::Err::gbye("Fail to set video mode: %s\n",
                    SDL_GetError());
            return false;
        }
    
        this->resize(dim);
        return true;
    }
    
    virtual void resize(const dim_type &dim) {
        glViewport(0, 0, (GLsizei)dim.first, (GLsizei)dim.second);
    }

    virtual void draw() {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glClearColor(0, 0, 0, 0);
        glClear(GL_COLOR_BUFFER_BIT);


        static IPoly *poly = NULL;
        if (!poly) {
            poly = IPoly::create_poly();
            poly->add_face(
                glm::vec2(-0.3, -0.3),
                glm::vec2(-0.3, 0.3),
                glm::vec2(0.3, 0.3)
            );
            poly->add_face(
                glm::vec2(-0.3, -0.3),
                glm::vec2(0.3, 0.3),
                glm::vec2(0.3, -0.3)
            );
        }

        static float th = 0.f;
        th += 0.22f;

        glColor3ub(0x59, 0x5B, 0xDE);

        poly->draw();

        glColor3ub(0x25, 0xE6, 0x52);
        glBegin(GL_LINES);
        glVertex2f(-1.0, 0.0);
        glVertex2f(1.0, 0.0);
        glEnd();

        glBegin(GL_LINES);
        glVertex2f(0.0, -1.0);
        glVertex2f(0.0, 1.0);
        glEnd();
    }

    virtual void flip_screen() {
        SDL_GL_SwapBuffers();
        // SDL_Flip(m_screen) ;
    }

};

IScreen *IScreen::create_screen(const dim_type &dim, int bpp) {
    CScreen *sc = new CScreen();
    if (! sc->_create_screen(dim, bpp)) {
        delete sc;
        return NULL;
    }
    return sc;
}

};



