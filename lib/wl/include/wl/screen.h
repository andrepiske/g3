#ifndef _WL_Screen_H_
#define _WL_Screen_H_

#include <wl/config.h>
#include <wl/scene.h>
#include <wl/math.h>
#include <utility>
#include <string>

wlStartNS
class IScreen
{
public:
    virtual ~IScreen() {}

    // static IScreen *create_screen(const Point2D &dim, int bpp);

    virtual void resize(const Point2D &dim) = 0;
    virtual Point2D get_size() const = 0;

    virtual void update_scene() = 0;

    virtual void draw() = 0;
    virtual void flip_screen() = 0;

    // call for OpenGL pixel perfection
    virtual void adjust_2d() = 0;
    // set the PROJECTION matrix to 2D orthogonal
    virtual void go_screen_2d() = 0;

    /**
    * Called before program exit.
    * No other methods will be called after this.
    */
    virtual void release() = 0;

    virtual void add_scene(const std::string &id, IScene *scene) = 0;
    virtual IScene *get_scene(const std::string &id) = 0;
    virtual std::string current_scene() const = 0; // get current scene

    // set current scene
    virtual void current_scene(const std::string &id, wlBool pause) = 0;
    virtual int unpause_scene() = 0;
    
public:
    /*
     pos and size are in window coordinates, but y is not inverted like
     in glScissor.
    */
    virtual void gl_scissor(const Vec2 &pos, const Vec2 &size) = 0;

    WLINLINE Vec2 scene_to_screen_coords(const Point2D &p,
            const IScene *s) const {
        const util::bounds<float> b = s->get_bounds();
        const Point2D sz = get_size();
        return Vec2(
            ((float)p.x / (float)sz.x) * (b.right-b.left) + b.left,
            ((float)p.y / (float)sz.y) * (b.bottom-b.top) + b.top);
    }


};

wlEndNS
#endif

