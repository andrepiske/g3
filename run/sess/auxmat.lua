require('lmat')
local LMAT = lmat
local math = math

module 'auxmat'

function make_pmetric_line_2d(a, b)
    local v = b - a
    return {
        { v[1], a[1] }, -- x { alpha, beta }
        { v[2], a[2] }, -- y [ alpha, beta }
    }
    -- line is { x=alpha_x*t + beta_x, y=alpha_y*t + beta_y }
end

function pmetric_coords(line, t)
    return line[1][1]*t + line[1][2], line[2][1]*t+line[2][2]
end

-- l0 and l1 are parametric lines returned by function 
-- 'make_pmetric_line_2d'
function line_intersect_2d(l0, l1)
    local A = LMAT.mat({ l0[1][1], -l1[1][1] },
                       { l0[2][1], -l1[2][1] })
    local b = LMAT.vec(l1[1][2] - l0[1][2],
                       l1[2][2] - l0[2][2])
    local m = A.obj:fullPivLuSolve(b.obj)
    local r = LMAT.vec(m:get(1, 1), m:get(2, 1))
    local u = {pmetric_coords(l0, r[1])}
    local v = {pmetric_coords(l1, r[2])}
    if math.abs(u[1] - v[1]) > 0.001 or
       math.abs(u[2] - v[2]) > 0.001 then
        return true
    end
    return r
end


