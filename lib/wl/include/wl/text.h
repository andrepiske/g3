#ifndef _WL_Text_H_
#define _WL_Text_H_

#include <wl/config.h>
#include <wl/sg2.h>
#include <string>
#include <wl/color.h>
#include <wl/font.h>

wlStartNS

class IText : public sg::Node2D
{
public:
    Color m_textcolor;
    IFont *m_font;

public:
    static IText *create_text(const std::string &text,
            const Color &color, IFont *font);

    virtual void set_text(const std::string &text) = 0;
    virtual std::string get_text() = 0;

    virtual void draw_self(IScreen *screen) = 0;

};

wlEndNS
#endif

