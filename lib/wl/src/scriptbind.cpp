#include <wl/scriptbind.h>
#include <wl/util.h>
#include <wl/log.h>
#include <boost/unordered_map.hpp>

wlStartNS

class CScriptBind : public IScriptBind
{
public:
    typedef boost::unordered_map< std::string, Variant* > BindMap;
    struct Bind_S {
        void *obj; // it's very important that this member is the first
        CScriptBind *cs;
        wlBool is_refcounted;
    };

    // name of the metatable variable for lua
    std::string m_mtname;
    // the bind map!
    BindMap m_bindmap;

    static int _cb_index(IScript *s) {
        std::string _s = s->to_string(-1);
        Bind_S *data = (Bind_S*) s->to_userdata(-2);
        s->pop(2);
        BindMap::iterator iter = data->cs->m_bindmap.find(_s);
        if (iter != data->cs->m_bindmap.end()) {
            iter->second->_v_scriptpush(s);
            return 1;
        }

        WL_LOG_WARN("Name '%s' not found no CScriptBind:Index",
                _s.c_str());
        s->push_nil();
        return 1;
    }

    static int _cb_newindex(IScript *s) {
        std::string _name = s->to_string(-2);
        if (!_name.size()) {
            WL_LOG_WARN("Cannot set zero-length name!", 0);
            return 0;
        }
        Bind_S *data = (Bind_S*) s->to_userdata(-3);
        Variant *value = new Variant();
        if (!value->_v_scriptget(s, -1)) {
            WL_LOG_ERROR("Cannot set '%s': not value", _name.c_str());
            return 0;
        }
        BindMap::iterator it = data->cs->m_bindmap.find(_name);
        if (it == data->cs->m_bindmap.end())
            WL_LOG_ERROR("Not field '%s' to set index", _name.c_str());
        it->second->_v_copyfrom(*value);
        return 0;
    }

    static int _cb_gc(IScript *s) {
        /*
        Bind_S *data = (Bind_S*) s->to_userdata(-1);
        BindMap::iterator it = data->cs->m_bindmap.begin();
        for (; it != data->cs->m_bindmap.end(); ++it)
            it->second->rc_pop();
        if (data->is_refcounted)
            ((RefCounted*)data->obj)->rc_pop();
        delete data->cs;
        return 0;
        */
        return 0;
    }

    CScriptBind() {
        m_mtname = "___WL_MT_SCRIPTBIND_";
    }

    virtual ~CScriptBind() {
    }

    void create_metatable(IScript *s) {
        s->new_table();
        s->push_cclosure(&CScriptBind::_cb_index, 0);
        s->setfield(-2, "__index");
        s->push_cclosure(&CScriptBind::_cb_newindex, 0);
        s->setfield(-2, "__newindex");
        s->push_cclosure(&CScriptBind::_cb_gc, 0);
        s->setfield(-2, "__gc");
    }

public:
    virtual void set_field(const std::string &name, Variant *value) {
        /*
        BindMap::iterator it = m_bindmap.find(name);
        value->rc_push();
        if (it != m_bindmap.end())
            it->second->rc_pop();
        m_bindmap[name] = value;
        */
    }

    void _bind(IScript *s, void *obj, wlBool refcounted) {
        /*
        Bind_S *b = (Bind_S*)s->new_userdata(sizeof(Bind_S));
        b->obj = obj;
        b->is_refcounted = refcounted;
        b->cs = this;
        create_metatable(s);
        s->setmetatable(-2);
        if (refcounted)
            ((RefCounted*)obj)->rc_push();
            */
    }
    
    virtual void bindrefcounted(IScript *s, RefCounted *object) {
        this->_bind(s, object, wlTrue);
    }

    virtual void bind(IScript *s, void *object) {
        this->_bind(s, object, wlFalse);
    }

};

IScriptBind *IScriptBind::create_bind() {
    return new CScriptBind();
}

wlEndNS


