#ifndef _WL_Scriptable_H_
#define _WL_Scriptable_H_

#include <wl/refcounted.h>
#include <lua.hpp>

wlStartNS

namespace scriptable {

typedef void(*PushProcT)(lua_State*, RefCounted*);

void push(lua_State *L, RefCounted *object);
void register_pushproc(RefCounted *object, PushProcT proc);
void unregister_pushproc(RefCounted *object);

template<class T>
class RegPush
{
private:
    RefCounted *_self;
    
public:
    RegPush(RefCounted *self, void(*regproc)(lua_State*L, T*)) {
        _self = self;
        register_pushproc(_self, (PushProcT)regproc);
    }

    virtual ~RegPush() {
        unregister_pushproc(_self);
    }

};


} // NS scriptable

wlEndNS

#endif


