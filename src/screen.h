#ifndef _screen_h_
#define _screen_h_

#include <utility>

namespace WL 
{

class IScreen
{
public:
    virtual ~IScreen() {}

    typedef std::pair<int, int> dim_type;

    static IScreen *create_screen(const dim_type &dim, int bpp);

    virtual void resize(const dim_type &dim) = 0;

    virtual void draw() = 0;
    virtual void flip_screen() = 0;

};

}; // namespace WL

#endif

