#ifndef _WL_GLstate_H_
#define _WL_GLstate_H_

#include <wl/config.h>
#include <wl/log.h>
#include <GL/gl.h>

wlStartNS

template<class Traits>
class GLStateBase
{
public:
    typedef typename Traits::KeyType KeyType;
    typedef typename Traits::ValueType ValueType;
    typedef Traits TraitsType;

protected:
    wlBool m_want_new_value;
    KeyType m_state;
    KeyType m_get_key;
    ValueType m_original_value;
    ValueType m_new_value;

public:
    GLStateBase(KeyType key, KeyType get_key, ValueType value) {
        m_want_new_value = wlTrue;
        m_state = key;
        m_get_key = get_key;
        m_new_value = value;
        m_original_value = TraitsType::gl_get(get_key);
        TraitsType::gl_set(key, value);
    }

    GLStateBase(KeyType key, ValueType value) {
        m_want_new_value = wlTrue;
        m_get_key = m_state = key;
        m_new_value = value;
        m_original_value = Traits::gl_get(key);
        TraitsType::gl_set(key, value);
    }

    ~GLStateBase() {
        if (m_want_new_value)
            pop();
    }

    WLINLINE void pop() {
        TraitsType::gl_set(m_state, m_original_value);
        m_want_new_value = wlFalse;
        // TODO: check
    }

    WLINLINE void push() {
        m_want_new_value = wlTrue;
        m_original_value = TraitsType::gl_get(m_state);
        TraitsType::gl_set(m_state, m_new_value);
        // TODO: check
    }
};

class GLCapsTraits
{
public:
    typedef GLenum KeyType;
    typedef wlBool ValueType;

    WLINLINE static void gl_set(KeyType k, ValueType value) {
        if (value)
            glEnable(k);
        else
            glDisable(k);
    }

    WLINLINE static ValueType gl_get(KeyType k) {
        GLboolean v;
        glGetBooleanv(k, &v);
        return v ? wlTrue : wlFalse;
    }

};

class GLEnable : public GLStateBase<GLCapsTraits>
{
public:
    GLEnable(GLenum cap) :
        GLStateBase(cap, wlTrue) {}
};

class GLDisable : public GLStateBase<GLCapsTraits>
{
public:
    GLDisable(GLenum cap) :
        GLStateBase(cap, wlFalse) {}
};

//////////////////////////////////////////////////////////////////////
// Matrix
//////////////////////////////////////////////////////////////////////

class GLMatrixPushTraits
{
public:
    struct float_array {
        GLfloat v[16]; 
    };
    typedef GLenum KeyType;
    typedef float_array ValueType;

    WLINLINE static GLint gl_current_matrix_mode() {
        GLint m;
        glGetIntegerv(GL_MATRIX_MODE, &m);
        return m;
    }

    WLINLINE static void gl_set(KeyType k, ValueType &value) {
        GLint matrix_mode;
        glGetIntegerv(GL_MATRIX_MODE, &matrix_mode);
        glMatrixMode(k);
        glLoadMatrixf(value.v);
        glMatrixMode(matrix_mode);
    }

    WLINLINE static ValueType gl_get(KeyType k) {
        GLenum e = 0;
        ValueType v;
        switch (k) {
            case GL_MODELVIEW:
                e = GL_MODELVIEW_MATRIX;
                break;
            case GL_PROJECTION:
                e = GL_PROJECTION_MATRIX;
                break;
            case GL_TEXTURE:
                e = GL_TEXTURE_MATRIX;
                break;
            case GL_COLOR:
                e = GL_COLOR_MATRIX;
                break;
            default:
                WL_LOG_ERROR("Invalid matrix to push: %d", (int)k);
        }

        glGetFloatv(e, v.v);
        return v;
    }

};

class GLMatrixPush
{
protected:
    typedef GLMatrixPushTraits Traits;
    Traits::ValueType m_value;
    Traits::KeyType m_key;
    wlBool m_want_back;

public:
    GLMatrixPush(GLenum matrix = Traits::gl_current_matrix_mode()) {
        m_key = matrix;
        m_value = Traits::gl_get(m_key);
        m_want_back = wlTrue;
    }
    
    ~GLMatrixPush() {
        if (m_want_back)
            pop(); 
    }

    WLINLINE void pop() {
        m_want_back = wlFalse;
        Traits::gl_set(m_key, m_value);
    }

    WLINLINE void push() {
        m_want_back = wlTrue;
        m_value = Traits::gl_get(m_key);
    }

};

template<class S>
class GLIntegerPushTraits
{
public:
    typedef GLenum KeyType;
    typedef GLint ValueType;

    WLINLINE static void gl_set(KeyType k, ValueType v) {
        S::gl_set(k, v);
    }

    WLINLINE static ValueType gl_get(KeyType k) {
        GLint v;
        glGetIntegerv(k, &v);
        return v;
    }

};

template<class S, GLenum e, GLenum ge>
class GLIntegerPush : public GLStateBase<
                      GLIntegerPushTraits<S> >
{
public:
    GLIntegerPush(GLint v) :
        GLStateBase<GLIntegerPushTraits<S> >(e, ge, v) {
    }
};

///////////////////////////////////////////////////////////////////////////////
// Matrix Mode
///////////////////////////////////////////////////////////////////////////////

class GLMatrixModePushTraits
{
public:
    WLINLINE static void gl_set(GLenum k, GLint v) {
        glMatrixMode(v);
    }
};

typedef GLIntegerPush<GLMatrixModePushTraits, 0, GL_MATRIX_MODE>
    GLMatrixModePush;

///////////////////////////////////////////////////////////////////////////////
// Texture
///////////////////////////////////////////////////////////////////////////////

class GLTexturePushTraits
{
public:
    WLINLINE static void gl_set(GLenum e, GLint v) {
        glBindTexture(e, (GLuint)v);
    }
};

typedef GLIntegerPush<GLTexturePushTraits,
    GL_TEXTURE_1D, GL_TEXTURE_BINDING_1D> GLBindTexture1D;
typedef GLIntegerPush<GLTexturePushTraits,
    GL_TEXTURE_2D, GL_TEXTURE_BINDING_2D> GLBindTexture2D;
typedef GLIntegerPush<GLTexturePushTraits,
    GL_TEXTURE_3D, GL_TEXTURE_BINDING_3D> GLBindTexture3D;
wlEndNS

#endif


