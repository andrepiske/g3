#ifndef _WL_Resource_H_
#define _WL_Resource_H_

#include <wl/config.h>
#include <wl/refcounted.h>
#include <string>
#include <vector>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <wl/fileio.h>

wlStartNS
// TODO: implement a caching method.

class Resource;

class ResourceIOBlock
{
protected:
    IFileIO *m_file;

public:
    ResourceIOBlock() {
        m_file = wlNull;
    }

    ResourceIOBlock(IFileIO *file) {
        if (!file)
            WL_LOG_ERROR("Cannot call ResourceIOBlock with NULL param", 0);

        m_file = file;
        m_file->rc_push();
    }

    ~ResourceIOBlock() {
        if (m_file)
            m_file->rc_pop();
    }

    ResourceIOBlock(const ResourceIOBlock &c) {
        this->operator=(c);
    }

    ResourceIOBlock &operator=(const ResourceIOBlock &c) {
        m_file = c.m_file;
        if (m_file)
            m_file->rc_push();
        return *this;
    }

    wlBool valid() {
        return m_file ? wlTrue : wlFalse;
    }

    IFileIO *file() {
        return m_file;
    }

};


class Resource : private boost::noncopyable, public RefCounted
{
protected:
    void __resource_deleted(Resource *rs);

public:
    // resource types
    enum resource_type {
        RST_UNKNOWN = 0,
        RST_ARCHIVE,
        RST_IMAGE,
        RST_SOUNDFX,
        RST_MUSIC,
        RST_FONT
    };

    typedef std::vector<Resource*> PVecType;

    /* resource name (not the full name) */
    std::string m_resname;
    /* block where this resource can be loaded */
    ResourceIOBlock m_resblock;
    /* parent resource (optional) */
    Resource *m_resparent;
    /* the type of resource */
    resource_type m_restype;
    /* children resources */
    PVecType m_reschildren;

public:
    Resource() {
        m_resname = "<nameless resource>";
        m_resparent = wlNull;
        m_restype = RST_UNKNOWN;
    }

    /**
     * Deleting a resource will delete all its children.
     * When a resource is deleted, it will remove itself from its parent.
     */
    virtual ~Resource();

    /* The resource must load itself.  */
    virtual void load_data() = 0;

    /* The resource is going to sleep.  */
    virtual void unload_data() = 0; 

    std::string get_full_path() const;

    Resource *get_root();

};

wlEndNS


#endif

