#ifndef __G3_SWEEPGENERATOR_H__
#define __G3_SWEEPGENERATOR_H__

#include <wl/config.h>
#include <wl/math.h>
#include <vector>
#include <wl/scenegraph.h>
#include <wl/refcounted.h>

namespace G3 {

class ICVMesh;
class ITool;
class ICurve;

struct SweepInfo {
    struct axis_t {
        WL::Vec3 pos;
        WL::Vec3 x, y, z;
    };
    typedef std::vector<axis_t> axes_t;
    axes_t axes;
    WL::sg::Node *drawing;
};

class ISweepGenerator : public WL::RefCounted
{
public:
    struct QualityParameters {
        double curve_dt;
        unsigned int number_steps;
    };

public:
    virtual void set_tool(ITool *tool) = 0;

    virtual void set_quality_parameters(QualityParameters &qa) = 0;

    virtual ICVMesh *make_sweep_mesh(SweepInfo **si,
            double from, double to,
            const WL::Vec3 &offset = WL::Vec3(0, 0, 0)) = 0;

    virtual ICVMesh *make_base_cylinder(double height,
            const WL::Vec3 &offset = WL::Vec3(0, 0, 0)) = 0;

    virtual ICVMesh *make_base_cylinder_fromtop(const WL::Vec3 &tip_pos) = 0;

    virtual void set_curve(ICurve *curve) = 0;
    virtual ICurve *get_curve() = 0;

    static ISweepGenerator *create();
};

} // NS G3

#endif




