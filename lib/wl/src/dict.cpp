#include <wl/dict.h>

wlStartNS

Dict::Dict() {
}

Dict::~Dict() {
}

wlBool Dict::has_key(Quark index) {
	return (m_values.end() != m_values.find(index));
}

std::string Dict::get_string(Quark index) {
	DictMap::iterator it = m_values.find(index);
	if (it == m_values.end())
		return std::string();
	return (const char*)it->second.data.ptr;
}

Quark Dict::get_string_quark(Quark index) {
	DictMap::iterator it = m_values.find(index);
	if (it == m_values.end())
		return 0;
	return quark_from_string((const char*)it->second.data.ptr);
}

u64 Dict::get_u64(Quark index) {
	DictMap::iterator it = m_values.find(index);
	if (it == m_values.end())
		return 0;
	return it->second.data.v_u64;
}

RefCounted* Dict::get_rc(Quark index) {
	DictMap::iterator it = m_values.find(index);
	if (it == m_values.end())
		return 0;
	return it->second.data.rc;
}

void* Dict::get_ptr(Quark index) {
	DictMap::iterator it = m_values.find(index);
	if (it == m_values.end())
		return 0;
	return it->second.data.ptr;
}

void Dict::set_string(Quark index, const std::string &n) {
	const char *s = ::strdup(n.c_str());
	DictItem t;
	t.type = DT_STRING;
	t.data.ptr = (void*)s;
	m_values[index] = t;
}

void Dict::set_set_u64(Quark index, u64 value) {
	DictItem t;
	t.type = DT_U64;
	t.data.v_u64 = value;
	m_values[index] = t;
}

void Dict::set_rc(Quark index, RefCounted *rc) {
	rc->rc_push();
	set_rc_nopush(index, rc);
}

void Dict::set_rc_nopush(Quark index, RefCounted *rc) {
	DictItem t;
	t.type = DT_RC;
	t.data.rc = rc;
	m_values[index] = t;
}

void Dict::set_ptr(Quark index, void *ptr) {
	DictItem t;
	t.type = DT_PTR;
	t.data.ptr = ptr;
	m_values[index] = t;
}

wlEndNS

