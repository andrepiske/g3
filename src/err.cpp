#include <iostream>
#include <stdlib.h>
#include "err.h"
#include <stdio.h>
#include <stdarg.h>
#include <sstream>

static WL::Err *g_err = NULL;

WL::Err*
WL::Err::get_inst() {
    if (!g_err)
        g_err = new Err();
    return g_err;
}

void
WL::Err::info(const char *msg, ...) {
    va_list a;
    va_start(a, msg);
    std::stringstream s;
    s << "INFO: " << msg << std::endl;
    vfprintf(stdout, s.str().c_str(), a);
    va_end(a);
}

void
WL::Err::fail(const char *msg, ...) {
}

void
WL::Err::gbye(const char *msg, ...) {
    
}


