#include "precompiled.h"
#include <wl/poly.h>
#include <wl/glstate.h>
#include <wl/log.h>
#include <wl/image.h>
#include <limits>

wlStartNS

class CPoly : public IPoly
{
public:

    CPoly() {
        m_drawmode = DM_FULL;
    }

    virtual ~CPoly() {
        ImageVec::iterator it = m_textures.begin();
        for (; it != m_textures.end(); ++it)
            (*it)->rc_pop();
    }

public:
    // DM_FULL, DM_WIREFRAME
    int m_drawmode;

    PolyFaceVec m_faces;
    ColorVec m_colors;
    Vec3Vec m_vertices;
    TexMapVec m_texmaps;
    ImageVec m_textures;

    virtual Vec3Vec *get_verticesptr() {
        return &m_vertices;
    }

    virtual ColorVec *get_colorsptr() {
        return &m_colors;
    }

    virtual TexMapVec *get_texmapsptr() {
        return &m_texmaps;
    }

    virtual void copy_to(IPoly *poly) {
        WL_LOG_ERROR("Poly copy_to not implemented", 0);
        /*
        CPoly *c = (CPoly*)poly;
        c->m_faces = PolyFaceVec(m_faces);
        c->m_vertices = Vec2Vec(m_vertices);
        c->m_colors = ColorVec(m_colors);
        c->m_texmaps = TexMapVec(m_texmaps);
        */
    }

    /*
    virtual void add_face(const b2Vec2& a, const b2Vec2& b,
            const b2Vec2& c) {

        int n = (int)m_vertices.size();
        PolyFace face;

        m_vertices.push_back(a);
        m_vertices.push_back(b);
        m_vertices.push_back(c);

        face.f[0] = n;
        face.f[1] = n+1;
        face.f[2] = n+2;

        m_faces.push_back(face);
    }
    */

    virtual void apply_transform(const Mat22 &mat, int to_) {
        Mat33 m = Mat33(
            Vec3(mat.u.x, mat.u.y, 0.f),
            Vec3(mat.v.x, mat.v.y, 0.f),
            Vec3(0.f, 0.f, 1.f));
        apply_transform(m, to_);
    }

    virtual void apply_transform(const Mat33 &mat, int to_) {
        if (to_ & TR_VERTICES) {
            Vec3Vec::iterator it;
            for (it = m_vertices.begin(); it != m_vertices.end(); ++it)
                *it = mul33(mat, *it);
        }
        if (to_ & TR_TEXMAPS) {
            TexMapVec::iterator it;
            for (it = m_texmaps.begin(); it != m_texmaps.end(); ++it)
                it->coord = mul33_1(mat, it->coord);
        }
    }

    virtual void apply_transform(const Mat44 &mat) {
        Vec3Vec::iterator it;
        for (it = m_vertices.begin(); it != m_vertices.end(); ++it)
            *it = mul44_1(mat, *it);
    }

    virtual void draw(IScreen *s) {
        const int dm = m_drawmode;
        PolyFaceVec::iterator it;
        for (it = m_faces.begin(); it != m_faces.end(); ++it) {
            glBegin(dm == DM_WIREFRAME ? GL_LINE_LOOP : GL_TRIANGLES);

            const PolyFace *face = &(*it);
            for (int i=0; i < 3; ++i) {
                const Vec3 &v = m_vertices[face->vert[i]];
                int color_id = face->color[i];
                if (color_id >= 0)
                    m_colors[color_id].gl_set();
                int txmap_id = face->texmap[i];
                if (txmap_id >= 0)
                    glTexCoord2fv((GLfloat*)&m_texmaps[txmap_id]);
                glVertex3f(v.x, v.y, v.z); 
            }
            glEnd();
        }
    }

    virtual void set_drawmode(int mode) {
        m_drawmode = mode;
    }

    virtual int get_drawmode() {
        return m_drawmode;
    }

    virtual void update() {
    }

    virtual void set_poly(Vec3Vec *v, ColorVec *co, TexMapVec *tx) {
        if (v)
            m_vertices = *v;
        if (co)
            m_colors = *co;
        if (tx)
            m_texmaps = *tx;
    }

    virtual PolyFaceVec *get_facesptr() {
        return &m_faces;
    }

    virtual ImageVec* get_texturesptr() {
        return &m_textures; 
    }

    virtual void set_textures(ImageVec *v) {
        ImageVec::iterator it;
        for (it = m_textures.begin(); it != m_textures.end(); ++it)
            (*it)->rc_pop();
        m_textures.clear();
        if (v) {
            for (it = v->begin(); it != v->end(); ++it) {
                (*it)->rc_push();
                m_textures.push_back(*it);
            }
        }
    }

    virtual void set_faces(PolyFaceVec *v) {
        if (v)
            m_faces = *v;
    }

    virtual BBox3 get_bb3() const {
        const float maxfloat = std::numeric_limits<float>::max();
        const float minfloat = std::numeric_limits<float>::min();
        BBox3 bb(Vec3(maxfloat, maxfloat, maxfloat),
            Vec3(minfloat, minfloat, minfloat));
        PolyFaceVec::const_iterator it = m_faces.begin();
        for (; it != m_faces.end(); ++it) {
            for (int i = 0; i < 3; ++i) {
                const Vec3 &v = m_vertices[it->vert[i]];
                if (v.x < bb.low.x) bb.low.x = v.x;
                if (v.y < bb.low.y) bb.low.y = v.y;
                if (v.z < bb.low.z) bb.low.z = v.z;
                if (v.x > bb.high.x) bb.high.x = v.x;
                if (v.y > bb.high.y) bb.high.y = v.y;
                if (v.z > bb.high.z) bb.high.z = v.z;
            }
        }
        return bb;
    }

    /*
    virtual util::bounds<float> get_bb() {
        // FIXME: fix these values (min. float, max. float)
        util::bounds<float> b(10000.f, -10000.f, -10000.f, 10000.f);
        PolyFaceVec::iterator it = m_faces.begin();
        for (; it != m_faces.end(); ++it) {
            for (int i=0; i < 3; ++i) {
                const Vec2 &v = m_vertices[ it->vert[i] ];
                if (v.x < b.left)
                    b.left = v.x;
                if (v.x > b.right)
                    b.right = v.x;
                if (v.y > b.bottom)
                    b.bottom = v.y;
                if (v.y < b.top)
                    b.top = v.y;
            }
        }
        return b;
    }
    */

};

IPoly *IPoly::create_poly() {
    return new CPoly();
}

///////////////////////////////////////////////////////////////////////////////
// IPolyNode
///////////////////////////////////////////////////////////////////////////////

class CPolyNode2D : public IPolyNode2D
{
public:
    IPoly *m_poly;

public:
    CPolyNode2D() {
        m_poly = wlNull;
    }

    virtual ~CPolyNode2D() {
        if (m_poly)
            m_poly->rc_pop();
    }

    virtual void set_poly(IPoly *poly) {
        if (m_poly)
            m_poly->rc_pop();
        m_poly = poly;
        if (m_poly)
            m_poly->rc_push();
    }

    virtual IPoly *get_poly() {
        return m_poly;
    }

    virtual void draw_self(IScreen *s) {
        if (!m_poly)
            return;

        m_poly->draw(s);
    }

};

IPolyNode2D *IPolyNode2D::create(IPoly *poly) {
    CPolyNode2D *node = new CPolyNode2D();
    node->m_poly = poly;
    if (poly)
        poly->rc_push();
    return node;
}

wlEndNS

