#include <wl/ar.h>
#include <boost/filesystem.hpp>
#include <boost/mpl/copy.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/back_inserter.hpp>
#include <iostream>
#include <wl/fileio.h>

wlStartNS

namespace bfs = ::boost::filesystem;
namespace mpl = ::boost::mpl;

class CAr : public IAr
{
public:
    typedef std::vector<std::string> NameVec;

    /* this could be an archive or a path */
    std::string m_path;

    /* Whether is a directory(true) or a compressed file(false) */
    wlBool m_directory;

    NameVec m_names;

    void load_data() {
        _update_names();
    }

    void unload_data() {
    }

    void _update_names() {
        m_names.clear();
        bfs::path path = bfs::path(m_path);

        // TODO: shouldn't it support symlinks?
        bfs::recursive_directory_iterator it(path);
        bfs::recursive_directory_iterator end;
        int curpath_len = m_path.length();
        for (; it != end; ++it)
            m_names.push_back(std::string(it->path().c_str())
                .substr(curpath_len+1,1<<20));
    }

    void _load_file(const std::string &path) {
        bfs::path bp = path;
        m_directory = bfs::is_directory(bp) ? wlTrue : wlFalse;
        m_path = bp.c_str();
        _update_names();
    }

    ::WL::Resource *get_resource(const std::string &path) {
        return wlNull;    
    }

    ResourceIOBlock _find_block_dir(const std::string &path) {
        NameVec::iterator it = m_names.begin();
        for (; it != m_names.end(); ++it) {
            if (path == *it) {
                bfs::path fullpath = m_path;
                fullpath /= path;
                IFileIO *f = IFileIO::open_file(fullpath.c_str(),
                        IFileIO::M_READ);
                if (!f)
                    break;
                return ResourceIOBlock(f);
            }
        }
        return ResourceIOBlock();
    }

    virtual ResourceIOBlock find_block(const std::string &path) {
        return m_directory ? _find_block_dir(path) : ResourceIOBlock();
    }

};

IAr *IAr::load_from_file(const std::string &path) {
    CAr *ar = new CAr();
    ar->_load_file(path);
    return ar;
}

wlEndNS

