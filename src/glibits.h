#ifndef __G3_GLIB_ITS_H__
#define __G3_GLIB_ITS_H__

// GLIB Inter Thread Signaling
#include <glib-object.h>
#include <boost/function.hpp>

namespace G3 {
    void call_function_in_main_thread(boost::function<void(void*)> f,
            void *userdata, int priority = G_PRIORITY_DEFAULT);
};

#endif


