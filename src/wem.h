#ifndef _G3_WEM_H_
#define _G3_WEM_H_

#include <wl/scenegraph.h>

namespace G3 {

class IWEMesh : public WL::sg::Node
{
public:


    static IWEMesh *create();

};

};

#endif


