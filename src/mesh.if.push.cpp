/* This is a generated file, do not edit it */
#include <boost/unordered_map.hpp>
#include <lua.hpp>
#include <wl/scriptable.h>
#include "mesh.if.h"


static
int _G3_IMesh__closure(lua_State *L) {
    const int proc_id = lua_tonumber(L, lua_upvalueindex(1));
    G3::IMesh *obj = *(G3::IMesh**)luaL_checkudata(L, 1, "WL-G3::IMesh");
    switch (proc_id) {
        case 1: { // set_vertex_color
            int _valid[] = {0,0,0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            _valid[3] = lua_isnumber(L, 5);
            _valid[4] = lua_isnumber(L, 6);
            int v_index;
            int v_r;
            int v_g;
            int v_b;
            int v_a;
            v_index = lua_tonumber(L, 2);
            v_r = lua_tonumber(L, 3);
            v_g = lua_tonumber(L, 4);
            v_b = lua_tonumber(L, 5);
            v_a = lua_tonumber(L, 6);
            obj->set_vertex_color(v_index, v_r, v_g, v_b, v_a);            
            return 0;
        }
        case 2: { // set_vertex
            int _valid[] = {0,0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            _valid[3] = lua_isnumber(L, 5);
            int v_index;
            double v_x;
            double v_y;
            double v_z;
            v_index = lua_tonumber(L, 2);
            v_x = lua_tonumber(L, 3);
            v_y = lua_tonumber(L, 4);
            v_z = lua_tonumber(L, 5);
            obj->set_vertex(v_index, v_x, v_y, v_z);            
            return 0;
        }
        case 3: { // set_face_normal
            int _valid[] = {0,0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            _valid[3] = lua_isnumber(L, 5);
            int v_index;
            double v_x;
            double v_y;
            double v_z;
            v_index = lua_tonumber(L, 2);
            v_x = lua_tonumber(L, 3);
            v_y = lua_tonumber(L, 4);
            v_z = lua_tonumber(L, 5);
            obj->set_face_normal(v_index, v_x, v_y, v_z);            
            return 0;
        }
        case 4: { // get_vertex_color
            int _valid[] = {0,0,0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            _valid[3] = lua_isnumber(L, 5);
            _valid[4] = lua_isnumber(L, 6);
            int v_index;
            int v_r;
            int v_g;
            int v_b;
            int v_a;
            v_index = lua_tonumber(L, 2);
            if (_valid[1]) {
                v_r = lua_tonumber(L, 3);
            }
            if (_valid[2]) {
                v_g = lua_tonumber(L, 4);
            }
            if (_valid[3]) {
                v_b = lua_tonumber(L, 5);
            }
            if (_valid[4]) {
                v_a = lua_tonumber(L, 6);
            }
            obj->get_vertex_color(v_index, _valid[1]?((&v_r)):0, _valid[2]?((&v_g)):0, _valid[3]?((&v_b)):0, _valid[4]?((&v_a)):0);            
            lua_pushnumber(L, v_r);
            lua_pushnumber(L, v_g);
            lua_pushnumber(L, v_b);
            lua_pushnumber(L, v_a);
            return 4;
        }
        case 5: { // set_num_vertices
            int _valid[] = {0,0};
            _valid[0] = lua_isnumber(L, 2);
            int v_num;
            v_num = lua_tonumber(L, 2);
            obj->set_num_vertices(v_num);            
            return 0;
        }
        case 6: { // num_faces
            int _valid[] = {0};
            int _v_ret;
            _v_ret = obj->num_faces();            
            lua_pushnumber(L, _v_ret);
            return 1;
        }
        case 7: { // get_face
            int _valid[] = {0,0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            _valid[3] = lua_isnumber(L, 5);
            int v_index;
            int v_a;
            int v_b;
            int v_c;
            v_index = lua_tonumber(L, 2);
            if (_valid[1]) {
                v_a = lua_tonumber(L, 3);
            }
            if (_valid[2]) {
                v_b = lua_tonumber(L, 4);
            }
            if (_valid[3]) {
                v_c = lua_tonumber(L, 5);
            }
            obj->get_face(v_index, _valid[1]?((&v_a)):0, _valid[2]?((&v_b)):0, _valid[3]?((&v_c)):0);            
            lua_pushnumber(L, v_a);
            lua_pushnumber(L, v_b);
            lua_pushnumber(L, v_c);
            return 3;
        }
        case 8: { // set_num_faces
            int _valid[] = {0,0};
            _valid[0] = lua_isnumber(L, 2);
            int v_num;
            v_num = lua_tonumber(L, 2);
            obj->set_num_faces(v_num);            
            return 0;
        }
        case 9: { // autoset_face_normal
            int _valid[] = {0,0};
            _valid[0] = lua_isnumber(L, 2);
            int v_index;
            v_index = lua_tonumber(L, 2);
            obj->autoset_face_normal(v_index);            
            return 0;
        }
        case 10: { // set_face
            int _valid[] = {0,0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            _valid[3] = lua_isnumber(L, 5);
            int v_index;
            int v_a;
            int v_b;
            int v_c;
            v_index = lua_tonumber(L, 2);
            v_a = lua_tonumber(L, 3);
            v_b = lua_tonumber(L, 4);
            v_c = lua_tonumber(L, 5);
            obj->set_face(v_index, v_a, v_b, v_c);            
            return 0;
        }
        case 11: { // get_vertex
            int _valid[] = {0,0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            _valid[3] = lua_isnumber(L, 5);
            int v_index;
            double v_x;
            double v_y;
            double v_z;
            v_index = lua_tonumber(L, 2);
            if (_valid[1]) {
                v_x = lua_tonumber(L, 3);
            }
            if (_valid[2]) {
                v_y = lua_tonumber(L, 4);
            }
            if (_valid[3]) {
                v_z = lua_tonumber(L, 5);
            }
            obj->get_vertex(v_index, _valid[1]?((&v_x)):0, _valid[2]?((&v_y)):0, _valid[3]?((&v_z)):0);            
            lua_pushnumber(L, v_x);
            lua_pushnumber(L, v_y);
            lua_pushnumber(L, v_z);
            return 3;
        }
        case 12: { // num_vertices
            int _valid[] = {0};
            int _v_ret;
            _v_ret = obj->num_vertices();            
            lua_pushnumber(L, _v_ret);
            return 1;
        }
    }
    return 0;
}

static
int _G3_IMesh__gc(lua_State *L) {
    G3::IMesh *ptr = *(G3::IMesh**)lua_touserdata(L, 1);
    ptr->rc_pop();
    return 0;
}

void _G3_IMesh__regMT(lua_State *L) {
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, _G3_IMesh__gc);
    lua_setfield(L, -2, "__gc");
    lua_pushnumber(L, 1);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "set_vertex_color");
    lua_pushnumber(L, 2);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "set_vertex");
    lua_pushnumber(L, 3);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "set_face_normal");
    lua_pushnumber(L, 4);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "get_vertex_color");
    lua_pushnumber(L, 5);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "set_num_vertices");
    lua_pushnumber(L, 6);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "num_faces");
    lua_pushnumber(L, 7);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "get_face");
    lua_pushnumber(L, 8);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "set_num_faces");
    lua_pushnumber(L, 9);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "autoset_face_normal");
    lua_pushnumber(L, 10);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "set_face");
    lua_pushnumber(L, 11);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "get_vertex");
    lua_pushnumber(L, 12);
    lua_pushcclosure(L, _G3_IMesh__closure, 1);
    lua_setfield(L, -2, "num_vertices");
}

void _G3_IMesh__luapush(lua_State *L, WL::RefCounted *_obj) {
    G3::IMesh *obj = dynamic_cast<G3::IMesh*>(_obj);
    *(void**)lua_newuserdata(L, sizeof(void*)) = (void*)obj;
    if (luaL_newmetatable(L, "WL-G3::IMesh"))
        _G3_IMesh__regMT(L);
    lua_setmetatable(L, -2);
    obj->rc_push();
}


