#include "wem.h"
#include <vector>
#include <wl/math.h>
#include <GL/gl.h>
#include <stdarg.h>

#define EDGE_CCWE_FRONT(t) ((t).ccwe[1])
#define EDGE_CCWE_BACK(t) ((t).ccwe[0])
#define EDGE_CWE_FRONT(t) ((t).cwe[1])
#define EDGE_CWE_BACK(t) ((t).cwe[1])

#define EDGE_FACE_CCWE(t) ((t).face[1])
#define EDGE_FACE_CWE(t) ((t).face[0])

namespace G3 {

using namespace WL;

enum side_t {
    Side_Left,
    Side_Right
};

typedef unsigned int vid_t;

struct Edge;

struct Face {
    Edge *edge;
    Face() : edge(0) {
    }
};

struct Edge {
    vid_t v_front, v_back; // Edge goes from BACK to FRONT.
    Face *face[2];
    Edge *cwe[2], *ccwe[2];

    Edge() :
        v_front((vid_t)-1), v_back((vid_t)-1)
    {
        face[0] = face[1] = 0;
        cwe[0] = cwe[1] = ccwe[0] = ccwe[1] = 0;
    }
};

class CWEMesh : public IWEMesh
{
public:
    typedef std::vector<Vec3> Vec3Vec;
    typedef std::vector<Face> FaceVec;

    Vec3Vec m_vertices;
    FaceVec m_faces;

    CWEMesh() {
    }
    
    Vec3 get_face_normal(const Face *face) {
        return Vec3(0,0,0);
    }

    vid_t get_vertex(const Vec3 &v) {
        // TODO make a decent allocation
        m_vertices.push_back(v);
        return (vid_t)(m_vertices.size()-1);
    }

    Edge *get_edge(const vid_t from, const vid_t to) {
        // TODO make a decent allocation
        Edge *e = new Edge();
        e->v_back = from;
        e->v_front = to;
        return e;
    }

    Face *build_face_ccw(Edge *e_initial) {
        Face *face;
        wlBool next_inv = 0;
        m_faces.push_back(Face());
        face = &m_faces[m_faces.size()-1];

        face->edge = e_initial;
        for (Edge *ed = e_initial; ed;) {
            Edge *next = next_inv ? EDGE_CWE_BACK(*ed) : EDGE_CCWE_FRONT(*ed);

            if (next_inv)
                EDGE_FACE_CWE(*ed) = face;
            else
                EDGE_FACE_CCWE(*ed) = face;

            if (!next_inv)
                next_inv = (ed->v_front != next->v_back);
            else
                next_inv = (ed->v_back == next->v_front);
            
            // XXX possible infinite loop here. buy a watchdog.
            if (next == e_initial)
                break;
        }

        return face;
    }

    /* attach edges in CCW sequence
    */
    void make_edge_loop(Edge *ed1, ...) {
        Edge *ed;
        std::vector<Edge*> edges;
        edges.push_back(ed1);
        va_list args;
        va_start(args, ed1);
        while ((ed = va_arg(args, Edge*)))
            edges.push_back(ed);
        va_end(args);
        make_edge_loop_list(edges);
    }

    void edge_attach_ccw(Edge &from, Edge &to) {
        EDGE_CCWE_FRONT(from) = &to;
        if (from.v_front == to.v_back) {
            EDGE_CWE_BACK(to) = &from;
        } else {
            EDGE_CWE_FRONT(to) = &from; // XXX is this right?
        }
    }

    void make_edge_loop_list(std::vector<Edge*> &edges) {
        const size_t num_edges = edges.size();
        Edge *back = edges[num_edges - 1];
        for (size_t i = 0; i < num_edges; ++i)
            edge_attach_ccw(*back, *edges[i]);
    }

    void build_cube() {
        const vid_t
            a = get_vertex(Vec3(-1, -1, 1)),
            b = get_vertex(Vec3(1, -1, 1)),
            c = get_vertex(Vec3(1, 1, 1)),
            d = get_vertex(Vec3(-1, 1, 1)),
            e = get_vertex(Vec3(-1, 1, -1)),
            f = get_vertex(Vec3(1, 1, -1)),
            g = get_vertex(Vec3(1, -1, -1)),
            h = get_vertex(Vec3(-1, -1, -1));
        
        Edge *e1  = get_edge(a, b),
             *e2  = get_edge(b, c),
             *e3  = get_edge(c, d),
             *e4  = get_edge(d, a),
             *e5  = get_edge(b, g),
             *e6  = get_edge(g, f),
             *e7  = get_edge(f, c),
             *e8  = get_edge(f, e),
             *e9  = get_edge(e, d),
             *e10 = get_edge(e, h),
             *e11 = get_edge(g, h),
             *e12 = get_edge(a, h);

        make_edge_loop(e1, e2, e3, e4);
        build_face_ccw(e1);
    }

    virtual void draw_self(IScreen *u) {
        /*
        glDisable(GL_LIGHTING);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        glColor3ub(0, 0xFF, 0);

        FaceVec::const_iterator it = m_faces.begin();
        for (; it != m_faces.end(); ++it) {
            const Face *face = &*it;
            const Edge *ed = face->edge;
            wlBool next_inv = 0;

            glBegin(GL_POLYGON);
            for (; ed; ) {
                Edge *next = 

            }
            glEnd();
        }
        */
    }

};

IWEMesh *IWEMesh::create() {
    return new CWEMesh();
}

} // NS G3

