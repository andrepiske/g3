#include "precompiled.h"
#include <wl/color.h>
#include <GL/gl.h>

wlStartNS

void Color::gl_set() const {
    glColor4ubv((const GLubyte*)m_color);
}

wlEndNS


