#ifndef _WL_Poly_H_
#define _WL_Poly_H_

#include <wl/config.h>
#include <vector>
#include <wl/math.h>
#include <wl/color.h>
#include <wl/screen.h>
#include <wl/sg2.h>
#include <wl/util.h>

wlStartNS

class IImage;

// A Polygon. Poly for short.
// This could be called MESH, but it's not!
class IPoly : virtual public RefCounted
{
public:

    /**
    * A polygon face. It has to be a triangle. May have per-vertex color and
    * texture map.
    */
    struct PolyFace {
        int vert[3];
        int color[3];
        int texmap[3];
    };

    struct TexMap {
        int tex; // the texture id
        Vec2 coord;
    };

    typedef std::vector<PolyFace> PolyFaceVec;
    typedef std::vector<Color> ColorVec;
    typedef std::vector<Vec3> Vec3Vec;
    typedef std::vector<TexMap> TexMapVec;
    typedef std::vector<IImage*> ImageVec;

    enum {
        // transforms
        TR_VERTICES = 0x01,
        TR_TEXMAPS = 0x02
    };

    enum EDrawMode {
        DM_FULL = 0,
        DM_WIREFRAME, // Will use 
    };

public:
    virtual ~IPoly() {};

    static IPoly *create_poly();
    
    /**
     * Make a copy of this to poly
     */
    virtual void copy_to(IPoly *poly) = 0;

    virtual void draw(IScreen *screen) = 0;

    virtual void set_drawmode(int mode) = 0;
    virtual int get_drawmode() = 0;

    /**
     * Get a pointer to internal vertices list.
     */
    virtual Vec3Vec *get_verticesptr() = 0;
    /**
     * Get a pointer to internal color list.
     */
    virtual ColorVec *get_colorsptr() = 0;
    /**
     * Get a pointer to internal texmap list.
     */
    virtual TexMapVec *get_texmapsptr() = 0;

    /**
     * When using get_XXXXptr and changing the vector contents, call this
     * to update possible caches.
     */
    virtual void update() = 0;

    /**
     * Set the vertices list, color list and texmap list.
     * The list will be copied to an internal buffer.
     * If v==NULL then no list is set, only updates are made.
     *
     * This will also notify cache managers.
     */
    virtual void set_poly(Vec3Vec *vertices = wlNull, ColorVec *colors =
        wlNull, TexMapVec *texmap = wlNull) = 0;

    virtual PolyFaceVec *get_facesptr() = 0;
    virtual ImageVec* get_texturesptr() = 0;

    virtual void set_faces(PolyFaceVec *v = NULL) = 0;
    virtual void set_textures(ImageVec*) = 0;

    virtual BBox3 get_bb3() const = 0;
    // virtual util::bounds<float> get_bb() = 0;

    /**
     * Apply transformations to 'to_' which may be one or more of:
        TR_VERTICES, TR_TEXMAPS.
     */
    virtual void apply_transform(const Mat22 &mat, int to_ = TR_VERTICES) = 0;
    virtual void apply_transform(const Mat33 &mat, int to_ = TR_VERTICES) = 0;
    virtual void apply_transform(const Mat44 &mat) = 0;
};

class IPolyNode2D : virtual public sg::Node2D
{
public:
    static IPolyNode2D *create(IPoly *poly = wlNull);

    virtual void set_poly(IPoly *poly) = 0;
    virtual IPoly *get_poly() = 0;
    
    // virtual void draw_self(IScreen *s) = 0;

};


wlEndNS
#endif

