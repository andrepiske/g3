#ifndef __G3_CVMESH_H__
#define __G3_CVMESH_H__

#include <wl/scenegraph.h>
#include <carve/mesh.hpp>

namespace G3 {

class ICVMesh : public WL::sg::Node
{
public:
    static ICVMesh *create();

    virtual wlBool load_from_off(const std::string &filename) = 0; 
    virtual void set_meshset(carve::mesh::MeshSet<3> *ms) = 0;

    virtual void apply_transform(const WL::Mat44 &mat) = 0;

    virtual void copy_from(ICVMesh *mesh) = 0;

    virtual void build_cube() = 0;
    virtual void translate(double x, double y, double z) = 0;
    virtual void apply_cut(ICVMesh *tool) = 0;
    virtual void stats1(int *vert_count, int *edge_count, int *face_count) = 0;

    virtual void merge_with(ICVMesh *other) = 0;

};

} // NS G3

#endif


