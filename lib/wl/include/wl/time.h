#ifndef _WL_Time_H_
#define _WL_Time_H_

#include <wl/config.h>

wlStartNS

/* A time span. Not related to current local/system time or date.
   The resolution of this structure is milliseconds. You should use another
   lib if you need better resolution.
*/
class TimeSpan
{
public:
    ::WL::u64 m_span;

public:
    TimeSpan() {
    }
    TimeSpan(::WL::u64 t) : m_span(t) {
    }

    static ::WL::u64 get_ticks();

    WLINLINE ::WL::u64 get_span() const {
        return m_span;
    }

    WLINLINE ::WL::u64 set_span(::WL::u64 span) {
        ::WL::u64 t = m_span;
        m_span = span;
        return t;
    }

    operator ::WL::u64() {
        return m_span;
    }

};

wlEndNS
#endif


