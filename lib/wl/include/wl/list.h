#ifndef _WL_List_H_
#define _WL_List_H_

#include <wl/config.h>
#include <wl/refcounted.h>
#include <list>

wlStartNS

class IList : public RefCounted
{
public:
    typedef std::list<RCBucket<RefCounted> > LT;

public:
    /* -1: end, 0: start */
    virtual void insert(int position, RefCounted *el) = 0;

    virtual int size() = 0;

    virtual RefCounted *get(int index) = 0;

    virtual LT &get_lt() = 0;

    static IList *create();

};

wlEndNS

#endif

