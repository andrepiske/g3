require 'eigen'

-- matrix notations:
-- n, m refers respectively to number of rows and number of columns
-- i, j refers respectively to row, column
-- a 3x4 matrix has 3 rows and 4 columns
local mat_mt = { }

-- call like:
-- _newmat( { 1, 2, 3}, {4, 5, 6} ) will create a 2x3 matrix 
local function _newmat(...)
    local mat = { }
    local ar = {...}
    mat.rows = #ar
    mat.cols = #(ar[1])
    mat.obj = eigen.matrix()
    mat.obj:resize(mat.rows, mat.cols)
    for i=1,mat.rows,1 do
        local row = ar[i]
        for j=1,mat.cols,1 do
            mat.obj:set(i, j, row[j])
        end
    end
    setmetatable(mat, mat_mt)
    return mat
end

local vec_mt = { }

local function _newvec(...)
    local v = {}
    local ar = {...}
    local sz = #ar
    v.obj = eigen.matrix()
    v.obj:resize(sz, 1)
    v.size = sz
    for idx,val in ipairs(ar) do
        v.obj:set(idx, 1, val)
    end
    setmetatable(v, vec_mt)
    return v
end

---*Matrix*---------------------------------------------------------------------------

-- this allows us to do mat[i][j] = Foo
-- or Bar = mat[i][j], as mat[i] will return a row accessor
local function mat_row_accessor(mat, row)
    local ac = { }
    local acmt = { }
    function acmt:__index(idx)
        return mat.obj:get(row, idx)
    end
    function acmt:__newindex(idx, value)
        mat.obj:set(row, idx, value)
    end
    setmetatable(ac, acmt)
    return ac
end

function mat_mt:__index(idx)
    if type(idx) ~= 'number' then
        return getmetatable(self)[idx]
    end
    return mat_row_accessor(self, idx)
end

function mat_mt:__tostring()
    local t = 'M' .. self.rows .. 'x' .. self.cols .. '('
    for i=1,self.rows,1 do
        local k = '['
        for j=1,self.cols,1 do
            k = k .. self.obj:get(i, j) .. (j==self.cols and ']' or ', ')
        end
        t = t .. k .. (i==self.rows and '' or ', ')
    end
    return t .. ')'
end

function mat_mt:transpose()
    local t = {}
    for j=1,self.cols,1 do
        local k = {}
        for i=1,self.rows,1 do
            k[#k + 1] = self.obj:get(i, j)
        end
        t[#t + 1] = k
    end
    return _newmat(unpack(t))
end

function mat_mt:mulvec(vec)
    local vecsz = vec.size or #vec -- this is a workaround
    -- lua 5.1 won't handle the __len properly in metatables of tables.
    -- lua 5.2 does fix it, but this is not the current version here.
    -- in lua 5.2 we could do: vecsz = #vec
    local r = { }
    for i=1,self.rows,1 do
        local s = 0
        for j=1,self.cols,1 do
            s = s + self.obj:get(i, j) * (j <= vecsz and vec[j] or 1)
        end
        r[#r + 1] = s
    end
    return _newvec(unpack(r))
end

---*Vector*--------------------------------------------------------------------

function vec_mt:__index(idx)
    if type(idx) == 'string' then
        return getmetatable(self)[idx]
    end
    return self.obj:get(idx, 1)
end

function vec_mt:__newindex(idx, newval)
    return self.obj:set(idx, newval)
end

function vec_mt:__add(other)
    local sz_a = self.size
    local sz_b = other.size
    local sz = math.max(sz_a, sz_b)
    local v = {}
    for i=1,sz,1 do
        local a = (i <= sz_a) and self.obj:get(i, 1) or 0
        local b = (i <= sz_b) and other.obj:get(i, 1) or 0
        v[i] = a + b
    end
    return _newvec(unpack(v))
end

function vec_mt:__tostring()
    local sz = self.size
    local t = 'V' .. sz .. '('
    for i=1,sz-1,1 do
        t = t .. self.obj:get(i, 1) .. ', '
    end
    return t .. self.obj:get(sz, 1) .. ')'
end

function vec_mt:__sub(other)
    local sz_a = self.size
    local sz_b = other.size
    local sz = math.max(sz_a, sz_b)
    local v = {}
    for i=1,sz,1 do
        local a = (i <= sz_a) and self.obj:get(i, 1) or 0
        local b = (i <= sz_b) and other.obj:get(i, 1) or 0
        v[i] = a - b
    end
    return _newvec(unpack(v))
end

function vec_mt:normlz()
    local l = self:len()
    local v = {}
    local sz = self.size
    for i=1,sz,1 do
        v[i] = self.obj:get(i, 1) / l
    end
    return _newvec(unpack(v))
end

function vec_mt.__mul(_a, _b)
    if type(_a) == 'number' or type(_b) == 'number' then
        local a, b
        if type(_a) == 'table' then
            a = _a
            b = _b
        else
            a = _b
            b = _a
        end
        local v = { }
        local sz = a.size
        for i=1,sz,1 do
            v[i] = b * a.obj:get(i, 1)
        end
        return _newvec(unpack(v))
    else
        -- expect _a and _b to be one of { mat, vec }
        local v = _a.obj:mul(_b.obj)
        return _newmat(unpack(v))
    end
end

function vec_mt:dot(other)
    local sz = self.size
    local eax = 0
    for i=1,sz,1 do
        eax = eax + self.obj:get(i, 1) * other.obj:get(i, 1)
    end
    return eax
end

-- this will only work for 3-sized vectors
function vec_mt:cross(other)
    local a = self
    local b = other
    return _newvec(
        (a[2] * b[3]) - (a[3] * b[2]),
        (a[3] * b[1]) - (a[1] * b[3]),
        (a[1] * b[2]) - (a[2] * b[1]))
end

function vec_mt:len2()
    local eax = 0
    for i=1,self.size,1 do
        local v = self.obj:get(i, 1)
        eax = eax + (v * v)
    end
    return eax
end

function vec_mt:len()
    return math.sqrt(self:len2())
end

function vec_mt:as_table()
    local t = {}
    for i=1,self.size,1 do
        t[i] = self.obj:get(i, 1)
    end
    return t
end

function vec_mt:clone()
    return _newvec(unpack(self:as_table()))
end

-------------------------------------------------------------------------------
module 'lmat'

function vec3()
    return _newvec(0, 0, 0)
end

function vec2()
    return _newvec(0, 0)
end

function vec(...)
    return _newvec(...)
end

function mat(...)
    return _newmat(...)
end

