#include "var.h"
#include <boost/unordered_map.hpp>
#include <boost/foreach.hpp>
#include <wl/log.h>
#include <string.h>

namespace G3 {

class CVar : public IVar
{
public:
    typedef boost::unordered_map<std::string, WL::RCBucket<IVar> > M_S;
    typedef boost::unordered_map<long, WL::RCBucket<IVar> > M_L;
    // typedef boost::unordered_map<double, WL::RCBucket<IVar> > M_D;

    struct M_M {
        M_S s;
        M_L l;
        // M_D d;
    };

    struct M_Str {
        WL::u32 len;
        wlByte *ptr;
    };

public:
    int m_type;
    union {
        M_Str s;
        long l;
        double d;
        M_M *t;
    } m_v;

public:
    CVar(int t) : m_type(t) {
        switch (t) {
            case VT_STRING:
                m_v.s.len = 0;
                m_v.s.ptr = 0;
                break;
            case VT_DOUBLE:
                m_v.d = 0.0;
                break;
            case VT_LONG:
                m_v.l = 0;
                break;
            case VT_TABLE:
                m_v.t = new M_M();
                break;
        }
    }

    virtual ~CVar() {
        switch (m_type) {
            case VT_STRING:
                if (m_v.s.ptr)
                    free(m_v.s.ptr);
                break;
            case VT_TABLE:
                delete m_v.t;
                break;
        }
    }

    virtual wlBool equal(IVar *other) {
        if (m_type != other->get_type())
            return 0;

        switch (m_type) {
            case VT_STRING: {
                const std::string ot = other->get_string();
                if (ot.size() != (size_t)m_v.s.len)
                    return 0;

                return !memcmp(m_v.s.ptr, ot.data(), (size_t)m_v.s.len);
            }

            case VT_DOUBLE:
                return other->get_double() == m_v.d;

            case VT_LONG:
                return other->get_long() == m_v.l;
        }

        return 0;
    }

    virtual int get_type() {
        return m_type;
    }

    virtual std::string get_string() {
        if (m_type != VT_STRING)
            return "";
        
        if (!m_v.s.len)
            return std::string(0, (char)0);

        return std::string((const char*)m_v.s.ptr, (size_t)m_v.s.len);
    }

    virtual double get_double() {
        if (m_type != VT_DOUBLE)
            return 0.0;
        return m_v.d;
    }

    virtual long get_long() {
        if (m_type != VT_LONG)
            return ~(long)0;
        return m_v.l;
    }

    virtual void set_string(const std::string &s) {
        if (m_type == VT_STRING) {
            const size_t newlen = s.size();
            if (!newlen) {
                if (m_v.s.ptr)
                    free(m_v.s.ptr);
                m_v.s.ptr = 0;
                m_v.s.len = 0;
            } else {
                m_v.s.ptr = (wlByte*)realloc((void*)m_v.s.ptr, newlen);
                m_v.s.len = newlen;
                memcpy(m_v.s.ptr, s.data(), newlen);
            }
        }
    }

    virtual void set_double(double num) {
        if (m_type == VT_DOUBLE)
            m_v.d = num;
    }

    virtual void set_long(long num) {
        if (m_type == VT_LONG)
            m_v.l = num;
    }

    virtual void set_pair(const std::string &s, IVar *v) {
        if (m_type == VT_TABLE)
            m_v.t->s[s] = v;
    }

    virtual void set_pair(long k, IVar *v) {
        if (m_type == VT_TABLE)
            m_v.t->l[k] = v;
    }

    virtual IVar *get_pair(const std::string &s) {
        if (m_type != VT_TABLE)
            return 0;
            
        M_S::iterator it = m_v.t->s.find(s);
        if (it == m_v.t->s.end())
            return 0;
        
        return it->second();
    }

    virtual IVar *get_pair(long k) {
        if (m_type != VT_TABLE)
            return 0;
            
        M_L::iterator it = m_v.t->l.find(k);
        if (it == m_v.t->l.end())
            return 0;
        
        return it->second();
    }

    virtual void unset_pair(const std::string &s) {
        if (m_type == VT_TABLE) {
            M_S::iterator it = m_v.t->s.find(s);
            if (it != m_v.t->s.end())
                m_v.t->s.erase(it);
        }
    }

    virtual void unset_pair(long k) {
        if (m_type == VT_TABLE) {
            M_L::iterator it = m_v.t->l.find(k);
            if (it != m_v.t->l.end())
                m_v.t->l.erase(it);
        }
    }

    virtual void table_clear() {
        if (m_type == VT_TABLE) {
            delete m_v.t;
            m_v.t = new M_M();
        }
    }

public:
    static void _serialize_buffer(WL::IFileIO *f, const void *s, size_t len) {
        const WL::u32 l = (WL::u32)len;
        if (!l) {
            const wlByte c = 2;
            f->write(&c, 1);
        } else {
            const wlByte c = 0;
            f->write(&c, 1);
            f->write(&l, 4);
            f->write(s, l);
        }
    }

    static void _serialize_double(WL::IFileIO *f, double v) {
        f->write(&v, 8);
    }

    static void _serialize_long(WL::IFileIO *f, long l) {
        const WL::u64 n = (WL::u64)l;
        f->write(&n, 8);
    }

    static wlBool _unserialize_buffer(WL::IFileIO *f, WL::u32 &len,
            void **buffer)
    {
        wlByte c;
        if (1 != f->read(&c, 1) || (c != 0 && c != 2))
            return 0;
        if (c == 2) {
            *buffer = 0;
            len = 0;
            return 1;
        }
        if (4 != f->read(&len, 4))
            return 0;
        *buffer = malloc(len);
        if (!*buffer)
            return 0;
        if (len != f->read(*buffer, len)) {
            free(*buffer);
            return 0;
        }
        return 1;
    }

    static wlBool _unserialize_double(WL::IFileIO *f, double &val) {
        return 8 == f->read(&val, 8);
    }

    static wlBool _unserialize_long(WL::IFileIO *f, long &val) {
        WL::u64 v = 0;
        if (8 == f->read(&v, 8)) {
            val = (long)v;
            return 1;
        }
        return 0;
    }

    static IVar *_unserialize_any(WL::IFileIO *f) {
        wlByte typec;
        if (1 != f->read(&typec, 1))
            return 0;
        if (!f->seek(-1, WL::IFileIO::SK_CUR)) {
            WL_LOG_WARN("IVAR read: Impossible to seek file", 0);
            return 0;
        }
        IVar *const v = new_from_type(typec);
        if (!v)
            return 0;
        if (!v->unserialize(f)) {
            v->rc_pop();
            return 0;
        }
        return v;
    }

    virtual wlBool serialize(WL::IFileIO *f) {
        const wlByte code = (wlByte)m_type;
        f->write(&code, 1);
        switch (m_type) {
            case VT_STRING:
                _serialize_buffer(f, m_v.s.ptr, (size_t)m_v.s.len);
                break;
            case VT_DOUBLE:
                _serialize_double(f, m_v.d);
                break;
            case VT_LONG:
                _serialize_long(f, m_v.l);
                break;
            case VT_TABLE: {
                BOOST_FOREACH(M_S::value_type &it, m_v.t->s) {
                    const wlByte subcode = VT_STRING;
                    f->write(&subcode, 1);
                    _serialize_buffer(f, it.first.data(), it.first.size());
                    if (!it.second->serialize(f))
                        return 0;
                }
                BOOST_FOREACH(M_L::value_type &it, m_v.t->l) {
                    const wlByte subcode = VT_LONG;
                    f->write(&subcode, 1);
                    _serialize_long(f, it.first);
                    if (!it.second->serialize(f))
                        return 0;
                }
                const wlByte subcode = 0;
                f->write(&subcode, 1);
            }
            break;
        }

        return 1;
    }

    virtual wlBool unserialize(WL::IFileIO *f) {
        wlByte code;
        if (1 != f->read(&code, 1)) return 0;
        if (code != m_type) {
            WL_LOG_WARN("Var::unserialize type mismatch: expected %d, got %d",
                (int)m_type, (int)code);
            return 0;
        }
        switch (code) {
            case VT_STRING: {
                WL::u32 len;
                void *buffer;
                if (!_unserialize_buffer(f, len, &buffer))
                    return 0;
                if (m_v.s.ptr)
                    free(m_v.s.ptr);
                m_v.s.ptr = (wlByte*)buffer;
                m_v.s.len = len;
            } break;
            case VT_LONG: {
                long v;
                if (!_unserialize_long(f, v))
                    return 0;
                m_v.l = v;
            } break;
            case VT_DOUBLE: {
                double v;
                if (!_unserialize_double(f, v))
                    return 0;
                m_v.d = v;
            } break;
            case VT_TABLE: {
                table_clear();
                for (long stop = 0; !stop;) {
                    wlByte code;
                    if (1 != f->read(&code, 1))
                        return 0;
                    switch (code) {
                        case VT_STRING: {
                            WL::u32 len;
                            void *buffer;
                            if (!_unserialize_buffer(f, len, &buffer))
                                return 0;
                            WL::RCBucket<IVar> val = _unserialize_any(f);
                            if (!val()) {
                                if (buffer)
                                    free(buffer);
                                return 0;
                            }
                            m_v.t->s[std::string((const char*)buffer,
                                    (size_t)len)] = val;
                            if (buffer)
                                free(buffer);
                        } break;
                        case VT_LONG: {
                            long key;
                            if (!_unserialize_long(f, key))
                                return 0;
                            WL::RCBucket<IVar> val = _unserialize_any(f);
                            if (!val())
                                return 0;
                            m_v.t->l[key] = val;
                        } break;
                        case 0:
                            stop = 1;
                            break;
                        default:
                            return 0;
                    }
                }
            }
            // no need to default: because this wouldn't pass code!=m_type
        }
        return 1;
    }

};

IVar* IVar::new_table() { 
    return new CVar(IVar::VT_TABLE);
}

IVar* IVar::new_long(long num) {
    IVar *v = new CVar(IVar::VT_LONG);
    v->set_long(num);
    return v;
}

IVar* IVar::new_double(double num) {
    IVar *v = new CVar(IVar::VT_DOUBLE);
    v->set_double(num);
    return v;
}

IVar* IVar::new_string(const std::string &t) {
    IVar *v = new CVar(IVar::VT_STRING);
    if (!t.empty())
        v->set_string(t);
    return v;
}

IVar *IVar::new_from_type(int typenum) {
    switch (typenum) {
        case VT_STRING:
            return new_string("");
        case VT_LONG:
            return new_long(0);
        case VT_DOUBLE:
            return new_double(0.0);
        case VT_TABLE:
            return new_table();
    }
    WL_LOG_WARN("IVar new_from_type: invalid type %d", typenum);
    return 0;
}


} // NS G3

