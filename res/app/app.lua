require('toolselect')
require('compview')
require('machining')

local function toolbar_on_toggle(ac, optid, optname)
    local s = ac:get_active()
    local c = g3_appctx:get_cutter()
    local flags = { c:get_display_flags(false, false, false, false) }
    flags[optid] = s
    c:set_display_flags(unpack(flags))
    g3_appctx:set_parameter_int(optname, s and 1 or 0)
end

local function setup_toolbar(toolbar)
    local items = {
        {"app-tog-show-sweep", "Show Sweep", false},
        {"app-tog-show-workpiece", "Show Workpiece", true},
        {"app-tog-show-toolpiece", "Show Tool", true},
        {"app-tog-show-cut", "Show Cut", true},
    }
    for id,item in ipairs(items) do
        local ac = gtk.ToggleAction.new(item[1], item[2], nil, nil)
        local active = item[3]
        --[[ FIXME if g3_appctx:has_parameter(item[1]) then
            active = (g3_appctx:get_parameter_int(item[1]) ~= 0)
        end]]
        ac:set_active(active)
        ac:connect('toggled', function() toolbar_on_toggle(ac, id, item[1]) end, nil)
        toolbar:insert(ac:create_tool_item(), -1)
    end
    toolbar:show_all()
end

function main(ctx, menubar, toolbar, notebook)
    local tsw = ToolSelectWindow.new()
    notebook:append_page(tsw:get_widget(), gtk.Label.new("Tool"))

    local machining_wnd = MachiningWindow.new()
    notebook:append_page(machining_wnd:get_widget(), gtk.Label.new("Machining"))

    g3_appctx = ctx

    setup_toolbar(toolbar)
    notebook:show_all()
end

