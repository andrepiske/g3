#ifndef __G3_CURVE_H__
#define __G3_CURVE_H__

#include <wl/config.h>
#include <wl/refcounted.h>
#include "math.h"

namespace G3 {

class ICurve : public WL::RefCounted
{
public:
    virtual WL::Vec3 get_value_at(double t) = 0;
    virtual WL::Vec3 get_derivative_at(double t) = 0;

    virtual double calc_dt_for_length(double t_base, double length) = 0;

};

class IPolynomialCurve : public ICurve
{
public:
    virtual void set_degree(WL::u32 degree) = 0;
    virtual WL::u32 get_degree() = 0;

    virtual void set_coeff(WL::u32 coeff, const WL::Vec3 &value) = 0;
    virtual WL::Vec3 get_coeff(WL::u32 coeff) = 0;
    
    static IPolynomialCurve* create();
};


} // NS G3

#endif

