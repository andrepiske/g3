require('class')

FormView = Class(function(S, _form_data)

    S.data = _form_data or Forms.FormData.new()
    S.editors = nil

    function S:get_editor(field_name)
        local eds = self:build_editors()
        return eds[field_name]
    end

    function S:get_editors()
        return self:build_editors()
    end

    function S:build_editors()
        if self.editors == nil then
            local comps = {}
            for field_name,field in pairs(self.data.fields) do
                local lay = gtk.Box.new(gtk.ORIENTATION_HORIZONTAL)
                local label = gtk.Label.new(field.label .. ':')
                local ed = FormView.build_editor(field)
                lay:pack_start(label, false, false, 5)
                lay:pack_end(ed, true, true, 5)
                comps[field_name] = { ['widget']=lay, ['editor']=ed }
            end
            self.editors = comps
        end
        return self.editors
    end

end)

function FormView.build_editor(field)
    local g = FormView.builders[field.type]
    if g ~= nil then
        return g(field)
    end
    return gtk.Entry.new()
end

FormView.builders = {}

return FormView

