#include <wl/scriptable.h>
#include <boost/unordered_map.hpp>
#include <wl/log.h>

#define WLDEBUG_SCRIPTABLE_ENGINE

wlStartNS

namespace scriptable {

#ifdef WLDEBUG_SCRIPTABLE_ENGINE
static u32 gD_pushed = 0;
static u32 gD_registered = 0;
#endif

typedef ::boost::unordered_map<RefCounted*, PushProcT> PushMapT;
static PushMapT g_pushmap;

///////////////////////////////////////////////////////////////////////////////

void register_pushproc(RefCounted *object, PushProcT proc) {
    g_pushmap[object] = proc; 
    WL_LOG_INFO("Registerd %p", object);
#ifdef WLDEBUG_SCRIPTABLE_ENGINE
    gD_registered++;
#endif
}

void unregister_pushproc(RefCounted *object) {
    PushMapT::iterator it = g_pushmap.find(object);
    g_pushmap.erase(it);
#ifdef WLDEBUG_SCRIPTABLE_ENGINE
    gD_registered--;
#endif
}

void push(lua_State *L, RefCounted *object) {
    PushMapT::iterator it = g_pushmap.find(object);
    if (it == g_pushmap.end()) {
        // FIXME must push userdata with garbage collector enabled
        WL_LOG_WARN("Pushed object without luapush: %p", object);
        // lua_pushlightuserdata(L, (void*)object);
        // lua_pushnil(L);
    } else {
        it->second(L, object);
    }
#ifdef WLDEBUG_SCRIPTABLE_ENGINE
    gD_pushed++;
    WL_LOG_INFO("Pushed object. P=%d R=%d", gD_pushed, gD_registered);
#endif
}

} // NS scriptable

wlEndNS


