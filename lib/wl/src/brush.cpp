#include "precompiled.h"
#include <wl/brush.h>

wlStartNS

class CBrush : public IBrush
{
public:
    VerticesT m_vertices;
    TrianglesT m_triangles;

    virtual VerticesT *get_vertices() {
        return &m_vertices;
    }

    virtual TrianglesT *get_triangles() {
        return &m_triangles;
    }

     

};

IBrush *IBrush::create_resource(ResourceIOBlock blk,
    Resource *loader) {
    IFileIO *file = blk.file();
    
    // TODO: load resource!
    return wlNull;
}

wlEndNS


