#include "commproto.h"
#include <iostream>
#include <sstream>
#include <boost/regex.hpp>
#include <wl/log.h>

namespace G3 {

class CCommProto : public ICommProto
{
public:
    std::stringstream m_sendmsg;
    std::string m_readmsg;
    std::string::iterator m_readpos;

    boost::regex _re_long, _re_double, _re_string;
    wlBool m_error_flag;

    typedef boost::match_results<std::string::iterator> RegexMrT;

public:
    CCommProto() {
        _re_long = boost::regex("[0-9]+");
        _re_double = boost::regex("[0-9]+(\\.[0-9]+)?");
        _re_string = boost::regex("[^ ]+");
        m_error_flag = 0;
    }

    void set_err_flag() {
        WL_LOG_WARN("CommProto regex failed", 0);
        m_error_flag++;
    }

    wlBool _default_match(RegexMrT &mr, const boost::regex &re) {
        if (regex_search(m_readpos, m_readmsg.end(), mr,
                    re, boost::match_default)) {
            m_readpos += mr.length() + mr.position();
            return 1;
        }
        set_err_flag();
        return 0;
    }

    virtual long read_long() {
        RegexMrT mr;
        if (!_default_match(mr, _re_long))
            return 0xFFFFFFFF;
        long r = 0;
        std::stringstream s(mr.str());
        s >> r;
        return r;
    }

    virtual double read_double() {
        RegexMrT mr;
        if (!_default_match(mr, _re_double))
            return 0.0; // FIXME return NaN
        double r = 0;
        std::stringstream s(mr.str());
        s >> r;
        return r;
    }

    virtual std::string read_string() {
        RegexMrT mr;
        if (!_default_match(mr, _re_string))
            return "";
        return mr.str();
    }

    virtual void write_long(long val) {
        m_sendmsg << val << ' ';
    }

    virtual void write_double(double val) {
        m_sendmsg << val << ' ';
    }

    virtual void write_string(const std::string &val) {
        m_sendmsg << val << ' ';
    }

    virtual void reset() {
        m_sendmsg.str(std::string());
        m_sendmsg.clear();
        m_readmsg = "";
        m_readpos = m_readmsg.end();
        m_error_flag = 0;
    }

    virtual void encode(char *buffer, size_t *length) {
        // TODO make this work!
    }

    virtual void decode(const char *buffer, size_t len) {
        reset();
        m_readmsg = std::string(buffer, len);
        m_readpos = m_readmsg.begin();
    }

};

ICommProto *ICommProto::create() {
    return new CCommProto();
}

} // NS G3



