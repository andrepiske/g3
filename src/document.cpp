#include "document.h"
#include <wl/fileio.h>

namespace G3 {

class CDocument : public IDocument
{
public:
    std::string m_code;


public:
    CDocument() {
        m_code = "-- Type your code here\n\n";
    }

    virtual ~CDocument() {
    }

    virtual wlBool store_to(WL::IFileIO *f) {

        const int m_code_len = (int)m_code.size();
        f->write(&m_code_len, 4);
        f->write(m_code.c_str(), m_code_len);

        return 1;
    }

    virtual wlBool load_from(WL::IFileIO *f) {
        int codelen=0;
        f->read(&codelen, 4);
        if (codelen < 0 || codelen >= 0x7FFFFF)
            codelen = 0;

        char *b = new char[codelen+1];
        b[codelen] = 0;
        f->read(b, codelen);
        m_code = b;
        delete [] b;

        return wlTrue;
    }


public:
    virtual const std::string get_code() const {
        return m_code;
    }

    virtual void set_code(const std::string &code) {
        m_code = code;
    }

};


IDocument *IDocument::create() {
    return new CDocument();
}



} // NS G3



