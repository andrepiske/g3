#include "comm.h"
#include <vector>
#include <algorithm>

#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <signal.h>

#include <boost/thread/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/unordered_map.hpp>

#include "glibits.h"
#include <time.h>

namespace G3 {

struct Client {
    Client() {
        m_read_buffer_size = 0;
        m_cli_socket = -1;
        m_read_buffer = 0;
        m_read = 0;
    }
    Client(int sock) {
        m_cli_socket = sock;
        m_read_buffer_size = 0;
        m_read_buffer = 0;
        m_read = 0;
    }
    ~Client() {
        if (m_read_buffer)
            free(m_read_buffer);
    }

    void buffer_assure_size(size_t q) {
        if (m_read_buffer_size >= q)
            return;

        m_read_buffer_size = (size_t)(0.5 + (double)q * 1.25);
        m_read_buffer = (char*)realloc(m_read_buffer, m_read_buffer_size);
    }

    wlBool read_data(char *buffer, size_t size, size_t *consumed) {
        buffer_assure_size(m_read + size);
        size_t nread = 0;
        char *write_ptr = &m_read_buffer[m_read];
        for (char *ptr = buffer; nread < size; ++ptr, ++nread) {
            if (*ptr == '\n') {
                *write_ptr = 0;
                *consumed = nread + 1;
                m_read = 0;
                return 1;
            } else {
                *write_ptr = *ptr;
                write_ptr++;
            }
        }
        m_read += nread;
        *consumed = nread;
        return 0;
        /*
        WL::u32 *msg_size = (WL::u32*)m_read_buffer.get();
        if (m_read < 4) {
            size_t to_read = size - m_read;
            if (m_read + size > 4)
                to_read = 4 - m_read;
            buffer_assure_size(4);
            memcpy(m_read_buffer.get(), buffer, to_read);
            *consumed = to_read;
            m_read += to_read;
            return 0;
        } else {
            if (*msg_size > 16777216) {
                *consumed = 0;
                WL_LOG_WARN("Network message too large, ignored.");
                // TODO make a proper ignore
                return 0;
            }
            size_t to_read = std::min(size, *msg_size - m_read + 4);
            buffer_assure_size(4 + *msg_size);
            memcpy(m_read_buffer.get()+(ptrdiff_t)m_read, buffer, to_read);
            msg_size = (WL::u32*)m_read_buffer.get();
            *consumed = to_read;
            m_read += to_read;
            if (m_read == *msg_size + 4)
                return 1;
            return 0;
        }
        return 1;
        */
    }

    void *get_msg_ptr() const {
        return (void*)m_read_buffer;
    }

    size_t get_msg_size() const {
        return strlen(m_read_buffer);
    }

    size_t m_read;
    int m_cli_socket;

    size_t m_read_buffer_size;
    char *m_read_buffer;
};

class CCommServer : public ICommServer
{
public:
    int m_socket;
    boost::thread m_rthread;

    typedef boost::interprocess::interprocess_mutex Mutex;
    typedef boost::interprocess::scoped_lock<Mutex> ScopedMutexLock;
    typedef boost::unordered_map<int, Client> ClientMap;

    ClientMap m_clients;
    Mutex m_clients_lock;
    boost::function<void(void*)> m_receive_callback;

public:
    CCommServer() {
    }

    virtual ~CCommServer() {
    }

    virtual bool listen(WL::u32 port) {
        m_socket = ::socket(AF_INET, SOCK_STREAM, 0);
        if (m_socket < 0)
            return 0;

        struct sockaddr_in si;
        memset(&si, 0, sizeof(si));
        si.sin_family = AF_INET;
        si.sin_port = htons(port);

        if (::bind(m_socket, (const sockaddr*)&si, sizeof(si))) {
            ::close(m_socket);
            return 0;
        }

        if (::listen(m_socket, 2)) {
            ::close(m_socket);
            return 0;
        }

        m_rthread = boost::thread(__rthread_handler, this);
        return 1;
    }

    virtual void send(const std::string &data) {
    }

    virtual void set_receive_callback(boost::function<void(void*)> cb) {
        m_receive_callback = cb;
    }

public:
    static void __rthread_handler(CCommServer *c) {
        c->_rthread_handler();
    }

    void _client_dispatch_message(Client *cli) {
        // TODO improve allocation performance here
        const size_t msg_size = cli->get_msg_size();
        char *msg = new char[msg_size + 1];
        memcpy(msg, cli->get_msg_ptr(), msg_size);
        msg[msg_size] = 0;
        call_function_in_main_thread(m_receive_callback, msg);
        /*G3::comm::ComMsg comsg;
        if (!comsg.ParseFromArray(cli->get_msg_ptr(), cli->get_msg_size())) {
            WL_LOG_WARN("Invalid message\n", 0);
        } else {
            if (comsg.type() == G3::comm::ComMsg_Type_Echo) {
                printf("got an echo: '%s'\n", comsg.echo().text().c_str());
            }
            WL_LOG_INFO("OK message\n", 0);
        }*/
    }

    wlBool _client_read(int cli_socket) {
        ssize_t qr, all_consumed = 0;
        char buffer[1024];
        qr = ::read(cli_socket, buffer, 1024);
        if (qr < 0 && (errno == EAGAIN || errno == EINTR))
            return 1;
        if (!qr)
            return 0;
        ScopedMutexLock _mlock(m_clients_lock);
        Client *cli = 0;
        ClientMap::iterator it = m_clients.find(cli_socket);
        if (it == m_clients.end()) {
            WL_LOG_WARN("Invalid client for reading", 0);
            return 0;
        }
        cli = &it->second;
        _mlock.unlock();
        while (all_consumed < qr) {
            size_t consumed = 0;
            wlBool has_message = cli->read_data(&buffer[all_consumed],
                        qr - all_consumed, &consumed);
            if (has_message)
                _client_dispatch_message(cli);
            all_consumed += consumed;
        }
        return 1;
    }

    int _client_accept(int srv_socket) {
        sockaddr_in csi;
        socklen_t sl = sizeof(csi);
        csi.sin_family = AF_INET;
        int cl_sock = ::accept(srv_socket, (sockaddr*)&csi, &sl);
        if (cl_sock < 0)
            return -1;
        int cl_flags = fcntl(cl_sock, F_GETFD, 0);
        fcntl(cl_sock, F_SETFD, cl_flags | O_NONBLOCK);
        ScopedMutexLock _mlock(m_clients_lock);
        m_clients[cl_sock] = cl_sock;
        return cl_sock;
    }

    void _rthread_handler();
};

ICommServer *ICommServer::create() {
    return new CCommServer();
}

void CCommServer::_rthread_handler() {
    /*
    for (;;) {
        struct timespec c;
        c.tv_nsec = 0;
        c.tv_sec = 1;
        nanosleep(&c, 0);
        // 
        // if (m_receive_callback)
            // m_receive_callback(this);
        call_function_in_main_thread(m_receive_callback, this);
    }
    */
    std::vector<pollfd> fds;

    pollfd srv_fd;
    srv_fd.fd = m_socket;
    srv_fd.revents = 0;
    srv_fd.events = POLLIN;
    fds.push_back(srv_fd);

    int flags = fcntl(m_socket, F_GETFD, 0);
    fcntl(m_socket, F_SETFD, flags | O_NONBLOCK);

    for (;;) {
        const size_t sz = fds.size();
        sigset_t mask;
        sigemptyset(&mask);
        // int num =
        ppoll((pollfd*)&fds[0], (nfds_t)sz, 0, &mask);
        for (size_t i = 0; i < sz; ++i) {
            pollfd &pf = fds[i];
            if (pf.revents) {
                if (i == 0) {
                    pollfd cli_fd;
                    cli_fd.fd = _client_accept(pf.fd);
                    if (cli_fd.fd < 0)
                        continue;
                    cli_fd.revents = 0;
                    cli_fd.events = POLLIN;
                    fds.push_back(cli_fd);
                } else {
                    if (!_client_read(pf.fd))
                        pf.fd = -pf.fd;
                }
                pf.revents = 0;
            }
        }
    }
}

} // NS G3

