#include "glibits.h"

namespace G3 {

static gboolean
its_prepare(GSource *src, gint *timeout) {
    *timeout = 0;
    return TRUE;
}

static gboolean
its_check(GSource *src) {
    return TRUE;
}

static gboolean
its_dispatch(GSource *src, GSourceFunc callback, gpointer ud) {
    return callback(ud);
}

static GSourceFuncs its_sourcefuncs =
    { its_prepare, its_check, its_dispatch, 0 };

struct UserData {
    boost::function<void(void*)> proc;
    void *ud;
};

static gboolean
its_callback(gpointer _ud) {
    UserData *ud = (UserData*)_ud;  
    ud->proc(ud->ud);
    delete ud;
    return FALSE;
}

void call_function_in_main_thread(boost::function<void(void*)> cbproc,
        void *userdata, int priority)
{
    GSource *src;
    src = g_source_new(&its_sourcefuncs, sizeof(GSource));
    UserData *ud = new UserData();
    ud->proc = cbproc;
    ud->ud = userdata;
    g_source_set_priority(src, priority);
    g_source_set_callback(src, its_callback, (gpointer)ud, 0);
    g_source_attach(src, NULL);
    g_source_unref(src);
}


} // NS G3

