#ifndef _WL_Input_H_
#define _WL_Input_H_

#include <wl/config.h>
#include <wl/event.h>

union SDL_Event;

wlStartNS

class IScreen;

class IInput : virtual public IEmitter
{
public:
    virtual ~IInput() {}

    static IInput *create(IScreen *screen);

    virtual void handle_sdl_event(SDL_Event *event) = 0;

};


wlEndNS

#endif


