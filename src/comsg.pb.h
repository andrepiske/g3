// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: comsg.proto

#ifndef PROTOBUF_comsg_2eproto__INCLUDED
#define PROTOBUF_comsg_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 2005000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 2005000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

namespace G3 {
namespace comm {

// Internal implementation detail -- do not call these.
void  protobuf_AddDesc_comsg_2eproto();
void protobuf_AssignDesc_comsg_2eproto();
void protobuf_ShutdownFile_comsg_2eproto();

class ComEcho;
class ComMsg;

enum ComMsg_Type {
  ComMsg_Type_Echo = 1
};
bool ComMsg_Type_IsValid(int value);
const ComMsg_Type ComMsg_Type_Type_MIN = ComMsg_Type_Echo;
const ComMsg_Type ComMsg_Type_Type_MAX = ComMsg_Type_Echo;
const int ComMsg_Type_Type_ARRAYSIZE = ComMsg_Type_Type_MAX + 1;

const ::google::protobuf::EnumDescriptor* ComMsg_Type_descriptor();
inline const ::std::string& ComMsg_Type_Name(ComMsg_Type value) {
  return ::google::protobuf::internal::NameOfEnum(
    ComMsg_Type_descriptor(), value);
}
inline bool ComMsg_Type_Parse(
    const ::std::string& name, ComMsg_Type* value) {
  return ::google::protobuf::internal::ParseNamedEnum<ComMsg_Type>(
    ComMsg_Type_descriptor(), name, value);
}
// ===================================================================

class ComEcho : public ::google::protobuf::Message {
 public:
  ComEcho();
  virtual ~ComEcho();

  ComEcho(const ComEcho& from);

  inline ComEcho& operator=(const ComEcho& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const ComEcho& default_instance();

  void Swap(ComEcho* other);

  // implements Message ----------------------------------------------

  ComEcho* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const ComEcho& from);
  void MergeFrom(const ComEcho& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required string text = 1;
  inline bool has_text() const;
  inline void clear_text();
  static const int kTextFieldNumber = 1;
  inline const ::std::string& text() const;
  inline void set_text(const ::std::string& value);
  inline void set_text(const char* value);
  inline void set_text(const char* value, size_t size);
  inline ::std::string* mutable_text();
  inline ::std::string* release_text();
  inline void set_allocated_text(::std::string* text);

  // @@protoc_insertion_point(class_scope:G3.comm.ComEcho)
 private:
  inline void set_has_text();
  inline void clear_has_text();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::std::string* text_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(1 + 31) / 32];

  friend void  protobuf_AddDesc_comsg_2eproto();
  friend void protobuf_AssignDesc_comsg_2eproto();
  friend void protobuf_ShutdownFile_comsg_2eproto();

  void InitAsDefaultInstance();
  static ComEcho* default_instance_;
};
// -------------------------------------------------------------------

class ComMsg : public ::google::protobuf::Message {
 public:
  ComMsg();
  virtual ~ComMsg();

  ComMsg(const ComMsg& from);

  inline ComMsg& operator=(const ComMsg& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const ComMsg& default_instance();

  void Swap(ComMsg* other);

  // implements Message ----------------------------------------------

  ComMsg* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const ComMsg& from);
  void MergeFrom(const ComMsg& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  typedef ComMsg_Type Type;
  static const Type Echo = ComMsg_Type_Echo;
  static inline bool Type_IsValid(int value) {
    return ComMsg_Type_IsValid(value);
  }
  static const Type Type_MIN =
    ComMsg_Type_Type_MIN;
  static const Type Type_MAX =
    ComMsg_Type_Type_MAX;
  static const int Type_ARRAYSIZE =
    ComMsg_Type_Type_ARRAYSIZE;
  static inline const ::google::protobuf::EnumDescriptor*
  Type_descriptor() {
    return ComMsg_Type_descriptor();
  }
  static inline const ::std::string& Type_Name(Type value) {
    return ComMsg_Type_Name(value);
  }
  static inline bool Type_Parse(const ::std::string& name,
      Type* value) {
    return ComMsg_Type_Parse(name, value);
  }

  // accessors -------------------------------------------------------

  // required .G3.comm.ComMsg.Type type = 1;
  inline bool has_type() const;
  inline void clear_type();
  static const int kTypeFieldNumber = 1;
  inline ::G3::comm::ComMsg_Type type() const;
  inline void set_type(::G3::comm::ComMsg_Type value);

  // optional .G3.comm.ComEcho echo = 2;
  inline bool has_echo() const;
  inline void clear_echo();
  static const int kEchoFieldNumber = 2;
  inline const ::G3::comm::ComEcho& echo() const;
  inline ::G3::comm::ComEcho* mutable_echo();
  inline ::G3::comm::ComEcho* release_echo();
  inline void set_allocated_echo(::G3::comm::ComEcho* echo);

  // @@protoc_insertion_point(class_scope:G3.comm.ComMsg)
 private:
  inline void set_has_type();
  inline void clear_has_type();
  inline void set_has_echo();
  inline void clear_has_echo();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::G3::comm::ComEcho* echo_;
  int type_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(2 + 31) / 32];

  friend void  protobuf_AddDesc_comsg_2eproto();
  friend void protobuf_AssignDesc_comsg_2eproto();
  friend void protobuf_ShutdownFile_comsg_2eproto();

  void InitAsDefaultInstance();
  static ComMsg* default_instance_;
};
// ===================================================================


// ===================================================================

// ComEcho

// required string text = 1;
inline bool ComEcho::has_text() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void ComEcho::set_has_text() {
  _has_bits_[0] |= 0x00000001u;
}
inline void ComEcho::clear_has_text() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void ComEcho::clear_text() {
  if (text_ != &::google::protobuf::internal::kEmptyString) {
    text_->clear();
  }
  clear_has_text();
}
inline const ::std::string& ComEcho::text() const {
  return *text_;
}
inline void ComEcho::set_text(const ::std::string& value) {
  set_has_text();
  if (text_ == &::google::protobuf::internal::kEmptyString) {
    text_ = new ::std::string;
  }
  text_->assign(value);
}
inline void ComEcho::set_text(const char* value) {
  set_has_text();
  if (text_ == &::google::protobuf::internal::kEmptyString) {
    text_ = new ::std::string;
  }
  text_->assign(value);
}
inline void ComEcho::set_text(const char* value, size_t size) {
  set_has_text();
  if (text_ == &::google::protobuf::internal::kEmptyString) {
    text_ = new ::std::string;
  }
  text_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* ComEcho::mutable_text() {
  set_has_text();
  if (text_ == &::google::protobuf::internal::kEmptyString) {
    text_ = new ::std::string;
  }
  return text_;
}
inline ::std::string* ComEcho::release_text() {
  clear_has_text();
  if (text_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = text_;
    text_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void ComEcho::set_allocated_text(::std::string* text) {
  if (text_ != &::google::protobuf::internal::kEmptyString) {
    delete text_;
  }
  if (text) {
    set_has_text();
    text_ = text;
  } else {
    clear_has_text();
    text_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// -------------------------------------------------------------------

// ComMsg

// required .G3.comm.ComMsg.Type type = 1;
inline bool ComMsg::has_type() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void ComMsg::set_has_type() {
  _has_bits_[0] |= 0x00000001u;
}
inline void ComMsg::clear_has_type() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void ComMsg::clear_type() {
  type_ = 1;
  clear_has_type();
}
inline ::G3::comm::ComMsg_Type ComMsg::type() const {
  return static_cast< ::G3::comm::ComMsg_Type >(type_);
}
inline void ComMsg::set_type(::G3::comm::ComMsg_Type value) {
  assert(::G3::comm::ComMsg_Type_IsValid(value));
  set_has_type();
  type_ = value;
}

// optional .G3.comm.ComEcho echo = 2;
inline bool ComMsg::has_echo() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void ComMsg::set_has_echo() {
  _has_bits_[0] |= 0x00000002u;
}
inline void ComMsg::clear_has_echo() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void ComMsg::clear_echo() {
  if (echo_ != NULL) echo_->::G3::comm::ComEcho::Clear();
  clear_has_echo();
}
inline const ::G3::comm::ComEcho& ComMsg::echo() const {
  return echo_ != NULL ? *echo_ : *default_instance_->echo_;
}
inline ::G3::comm::ComEcho* ComMsg::mutable_echo() {
  set_has_echo();
  if (echo_ == NULL) echo_ = new ::G3::comm::ComEcho;
  return echo_;
}
inline ::G3::comm::ComEcho* ComMsg::release_echo() {
  clear_has_echo();
  ::G3::comm::ComEcho* temp = echo_;
  echo_ = NULL;
  return temp;
}
inline void ComMsg::set_allocated_echo(::G3::comm::ComEcho* echo) {
  delete echo_;
  echo_ = echo;
  if (echo) {
    set_has_echo();
  } else {
    clear_has_echo();
  }
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace comm
}  // namespace G3

#ifndef SWIG
namespace google {
namespace protobuf {

template <>
inline const EnumDescriptor* GetEnumDescriptor< ::G3::comm::ComMsg_Type>() {
  return ::G3::comm::ComMsg_Type_descriptor();
}

}  // namespace google
}  // namespace protobuf
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_comsg_2eproto__INCLUDED
