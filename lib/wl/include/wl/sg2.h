#ifndef _WL_SceneGraph2D_H_
#define _WL_SceneGraph2D_H_

#include <wl/config.h>
#include <wl/scenegraph.h>
#include <wl/event.h>

wlStartNS

namespace sg {

class TransformSimple2D : public Transform
{
private:
    mutable wlBool m_reload;
    Vec2 m_translation;
    float m_rotation;
    Vec2 m_scale;

public:
    TransformSimple2D() {
        m_reload = 1;
        m_scale = Vec2(1.f, 1.f);
        m_rotation = 0.f;
        m_translation = Vec2(0.f, 0.f);
    }

    const Vec2 &get_translation() const {
        return m_translation;
    }

    float get_rotation() const {
        return m_rotation;
    }

    const Vec2 &get_scale() const {
        return m_scale;
    }

    void set_translation(const Vec2 &t) {
        m_translation = t;
        m_reload = 1;
    }

    void set_rotation(float angle) {
        m_rotation = angle;
        m_reload = 1;
    }

    void set_scale(const Vec2 &scale) {
        m_scale = scale;
        m_reload = 1;
    }

    virtual const Mat44 &get_matrix() const {
        if (m_reload) {
            Mat44 m;
            m.set_identity();
            m = mulmat(Mat44::rotation_z(m_rotation), m);
            m = mulmat(Mat44::scale(
                Vec3(m_scale.x, m_scale.y, 1.f)), m);
            m = mulmat(Mat44::translation(
                Vec3(m_translation.x, m_translation.y, 0.f)), m);
            m_reload = 0;
            m_matrix = m;
        }
        return m_matrix;
    }

};

class Node2D : public Node, public ISink
{
/* read-only public properties */
public:
    /* whether this is solid or not. Invalidates ATTR */
    wlBool m_solid;

    /* range from -inf to +inf, default zero. Invalidates ATTR */
    int m_zindex;

    /* whether this node should be redrawn or not.
    if this is false, then it must be redrawn.
    */
    wlBool m_draw_valid;

public:
    WLINLINE void set_solid(wlBool solid) {
        m_solid = solid;
        m_attr_valid = wlFalse;
    }

public:
    Node2D() {
        m_zindex = 0; 
        m_solid = wlFalse;
    }

    void set_zindex(int z);

    virtual BBox2 calculate_bbox() const {
        return BBox2(Vec2(0, 0), Vec2(0, 0));
    }

    const BBox2 &get_bbox() const;

    virtual void invalidate_bounding_box() {
        m_bbox_cache_valid = wlFalse;
        Node::invalidate_bounding_box();
    }

    virtual void _post_draw(IScreen *screen);

public:
    virtual void event_received(Quark signal, RefCounted *param);

private:
    mutable BBox2 m_bbox_cache;
    mutable wlBool m_bbox_cache_valid;

};

};

wlEndNS

#endif

