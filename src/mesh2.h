#ifndef _G3_MESH2_H_
#define _G3_MESH2_H_

#include <wl/scenegraph.h>

namespace G3 {

class IMesh2 : public WL::sg::Node
{
public:

    static IMesh2 *create();

};

} // NS G3

#endif

