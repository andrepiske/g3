#include "drawsurface.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <tegtkgl.h>

namespace G3 {

enum DrawState
{
    DS_ALIVE = 1, // We've got a widget
    DS_WAIT, // We don't have a widget
    DS_DEAD // ???
};

// Draw function called by the timer
static gboolean _draw_function(gpointer ud);

class CDrawSurface : public IDrawSurface
{
public:
    GtkWidget *m_glwid;
    int m_state;
    guint m_timeout_id;
    WL::RCBucket<IDrawGuy> m_drawguy;

public:
    CDrawSurface() {
        m_state = DS_WAIT;
        m_glwid = NULL;
        m_timeout_id = 0;
    }

    virtual void set_drawing_guy(IDrawGuy *guy) {
        m_drawguy = guy;
    }

    virtual WL::Vec2 get_size() {
        GdkWindow *wnd;
        wnd = gtk_widget_get_window(m_glwid);
        return wnd ? WL::Vec2(
            gdk_window_get_width(wnd),
            gdk_window_get_height(wnd))
            : WL::Vec2(1, 1);
    }

    virtual void set_widget(GtkWidget *widget) {
        if (widget) {
            g_object_ref_sink(G_OBJECT(widget));
        }

        if (m_glwid) {
            g_object_unref(G_OBJECT(m_glwid));
            m_glwid = NULL;
        }
        if (m_timeout_id) {
            GSource *src;
            src = g_main_context_find_source_by_id(NULL, m_timeout_id);
            g_source_destroy(src);
            m_timeout_id = 0;
        }

        if (widget) {
            m_timeout_id = g_timeout_add_full(1000, 10, _draw_function,
                (gpointer)this, 0);
            m_state = DS_ALIVE;
            m_glwid = widget;
        } else {
            m_state = DS_WAIT;
        }
    }

    virtual ~CDrawSurface() {
        set_widget(NULL);
    }

    /*
    virtual WL::Point2D get_size() const {
        GdkWindow *wnd;
        wnd = gtk_widget_get_window(m_glwid);
        return wnd ? WL::Point2D(
            gdk_window_get_width(wnd),
            gdk_window_get_height(wnd))
            : WL::Point2D(1, 1);
    }
    */

    virtual GtkWidget *get_widget() {
        return m_glwid;
    }

    virtual IDrawGuy *get_drawing_guy() {
        return m_drawguy();
    }

    gboolean draw_timer_function() {
        GdkWindow *wnd;
        wnd = gtk_widget_get_window(m_glwid);
        if (!wnd)
            return TRUE;

        glViewport(0, 0,
            (GLsizei)gdk_window_get_width(wnd),
            (GLsizei)gdk_window_get_height(wnd));

        if (m_drawguy()) {
            m_drawguy->draw(this);
        } else {
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_BLEND);
            glClearColor(0.f, 0.f, 1.f, 1.f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
        }

        return TRUE;
    }

};

static gboolean _draw_function(gpointer ud) {
    CDrawSurface *ds = static_cast<CDrawSurface*>(ud);
    switch (ds->m_state) {
        case DS_WAIT:
            return TRUE;
        case DS_DEAD: // dear compiler: we won't *ever* reach this place.
            return FALSE;
    }

    if (!gtk_widget_get_realized(ds->m_glwid))
        return TRUE;

    te_gtkgl_make_current(TE_GTKGL(ds->m_glwid));
    if (!ds->draw_timer_function())
        return FALSE;

    te_gtkgl_swap(TE_GTKGL(ds->m_glwid));
    return TRUE;
}

IDrawSurface *IDrawSurface::create() {
    return new CDrawSurface();
}

};

