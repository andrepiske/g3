#ifndef _G3_Var_H_
#define _G3_Var_H_

#include <wl/refcounted.h>
#include <wl/fileio.h>
#include <string>

namespace G3 {

class IVar : public WL::RefCounted
{
public:
    enum Type {
        VT_STRING = 1,
        VT_DOUBLE,
        VT_LONG,
        VT_TABLE
    };

    virtual wlBool equal(IVar *other) = 0;
    virtual int get_type() = 0;

    virtual std::string get_string() = 0;
    virtual double get_double() = 0;
    virtual long get_long() = 0;

    virtual void set_string(const std::string &s) = 0;
    virtual void set_double(double num) = 0;
    virtual void set_long(long num) = 0;

    virtual void set_pair(const std::string &s, IVar *v) = 0;
    virtual void set_pair(long k, IVar *v) = 0;

    virtual IVar *get_pair(const std::string &s) = 0;
    virtual IVar *get_pair(long k) = 0;

    virtual void unset_pair(const std::string &s) = 0;
    virtual void unset_pair(long k) = 0;
    virtual void table_clear() = 0;

    virtual wlBool serialize(WL::IFileIO *f) = 0;
    virtual wlBool unserialize(WL::IFileIO *f) = 0;

    static IVar *new_table();
    static IVar *new_long(long num);
    static IVar *new_double(double num);
    static IVar *new_string(const std::string &t);
    static IVar *new_from_type(int typenum);

};

} // NS G3

#endif

