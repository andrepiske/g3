#include "cutter.h"
#include "curve.h"
#include "tool.h"
#include "toolpiece.h"
#include "workpiece.h"
#include <wl/scriptable.h>
#include <gtk/gtk.h>
#include "curve.h"
#include <deque>
#include <vector>
#include "cvmesh.h"
#include "sweepgenerator.h"
#include <wl/log.h>

#include <GL/gl.h>
#include <opencsg.h>
#include "opencsgdraw.h"
#include <wl/image.h>

extern void _G3_ICutter__luapush(lua_State *L, WL::RefCounted *_obj);

namespace G3 {

static gboolean _timer_function_wrapper(gpointer d);

enum CutJobState {
    CJS_UNITIALIZED,
    CJS_START,
    CJS_RUNNING,
    CJS_DONE
};

enum CutJobCurveKind {
    CJK_STRAIGHT_XZ,
    CJK_UPDOWN,
    CJK_CIRCULAR
};

struct CutJob {
    WL::RCBucket<ICurve> curve;
    WL::RCBucket<IToolPiece> toolpiece;
    double t_value;
    double t_increment;
    int curve_kind;
    int state;
    wlBool do_mill;
    WL::RCBucket<ISweepGenerator> sweeper;
    WL::RCBucket<ICVMesh> sweep_mesh;

    CutJob() : t_increment(0.2) {
        t_value = 0.0;
        sweeper = ISweepGenerator::create();
        do_mill = 1;
        state = CJS_UNITIALIZED;
    }

    ICVMesh *get_start_mesh() {
        return sweeper->make_base_cylinder_fromtop(toolpiece->get_position());
    }

    ICVMesh *get_end_mesh() {
        return sweeper->make_base_cylinder_fromtop(toolpiece->get_position());
    }

    void init(ICurve *curve, IToolPiece *toolp, wlBool mill) {
        sweeper->set_tool(toolp->get_tool());
        sweeper->set_curve(curve);
        this->curve = curve;
        do_mill = mill;
        toolpiece = toolp;
        curve = curve;
        state = CJS_START;
        t_value = t_increment;
    }

    void _update_working_mesh() {
        if (!do_mill)
            return;
        ICVMesh *msh = sweeper->make_sweep_mesh(0, 0.0,
                t_value > 1.0 ? 1.0 : t_value,
                toolpiece->get_position());
        sweep_mesh = msh;
    }

    void next() {
        switch (state) {
            case CJS_START:
                toolpiece->move_start(curve);
                state = CJS_RUNNING;
                break;
            case CJS_DONE:
                break;
            case CJS_RUNNING:
                t_value += t_increment;
                toolpiece->move_step(t_increment);
                _update_working_mesh();
                if (reached_end()) {
                    toolpiece->move_finish();
                    state = CJS_DONE;
                }
                break;
            default:
                WL_LOG_ERROR("Cut job state is unitialized", 0);
        }
    }

    wlBool reached_end() {
        return t_value > 1.0;
    }

};

struct CutterDisplayFlags {
    bool show_sweep, show_workpiece, show_toolpiece, show_cut;
};

class CCutter : public ICutter, public WL::scriptable::RegPush<WL::RefCounted>
{
    typedef std::vector<OpenCSG::Primitive*> OCSGPrimitiveList;

    WL::RCBucket<IWorkPiece> m_workpiece;
    WL::RCBucket<IToolPiece> m_toolpiece;
    wlBool m_tool_on;
    wlBool m_paused;

    WL::RCBucket<ICurve> m_jobcurve;
    ISweepGenerator::QualityParameters m_cutter_qap;

    typedef std::deque<CutJob> CutJobList;
    CutJobList m_jobs;

    double m_cutspeed;
    OCSGPrimitiveList m_csg_primitives;

    CutterDisplayFlags m_display_flags;

public:
    CCutter() :
        WL::scriptable::RegPush<WL::RefCounted>(this, _G3_ICutter__luapush)
    {
        m_cutspeed = 0.02;
        m_workpiece = IWorkPiece::create();

        ITool *tool = ITool::create();
        m_toolpiece = IToolPiece::create();
        tool->set_tool_radius(0.4);
        m_toolpiece->set_tool(tool);

        g_timeout_add_full(1000, 23, _timer_function_wrapper,
                (gpointer)this, 0);

        m_tool_on = 0;
        m_paused = 0;

        OpenCSG::setOption(OpenCSG::OffscreenSetting,
                OpenCSG::AutomaticOffscreenType);
        OpenCSG::setOption(OpenCSG::AlgorithmSetting,
                OpenCSG::SCS);
        OpenCSG::setOption(OpenCSG::DepthComplexitySetting,
                OpenCSG::NoDepthComplexitySampling);

        m_csg_primitives.push_back(new OCSGPrimitive(m_workpiece,
                OpenCSG::Intersection));

        set_cutting_quality(1.0, 1.0);
        set_display_flags(false, true, true, true);
    }

    virtual void draw_self(WL::IScreen *u) {
        glDisable(GL_TEXTURE_2D);
        glEnable(GL_LIGHTING);

        if (m_display_flags.show_cut) {
            glDepthFunc(GL_LESS);
            OpenCSG::render(m_csg_primitives);
            glDepthFunc(GL_EQUAL);

            OCSGPrimitiveList::iterator it = m_csg_primitives.begin();
            for (it = m_csg_primitives.begin(); it != m_csg_primitives.end(); it++)
                (*it)->render();
            glDepthFunc(GL_LESS);
        } else {
            if (m_display_flags.show_toolpiece)
                m_toolpiece->draw(0);
            if (m_display_flags.show_workpiece)
                m_workpiece->draw(0);
        }

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        if (m_display_flags.show_sweep) {
            OCSGPrimitiveList::iterator it = m_csg_primitives.begin();
            it++;
            for (; it != m_csg_primitives.end(); it++)
                (*it)->render();
        }

        if (m_display_flags.show_toolpiece) {
            m_toolpiece->draw(0);
        }
    }

    virtual void dbg_machining_step() {
        _machining_step();
    }

    virtual void dbg_set_paused(bool paused) {
        m_paused = paused;
    }

    virtual ITool *get_tool() {
        return m_toolpiece->get_tool();
    }

    void _timer_function() {
        if (m_paused)
            return;

        _machining_step();
    }

    void _machining_step() {
        if (!m_jobs.empty()) {
            CutJob *job = &*m_jobs.begin();
            if (job->state == CJS_START && job->do_mill) {
                ICVMesh *mesh = job->get_start_mesh();
                if (mesh) {
                    m_csg_primitives.push_back(new OCSGPrimitive(mesh,
                                OpenCSG::Subtraction));
                }
                m_csg_primitives.push_back(new OCSGPrimitive(0,
                    OpenCSG::Subtraction));
            }
            job->next();
            if (job->do_mill) {
                OCSGPrimitive *prim = static_cast<OCSGPrimitive*>(
                        m_csg_primitives[m_csg_primitives.size()-1]);
                prim->set_node(job->sweep_mesh);
            }

            if (job->reached_end()) {
                ICVMesh *mesh = job->do_mill ? job->get_end_mesh() : 0;
                if (mesh) {
                    m_csg_primitives.push_back(new OCSGPrimitive(mesh,
                                OpenCSG::Subtraction));
                }

                CutJobList::iterator it = m_jobs.begin();
                it++;
                m_jobs.pop_front();
            }
        }
    }

    virtual void workpiece_stats1(int *vc, int *ec, int *fc) {
        // return m_workpiece_mesh->stats1(vc, ec, fc);
    }

    virtual void apply_cut() {
        // m_workpiece_mesh->apply_cut(m_tool_mesh);
    }
    
    virtual void translate_tool(double x, double y, double z) {
        // m_tool_mesh->translate(x, y, z);
    }

    virtual void create_raw_block(double width, double height, double depth) {
        ICVMesh *mesh = ICVMesh::create();
        mesh->build_cube();
        mesh->apply_transform(WL::Mat44::scale(WL::Vec3(width, depth, height)));
        m_workpiece->set_cvmesh(mesh);
    }

    virtual void set_tool_state(bool is_on) {
        m_tool_on = (wlBool)is_on;
    }

    virtual void set_cutspeed(double cv) {
        m_cutspeed = cv;
    }

    virtual void set_cutting_quality(double curvedt, double numsteps) {
        m_cutter_qap.curve_dt = 0.02 / curvedt;
        m_cutter_qap.number_steps = numsteps * 30.0;
    }

    virtual void set_display_flags(int show_sweep, int show_workpiece,
            int show_toolpiece, int show_cut)
    {
        m_display_flags.show_sweep = show_sweep;
        m_display_flags.show_workpiece = show_workpiece;
        m_display_flags.show_toolpiece = show_toolpiece;
        m_display_flags.show_cut = show_cut;
    }

    virtual void get_display_flags(int *show_sweep, int *show_workpiece,
            int *show_toolpiece, int *show_cut)
    {
        if (show_sweep)
            *show_sweep = m_display_flags.show_sweep;
        if (show_workpiece)
            *show_workpiece = m_display_flags.show_workpiece;
        if (show_toolpiece)
            *show_toolpiece = m_display_flags.show_toolpiece;
        if (show_cut)
            *show_cut = m_display_flags.show_cut;
    }

    void _put_new_job(ICurve *curve) {
        // m_jobcurve = curve;
        m_jobs.push_back(CutJob());
        CutJob *job = &*m_jobs.rbegin();
        job->t_increment = m_cutspeed;
        job->init(curve, m_toolpiece, m_tool_on);
        printf("qa: %.5f %d\n", m_cutter_qap.curve_dt,
                m_cutter_qap.number_steps);
        job->sweeper->set_quality_parameters(m_cutter_qap);
    }

    virtual void move_tool_straight(double x, double y, double z) {
        WL::RCBucket<IPolynomialCurve> c = IPolynomialCurve::create();
        c->set_degree(1);
        c->set_coeff(0, WL::Vec3(0, 0, 0));
        c->set_coeff(1, WL::Vec3(x, y, z));

        _put_new_job(c);
    }

    virtual IWorkPiece *get_workpiece() {
        return m_workpiece;
    }

    virtual IToolPiece *get_toolpiece() {
        return m_toolpiece;
    }

};

gboolean _timer_function_wrapper(gpointer d) {
    ((CCutter*)d)->_timer_function();
    return TRUE;
}

ICutter *ICutter::create() {
    return new CCutter();
}

} // NS G3


