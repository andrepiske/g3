#ifndef _G2_Thing_H_
#define _G2_Thing_H_

#include "config.h"
#include <vector>

namespace WL {
class ISprite;
class IScreen;
};

class ThingDef
{
public:
    typedef std::vector<WL::ISprite*> SpriteVec;

protected:
    std::string m_name;
    SpriteVec m_sprites;
    
public:
    ThingDef() {
    }

    void draw(IScreen *s);


};

/**
* A thing is every instance of ThingDef
*/
class Thing
{
public:

};


#endif


