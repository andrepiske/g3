#include "toolpiece.h"
#include "tool.h"
#include "sweepgenerator.h"
#include "cvmesh.h"
#include "curve.h"

namespace G3 {

class CToolPiece : public IToolPiece
{
    WL::RCBucket<ITool> m_tool;
    ISweepGenerator *m_sweeper;
    WL::RCBucket<ICVMesh> m_toolmesh;

    WL::RCBucket<ICurve> m_move_curve;
    double m_move_t;

    WL::Vec3 m_position;
    WL::RCBucket<WL::sg::Transform> m_pos_transform;
    
public:
    CToolPiece() {
        m_sweeper = ISweepGenerator::create();
        m_pos_transform = new WL::sg::Transform();
        m_position = WL::Vec3(0, 0, 0);
        _update_translate_matrix();
        set_transform(m_pos_transform);
    }

    virtual void draw_self(WL::IScreen *u) {
        if (m_toolmesh.get_ptr())
            m_toolmesh->draw(u);
    }

    virtual WL::Vec3 get_position() {
        return m_position; // FIXME consider the moving curve
    }

    void _update_translate_matrix() {
        WL::Vec3 d(0, 0, 0);
        if (m_move_curve.get_ptr()) {
            d = m_move_curve->get_value_at(m_move_t);
        }
        m_pos_transform->set_matrix(WL::Mat44::translation(m_position + d));
    }

    virtual void set_position(const WL::Vec3 &p) {
        m_position = p;
        _update_translate_matrix();
    }

    virtual void move_start(ICurve *curve) {
        m_move_t = 0.0;
        m_move_curve = curve;
    }

    virtual void move_step(double t) {
        m_move_t += t;
        if (m_move_t > 1.0)
            m_move_t = 1.0;
        _update_translate_matrix();
    }

    virtual void move_finish() {
        const WL::Vec3 delta = m_move_curve->get_value_at(1.0);
        m_position += delta;
        m_move_curve = 0;
    }

    virtual ITool *get_tool() {
        return m_tool;
    }

    void _update_mesh() {
        m_toolmesh = 0;
        if (!m_tool.get_ptr())
            return;
        m_toolmesh = m_sweeper->make_base_cylinder(2.0);
    }

    virtual void set_tool(ITool *tool) {
        m_tool = tool;
        m_sweeper->set_tool(tool);
        _update_mesh();
    }


};

IToolPiece *IToolPiece::create() {
    return new CToolPiece();
}


} // NS G3

