#include <stdio.h>
#include <iostream>
#include <gtk/gtk.h>
#include <tegtkgl.h>

#include "drawsurface.h"
#include "g3.h"
#include <wl/uri.h>
#include <wl/log.h>

int main(int argc, char *argv[]) {

    WL::Log::get_singleton()->push_file(stderr);
    WL_LOG_INFO("G3 Starting");

    gtk_init(&argc, &argv);
    G3::IMain::instance()->initialize(argc, argv);
    G3::IMain::instance()->run();

    return 0;
}

