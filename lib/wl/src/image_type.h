#ifndef _WL_CImage_H_
#define _WL_CImage_H_

#include <GL/gl.h>
#include <wl/image.h>
#include <FreeImage.h>

wlStartNS
class CImage : public IImage
{
public:
    int m_width, m_height;
    GLuint m_glid;

    CImage() {
        m_width = 0;
        m_height = 0;
        m_glid = GL_INVALID_VALUE;
        m_restype = RST_IMAGE;
    }

    virtual void draw_quad(const Vec2 &o, const Vec2 &d);

    virtual ~CImage() {}

    virtual dim_type size() {
        // TODO: find another way to validate the width/height fields
        // this is loading the full image!
        get_glbind();
        return dim_type(m_width, m_height);
    }

    virtual GLuint get_glbind();

    void set_gl_hints();
};

class CResourceImage : public CImage
{
public:
    CResourceImage() : CImage() {
    }

    wlBool load_image(wlBool load_gl);

    virtual GLuint get_glbind();

    void load_gl_image(FIBITMAP*);

    virtual void load_data();
    virtual void unload_data();

};

class CBufferedImage : public CImage
{
public:
    wlByte *m_buffer;
    int m_rowsize;
    int m_pixelformat;

    CBufferedImage();
    virtual ~CBufferedImage();

    int _pixelsize(int pf);

    int _gl_pixelsize(int pf);

    int _gl_pixelformat(int pf);

    void load_buffer(const void *_buf, int w, int h, int pf, int rs);
    void internal_create_blank(int w, int h, int pf);

    virtual wlBool get_row(wlByte **bb, int row);
    virtual void update_from_buffer();

    virtual GLuint get_glbind();

    /* we won't use load/unload data because there is no way of
       reloading a buffered image */
    virtual void load_data() {}
    virtual void unload_data() {}

};
wlEndNS

#endif

