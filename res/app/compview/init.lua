require('lgob.gtk')
require('class')
require('lgob.cairo')

compview = {}

function compview.create_view()
    return compview.CompView.new()
end

compview.CompView = Class(function(S)

    S.adj_horz = gtk.Adjustment.new(90, -100, 100, 10, 10)
    S.adj_vert = gtk.Adjustment.new(0, -100, 100, 10, 10)
    --S.adj_horz = gtk.Adjustment.new(0, -1, 1, 0.01, 0.005)
    --S.adj_vert = gtk.Adjustment.new(0, -1, 1, 0.01, 0.005)

    S.scroll_window = gtk.ScrolledWindow.new(S.adj_horz, S.adj_vert)
    S.scroll_window:set_policy(gtk.POLICY_ALWAYS, gtk.POLICY_ALWAYS)

    S.view_area = gtk.DrawingArea.new()
    S.view_area:connect('draw', function(ud, ct) S:draw_view(ct) end)
    S.scroll_window:add(S.view_area)

    function S:draw_view(uct)
        local view_width = self.view_area:get_allocated_width()
        local view_height = self.view_area:get_allocated_height()
        local cairo = cairo
        local ct = cairo.Context.wrap(uct)

        local bgpaint = cairo.Pattern.create_linear(0, 0, 0, view_height)
        bgpaint:add_color_stop_rgb(0, 224/255, 224/255, 224/255)
        bgpaint:add_color_stop_rgb(view_height, 1, 1, 1)

        ct:set_source(bgpaint)
        ct:rectangle(0, 0, view_width, view_height)
        ct:fill()
    end

    function S:get_widget()
        return self.scroll_window
    end

end)


module('compview')

