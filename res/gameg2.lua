

-- prefixes are:
-- X_Y where X in [l,r,f,b] (left right fron back)
-- and Y in [w, s] (walking, still)
mario = {
    little = {
        l_w = { 49, 0, 15, 19 },
        l_s = { 169, 0, 14, 20 },
        r_s = { 209, 0, 14, 20 },
        r_w = { 328, 0, 15, 19 }
    }
}

sprites = { mario }

