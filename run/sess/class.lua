
Class = function(...)
    local args = {...}
    local Kmeta = {}

    function Kmeta:__index(n)
        local v = self['##values'][n]
        if v == nil then
            for _,p in ipairs(self['##parents']) do
                v = p[n]
                if v ~= nil then
                    return v
                end
            end
        end
        return v
    end
    function Kmeta:__newindex(n, nv)
        self['##values'][n] = nv
    end

    local parents = {}
    local initproc = nil
    local pri = 0
    local autoinits = {}
    if #args > 0 and type(args[1])=='function' then
        initproc = args[1]
        pri = pri + 1
    end
    for i=(pri+1),#args,1 do
        local a = args[i]
        if type(a) == 'table' and a.AutoInit == AutoInit then
            autoinits[#autoinits + 1] = a
            a = a.C
        end
        parents[i-pri] = a
    end
    return {
        ['new']=function(...)
            local K = {}
            K['##values'] = {}
            local pr = {}
            for i,v in ipairs(parents) do
                pr[i] = v
            end
            K['##parents'] = pr
            K['##init'] = initproc
            setmetatable(K, Kmeta)
            for _,v in ipairs(autoinits) do
                v.C.new(K, v.P)
            end
            initproc(K, ...)
            return K
        end,
        ['init']=initproc,
    }
end

AutoInit = function(s, ...)
    return { ['AutoInit']=AutoInit, ['C']=s, ['P']={...} }
end


