#include <wl/kreator.h>
#include <boost/unordered_map.hpp>

wlStartNS

// The default kreator instance.
static
Kreator *g_default_kreator = 0;

//////////////////////////////////////////////////////////////////////

class DefaultKreator : public Kreator
{
public:
    typedef boost::unordered_map<Quark, IKreatorRegistry*> RegMap;
    RegMap m_regmap;
    
public:
    virtual RefCounted *create(Quark name) {
        RegMap::const_iterator it = m_regmap.find(name);
        if (it != m_regmap.end())
            return it->second->create();
        
        return 0;
    }

    virtual void register_local(Quark name, IKreatorRegistry *r) {
         m_regmap[name] = r;
    }

};

Kreator *Kreator::default_instance() {
    if (!g_default_kreator)
        g_default_kreator = new DefaultKreator();

    return g_default_kreator;
}

//////////////////////////////////////////////////////////////////////

RefCounted *k_create(Quark type) {
    return Kreator::default_instance()->create(type);
}

wlEndNS

