/* This is a generated file, do not edit it */
#include <boost/unordered_map.hpp>
#include <lua.hpp>
#include <wl/scriptable.h>
#include "cutter.if.h"


static
int _G3_ICutter__closure(lua_State *L) {
    const int proc_id = lua_tonumber(L, lua_upvalueindex(1));
    G3::ICutter *obj = *(G3::ICutter**)luaL_checkudata(L, 1, "WL-G3::ICutter");
    switch (proc_id) {
        case 1: { // get_display_flags
            int _valid[] = {0,0,0,0,0};
            _valid[0] = lua_isboolean(L, 2);
            _valid[1] = lua_isboolean(L, 3);
            _valid[2] = lua_isboolean(L, 4);
            _valid[3] = lua_isboolean(L, 5);
            int v_show_sweep;
            int v_show_workpiece;
            int v_show_toolpiece;
            int v_show_cut;
            if (_valid[0]) {
                v_show_sweep = lua_toboolean(L, 2);
            }
            if (_valid[1]) {
                v_show_workpiece = lua_toboolean(L, 3);
            }
            if (_valid[2]) {
                v_show_toolpiece = lua_toboolean(L, 4);
            }
            if (_valid[3]) {
                v_show_cut = lua_toboolean(L, 5);
            }
            obj->get_display_flags(_valid[0]?((&v_show_sweep)):0, _valid[1]?((&v_show_workpiece)):0, _valid[2]?((&v_show_toolpiece)):0, _valid[3]?((&v_show_cut)):0);            
            lua_pushboolean(L, v_show_sweep);
            lua_pushboolean(L, v_show_workpiece);
            lua_pushboolean(L, v_show_toolpiece);
            lua_pushboolean(L, v_show_cut);
            return 4;
        }
        case 2: { // get_tool
            int _valid[] = {0};
            ::WL::RefCounted* _v_ret;
            _v_ret = obj->get_tool();            
            ::WL::scriptable::push(L, (::WL::RefCounted*)_v_ret);
            return 1;
        }
        case 3: { // dbg_set_paused
            int _valid[] = {0,0};
            _valid[0] = lua_isboolean(L, 2);
            int v_is_paused;
            v_is_paused = lua_toboolean(L, 2);
            obj->dbg_set_paused(v_is_paused);            
            return 0;
        }
        case 4: { // dbg_machining_step
            int _valid[] = {0};
            obj->dbg_machining_step();            
            return 0;
        }
        case 5: { // set_cutspeed
            int _valid[] = {0,0};
            _valid[0] = lua_isnumber(L, 2);
            double v_cs;
            v_cs = lua_tonumber(L, 2);
            obj->set_cutspeed(v_cs);            
            return 0;
        }
        case 6: { // apply_cut
            int _valid[] = {0};
            obj->apply_cut();            
            return 0;
        }
        case 7: { // set_display_flags
            int _valid[] = {0,0,0,0,0};
            _valid[0] = lua_isboolean(L, 2);
            _valid[1] = lua_isboolean(L, 3);
            _valid[2] = lua_isboolean(L, 4);
            _valid[3] = lua_isboolean(L, 5);
            int v_show_sweep;
            int v_show_workpiece;
            int v_show_toolpiece;
            int v_show_cut;
            v_show_sweep = lua_toboolean(L, 2);
            v_show_workpiece = lua_toboolean(L, 3);
            v_show_toolpiece = lua_toboolean(L, 4);
            v_show_cut = lua_toboolean(L, 5);
            obj->set_display_flags(v_show_sweep, v_show_workpiece, v_show_toolpiece, v_show_cut);            
            return 0;
        }
        case 8: { // move_tool_straight
            int _valid[] = {0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            double v_x;
            double v_y;
            double v_z;
            v_x = lua_tonumber(L, 2);
            v_y = lua_tonumber(L, 3);
            v_z = lua_tonumber(L, 4);
            obj->move_tool_straight(v_x, v_y, v_z);            
            return 0;
        }
        case 9: { // create_raw_block
            int _valid[] = {0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            double v_width;
            double v_height;
            double v_depth;
            v_width = lua_tonumber(L, 2);
            v_height = lua_tonumber(L, 3);
            v_depth = lua_tonumber(L, 4);
            obj->create_raw_block(v_width, v_height, v_depth);            
            return 0;
        }
        case 10: { // workpiece_stats1
            int _valid[] = {0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            int v_vert_count;
            int v_edge_count;
            int v_face_count;
            if (_valid[0]) {
                v_vert_count = lua_tonumber(L, 2);
            }
            if (_valid[1]) {
                v_edge_count = lua_tonumber(L, 3);
            }
            if (_valid[2]) {
                v_face_count = lua_tonumber(L, 4);
            }
            obj->workpiece_stats1(_valid[0]?((&v_vert_count)):0, _valid[1]?((&v_edge_count)):0, _valid[2]?((&v_face_count)):0);            
            lua_pushnumber(L, v_vert_count);
            lua_pushnumber(L, v_edge_count);
            lua_pushnumber(L, v_face_count);
            return 3;
        }
        case 11: { // translate_tool
            int _valid[] = {0,0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            _valid[2] = lua_isnumber(L, 4);
            double v_x;
            double v_y;
            double v_z;
            v_x = lua_tonumber(L, 2);
            v_y = lua_tonumber(L, 3);
            v_z = lua_tonumber(L, 4);
            obj->translate_tool(v_x, v_y, v_z);            
            return 0;
        }
        case 12: { // set_cutting_quality
            int _valid[] = {0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_isnumber(L, 3);
            double v_curvedt;
            double v_numsteps;
            v_curvedt = lua_tonumber(L, 2);
            v_numsteps = lua_tonumber(L, 3);
            obj->set_cutting_quality(v_curvedt, v_numsteps);            
            return 0;
        }
        case 13: { // set_tool_state
            int _valid[] = {0,0};
            _valid[0] = lua_isboolean(L, 2);
            int v_is_on;
            v_is_on = lua_toboolean(L, 2);
            obj->set_tool_state(v_is_on);            
            return 0;
        }
    }
    return 0;
}

static
int _G3_ICutter__gc(lua_State *L) {
    G3::ICutter *ptr = *(G3::ICutter**)lua_touserdata(L, 1);
    ptr->rc_pop();
    return 0;
}

void _G3_ICutter__regMT(lua_State *L) {
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, _G3_ICutter__gc);
    lua_setfield(L, -2, "__gc");
    lua_pushnumber(L, 1);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "get_display_flags");
    lua_pushnumber(L, 2);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "get_tool");
    lua_pushnumber(L, 3);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "dbg_set_paused");
    lua_pushnumber(L, 4);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "dbg_machining_step");
    lua_pushnumber(L, 5);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "set_cutspeed");
    lua_pushnumber(L, 6);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "apply_cut");
    lua_pushnumber(L, 7);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "set_display_flags");
    lua_pushnumber(L, 8);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "move_tool_straight");
    lua_pushnumber(L, 9);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "create_raw_block");
    lua_pushnumber(L, 10);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "workpiece_stats1");
    lua_pushnumber(L, 11);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "translate_tool");
    lua_pushnumber(L, 12);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "set_cutting_quality");
    lua_pushnumber(L, 13);
    lua_pushcclosure(L, _G3_ICutter__closure, 1);
    lua_setfield(L, -2, "set_tool_state");
}

void _G3_ICutter__luapush(lua_State *L, WL::RefCounted *_obj) {
    G3::ICutter *obj = dynamic_cast<G3::ICutter*>(_obj);
    *(void**)lua_newuserdata(L, sizeof(void*)) = (void*)obj;
    if (luaL_newmetatable(L, "WL-G3::ICutter"))
        _G3_ICutter__regMT(L);
    lua_setmetatable(L, -2);
    obj->rc_push();
}


