#ifndef _WL_RefCounted_H_
#define _WL_RefCounted_H_

#include <wl/config.h>
#include <wl/log.h>

wlStartNS

class RefCounted
{
public:
    RefCounted() : m_ref_count(1), m_floating(1) {
    }
    virtual ~RefCounted() {}

    virtual void rc_push() const {
        if (m_floating)
            m_floating = 0;
        else
            m_ref_count++;
    }

    template<class T>
    T* rc_push_r() const {
        rc_push();
        return (T*)this;
    }

    template<class T>
    const T* rc_push_rc() const {
        rc_push();
        return (const T*)this;
    }

    template<class T>
    T* rc_pop_r() const {
        rc_pop();
        if (!m_ref_count)
            return wlNull;

        return (T*)this;
    }

    template<class T>
    const T* rc_pop_rc() const {
        rc_pop();
        if (!m_ref_count)
            return wlNull;

        return (const T*)this;
    }

    virtual void rc_pop() const {
        if (!m_ref_count) {
            WL_LOG_WARN("Reference counted object went < 0", 0);
            return;
        }

        m_ref_count--;
        if (!m_ref_count)
            delete this;
    }

public:
    mutable u32 m_ref_count;
    mutable int m_floating : 1;
};

template <class T>
class RCBucket
{
protected:
    T *m_ptr;

public:
    typedef RCBucket<T> ThisT;

    RCBucket() {
        m_ptr = wlNull;
    }

    RCBucket(const RCBucket &r) {
        m_ptr = r.m_ptr;
        if (m_ptr)
            m_ptr->rc_push();
    }

    RCBucket(T *ptr) {
        m_ptr = ptr;
        if (ptr)
            ptr->rc_push();
    }

    static ThisT pushless(T *ptr) {
        ThisT b;
        b.m_ptr = ptr;
        return b;
    }

    RCBucket &operator=(T *ptr) {
        if (ptr)
            ptr->rc_push();
        if (m_ptr)
            m_ptr->rc_pop();
        m_ptr = ptr;
        return *this;
    }

    RCBucket &operator=(RCBucket &b) {
        if (b.m_ptr)
            b.m_ptr->rc_push();
        if (m_ptr)
            m_ptr->rc_pop();
        m_ptr = b.m_ptr;
        return *this;
    }

    RCBucket &operator=(const RCBucket &b) {
        if (b.m_ptr)
            b.m_ptr->rc_push();
        if (m_ptr)
            m_ptr->rc_pop();
        m_ptr = b.m_ptr;
        return *this;
    }


    ~RCBucket() {
        if (m_ptr)
            m_ptr->rc_pop();
    }

    operator T* () {
        return m_ptr;
    }

    operator const T* () {
        return (const T*)m_ptr;
    }

    T* operator->() {
        return m_ptr;
    }

    const T* operator->() const {
        return m_ptr;
    }

    T* operator()() {
        return m_ptr;
    }

    const T* operator()() const {
        return m_ptr;
    }

    T *get_ptr() {
        return m_ptr;
    }

    const T *get_ptr() const {
        return m_ptr;
    }

    RefCounted *get_ptr_rc() {
        return m_ptr;
    }

    const RefCounted *get_ptr_rc() const {
        return m_ptr;
    }

};

wlEndNS
#endif


