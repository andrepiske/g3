require('class')
require('node')
require('lmat')

Main = Class(function(S, _g3, _nb)
    S.g3 = _g3
    S.notebook = _nb
    S.meshes = {}

    function S:run()
    end

    function S:new_mesh()
        local mesh = ShMesh.new(self.g3, self.g3:add_mesh())
        table.insert(self.meshes, mesh)
        return mesh
    end

    function S:start()
        local function base_transform(_a, _b, _tr)
            local a = lmat.vec(unpack(_a)):normlz()
            local b = lmat.vec(unpack(_b)):normlz()
            local tr = _tr and lmat.vec(unpack(_tr)) or lmat.vec(0, 0, 0)
            return function(v)
                return a*v[1] + b*v[2] + tr
            end
        end

        local function faceset(fc, vts)
            local fv = fc.vertices
            for i,v in ipairs(vts) do
                table.insert(fv, lmat.vec(v[1], v[2]))
            end
        end

        local Square = {
            { -1, -1 },
            { 1, -1 },
            { 1, 1 },
            { -1, 1 },
        }

        local msh = Mesh.new()

        local fc = Face.new()
        faceset(fc, Square)

        table.insert(msh.faces, fc)
        msh:apply_mesh(self:new_mesh())
    end

end)

function main(g3, nb)
    if mainInstance == nil then
        mainInstance = Main.new(g3, nb)
        mainInstance:start()
    end
    mainInstance:run()
end


