#ifndef _WL_err_h_
#define _WL_err_h_

namespace WL
{

class Err
{
private:
    Err *s_this;

public:
    Err() {
    }

    static Err *get_inst();

    static void info(const char *s, ...);
    static void fail(const char *s, ...);
    static void gbye(const char *s, ...);
    
};

};

#endif

