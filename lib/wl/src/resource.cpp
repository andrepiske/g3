#include "precompiled.h"
#include <wl/resource.h>
#include <sstream>
#include <list>
#include <wl/ar.h>

wlStartNS

std::string Resource::get_full_path() const {
    typedef std::list<std::string> ls;
    ls path;
    const Resource *res = this;

    while (res->m_resparent) {
        path.push_front(res->m_resname);
        res = res->m_resparent;
    }

    std::stringstream str_path;
    ls::iterator it;
    for (it = path.begin(); it != path.end(); ++it)
        str_path << '/' << (*it);
        
    return str_path.str(); 
}

Resource *Resource::get_root() {
    Resource *k = this;
    while (k->m_resparent)
        k = k->m_resparent;

    return k;
}

Resource::~Resource() {
    PVecType::iterator it = m_reschildren.begin();
    for (; it!= m_reschildren.end(); ++it) {
        Resource *rc = *it;
        // call this before rc_pop, or __resource_deleted will be called
        // m_children.size() times, and this iterator will crash. so mad!
        rc->m_resparent = wlNull;
        rc->rc_pop();
    }
    if (m_resparent)
        m_resparent->__resource_deleted(this);
}

void Resource::__resource_deleted(Resource *rc) {
    PVecType::iterator it = m_reschildren.begin();
    for (; it!= m_reschildren.end(); ++it) {
        if ((*it) == rc) {
            m_reschildren.erase(it);
            return;
        }
    }
}

wlEndNS

