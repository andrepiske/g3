#ifndef _WL_Resourcear_H_
#define _WL_Resourcear_H_

#include <wl/config.h>
#include <wl/resource.h>
#include <string>

wlStartNS
class IAr : public Resource
{
public:
    IAr() {}
    virtual ~IAr() {}

    static IAr *load_from_file(const std::string &path);

    virtual ResourceIOBlock find_block(const std::string &path) = 0;

};

wlEndNS
#endif


