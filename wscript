import waflib
import shutil
import os

wl_files = [ 'anim.cpp',
    'ar.cpp',
    'color.cpp',
    'drawutil.cpp',
    'event.cpp',
    'fileio.cpp',
    'font.cpp',
    'glstate.cpp',
    'image.cpp',
    # 'input.cpp',
    'log.cpp',
    'math.cpp',
    # 'polybuilder.cpp',
    'poly.cpp',
    'resource.cpp',
    'scene.cpp',
    'sg2.cpp',
    'scenegraph.cpp',
    # 'screen.cpp',
    # 'scriptbind.cpp',
    'script.cpp',
    'sprite.cpp',
    'text.cpp',
    'time.cpp',
    'dict.cpp',
    'util.cpp',
    'uri.cpp',
    'list.cpp',
    'filesys.cpp',
    'scriptable.cpp',
    # 'kreator.cpp',
    # 'wlinit.cpp',
    'quark.cpp' ]

g3_files = [
    'src/tool.if.push.cpp',
    'src/opencsgdraw.cpp',
    'src/cutter.if.push.cpp',
    'src/cutter.cpp',
    'src/glibits.cpp',
    'src/sweepinfo.cpp',
    'src/script_app.if.push.cpp',
    'src/commproto.cpp',
    'src/comm.cpp',
    'src/curve.cpp',
    'src/tool.cpp',
    'src/toolpiece.cpp',
    'src/workpiece.cpp',
    'src/sweepgenerator.cpp',
    'src/cvmesh.cpp',
    'src/var.cpp',
    'src/g3d.cpp',
    # 'src/mesh.cpp',
    'src/g3.cpp',
    'src/document.cpp',
    'src/util.cpp',
    'src/main.cpp',

    'src/wem.cpp',
    # 'src/mesh2.cpp',

    # 'src/math_script.cpp',
    # 'src/booth.cpp',
    'src/drawsurface.cpp',
    # 'src/testdraw.cpp',
    'lib/tegtkgl/tegtkgl.c',

    # generated files.
    'src/g3d.if.push.cpp',
    'src/mesh.if.push.cpp',
    # 'src/imath.if.push.cpp',
    # 'src/list.if.push.cpp',
]

def options(opt):
    opt.load('compiler_cxx compiler_c')

def configure(conf):
    flags=['--cflags', '--libs']
    conf.load('compiler_cxx compiler_c')
    conf.check_cfg(package='freetype2', uselib_store='freetype2', args=flags)
    conf.check_cfg(package='protobuf', uselib_store='protobuf', args=flags, mandatory=True)
    conf.check_cfg(package='lua5.1', uselib_store='lua51', args=flags, mandatory=True)
    conf.check_cfg(package='gtk+-3.0 gmodule-2.0 gthread-2.0', uselib_store='gtk', args=flags, mandatory=True)
    # conf.check_cfg(package='gtksourceview-3.0', uselib_store='gtksv', args=flags, mandatory=True)

    # conf.env.CXXFLAGS += ['-O3', '-Wall', '-march=native', '-mtune=native', '-Wno-pmf-conversions']
    conf.env.CXXFLAGS += ['-O0', '-g', '-Wall', '-Wno-pmf-conversions'] # , '-fno-rtti'] OpenMesh demands RTTI. that sucks.

    # find a better way to do this
    conf.env.STLIB_ST = '%s'
    conf.env.STLIB += ['../lib/Scintilla/lib/scintilla.a']

def build(bld):
    bld.program(source=[bld.path.make_node('lib/wl/src/' + x) for x in wl_files]
        + [bld.path.make_node(x) for x in g3_files],
        uselib=['freetype2', 'gtk', 'lua51', 'protobuf'],
        libpath=['/usr/lib/OpenMesh'],
        lib=['freeimage', 'GL', 'GLU', 'boost_filesystem', 'boost_thread',
            'boost_system', 'pcrecpp', 'carve', 'boost_regex', 'opencsg', 'GLEW'],
        includes=['lib/wl/include', 'lib/tegtkgl', 'lib/Scintilla/include',
            '/usr/include/eigen3'],
        target='g3')

def cleanrun(self):
    print('Not doing it')
    '''
    out = self.path.find_node('run')
    if out is not None:
        shutil.rmtree(out.abspath())
    '''

def makerun(self):
    res_files = [
        'boneco.svg',
        'ui.glade',
        'wireframe_icon.png',
        'shownormals_icon.png',
        'showgrid_icon.png',
        'cube.mtl',
        'cube.obj',
        'run_icon.png'
    ]
    files = [
        ('build/g3', 'g3'),
    ] + [ 'res/' + x for x in res_files ]

    out = self.path.make_node('run')
    out.mkdir()

    out_base = out.abspath()
    from_base = self.path.abspath()
    for fd in files:
        if isinstance(fd, tuple):
            f = fd[0]
        else:
            f = fd
        if not os.path.exists(os.path.join(from_base, f)):
            raise Exception('File "%s" not found' % f)

    for f in files:
        if isinstance(f, tuple):
            from_path = f[0]
            to_path = f[1]
        else:
            from_path = to_path = f

        to_path = os.path.join(out_base, to_path)
        if os.path.exists(to_path):
            os.remove(to_path)
            waflib.Logs.info('Replace "%s"' % from_path)
        else:
            waflib.Logs.info('Copy "%s"' % from_path)

        from_path = os.path.join(from_base, from_path)
        to_dir = os.path.dirname(to_path)
        if not os.path.exists(to_dir):
            os.makedirs(to_dir)

        shutil.copy(from_path, to_path)

    

