#include "sweepgenerator.h"
#include "cvmesh.h"
#include "curve.h"
#include <iostream>
#include <list>
#include <vector>
#include <carve/geom.hpp>
#include "tool.h"

#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wunused-variable"

typedef carve::mesh::MeshSet<3> meshset_t;
typedef carve::poly::Polyhedron poly_t;
typedef meshset_t::mesh_t mesh_t;
typedef meshset_t::vertex_t vertex_t;
typedef meshset_t::face_t face_t;

namespace G3 {

extern WL::sg::Node *_sweepinfo_new_drawing(SweepInfo*);

class CSweepGenerator : public ISweepGenerator
{
private:
    WL::RCBucket<ICurve> m_curve;
    WL::RCBucket<ITool> m_tool;
    WL::Vec3 m_normal;

    QualityParameters m_qa_params;
    // SweepInfo m_sweep_info;

public:
    CSweepGenerator() {
        // normal is always (0, 1, 0), because
        // this simulator doesn't support other modes.
        m_normal = WL::Vec3(0, 1, 0);
        m_qa_params.curve_dt = 0.02;
        m_qa_params.number_steps = 30;
    }

    virtual void set_tool(ITool *tool) {
        m_tool = tool;
    }

    virtual void set_quality_parameters(QualityParameters &qa) {
        m_qa_params = qa;
    }

    struct stepr_t {
        vertex_t bl, br; // bottom left & right
        vertex_t tl, tr; // top left & right
    };

    static vertex_t carve_vertex(const WL::Vec3 &v) {
        return carve::geom::VECTOR(v.x, v.y, v.z);
    }

    virtual ICVMesh *make_sweep_mesh(SweepInfo **si, double t_from, double t_to,
            const WL::Vec3 &offset)
    {
        ICVMesh *msh = generate_sweep_mesh(t_from, t_to, offset);
        if (si)
            *si = 0;
        // if (si)
            // *si = &m_sweep_info;
        return msh;
    }

    struct sgcylgen_s {
        typedef std::vector<vertex_t> arc_t;
        std::vector<arc_t> arcs;
        sgcylgen_s() {
        }
        arc_t &new_arc() {
            arcs.push_back(arc_t());
            return arcs[arcs.size() - 1];
        }
        void gen_faces(std::vector<face_t*> &faces) {
            const size_t num_arcs = arcs.size();
            const size_t num_points = arcs[0].size(); // points per arc
            for (size_t na = 1; na < num_arcs; ++na) {
                const size_t nal = na - 1;
                arc_t &a = arcs[na];
                arc_t &al = arcs[nal];
                for (size_t np = 0; np < num_points; ++np) {
                    const size_t npl = (np ? np-1 : num_points-1);
                    vertex_t *const bl = &al[npl];
                    vertex_t *const br = &al[np];
                    vertex_t *const tl = &a[npl];
                    vertex_t *const tr = &a[np];
                    faces.push_back(new face_t(br, bl, tl));
                    faces.push_back(new face_t(br, tl, tr));
                }
            }
        }
        void close_arc_face(std::vector<face_t*> &faces, arc_t &arc,
            size_t start, size_t end, size_t step)
        {
            vertex_t *closer = &arc[end];
            for (size_t i = start; i != end; i += step) {
                vertex_t *a = &arc[i];
                vertex_t *b = &arc[i+step];
                faces.push_back(new face_t(a, b, closer));
            }
        }
    };

    ICVMesh *generate_sweep_mesh(double t_from, double t_to,
        const WL::Vec3 &offset)
    {
        using namespace WL;
        ICVMesh *cvmesh = ICVMesh::create();
        // m_sweep_info = SweepInfo();
        // m_sweep_info.drawing = _sweepinfo_new_drawing(&m_sweep_info);

        const double top_limit = m_tool->get_height_limit();
        const double radius = m_tool->get_tool_radius();

        std::vector<face_t*> faces;
        sgcylgen_s cyl;

        const double curve_dt = m_qa_params.curve_dt;
        const WL::Vec3 top_vec(0, top_limit, 0);

        bool quit_loop = 0;
        for (double i = t_from; !quit_loop; i += curve_dt) {
            const Vec3 val  = m_curve->get_value_at(i) + offset;
            const Vec3 diff = m_curve->get_derivative_at(i).normal();
            const Vec3 delta = diff.cross(m_normal).normal()*radius;
            sgcylgen_s::arc_t &arc = cyl.new_arc();

            if (i > t_to) {
                i = t_to;
                quit_loop = 1;
            }

            // const Vec3 dh = m_normal * 2.f;
            arc.push_back(carve_vertex(val.vec_xz() - delta + top_vec)); // tl
            arc.push_back(carve_vertex(val - delta)); // bl

            const Vec3 bs_z = diff.cross(m_normal).normal();
            const Vec3 bs_x = m_normal.cross(bs_z).normal();
            const Vec3 bs_y = bs_z.cross(bs_x).normal();
            Mat33 bs_bc;
            bs_bc = calc_basechanger(
                Vec3(1, 0, 0), Vec3(0, 1, 0), Vec3(0, 0, 1),
                bs_x, bs_y, bs_z);
            /*{
                SweepInfo::axis_t at;
                at.pos = val;
                at.x = bs_x.normal() / 7.f;
                at.y = bs_y.normal() / 7.f;
                at.z = bs_z.normal() / 7.f;
                m_sweep_info.axes.push_back(at);
            }*/

            unsigned int num_steps = m_qa_params.number_steps;
            const double step_size = wlPI / (double)num_steps;
            for (size_t j = 1; j < num_steps; ++j) {
                const double r = step_size * (double)j;
                const Vec3 pt(radius*sin(r),
                        0.0, // -radius*sin(r),
                        -radius*cos(r));
                Vec3 pt_rot = mul33(bs_bc, pt);
                arc.push_back(carve_vertex(
                    val + pt_rot
                ));
            }

            arc.push_back(carve_vertex(val + delta)); // br
            arc.push_back(carve_vertex(val.vec_xz() + delta + top_vec)); // tr
        }

        // fecha o inicio:
        {
            sgcylgen_s::arc_t &arc = cyl.arcs[0];
            const size_t npoints = arc.size();

            vertex_t *tl, *tr, *bl, *br;
            tl = &arc[0];
            tr = &arc[npoints-1];
            bl = &arc[1];
            br = &arc[npoints-2];

            for (size_t i = 1; i < npoints-2; ++i) {
                vertex_t *a = &arc[i];
                vertex_t *b = &arc[i+1];
                faces.push_back(new face_t(a, b, br));
            }
            faces.push_back(new face_t(bl, br, tl));
            faces.push_back(new face_t(br, tr, tl));
        }
        // fecha o final:
        {
            const size_t narcs = cyl.arcs.size();
            sgcylgen_s::arc_t &arc = cyl.arcs[narcs-1];
            const size_t npoints = arc.size();

            vertex_t *tl, *tr, *bl, *br;
            tl = &arc[0];
            tr = &arc[npoints-1];
            bl = &arc[1];
            br = &arc[npoints-2];

            for (size_t i = 1; i < npoints-2; ++i) {
                vertex_t *a = &arc[i];
                vertex_t *b = &arc[i+1];
                faces.push_back(new face_t(b, a, br));
            }
            faces.push_back(new face_t(bl, tl, br));
            faces.push_back(new face_t(br, tl, tr));
        }


        cyl.gen_faces(faces);

        meshset_t *ms = new meshset_t(faces);
        cvmesh->set_meshset(ms);
        return cvmesh;
    }

    virtual void set_curve(ICurve *curve) {
        m_curve = curve;
    }

    virtual ICurve *get_curve() {
        return m_curve;
    }

    virtual ICVMesh *make_base_cylinder_fromtop(const WL::Vec3 &tip_pos)
    {
        const double top = m_tool->get_height_limit();
        return _make_base_cylinder(
                tip_pos.vec_xz() + WL::Vec3(0, top, 0),
                tip_pos);
    }

    virtual ICVMesh *make_base_cylinder(double height,
            const WL::Vec3 &offset)
    {
        return _make_base_cylinder(offset + WL::Vec3(0, height, 0), offset);
    }

    ICVMesh *_make_base_cylinder(const WL::Vec3 &top,
            const WL::Vec3 &bottom)
    {
        using namespace WL;
        ICVMesh *cvmesh = ICVMesh::create();
        sgcylgen_s cyl;

        const double radius = m_tool->get_tool_radius();
        std::vector<face_t*> faces;

        cyl.new_arc();
        cyl.new_arc();
        sgcylgen_s::arc_t &arc1 = cyl.arcs[0]; // bottom arc
        sgcylgen_s::arc_t &arc2 = cyl.arcs[1]; // top arc

        const size_t num_steps = (size_t)m_qa_params.number_steps;
        for (size_t step = 0; step < num_steps; ++step) {
            const double ang = 2.0 * wlPI * (double)step / (double)num_steps;
            const double angs = sin(ang);
            const double angc = cos(ang);
            const Vec3 v = Vec3(angc * radius, 0, angs * radius);

            arc1.push_back(carve_vertex(v + bottom));
            arc2.push_back(carve_vertex(v + top));
        }

        cyl.gen_faces(faces);
        cyl.close_arc_face(faces, arc1, 0, arc1.size()-1, 1);
        cyl.close_arc_face(faces, arc2, arc2.size()-1, 0, -1);

        meshset_t *ms = new meshset_t(faces);
        cvmesh->set_meshset(ms);
        return cvmesh;
    }

};

ISweepGenerator *ISweepGenerator::create() {
    return new CSweepGenerator();
}

} // NS G3

