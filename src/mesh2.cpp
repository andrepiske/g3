#include "mesh2.h"
#include <wl/log.h>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

typedef OpenMesh::PolyMesh_ArrayKernelT<> MyMesh;

namespace G3 {

class CMesh2 : public IMesh2
{
private:
    MyMesh m_mesh;

public:
    CMesh2() {
        init();
    }

    void init() {
        if (!OpenMesh::IO::read_mesh(m_mesh, "res/cube.obj")) {
            WL_LOG_ERROR("Could not load cube", 0);
            exit(1);
        }
    }

    virtual void draw_self(WL::IScreen *_u) {
    }

};


IMesh2 *IMesh2::create() {
    return new CMesh2();
}

} // NS G3
