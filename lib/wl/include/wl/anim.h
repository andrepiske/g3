#ifndef _WL_Anim_H_
#define _WL_Anim_H_

#include <wl/config.h>
#include <wl/color.h>
#include <wl/refcounted.h>
#include <wl/quark.h>

wlStartNS

class IAnimTime : public RefCounted
{
public:
    /**
     * Get time elapsed, in seconds, since some fixed arbitrary point
     * This can be based on real clock, or maybe a frame-rate based time.
     */
    virtual float get_time() = 0;

    /**
     * This must be called once every frame.
     */
    virtual void update() = 0;

    static IAnimTime *create();

};

class IAnim : public RefCounted
{
public:
    // the duration in seconds
    float m_duration;

    // how many times to loop:
    // x = 0 means no loop, x < 0 means indefinite loop
    int m_loop;

public:
    virtual void update() = 0;

};


wlEndNS
#endif

