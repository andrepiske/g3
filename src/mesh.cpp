#include "mesh.h"
#include <wl/scriptable.h>
#include <wl/math.h>
#include <vector>
#include <GL/gl.h>

// external
void _G3_IMesh__luapush(lua_State *L, G3::IMesh *obj);

// TODO: gambiarra
extern int _S_G_DrawWireframe;

namespace G3 {

class CMesh : public IMesh,
              public WL::scriptable::RegPush<IMesh>
{
public:
    struct Face {
        WL::Vec3 normal;
        size_t a, b, c;
        Face() : a(0), b(0), c(0) {
        }
    };

    struct Vertex {
        float co[3]; // coordinates
        float color[4];

        WL::Vec3 get_coords() const {
            return WL::Vec3(co[0], co[1], co[2]);
        }

        // FIXME:
        // by not initializing the variables, a security hole is created.
        // please let begin initializing *all* variables from now on.
    };

public:
    typedef std::vector<Vertex> VertexVec;
    typedef std::vector<Face> FaceVec;

    VertexVec m_vertices;
    FaceVec m_faces;

public:
    CMesh() : WL::scriptable::RegPush<IMesh>(this, _G3_IMesh__luapush) {
    }

    virtual ~CMesh() {
    }

    virtual int num_vertices() {
        return (int)m_vertices.size();
    }

    virtual int num_faces() {
        return (int)m_faces.size();
    }

    virtual void set_num_vertices(int num) {
        m_vertices.resize((size_t)num);
    }

    virtual void set_num_faces(int num) {
        m_faces.resize((size_t)num);
    }

    virtual void set_vertex(int index, double x, double y, double z) {
        Vertex &v = m_vertices[(size_t)index];
        v.co[0] = (float)x;
        v.co[1] = (float)y;
        v.co[2] = (float)z;
    }

    virtual void get_vertex(int index, double *x, double *y, double *z) {
        Vertex &v = m_vertices[(size_t)index];
        if (x) *x = v.co[0];
        if (y) *y = v.co[1];
        if (z) *z = v.co[2];
    }

    virtual void get_vertex_color(int index, int *r, int *g, int *b, int *a) {
        Vertex &v = m_vertices[(size_t)index];
        if (r) *r = (int)(v.color[0] * 255.f);
        if (g) *g = (int)(v.color[1] * 255.f);
        if (b) *b = (int)(v.color[2] * 255.f);
        if (a) *a = (int)(v.color[3] * 255.f);
    }

    virtual void set_vertex_color(int index, int r, int g, int b, int a) {
        Vertex &v = m_vertices[(size_t)index];
        v.color[0] = (float)r / 255.f;
        v.color[1] = (float)g / 255.f;
        v.color[2] = (float)b / 255.f;
        v.color[3] = (float)a / 255.f;
    }

    virtual void set_face(int index, int a, int b, int c) {
        Face &f = m_faces[(size_t)index];
        f.a = (size_t)a;
        f.b = (size_t)b;
        f.c = (size_t)c;
    }

    virtual void get_face(int index, int *a, int *b, int *c) {
        Face &f = m_faces[(size_t)index];
        if (a) *a = (int)f.a;
        if (b) *b = (int)f.b;
        if (c) *c = (int)f.c;
    }

    virtual void autoset_face_normal(int index) {
        using namespace WL;

        Face &f = m_faces[(size_t)index];
        const Vec3 v1( m_vertices[f.b].get_coords() - m_vertices[f.a].get_coords() );
        const Vec3 v2( m_vertices[f.c].get_coords() - m_vertices[f.b].get_coords() );

        f.normal = v1.cross(v2);
        f.normal.normalize();
    }

    virtual void set_face_normal(int index, double x, double y, double z) {
        Face &f = m_faces[(size_t)index];
        f.normal = WL::Vec3(x, y, z);
    }

    virtual void draw_self(WL::IScreen *u) {
        FaceVec::const_iterator fit = m_faces.begin();
        for (; fit != m_faces.end(); ++fit) {
            const size_t *vidx = &fit->a;

            // /*
            if (_S_G_DrawWireframe) {
                glLineWidth(2.0);
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            } else {
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            }
            // */

            glEnable(GL_LIGHTING);
            glBegin(GL_TRIANGLES);
            glNormal3fv((const GLfloat*)(&fit->normal));
            for (size_t i = 0; i < 3; ++i, vidx++) {
                const Vertex &v = m_vertices[*vidx];
                glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE,
                        (GLfloat*)v.color);
                // Let this here for future use: glColor4fv((GLfloat*)v.color);
                glVertex3fv((GLfloat*)v.co);
            }
            glEnd();

            /*
            glDisable(GL_LIGHTING);
            glLineWidth(1.0);
            glBegin(GL_LINES);
            const WL::Vec3 fn = fit->normal;
            WL::Vec3 fn2(fn);
            fn2.normalize();
            fn2 += fn;
            glVertex3fv((const GLfloat*)&fn);
            glVertex3fv((const GLfloat*)&fn2);
            glEnd();
            */
        }
    }


};

IMesh *IMesh::create() {
    return new CMesh();
}

} // NS G3

