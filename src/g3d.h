#ifndef _G3_Gd_H_
#define _G3_Gd_H_

#include <wl/refcounted.h>
#include <wl/scenegraph.h>
#include "drawsurface.h"
#include "mesh.h"

namespace G3 { 

// namespace Math { class IVertexGroup; }

class IGd : public IDrawGuy
{
// input
public:
    virtual void input_motion_start() = 0;
    virtual void input_motion(double dx, double dy) = 0;
    virtual void input_motion_stop() = 0;

public:
    virtual WL::sg::Node *get_rootnode() = 0;

    virtual void set_draw_wireframe(int wireframe) = 0;

    virtual void rem_mesh(IMesh* mesh) = 0;        
    virtual IMesh* add_mesh() = 0;        

    virtual IMesh *get_mesh(int index) = 0;
    virtual int num_meshes() = 0;

    virtual void draw(IDrawSurface *surface) = 0;

    virtual void calculate_camera_vectors(WL::Vec3 &direction, WL::Vec3 &up) = 0;

    virtual WL::Vec3 get_camera_pos() = 0;
    virtual void set_camera_pos(const WL::Vec3 &cp) = 0;

    virtual void get_camera_args(double &alpha, double &beta) = 0;
    virtual void set_camera_args(double alpha, double beta) = 0;

    static IGd *create();

};

} // NS G3

#endif

