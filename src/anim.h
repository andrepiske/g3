#ifndef _WL_anim_h_
#define _WL_anim_h_

namespace WL {

template<class T>
class IAnim
{
public:
   virtual T tick(T l);

};


};

#endif

