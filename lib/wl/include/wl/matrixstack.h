#ifndef _WL_MatrixStack_H_
#define _WL_MatrixStack_H_

#include <wl/config.h>
#include <wl/math.h>
#include <list>

wlStartNS

template<class M, int N=20>
class MatrixStack {

    typedef std::list<M> DynamicT;

    M m_fixedstack[N];
    DynamicT m_dyn;

    unsigned long m_fixed_count;
    M m_current;


public:
    MatrixStack() {
        m_fixed_count = 0;
        m_current.set_identity();
    }

    const M &value() const {
        return m_current;
    }

    M &value() {
        return m_current;
    }

    WLINLINE void push_and_set(const M &m) {
        push();
        m_current = m;
    }

    void push() {
        if (m_fixed_count == N) {
            m_dyn.push_back(m_current);
        } else {
            m_fixedstack[m_fixed_count] = m_current;
            m_fixed_count++;
        }
    }

    void reset() {
        m_dyn.clear();
        m_fixed_count = 0;
        m_current.set_identity();
    }

    void pop() {
        if (!m_fixed_count)
            return;

        if (m_fixed_count == N) {
            m_current = m_dyn.front();
            m_dyn.clear();
        } else if (m_fixed_count < N) {
            m_current = m_fixedstack[m_fixed_count--];
        } else {
            m_current = m_dyn.back();
            m_dyn.pop_back();
        }
    }
    


};


wlEndNS

#endif

