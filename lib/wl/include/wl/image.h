#ifndef _WL_Resource_Image_H_
#define _WL_Resource_Image_H_

#include <wl/config.h>
#include <wl/resource.h>
#include <wl/math.h>
#include <utility>
#include <string>
#include <GL/gl.h>

wlStartNS
class IImage : public Resource
{
public:
    enum pixel_format_t {
        PF_GRAY = 0,
        PF_RGB,
        PF_RGBA,
    };
    
    typedef std::pair<int, int> dim_type;

    virtual void draw_quad(const Vec2 &origin, const Vec2 &dim) = 0;

    // get image size
    virtual dim_type size() = 0;

    /* same as above, but converted */
    Vec2 size_vec() {
        dim_type d = size();
        return Vec2((float)d.first, (float)d.second);
    }

    static IImage *create_resource(ResourceIOBlock blk);

    static IImage *create_from_buffer(const void *buffer,
        int width, int height, int pixel_format, int row_size);

    static IImage *create_blank(int width, int height, int pf);

    /* Get a writable buffer (if supported) */
    virtual wlBool get_buffer(wlByte **bb) { return wlFalse; }
    virtual wlBool get_row(wlByte **bb, int row) { return wlFalse; }

    virtual void update_from_buffer() { }

    virtual GLuint get_glbind() = 0;

};

wlEndNS
#endif

