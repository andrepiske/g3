#ifndef _G3_Util_H_
#define _G3_Util_H_

#include "var.h"
#include <wl/math.h>

//#include <boost/function.hpp>
//#include <gtk/gtk.h>

namespace G3 {
namespace util {

IVar *var_from_vec3(const WL::Vec3 &v);
wlBool vec3_from_var(IVar *var, WL::Vec3 &out_v);

/*
void
connect_gsignal(void *object, const char *signal_name,
    boost::function<void()> &callback);
*/

} // NS util
} // NS G3

#endif

