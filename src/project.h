#ifndef _G3_Project_H_
#define _G3_Project_H_

#include <wl/refcounted.h>
#include <string>

namespace G3 {

class IProject : public WL::RefCounted
{
public:
    virtual wlBool load_from_path(const char *path) = 0;

    static IProject *create();

};



} // NS G3

#endif

