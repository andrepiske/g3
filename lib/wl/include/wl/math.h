#ifndef _WL_Math_H_
#define _WL_Math_H_

#include <wl/config.h>
#include <math.h>
// cmath for sqrt
#include <cmath>
#include <string>

wlStartNS

struct Vec2 {
    float x, y;

    Vec2() {
    }

    Vec2(float _x, float _y) : x(_x), y(_y) {
    }

    Vec2(const Vec2 &e) : x(e.x), y(e.y) {
    }

    WLINLINE Vec2 &operator=(const Vec2 &e) {
        x = e.x, y = e.y;
        return *this;
    }

    WLINLINE float operator[](int n) const {
        return (&x)[n];
    }

    WLINLINE float& operator[](int n) {
        return (&x)[n];
    }

    WLINLINE Vec2 operator*(float n) const {
        return Vec2(n * x, n * y);
    }

    WLINLINE Vec2 &operator*=(float n) {
        x *= n;
        y *= n;
        return *this;
    }

    WLINLINE Vec2 operator/(float n) const {
        return Vec2(x / n, y / n);
    }

    WLINLINE Vec2 &operator/=(float n) {
        x /= n;
        y /= n;
        return *this;
    }

    WLINLINE Vec2 operator+(const Vec2 &e) const {
        return Vec2(x + e.x, y + e.y);
    }

    WLINLINE Vec2 &operator+=(const Vec2 &e) {
        x += e.x;
        y += e.y;
        return *this;
    }

    WLINLINE Vec2 operator -() const {
        return Vec2(-x, -y);
    }

    WLINLINE Vec2 operator-(const Vec2 &e) const {
        return Vec2(x - e.x, y - e.y);
    }

    WLINLINE Vec2 &operator-=(const Vec2 &e) {
        x -= e.x;
        y -= e.y;
        return *this;
    }

    WLINLINE Vec2 vec_rem(int index) const {
        Vec2 k = *this;
        k[index] = 0;
        return k;
    }

    WLINLINE Vec2 vec_x() const {
        return Vec2(x, 0);
    }

    WLINLINE Vec2 vec_y() const {
        return Vec2(0, y);
    }

    WLINLINE void set_zero() {
        x = y = 0.f;
    }

    WLINLINE float length() const {
        return sqrt(x * x + y * y);
    }

    WLINLINE float length_squared() const {
        return x * x + y * y;
    }

    WLINLINE void normalize() {
        float l = length();
        x /= l;
        y /= l;
    }

};

struct Mat33;

struct Vec3 {
    float x, y, z;

    Vec3() {
    }

    Vec3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {
    }

    Vec3(const Vec3 &e) : x(e.x), y(e.y), z(e.z) {
    }

    WLINLINE Vec3 &operator=(const Vec3 &e) {
        x = e.x, y = e.y, z = e.z;
        return *this;
    }

    WLINLINE float operator[](int n) const {
        return (&x)[n];
    }

    WLINLINE float& operator[](int n) {
        return (&x)[n];
    }

    WLINLINE Vec3 operator*(float n) const {
        return Vec3(x * n, y * n, z * n);
    }

    WLINLINE Vec3 &operator*=(float n) {
        x *= n;
        y *= n;
        z *= n;
        return *this;
    }

    WLINLINE Vec3 operator/(float n) const {
        return Vec3(x / n, y / n, z / n);
    }

    WLINLINE Vec3 &operator/=(float n) {
        x /= n;
        y /= n;
        z /= n;
        return *this;
    }

    WLINLINE Vec3 operator+(const Vec3 &e) const {
        return Vec3(x + e.x, y + e.y, z + e.z);
    }

    WLINLINE Vec3 &operator+=(const Vec3 &e) {
        x += e.x;
        y += e.y;
        z += e.z;
        return *this;
    }

    WLINLINE Vec3 operator-(const Vec3 &e) const {
        return Vec3(x - e.x, y - e.y, z - e.z);
    }

    WLINLINE Vec3 &operator-=(const Vec3 &e) {
        x -= e.x;
        y -= e.y;
        z -= e.z;
        return *this;
    }

    WLINLINE void set_zero() {
        x = y = z = 0.f;
    }

    WLINLINE Vec3 vec_rem(int index) const {
        Vec3 k = *this;
        k[index] = 0;
        return k;
    }

    WLINLINE Vec3 vec_x() const {
        return Vec3(x, 0, 0);
    }

    WLINLINE Vec3 vec_y() const {
        return Vec3(0, y, 0);
    }

    WLINLINE Vec3 vec_z() const {
        return Vec3(0, 0, z);
    }

    WLINLINE Vec3 vec_xy() const {
        return Vec3(x, y, 0);
    }

    WLINLINE Vec3 vec_xz() const {
        return Vec3(x, 0, z);
    }

    WLINLINE Vec3 vec_yz() const {
        return Vec3(0, y, z);
    }

    WLINLINE float length() const {
        return sqrt(x*x + y*y + z*z);
    }

    WLINLINE float length_squared() const {
        return x * x + y * y + z * z;
    }

    WLINLINE Vec3 normal() const {
        const float l = length();
        return Vec3(x / l, y / l, z / l);
    }

    WLINLINE void normalize() {
        float l = length();
        x /= l;
        y /= l;
        z /= l;
    }

    WLINLINE Vec3 cross(const Vec3 &ot) const {
        return Vec3(y * ot.z - z * ot.y,
                    z * ot.x - x * ot.z,
                    x * ot.y - y * ot.x);
    }

};

struct Vec4 {
    float x, y, z, w;

    Vec4() {
    }

    Vec4(float _x, float _y, float _z, float _w) : x(_x), y(_y), z(_z), w(_w) {
    }

    Vec4(const Vec4 &e) : x(e.x), y(e.y), z(e.z), w(e.w) {
    }

    WLINLINE Vec4 &operator=(const Vec4 &e) {
        x = e.x, y = e.y, z = e.z; w = e.w;
        return *this;
    }

    WLINLINE float operator[](int n) const {
        return (&x)[n];
    }

    WLINLINE float& operator[](int n) {
        return (&x)[n];
    }

    WLINLINE Vec4 operator*(float n) const {
        return Vec4(x * n, y * n, z * n, w * n);
    }

    WLINLINE Vec4 &operator*=(float n) {
        x *= n;
        y *= n;
        z *= n;
        w *= n;
        return *this;
    }

    WLINLINE Vec4 operator/(float n) const {
        return Vec4(x / n, y / n, z / n, w / n);
    }

    WLINLINE Vec4 &operator/=(float n) {
        x /= n;
        y /= n;
        z /= n;
        w /= n;
        return *this;
    }

    WLINLINE Vec4 operator+(const Vec4 &e) const {
        return Vec4(x + e.x, y + e.y, z + e.z, w + e.w);
    }

    WLINLINE Vec4 &operator+=(const Vec4 &e) {
        x += e.x;
        y += e.y;
        z += e.z;
        w += e.w;
        return *this;
    }

    WLINLINE Vec4 operator-(const Vec4 &e) const {
        return Vec4(x - e.x, y - e.y, z - e.z, w - e.w);
    }

    WLINLINE Vec4 &operator-=(const Vec4 &e) {
        x -= e.x;
        y -= e.y;
        z -= e.z;
        w -= e.w;
        return *this;
    }

    WLINLINE void set_zero() {
        x = y = z = w = 0.f;
    }

    WLINLINE float length() const {
        return sqrt(x*x + y*y + z*z + w*w);
    }

    WLINLINE float length_squared() const {
        return x * x + y * y + z * z + w * w;
    }

    WLINLINE void normalize() {
        float l = length();
        x /= l;
        y /= l;
        z /= l;
        w /= l;
    }

};


struct Mat22
{
    Vec2 u, v;

    Mat22() {
    }

    Mat22(const Vec2 &uu, const Vec2 &vv) :
        u(uu), v(vv) {
    }

    WLINLINE void swap_cols(size_t a, size_t b) {
        Vec2 *ta = (Vec2*)this;
        Vec2 t = ta[a];
        t[a] = t[b];
        t[b] = t[a];
    }

    WLINLINE void swap_lines(size_t a, size_t b) {
        const Vec2 t(u[a], v[a]);
        u[a] = u[b];
        v[a] = v[b];
        u[b] = t[0];
        v[b] = t[1];
    }


    WLINLINE Mat22 &operator=(const Mat22 &e) {
        u = e.u;
        v = e.v;
        return *this;
    }

    WLINLINE void set_zero() {
        u.set_zero();
        v.set_zero();
    }

    WLINLINE void set_identity() {
        u = Vec2(1.f, 0.f);
        v = Vec2(0.f, 1.f);
    }

    WLINLINE Mat22 get_transpose() const {
        return Mat22(
            Vec2(u.x, v.x),
            Vec2(u.y, v.y));
    }

    WLINLINE static Mat22 rotation(float angle_rad) {
        return rotation_cs(cos(angle_rad), sin(angle_rad));
    }

    WLINLINE static Mat22 rotation_cs(float _cos, float _sin) {
        return Mat22(
            Vec2(_cos, _sin),
            Vec2(-_sin, _cos));
    }

};

struct Mat33
{
    // u, v, w are COLUMNS
    Vec3 u, v, w;

    Mat33() {
    }

    Mat33(const Vec3 &uu, const Vec3 &vv, const Vec3 &ww) :
        u(uu), v(vv), w(ww) {
    }

    WLINLINE void to_vec16(float n[16]) const {
        n[ 0] = u.x; n[ 1] = u.y; n[ 2] = u.z; n[ 3] = 0.f;
        n[ 4] = v.x; n[ 5] = v.y; n[ 6] = v.z; n[ 7] = 0.f;
        n[ 8] = w.x; n[ 9] = w.y; n[10] = w.z; n[11] = 0.f;
        n[12] = 0.f; n[13] = 0.f; n[14] = 0.f; n[15] = 1.f;
    }

    WLINLINE void swap_cols(size_t a, size_t b) {
        Vec3 *ta = (Vec3*)this;
        Vec3 t = ta[a];
        t[a] = t[b];
        t[b] = t[a];
    }

    WLINLINE void swap_lines(size_t a, size_t b) {
        const Vec3 t(u[a], v[a], w[a]);
        u[a] = u[b];
        v[a] = v[b];
        w[a] = w[b];
        u[b] = t[0];
        v[b] = t[1];
        w[b] = t[2];
    }

    /*
    Mat33(float ux, float uy, float uz, float vx, float vy, float vz,
        float wx, float wy, float wz) : u(ux, uy, uz), v(vx, vy, vz),
        w(wx, wy, wz) {
    }
    */

    Mat33 inverse() const;
    Vec3 solve(const Vec3 &s) const;

    WLINLINE Mat33 &operator=(const Mat33 &e) {
        u = e.u;
        v = e.v;
        w = e.w;
        return *this;
    }

    WLINLINE void set_zero() {
        u.set_zero();
        v.set_zero();
        w.set_zero();
    }

    WLINLINE void set_identity() {
        u = Vec3(1.f, 0.f, 0.f);
        v = Vec3(0.f, 1.f, 0.f);
        w = Vec3(0.f, 0.f, 1.f);
    }

    WLINLINE Mat33 get_transpose() const {
        return Mat33(
            Vec3(u.x, v.x, w.x),
            Vec3(u.y, v.y, w.y),
            Vec3(u.z, v.z, w.z));
    }

    WLINLINE static Mat33 rotation(float angle_rad) {
        return rotation_cs(cos(angle_rad), sin(angle_rad));
    }

    WLINLINE static Mat33 rotation_cs(float _cos, float _sin) {
        return Mat33(
            Vec3(_cos, _sin, 0.f),
            Vec3(-_sin, _cos, 0.f),
            Vec3(0.f, 0.f, 1.f));
    }

    WLINLINE static Mat33 translation(const Vec2 &v) {
        return Mat33(
            Vec3(1.f, 0.f, 0.f),
            Vec3(0.f, 1.f, 0.f),
            Vec3(v.x, v.y, 1.f));
    }

    WLINLINE static Mat33 scale(const Vec2 &v) {
        return Mat33(
            Vec3(v.x, 0.f, 0.f),
            Vec3(0.f, v.y, 0.f),
            Vec3(0.f, 0.f, 1.f));
    }

    WLINLINE static Mat33 scale(float n) {
        return scale(Vec2(n, n));
    }

};

struct Mat44
{
    Vec4 u, v, w, t;

    Mat44() {
    }

    Mat44(const Vec4 &uu, const Vec4 &vv, const Vec4 &ww, const Vec4 &tt) :
        u(uu), v(vv), w(ww), t(tt) {
    }

    Mat44(const float k[16]) :
        u(k[ 0], k[ 1], k[ 2], k[ 3]),
        v(k[ 4], k[ 5], k[ 6], k[ 7]),
        w(k[ 8], k[ 9], k[10], k[11]),
        t(k[12], k[13], k[14], k[15])
    {
    }

    WLINLINE void swap_cols(size_t a, size_t b) {
        Vec4 *ta = (Vec4*)this;
        Vec4 k = ta[a];
        t[a] = k[b];
        t[b] = k[a];
    }

    WLINLINE void swap_lines(size_t a, size_t b) {
        const Vec4 k(u[a], v[a], w[a], t[a]);
        u[a] = u[b];
        v[a] = v[b];
        w[a] = w[b];
        t[a] = t[b];
        u[b] = k[0];
        v[b] = k[1];
        w[b] = k[2];
        t[b] = k[3];
    }


    static Mat44 from_33(const Mat33 &m) {
        return Mat44(
            Vec4(m.u.x, m.u.y, m.u.z, 0.f),
            Vec4(m.v.x, m.v.y, m.v.z, 0.f),
            Vec4(m.w.x, m.w.y, m.w.z, 0.f),
            Vec4(0.f, 0.f, 0.f, 1.f));
    }

    WLINLINE void to_vec16(float n[16]) const {
    /*
        n[ 0] = u.x; n[ 4] = u.y; n[ 8] = u.z; n[12] = u.w;
        n[ 1] = v.x; n[ 5] = v.y; n[ 9] = v.z; n[13] = v.w;
        n[ 2] = w.x; n[ 6] = w.y; n[10] = w.z; n[14] = w.w;
        n[ 3] = t.x; n[ 7] = t.y; n[11] = t.z; n[15] = t.w;
    */
        n[ 0] = u.x; n[ 1] = u.y; n[ 2] = u.z; n[ 3] = u.w;
        n[ 4] = v.x; n[ 5] = v.y; n[ 6] = v.z; n[ 7] = v.w;
        n[ 8] = w.x; n[ 9] = w.y; n[10] = w.z; n[11] = w.w;
        n[12] = t.x; n[13] = t.y; n[14] = t.z; n[15] = t.w;
    }

    /*
    Mat33(float ux, float uy, float uz, float vx, float vy, float vz,
        float wx, float wy, float wz) : u(ux, uy, uz), v(vx, vy, vz),
        w(wx, wy, wz) {
    }
    */

    WLINLINE Mat44 &operator=(const Mat44 &e) {
        u = e.u;
        v = e.v;
        w = e.w;
        t = e.t;
        return *this;
    }

    WLINLINE void set_zero() {
        u.set_zero();
        v.set_zero();
        w.set_zero();
        t.set_zero();
    }

    WLINLINE void set_identity() {
        u = Vec4(1.f, 0.f, 0.f, 0.f);
        v = Vec4(0.f, 1.f, 0.f, 0.f);
        w = Vec4(0.f, 0.f, 1.f, 0.f);
        t = Vec4(0.f, 0.f, 0.f, 1.f);
    }

    WLINLINE Mat44 get_transpose() const {
        return Mat44(
            Vec4(u.x, v.x, w.x, t.x),
            Vec4(u.y, v.y, w.y, t.y),
            Vec4(u.z, v.z, w.z, t.z),
            Vec4(u.w, v.w, w.w, t.w));
    }

    /*
    WLINLINE static Mat33 rotation(float angle_rad) {
        return rotation_cs(cos(angle_rad), sin(angle_rad));
    }

    WLINLINE static Mat33 rotation_cs(float _cos, float _sin) {
        return Mat33(
            Vec3(_cos, _sin, 0.f),
            Vec3(-_sin, _cos, 0.f),
            Vec3(0.f, 0.f, 1.f));
    }

    */
    WLINLINE static Mat44 rotation_y_cs(float s, float c) {
        return Mat44(
                Vec4(c, 0.f, s, 0.f),
                Vec4(0.f, 1.f, 0.f, 0.f),
                Vec4(-s, 0.f, c, 0.f),
                Vec4(0.f, 0.f, 0.f, 1.f));
    }

    WLINLINE static Mat44 rotation_y(float angle) {
        return rotation_y_cs(sin(angle), cos(angle));
    }

    WLINLINE static Mat44 rotation_x_cs(float s, float c) {
        return Mat44(
                Vec4(1.f, 0.f, 0.f, 0.f),
                Vec4(0.f, c, -s, 0.f),
                Vec4(0.f, s, c, 0.f),
                Vec4(0.f, 0.f, 0.f, 1.f));
    }

    WLINLINE static Mat44 rotation_x(float angle) {
        return rotation_x_cs(sin(angle), cos(angle));
    }

    WLINLINE static Mat44 rotation_z_cs(float s, float c) {
        return Mat44(
                Vec4(c, -s, 0.f, 0.f),
                Vec4(s, c, 0.f, 0.f),
                Vec4(0.f, 0.f, 1.f, 0.f),
                Vec4(0.f, 0.f, 0.f, 1.f));
    }

    WLINLINE static Mat44 rotation_z(float angle) {
        return rotation_z_cs(sin(angle), cos(angle));
    }
    
    WLINLINE static Mat44 translation(const Vec3 &v) {
        return Mat44(
            Vec4(1.f, 0.f, 0.f, 0.f),
            Vec4(0.f, 1.f, 0.f, 0.f),
            Vec4(0.f, 0.f, 1.f, 0.f),
            Vec4(v.x, v.y, v.z, 1.f));
    }

    WLINLINE static Mat44 scale(const Vec3 &v) {
        return Mat44(
            Vec4(v.x, 0.f, 0.f, 0.f),
            Vec4(0.f, v.y, 0.f, 0.f),
            Vec4(0.f, 0.f, v.z, 0.f),
            Vec4(0.f, 0.f, 0.f, 1.f));
    }

    WLINLINE static Mat44 scale(float n) {
        return scale(Vec3(n, n, n));
    }

};


/**
 * A discrete (int type) 2D point.
 */
struct Point2D {
    s32 x, y;

    Point2D() {
    }

    Point2D(int _x, int _y) :
        x(_x), y(_y) {
    }

    Point2D(const Point2D &c) :
        x(c.x), y(c.y) {
    }

    Point2D &operator=(const Point2D &c) {
        x = c.x, y = c.y;
        return *this;
    }

    bool operator==(const Point2D &b) const {
        return x == b.x && y == b.y;
    }

    bool operator!=(const Point2D &b) const {
        return x != b.x || y != b.y;
    }

};

struct BBox3 {
    BBox3() {
    }

    BBox3(const Vec3 &lo, const Vec3 &hi) :
        low(lo), high(hi) {
    }

    BBox3(const BBox3 &b) :
        low(b.low), high(b.high) {
    }

    BBox3 &operator=(const BBox3 &c) {
        low = c.low;
        high = c.high;
        return *this;
    }

    void expand_to_fill(const BBox3 &bb) {
        if (bb.low.z < low.z) low.z = bb.low.z;
        if (bb.low.y < low.y) low.y = bb.low.y;
        if (bb.low.x < low.x) low.x = bb.low.x;
        if (bb.high.z > high.z) high.z = bb.high.z;
        if (bb.high.y > high.y) high.y = bb.high.y;
        if (bb.high.x > high.x) high.x = bb.high.x;
    }

    wlBool contains(const Vec3 &f) const {
        return (f.x >= low.x && f.y >= low.y && f.z >= low.z
                && f.x <= high.x && f.y <= high.y && f.z <= high.z);
    }

    /*
    wlBool has_intersection(const Vec3 &f) const {
        return (f.low.x
    }
    */

    /*
    wlBool contains(const Vec2 &f) const {
        const Vec2 s = pos + size;
        return (f.x >= pos.x && f.y >= pos.y && f.x <= s.x && f.y <= s.y);
    }
    */

    Vec3 low, high;
};

struct BBox2 {
    BBox2() {
    }

    BBox2(const Vec2 &lo, const Vec2 &hi) :
        low(lo), high(hi) {
    }

    BBox2(const BBox2 &b) :
        low(b.low), high(b.high) {
    }

    BBox2 &operator=(const BBox2 &c) {
        low = c.low;
        high = c.high;
        return *this;
    }

    void expand_to_fill(const BBox2 &bb) {
        if (bb.low.x < low.x) low.x = bb.low.x;
        if (bb.low.y < low.y) low.y = bb.low.y;
        if (bb.high.x > high.x) high.x = bb.high.x;
        if (bb.high.y > high.y) high.y = bb.high.y;
    }

    wlBool contains(const Vec2 &f) const {
        return (f.x >= low.x && f.y >= low.y
                && f.x <= high.x && f.y <= high.y);
    }

    Vec2 low, high;
};

WLINLINE Vec2 mul22(const Mat22 &m, const Vec2 &v) {
    return Vec2(
        v.x*m.u.x + v.y*m.v.x,
        v.x*m.u.y + v.y*m.v.y);
}

WLINLINE Vec3 mul33(const Mat33 &m, const Vec3 &v) {
    return WL::Vec3(
        v.x*m.u.x + v.y*m.v.x + v.z*m.w.x,
        v.x*m.u.y + v.y*m.v.y + v.z*m.w.y,
        v.x*m.u.z + v.y*m.v.z + v.z*m.w.z);
}

WLINLINE Vec2 mul33_1(const Mat33 &m, const Vec2 &v) {
    return Vec2(
            m.u.x*v.x + m.v.x*v.y + m.w.x,
            m.u.y*v.x + m.v.y*v.y + m.w.y);
}

WLINLINE Vec3 mul44_1(const Mat44 &m, const Vec3 &v) {
    return Vec3(
            m.u.x*v.x + m.v.x*v.y + m.w.x*v.z + m.t.x,
            m.u.y*v.x + m.v.y*v.y + m.w.y*v.z + m.t.y,
            m.u.z*v.x + m.v.z*v.y + m.w.z*v.z + m.t.z);
}

WLINLINE Vec2 mul44_1(const Mat44 &m, const Vec2 &v) {
    Vec3 k = mul44_1(m, Vec3(v.x, v.y, 0.f));
    return Vec2(k.x, k.y);
}

WLINLINE Mat33 mulmat(const Mat33 &a, const Mat33 &b) {
    return Mat33(
        Vec3(
            a.u.x*b.u.x + a.v.x*b.u.y + a.w.x*b.u.z,
            a.u.y*b.u.x + a.v.y*b.u.y + a.w.y*b.u.z,
            a.u.z*b.u.x + a.v.z*b.u.y + a.w.z*b.u.z),
        Vec3(
            a.u.x*b.v.x + a.v.x*b.v.y + a.w.x*b.v.z,
            a.u.y*b.v.x + a.v.y*b.v.y + a.w.y*b.v.z,
            a.u.z*b.v.x + a.v.z*b.v.y + a.w.z*b.v.z),
        Vec3(
            a.u.x*b.w.x + a.v.x*b.w.y + a.w.x*b.w.z,
            a.u.y*b.w.x + a.v.y*b.w.y + a.w.y*b.w.z,
            a.u.z*b.w.x + a.v.z*b.w.y + a.w.z*b.w.z));
}

WLINLINE Mat44 mulmat(const Mat44 &a, const Mat44 &b) {
    return Mat44(
        Vec4(
            a.u.x*b.u.x + a.v.x*b.u.y + a.w.x*b.u.z + a.t.x*b.u.w,
            a.u.y*b.u.x + a.v.y*b.u.y + a.w.y*b.u.z + a.t.y*b.u.w,
            a.u.z*b.u.x + a.v.z*b.u.y + a.w.z*b.u.z + a.t.z*b.u.w,
            a.u.w*b.u.x + a.v.w*b.u.y + a.w.w*b.u.z + a.t.w*b.u.w),
        Vec4(
            a.u.x*b.v.x + a.v.x*b.v.y + a.w.x*b.v.z + a.t.x*b.v.w,
            a.u.y*b.v.x + a.v.y*b.v.y + a.w.y*b.v.z + a.t.y*b.v.w,
            a.u.z*b.v.x + a.v.z*b.v.y + a.w.z*b.v.z + a.t.z*b.v.w,
            a.u.w*b.v.x + a.v.w*b.v.y + a.w.w*b.v.z + a.t.w*b.v.w),
        Vec4(
            a.u.x*b.w.x + a.v.x*b.w.y + a.w.x*b.w.z + a.t.x*b.w.w,
            a.u.y*b.w.x + a.v.y*b.w.y + a.w.y*b.w.z + a.t.y*b.w.w,
            a.u.z*b.w.x + a.v.z*b.w.y + a.w.z*b.w.z + a.t.z*b.w.w,
            a.u.w*b.w.x + a.v.w*b.w.y + a.w.w*b.w.z + a.t.w*b.w.w),
        Vec4(
            a.u.x*b.t.x + a.v.x*b.t.y + a.w.x*b.t.z + a.t.x*b.t.w,
            a.u.y*b.t.x + a.v.y*b.t.y + a.w.y*b.t.z + a.t.y*b.t.w,
            a.u.z*b.t.x + a.v.z*b.t.y + a.w.z*b.t.z + a.t.z*b.t.w,
            a.u.w*b.t.x + a.v.w*b.t.y + a.w.w*b.t.z + a.t.w*b.t.w));
}

std::string mat_to_string(const Mat33 &mat);
std::string mat_to_string(const Mat44 &mat);

Mat33 calc_basechanger(const Vec3 &, const Vec3 &, const Vec3 &,
        const Vec3 &, const Vec3 &, const Vec3 &);


wlEndNS

#endif


