#ifndef _G3_Mesh_H_
#define _G3_Mesh_H_

#include <wl/scenegraph.h>

namespace G3 {

class IMesh : public WL::sg::Node
{
public:

    virtual int num_vertices() = 0;
    virtual int num_faces() = 0;

    virtual void set_num_vertices(int num) = 0;
    virtual void set_num_faces(int num) = 0;

    virtual void set_vertex(int index, double x, double y, double z) = 0;
    virtual void get_vertex(int index, double *x, double *y, double *z) = 0;

    virtual void set_face(int index, int a, int b, int c) = 0;
    virtual void get_face(int index, int *a, int *b, int *c) = 0;
    virtual void autoset_face_normal(int index) = 0;
    virtual void set_face_normal(int index, double x, double y, double z) = 0;

    virtual void get_vertex_color(int index, int *r, int *g, int *b, int *a) = 0;
    virtual void set_vertex_color(int index, int r, int g, int b, int a) = 0;

    static IMesh *create();
};

} // NS G3

#endif


