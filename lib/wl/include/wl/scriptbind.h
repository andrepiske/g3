#ifndef _WL_ScriptBind_H_
#define _WL_ScriptBind_H_

#include <wl/config.h>
#include <wl/script.h>
#include <wl/variant.h>

wlStartNS

class IScriptBind
{
public:
    virtual ~IScriptBind() {}

    static IScriptBind *create_bind();

    virtual void set_field(const std::string &name, Variant *value) = 0;

    virtual void bind(IScript *script, void *object) = 0;
    virtual void bindrefcounted(IScript *s, RefCounted *obj) = 0;

    template<class T>
    static wlBool register_class(IScript *s);

};

wlEndNS

#endif

