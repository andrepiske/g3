
function main(gd)
	local m = gd:add_mesh()
	m:set_num_vertices(3)
	m:set_num_faces(1)
	m:set_vertex(0, 3, 3, -1)
	m:set_vertex(1, -3, 3, -1)
	m:set_vertex(2, 3, -3, -1)
	m:set_face(0, 0, 1, 2)
end

