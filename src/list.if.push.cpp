/* This is a generated file, do not edit it */
#include <boost/unordered_map.hpp>
#include <lua.hpp>
#include <wl/scriptable.h>
#include "list.if.h"


static
int _WL_IList__closure(lua_State *L) {
    const int proc_id = lua_tonumber(L, lua_upvalueindex(1));
    WL::IList *obj = *(WL::IList**)luaL_checkudata(L, 1, "WL-WL::IList");
    switch (proc_id) {
        case 1: { // get
            int _valid[] = {0,0};
            _valid[0] = lua_isnumber(L, 2);
            ::WL::RefCounted* _v_ret;
            int v_index;
            v_index = lua_tonumber(L, 2);
            _v_ret = obj->get(v_index);            
            ::WL::scriptable::push(L, (::WL::RefCounted*)_v_ret);
            return 1;
        }
        case 2: { // size
            int _valid[] = {0};
            int _v_ret;
            _v_ret = obj->size();            
            lua_pushnumber(L, _v_ret);
            return 1;
        }
        case 3: { // insert
            int _valid[] = {0,0,0};
            _valid[0] = lua_isnumber(L, 2);
            _valid[1] = lua_istable(L, 3);
            int v_position;
            ::WL::RefCounted* v_el;
            v_position = lua_tonumber(L, 2);
            v_el = (::WL::RefCounted*)lua_touserdata(L, 3);
            obj->insert(v_position, (any*)v_el);            
            return 0;
        }
    }
    return 0;
}

static
int _WL_IList__gc(lua_State *L) {
    WL::IList *ptr = *(WL::IList**)lua_touserdata(L, 1);
    ptr->rc_pop();
    return 0;
}

void _WL_IList__regMT(lua_State *L) {
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, _WL_IList__gc);
    lua_setfield(L, -2, "__gc");
    lua_pushnumber(L, 1);
    lua_pushcclosure(L, _WL_IList__closure, 1);
    lua_setfield(L, -2, "get");
    lua_pushnumber(L, 2);
    lua_pushcclosure(L, _WL_IList__closure, 1);
    lua_setfield(L, -2, "size");
    lua_pushnumber(L, 3);
    lua_pushcclosure(L, _WL_IList__closure, 1);
    lua_setfield(L, -2, "insert");
}

void _WL_IList__luapush(lua_State *L, WL::IList *obj) {
    *(void**)lua_newuserdata(L, sizeof(void*)) = (void*)obj;
    if (luaL_newmetatable(L, "WL-WL::IList"))
        _WL_IList__regMT(L);
    lua_setmetatable(L, -2);
    obj->rc_push();
}


