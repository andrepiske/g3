#ifndef __G3_TOOL_H__
#define __G3_TOOL_H__

#include <wl/config.h>
#include <wl/refcounted.h>

namespace G3 {

class ITool : public WL::RefCounted
{
public:
    enum Tip {
        TIP_CONICAL = 1,
        TIP_SPHERICAL,
        TIP_FLAT,
        TIP_FLAT_WITH_RADIUS,

        TIP_END_TIPS
    };

public:
    virtual double get_tip_radius() = 0;
    virtual double get_tip_angle() = 0;
    virtual double get_tool_radius() = 0;
    virtual int get_tip() = 0;
    virtual double get_height_limit() = 0;

    virtual void set_tip(int tip) = 0;
    virtual void set_tip_radius(double value) = 0;
    virtual void set_tip_angle(double value) = 0;
    virtual void set_tool_radius(double value) = 0;
    virtual void set_height_limit(double value) = 0;

    static ITool *create();
};



} // NS G3

#endif

