require('class')

local HASPROPERTIES_ID = { }

HasProperties = Class(function(unused, S)

    local mt = getmetatable(S)
    if rawget(mt, HASPROPERTIES_ID) == nil then
        local orig_index = mt.__index
        local orig_newindex = mt.__newindex

        local funcget = function(obj, name)
            local values = rawget(obj, '##values')
            local v = values['name']
            if type(v) == 'function' then
                return v
            end
            return orig_index(obj, name)
        end

        local this_index = function(obj, idx)
            local getter = type(idx) == 'string' and funcget(obj, '_pget_' .. idx) or nil
            if getter == nil then
                return orig_index(obj, idx)
            end
            return getter(obj)
        end

        local this_newindex = function(obj, idx, newvalue)
            local setter = type(idx) == 'string' and funcget(obj, '_pset_' .. idx) or nil
            if setter == nil then
                orig_newindex(obj, idx, newvalue)
                return
            end
            setter(obj, newvalue)
        end

        rawset(mt, '__index', this_index)
        rawset(mt, '__newindex', this_newindex)

        rawset(mt, HASPROPERTIES_ID, true)
    end

end)


