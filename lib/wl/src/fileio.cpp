#include "precompiled.h"
#include <wl/fileio.h>
#include <stdio.h>
#include <boost/shared_ptr.hpp>
#include <wl/log.h>
#include <memory.h>

// TODO: use size_t for everything here

wlStartNS

class CFileIO : public IFileIO
{
public:
    FILE *m_stream;
    wlBool m_closed;

public:
    CFileIO() {
        m_closed = wlFalse;
        m_stream = 0;
    }

    virtual ~CFileIO() {
        if (!m_closed)
            fclose(m_stream);
    }

    virtual long tell() {
        if (m_closed)
            return -1;
        return (long)ftell(m_stream);
    }

    virtual wlBool seek(long bytes, int from) {
        if (m_closed)
            return wlFalse;
        long int origin;
        switch (from) {
            case SK_CUR:
            case SK_START:
                origin = SEEK_SET;
                break;
            case SK_END:
                origin = SEEK_END;
                break;
            default:
                return wlFalse;
        }
        if (from == SK_CUR) {
            bytes = tell() + bytes;
        }
        if (fseek(m_stream, bytes, origin))
            return wlFalse;
        return wlTrue;
    }

    virtual long read(void *data, long length) {
        if (m_closed)
            return -1;
        long r = (long)fread(data, 1, (long)length, m_stream);
        return r;
    }

    virtual wlBool eof() {
        if (m_closed)   
            return wlTrue;
        return feof(m_stream) ? wlTrue : wlFalse;
    }

    virtual long write(const void *data, long length) {
        if (m_closed)
            return -1;
        return (long)fwrite(data, 1, (long)length, m_stream);
    }

    virtual void force_close() {
        if (!m_closed)
            fclose(m_stream);
        m_closed = wlTrue;
    }

    virtual long size() {
        long t = tell();
        seek(0, SK_END);
        long sz = tell();
        seek(t, SK_START);
        return sz;
    }

};

IFileIO *IFileIO::open_file(const std::string &path, int mode) {
    char _mode[4];
    char *p = _mode;
    for (unsigned int i=0; i < (unsigned int)sizeof(_mode); ++i)
        _mode[i] = 0;

    if ((mode & M_READ) == M_READ)
        *p++ = 'r';
    if ((mode & M_WRITE) == M_WRITE)
        *p++ = 'w';
    if ((mode & M_TEXT) == M_TEXT)
        *p++ = 't';
    else
        *p++ = 'b';

    FILE *fs = fopen(path.c_str(), _mode);
    if (!fs)
        return wlNull;

    CFileIO *file = new CFileIO();
    file->m_stream = fs;
    return file;
}

///////////////////////////////////////////////////////////////////////////////
// IRangedFileIO
///////////////////////////////////////////////////////////////////////////////

class CRangedFileIO : public IRangedFileIO
{
public:
    long m_init;
    long m_length;
    wlBool m_closed;
    IFileIO *m_file;
    wlBool m_ownsfile;

    virtual ~CRangedFileIO() {
        if (m_file) {
            m_file->rc_pop();
        }
    }

    virtual long tell() {
        if (m_closed)
            return -1;
        return m_file->tell() - m_init;
    }

    virtual long size() {
        return m_length; 
    }

    virtual wlBool eof() {
        if (m_closed)
            return wlTrue;
        return m_file->tell() >= (m_init + m_length);
    }

    virtual wlBool seek(long bytes, int from) {
        if (m_closed)
            return wlFalse;
        long new_seek;
        switch (from) {
            case SK_START:
                new_seek = bytes + m_init;
                break;
            case SK_CUR:
                new_seek = tell() + bytes;
                break;
            case SK_END:
                new_seek = m_init + m_length - bytes;
                break;
            default:
                return wlFalse;
        }
        return m_file->seek(new_seek, SK_START);
    }

    virtual long read(void *data, long length) {
        if (m_closed)
            return -1;
        return m_file->read(data, length);
    }
    virtual long write(const void *data, long length) {
        if (m_closed)
            return -1;
        return m_file->write(data, length);
    }

    virtual void force_close() {
        m_closed = wlTrue;
    }

};

IRangedFileIO *IRangedFileIO::create(IFileIO *fileio, long init, long length) {
    CRangedFileIO *c = new CRangedFileIO();
    fileio->rc_push();
    c->m_closed = wlFalse;
    c->m_file = fileio;
    c->m_init = init;
    c->m_length = length;
    return c;
}

///////////////////////////////////////////////////////////////////////////////
// BufferedFileIO
///////////////////////////////////////////////////////////////////////////////

// TODO: implement the force closed
class CBufferedFileIO : public IBufferedFileIO
{
public:
    void *m_buffer;
    int m_size;
    wlBool m_free_on_delete;
    wlBool m_readonly;
    int m_cursor;
    
public:
    CBufferedFileIO() {
    }

    virtual ~CBufferedFileIO() {
        if (m_free_on_delete) {
            char *b = (char*)m_buffer;
            delete [] b;
            m_buffer = wlNull;
        }
    }

    virtual long tell() {
        return (long) m_cursor;
    }

    virtual long size() {
        return (long) m_size;
    }

    virtual wlBool eof() {
        return m_cursor == m_size ? wlTrue : wlFalse;
    }

    virtual wlBool seek(long bytes, int from) {
        int new_pos = m_cursor;
        switch (from) {
            case SK_START:
                new_pos = (int) bytes;
                break;
            case SK_END:
                new_pos = m_size + (int) bytes;
                break;
            case SK_CUR:
                new_pos = m_cursor + (int) bytes;
                break;
            default:
                WL_LOG_ERROR("Invalid seek from: %d", from);
        }
        if (new_pos > m_size)
            new_pos = m_size;
        if (new_pos <= 0)
            new_pos = 0;

        m_cursor = new_pos;
        return wlTrue;
    }

    virtual long read(void *data, long length) {
        int bytes = (int) length;
        int available = m_size - m_cursor;
        if (bytes < 0)
            bytes = 0;
        if (bytes > available)
            bytes = available;
        memcpy(data, m_buffer, bytes);
        m_cursor += bytes;
        return bytes;
    }

    virtual long write(const void *data, long length) {
        if (m_readonly)
            return 0;

        int bytes = (int) length;
        int available = m_size - m_cursor;
        if (bytes < 0)
            bytes = 0;
        if (bytes > available)
            bytes = available;
        memcpy(m_buffer, data, bytes);
        m_cursor += bytes;
        return bytes;
    }

    virtual void force_close() {
    }

};

IBufferedFileIO *IBufferedFileIO::create_from_string(const std::string &text) {
    int len = (int) text.size();
    char *buffer = new char[len];
    memcpy(buffer, text.c_str(), len);
    return IBufferedFileIO::create((void*)buffer, len, wlTrue, wlTrue);
}

IBufferedFileIO *IBufferedFileIO::create(void *buffer, int size,
        wlBool readonly, wlBool free_on_delete) {
    CBufferedFileIO *f = new CBufferedFileIO();
    f->m_readonly = readonly;
    f->m_buffer = buffer;
    f->m_size = size;
    f->m_free_on_delete = free_on_delete;
    f->m_cursor = 0;
    return f;
}


wlEndNS
