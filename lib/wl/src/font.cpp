#include "precompiled.h"
#include <ft2build.h>
#include FT_FREETYPE_H
#include <assert.h>
#include <sstream>
#include <iostream>
#include <GL/gl.h>
#include <wl/font.h>
#include <wl/image.h>
#include <wl/glstate.h>
#include <wl/fileio.h>

wlStartNS

/*
TODO: fonts are being loaded into memory and unloaded
only when font won't be used anymore. I think it's optimal
to load full font file onto memory instead of keeping a file handle
opened and making frequent file I/O's. But this should be an option
because system memory can be limited; and fonts may be cached anyways.
*/
class CFont : public IFont
{
public:
    static FT_Library s_library;

    FT_Face m_face;

    // this is the whole file loaded onto memory
    wlByte *m_fileptr;
    IImage *m_glyphs[256];
    float m_face_size;

    CFont(float size) {
        if (!s_library) {
            FT_Init_FreeType(&s_library);
        }

        for (int i=0; i < 256; ++i)
            m_glyphs[i] = wlNull;

        m_fileptr = wlNull;
        m_face_size = size;
    }

    virtual ~CFont() {
        if (m_fileptr)
            delete [] m_fileptr;
    }

    wlBool load_from_resource(ResourceIOBlock blk) {
        IFileIO *file = blk.file();
        if (!file)
            return wlFalse;

        file->seek(0, IFileIO::SK_START);
        long file_size = file->size();
        assert(file_size > 0);
        m_fileptr = new wlByte[file_size];

        if (file_size != file->read(m_fileptr, file_size)) {
            delete [] m_fileptr;
            return wlFalse;
        }

        int err;
        err = FT_New_Memory_Face(s_library, (const FT_Byte*)m_fileptr,
            (FT_Long)file_size, 0, &m_face);
        if (err) {
            delete [] m_fileptr;
            return wlFalse;
        }

        if (FT_Set_Char_Size(m_face, 0, (int)(m_face_size * 64.f), 96, 96)) {
            delete [] m_fileptr;
            return wlFalse;
        }

        return wlTrue;
    }

    virtual void get_text_info(const std::string &text,
        TextInfo &tinfo) {
    
        Vec2 origin = Vec2(0.f, 0.f);
        _text_raster(text, tinfo, wlFalse, origin);

    }

    virtual void draw_text(const std::string &text,
        const Vec2 &origin) {

        TextInfo t;
        _text_raster(text, t, wlTrue, origin); 
    }

    void _text_raster(const std::string &text, TextInfo &tinfo,
        wlBool render, const Vec2 &origin) {

        const char *ptr = text.c_str();
        int err;

        FT_GlyphSlot slot = m_face->glyph;

        tinfo.width = 0.f;
        tinfo.height = 0.f;
        tinfo.bearY = 0.f;
        tinfo.bb_width = tinfo.bb_height = 0;

        float adv = 0.f;
        for (; *ptr; ptr++) {
            err = FT_Load_Char(m_face, *ptr, FT_LOAD_RENDER);
            if (err)
                continue;

            FT_Bitmap *fbmp = &slot->bitmap;
            IImage *img = m_glyphs[(int)(unsigned char)*ptr];
            if (!img) {
                /* WL_LOG_INFO("Create glyph %c (%dx%d)", (char)*ptr,
                    fbmp->width, fbmp->rows); */
                img = m_glyphs[(int)(unsigned char)*ptr] = IImage::create_from_buffer(
                    (const void*)fbmp->buffer, fbmp->width, fbmp->rows,
                    IImage::PF_GRAY, fbmp->pitch);
            }

            {
                float bearY = (float)(slot->metrics.horiBearingY >> 6);
                float height = (float)(slot->metrics.height >> 6);
                if (bearY > tinfo.bearY)
                    tinfo.bearY = bearY;
                if (height > tinfo.height)
                    tinfo.height = height;
                if (fbmp->rows > tinfo.bb_height)
                    tinfo.bb_height = fbmp->rows;
            }

            float adv_y = (float)(-slot->bitmap_top);
            if (render) {
                img->draw_quad(Vec2(adv, adv_y)+origin,
                        Vec2((float)fbmp->width, (float)fbmp->rows));
            }
            adv += (float)(slot->advance.x >> 6);
        }

        tinfo.width = adv;
    }

    // TODO: implement these!
    virtual void load_data() {}
    virtual void unload_data() {}
    
};

FT_Library CFont::s_library = wlNull;

IFont *IFont::create_resource(ResourceIOBlock blk, float size) {
    CFont *f = new CFont(size);
    if (!f->load_from_resource(blk))
        return wlNull;
    return f;
}

wlEndNS

